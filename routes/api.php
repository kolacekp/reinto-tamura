<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('appliParams/getAll',"AppliParamController@getAllAppliParams");

Route::get('admin/getUsers','UserController@getUsers');
Route::get('admin/getRoles','UserController@getRoles');

Route::get('persons/salesManagers', 'PersonController@getSalesManagers');
Route::get('persons/engineers', 'PersonController@getEngineers');
Route::get('persons/databaseUsers', 'PersonController@getDatabaseUsers');
Route::get('persons/getPerson', 'PersonController@getPerson');
Route::get('persons/checkEmailExists', 'PersonController@checkEmailExists');

Route::get('customers/companies', 'CustomerController@getCompanies');
Route::get('customers/purchasers', 'CustomerController@getPurchasers');
Route::get('customers/salesManagers', 'CustomerController@getSalesManagers');
Route::get('customers/engineers', 'CustomerController@getEngineers');
Route::get('customers/purchaseManagers', 'CustomerController@getPurchaseManagers');
Route::get('customers/directory', 'CustomerController@getCustomersDirectory');
Route::get('customers/detail', 'CustomerController@getCustomerDetail');
Route::get('customers/groupCustomers', 'CustomerController@getGroupCustomers');
Route::get('customers/all', 'CustomerController@getAll');

Route::get('tamuraCodifications/parentMarketEntities', 'TamuraCodificationController@getParentMarketEntities');
Route::get('tamuraCodifications/childMarketEntities', 'TamuraCodificationController@getChildMarketEntities');
Route::get('tamuraCodifications/parentProductEntities', 'TamuraCodificationController@getParentProductEntities');
Route::get('tamuraCodifications/childProductEntities', 'TamuraCodificationController@getChildProductEntities');
Route::get('tamuraCodifications/designOffices', 'TamuraCodificationController@getDesignOffices');
Route::post('tamuraCodifications/getMarketParent', 'TamuraCodificationController@getMarketParent');

Route::get('categories/categories', 'CategoryController@getCategories');

Route::get('rfqs/get', 'RfqController@get');
Route::get('rfqs/deadReasons', 'RfqController@getDeadReasons');
Route::get('rfqs/statusLogs', 'RfqController@getStatusLogs');
Route::get('rfqs/specifications', 'RfqController@getSpecifications');
Route::get('rfqs/getRfqForNpi', 'RfqController@getRfqForNpi');
Route::get('rfqs/checkCustomerPn', 'RfqController@checkCustomerPn');
Route::get('rfqs/productions','RfqController@getProductions');
Route::get('rfqs/productionById','RfqController@getProductionById');
Route::get('rfqs/deleteProduction','RfqController@deleteProduction');

Route::get('searchParams/getPersonalizations', 'SearchParamController@getPersonalizations');

Route::get('projects/fetch', 'ProjectController@fetch');
Route::get('projects/count', 'ProjectController@getCounts');
Route::get('projects/get', 'ProjectController@get');
Route::get('projects/getProjects', 'ProjectController@getProjects');
Route::get('projects/checkName', 'ProjectController@checkName');

Route::get('npi/get', 'NpiController@get');

Route::get('incoterms/getAll', 'IncotermController@getAll');

Route::get('countries/all', 'CountryController@all');

Route::get('currencies/all', 'CurrencyController@all');
Route::get('currencies/default', 'CurrencyController@getDefault');

Route::get('stdItems/getStdItems', 'StdItemController@getStdItems');

Route::get('material/getMaterials', 'MaterialController@getMaterials');

Route::get('rfqMaterial/get', 'RfqController@getMaterialsForRfq');
Route::get('rfqMaterial/getTypes', 'RfqController@getMaterialTypes');
Route::get('rfqMaterial/getForType', 'RfqController@getMaterialsForType');
Route::get('rfqMaterial/getForTypeAndPartNo', 'RfqController@getMaterialForTypeAndPartNo');
Route::get('rfqMaterial/getCost','RfqController@getCost');

Route::get('lme/getAll', 'LmeController@getAll');

Route::get('tamuraEntities/all', 'TamuraEntityController@getAll');
Route::get('tamuraEntities/factories', 'TamuraEntityController@getFactories');
Route::get('tamuraEntities/salesOffices', 'TamuraEntityController@getSalesOffices');

Route::get('tamura-directory/fetch', 'TamuraEntityController@fetch');
Route::get('tamura-directory/getEntityById', 'TamuraEntityController@getEntityById');
Route::get('tamura-directory/getFactorySales', 'NpiController@getNpiByTamuraEntity');

Route::get('timeSheets/getByRfq', 'TimeSheetController@getByRfq');

Route::get('time-sheet-invoice/getSisterCompanies', 'TimeSheetController@getSisterCompanies');
Route::get('time-sheet-invoice/getCustomersByEntityId', 'TimeSheetController@getCustomersByEntityId');
Route::get('time-sheet-invoice/getProjectRfqByCustomerId', 'TimeSheetController@getProjectRfqByCustomerId');
Route::get('time-sheet-invoice/geTimeSheetByRfqId', 'TimeSheetController@geTimeSheetByRfqId');

Route::get('sale/getSalesByNPI','SaleController@getSalesBtNPI');
Route::get('sale/get','SaleController@getSaleById');
Route::post('sale/createSale','SaleController@findOrCreateSale');
Route::post('sale/updateSale','SaleController@updateSale');
Route::post('sale/changeSaleOffice','SaleController@changeSaleOffice');

Route::post('projects/create', 'ProjectController@createProject');
Route::post('projects/update', 'ProjectController@updateProject');

Route::post('rfqs/create', 'RfqController@createRfq');
Route::post('rfqs/update', 'RfqController@updateRfq');
Route::post('rfqs/changeRfqProject', 'RfqController@changeRfqProject');
Route::post('rfqs/copy', 'RfqController@copyRfq');
Route::post('rfqs/clone', 'RfqController@cloneRfq');

Route::post('persons/createDatabaseUser', 'PersonController@createDatabaseUser');
Route::post('persons/updateDatabaseUserForm', 'PersonController@updateDatabaseUserForm');
Route::post('persons/deleteDatabaseUser', 'PersonController@deleteDatabaseUser');
Route::post('persons/updateDatabaseUser', 'PersonController@updateDatabaseUser');
Route::post('persons/updateCustomerPerson', 'PersonController@updateCustomerPerson');
Route::post('persons/changeCustomer', 'PersonController@changeCustomer');

Route::post('persons/addPerson', 'PersonController@addPerson');

Route::post('customers/update', 'CustomerController@update');
Route::post('customers/delete', 'CustomerController@deleteCustomer');
Route::post('customers/addCustomerGroup', 'CustomerController@addCustomerGroup');
Route::post('customers/addCustomerCompany', 'CustomerController@addCustomerCompany');

Route::post('material/create', 'MaterialController@create');
Route::post('material/update', 'MaterialController@update');
Route::post('material/delete', 'MaterialController@delete');

Route::post('lme/update', 'LmeController@update');

Route::post('login', 'UserController@login');

Route::post('searchParams/save', 'SearchParamController@saveSearchParams');

Route::post('npiUpdateComment/updateComment', 'NpiController@updateComment');
Route::post('npi/changeSampleTeuPn', 'NpiController@changeSampleTeuPn');
Route::post('rfq/generateNPI','NpiController@generateNPI');

Route::post('currencies/convert', 'CurrencyController@convert');
Route::post('currencies/npiConvert', 'CurrencyController@npiConvert');
Route::post('currencies/newSaleConvert', 'CurrencyController@newSaleConvert');
Route::post('currencies/convertRfqComValues', 'CurrencyController@convertRfqComValues');
Route::post('currencies/saleConvertToPound','CurrencyController@saleConvertToPound');
Route::post('currencies/change', 'CurrencyController@change');
Route::post('currencies/changeDefault', 'CurrencyController@changeDefault');
Route::post('currencies/create', 'CurrencyController@create');
Route::post('currencies/delete', 'CurrencyController@delete');

Route::post('stdItems/create', 'StdItemController@create');
Route::post('stdItems/update', 'StdItemController@update');
Route::post('stdItems/delete', 'StdItemController@delete');

Route::post('timeSheets/create', 'TimeSheetController@create');

Route::post('tamura-directory/create', 'TamuraEntityController@create');
Route::post('tamura-directory/update', 'TamuraEntityController@update');
Route::post('tamura-directory/delete', 'TamuraEntityController@delete');
Route::post('tamura-directory/design/person-update','TamuraEntityController@person_update');
Route::post('tamura-directory/factories/update','TamuraEntityController@factoryEntityUpdate');
Route::post('tamura-directory/sales/update','TamuraEntityController@salesEntityUpdate');

Route::post('appliParam/updateDate', 'AppliParamController@updateDate');

Route::post('mail/sendMailToPerson', 'MailController@sendMailToPerson');

Route::post('users/makeUsersFromPersons', 'UserController@makeUsersFromPersons');
Route::post('users/updateUser','UserController@updateUser');

Route::post('rfqs/updateSpecification', 'RfqController@updateSpecification');
Route::post('rfqs/deleteSpecification', 'RfqController@deleteSpecification');
Route::post('rfqs/addSpecification', 'RfqController@addSpecification');
Route::post('rfqs/addProduction','RfqController@addProduction');
Route::post('rfqs/editProduction','RfqController@editProduction');

Route::post('rfqMaterial/create', 'RfqController@createRfqMaterial');
Route::post('rfqMaterial/delete', 'RfqController@deleteRfqMaterial');
Route::post('rfqMaterial/update', 'RfqController@updateRfqMaterial');

/**
 * Import routes...
 */
Route::post('import/appliParam', 'ImportController@importAppliParam');
Route::post('import/category', 'ImportController@importCategory');
Route::post('import/country', 'ImportController@importCountry');
Route::post('import/currency', 'ImportController@importCurrency');
Route::post('import/customer', 'ImportController@importCustomer');
Route::post('import/excelExtractLog', 'ImportController@importExcelExtractLog');
Route::post('import/incoterm', 'ImportController@importIncoterm');
Route::post('import/lme', 'ImportController@importLme');
Route::post('import/mat', 'ImportController@importMat');
Route::post('import/npi', 'ImportController@importNpi');
Route::post('import/person', 'ImportController@importPerson');
Route::post('import/project', 'ImportController@importProject');
Route::post('import/quotationFormTemplateList', 'ImportController@importQuotationFormTemplateList');
Route::post('import/rfq', 'ImportController@importRfq');
Route::post('import/rfqDeadReason', 'ImportController@importRfqDeadReason');
Route::post('import/rfqEstimatedFactoryCost', 'ImportController@importRfqEstimatedFactoryCost');
Route::post('import/rfqExportError', 'ImportController@importRfqExportError');
Route::post('import/rfqMat', 'ImportController@importRfqMat');
Route::post('import/rfqSpecification', 'ImportController@importRfqSpecification');
Route::post('import/rfqStatusLog', 'ImportController@importRfqStatusLog');
Route::post('import/sale', 'ImportController@importSale');
Route::post('import/searchParam', 'ImportController@importSearchParam');
Route::post('import/standardItem', 'ImportController@importStandardItem');
Route::post('import/tamuraCodification', 'ImportController@importTamuraCodification');
Route::post('import/tamuraEntity', 'ImportController@importTamuraEntity');
Route::post('import/timeSheet', 'ImportController@importTimeSheet');