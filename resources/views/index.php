<!DOCTYPE html>
<html lang="en" ng-app="tamuraApp">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=11" />  
    <title>Tamura</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    


    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.min.css" />
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="libs/font_awesome/css/font-awesome.min.css" />

    <link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600,700,900" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,600,700,900&amp;subset=latin-ext" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900&amp;subset=latin-ext" rel="stylesheet">

    <link type="text/css" rel="stylesheet" href="bower_components/angular-flash-alert/dist/angular-flash.min.css" />
    
    <link rel="stylesheet" href="bower_components/angular-ui-tree/dist/angular-ui-tree.min.css">

    <link rel="styleSheet" href="bower_components/angular-ui-grid/ui-grid.css"/>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body ng-cloak>
    

    
    <div ng-show="isLogged()">
        <div id="top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <img src="img/logo.png" title="Tamura Europe" class="logo-img">
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <h1 class="top-heading">Tamura-Europe RFQ Database</h1>
                    </div>
                </div>
            </div>
        </div>

        <div id="menu" ng-controller="TopMenuCtrl">
            <nav class="navbar navbar-default navbar-top">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li ng-repeat="menuItem in topMenu" ng-if="(menuItem.state != 'db-user' &&  menuItem.state != 'admin')  || (isSuperUser() && (menuItem.state == 'db-user' || menuItem.state == 'admin'))" ng-class="{'active' : activeTopMenu === menuItem.state}">
                          <a ui-sref="{{menuItem.state ? menuItem.state : '({})'}}">{{menuItem.name}}</a>
                      </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                      <li><a href ng-click="logout()">Logout</a></li>
                    </ul>
                  </div><!-- /.navbar-collapse -->
              </nav>
        </div>

        <!-- LOADING -->
        <div id="loader" ng-show="loading">
        </div>
        
        <div class="flash-messages">
            <div class="container-fluid">
                <flash-message></flash-message>
            </div>
        </div>
        
        
        <!-- MAIN CONTENT -->       
        <div class="content" id="content" ui-view>
            
            
        </div>
 
    </div>
    
    <div ng-hide="isLogged()" ng-controller="UserCtrl" class="login-pg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2 col-sm-offset-5">
                    <div class="login-box">
                        <h2 class="login-heading">{{ 'login.tamura-europe' | translate }}</h2>
                        <h3>{{ 'login.login' | translate }}</h3>
                        <h4 ng-if="notLoggedMessage"><br/>{{ 'login.login-failed' | translate }}</h4>
                        <br/>
                        <form>
                            <div class="form-group">
                                <input required type="text" class="form-control" ng-model="email" id="email" placeholder="{{ 'login.enter-email' | translate }}" />
                            </div>
                            <div class="form-group">
                                <input required type="password" class="form-control" ng-model="password" id="password" placeholder="{{ 'login.enter-password' | translate }}" />
                            </div>
                            <button class="btn btn-login" ng-click="login()">{{ 'login.login' | translate }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="libs/jquery/jquery_3.1.1.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="bower_components/angular/angular.js"></script>
    <script type="text/javascript" src="bower_components/angular-animate/angular-animate.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-aria/angular-aria.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-resource/angular-resource.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-material/angular-material.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-translate/angular-translate.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-translate-handler-log/angular-translate-handler-log.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-sanitize/angular-sanitize.js"></script>
    <script type="text/javascript" src="bower_components/ng-file-upload/ng-file-upload.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-flash-alert/dist/angular-flash.min.js"></script>
    <script type="text/javascript" src="bower_components/angular-ui-tree/dist/angular-ui-tree.js"></script>
    <script type="text/javascript" src="bower_components/angular-modal-service/dst/angular-modal-service.js"></script>
    <script type="text/javascript" src="bower_components/angular-ui-grid/ui-grid.js"></script>
    <script type="text/javascript" src="bower_components/ng-file-upload/ng-file-upload-all.js"></script>
    <script type="text/javascript" src="bower_components/pdf/pdfmake.min.js"></script>
    <script type="text/javascript" src="bower_components/pdf/vfs_fonts.js"></script>
    <script type="text/javascript" src="bower_components/ng-csv/build/ng-csv.min.js"></script>

    <!-- MAIN app script -->
    <script type="text/javascript" src="js/tamuraApp.js"></script>

    <!-- HERE will be included controller, services, etc. -->
    <script type="text/javascript" src="js/controllers/topMenuController.js"></script>
    <script type="text/javascript" src="js/controllers/userController.js"></script>
    <script type="text/javascript" src="js/controllers/searchController.js"></script>
    <script type="text/javascript" src="js/controllers/projectRfqNpiSalesController.js"></script>
    <script type="text/javascript" src="js/controllers/customerDirectoryController.js"></script>
    <script type="text/javascript" src="js/controllers/tamuraDirectoryController.js"></script>
    <script type="text/javascript" src="js/controllers/timeSheetInvoiceController.js"></script>
    <script type="text/javascript" src="js/controllers/lmeMgtController.js"></script>
    <script type="text/javascript" src="js/controllers/exchRateMgtController.js"></script>
    <script type="text/javascript" src="js/controllers/stdItemsMgtController.js"></script>
    <script type="text/javascript" src="js/controllers/materialMgtController.js"></script>
    <script type="text/javascript" src="js/controllers/dbUserController.js"></script>
    <script type="text/javascript" src="js/controllers/adminController.js"></script>

    <!-- Components and Dialogs -->
    <script type="text/javascript" src="js/components/newStdItemDialog/newStdItemDialogController.js"></script>
    <script type="text/javascript" src="js/components/newMaterialDialog/newMaterialDialogController.js"></script>
    <script type="text/javascript" src="js/components/changePersonCustomerDialog/changePersonCustomerDialogController.js"></script>
    <script type="text/javascript" src="js/components/mailToPersonDialog/mailToPersonDialogController.js"></script>
    <script type="text/javascript" src="js/components/rfqSpecificationDialog/rfqSpecificationDialogController.js"></script>
    <script type="text/javascript" src="js/components/newRfqSpecificationDialog/newRfqSpecificationDialogController.js"></script>
    <script type="text/javascript" src="js/components/rfqMaterialDialog/newRfqMaterialDialogController.js"></script>
    <script type="text/javascript" src="js/components/rfqMaterialDialog/editRfqMaterialDialogController.js"></script>
    <script type="text/javascript" src="js/components/rfqChangeProjectDialog/rfqChangeProjectDialogController.js"></script>
    <script type="text/javascript" src="js/components/rfqCloneDialog/rfqCloneDialogController.js"></script>
    <script type="text/javascript" src="js/components/addCustomerGroupDialog/addCustomerGroupDialogController.js"></script>
    <script type="text/javascript" src="js/components/addCustomerCompanyDialog/addCustomerCompanyDialogController.js"></script>
    <script type="text/javascript" src="js/components/addTamuraEntityDialog/addTamuraEntityDialogController.js"></script>
    <script type="text/javascript" src="js/components/rfqManageTimeSheetDialog/rfqManageTimeSheetDialogController.js"></script>
    <script type="text/javascript" src="js/components/dbUserDialog/addDbUserDialogController.js"></script>
    <script type="text/javascript" src="js/components/dbUserDialog/editDbUserDialogController.js"></script>
    <script type="text/javascript" src="js/components/addCurrencyDialog/addCurrencyDialogController.js"></script>
    <script type="text/javascript" src="js/components/editUser/editUserController.js"></script>
    <script type="text/javascript" src="js/components/addPersonToDirectory/addPersonToDirectory.js"></script>
    <script type="text/javascript" src="js/components/rfqProduction/addRfqProduction.js"></script>
    <script type="text/javascript" src="js/components/rfqProduction/editRfqProduction.js"></script>


</body>
</html>
