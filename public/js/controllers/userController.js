tamuraApp.controller('UserCtrl', function ($scope, $state, $rootScope, $http, $translate, localStorageService) {
	
    // initialization
    $scope.notLoggedMessage = false;
    $scope.email = '';
    $scope.password = '';

    $scope.login = function () 
    {       
        $http({
            method :'POST',
            url : 'api/login',
            data : {
                'email' :    $scope.email,
                'password' : $scope.password
            }
        }).then(function (successResponse){
            // success callback
            $scope.notLoggedMessage = false;
            localStorageService.set("isLogged", true);  
            localStorageService.set("loggedUser", successResponse.data);    
            
            $state.go('search', {}, {reload: true});
            
        }, function (errorResponse) {
            // error callback
            $scope.notLoggedMessage = true;
            localStorageService.set("isLogged", false);  
            localStorageService.set("loggedUser", null);       
        });
    };
    
});