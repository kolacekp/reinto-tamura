tamuraApp.controller("CustomerDirectoryCtrl", function ($scope, $http, $rootScope, $translate, Flash, $mdDialog,$window,$state) {
    
    $rootScope.activeTopMenu = "customer-directory";

    $scope.customerTree = [];
    $scope.customerGroups = [];
    $scope.customerId = null;
    $scope.entityViewed = false;
    $scope.entity = null;
    $scope.deactivatedViewed = false;
    $scope.deactivatedPersonsViewed = true;
    $scope.keyAccountManagers = [];
    $scope.keyAccountManagersAll = [];

    $scope.canAddGroup = false;

    $scope.entityPersonsGridOptions = {
        showGridFooter : true,
        data: [],
        columnDefs: [
            {name : 'id', visible : false},
            {field: 'name', displayName: 'Person Name',cellEditableCondition: true },
            {field: 'phone', displayName: 'Phone',cellEditableCondition: true },
            {field: 'mobile', displayName: 'Mobile',cellEditableCondition: true },
            {field: 'email', displayName: 'E-mail',cellEditableCondition: true },
            {field: 'purchaser',  displayName: 'Purchase Manager', cellEditableCondition: true, type: 'boolean'},
            {field: 'engineer',  displayName: 'Engineer', cellEditableCondition: true,  type: 'boolean'},
            {field: 'deactivated',  displayName: 'Deactivated', cellEditableCondition: true,  type: 'boolean'}

        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };


    $scope.saveRow = function(rowEntity)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/persons/updateCustomerPerson',
            data : {
                person : rowEntity
            }
        }).then(function (successResponse){
            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.showDeactivated = function()
    {
        $scope.deactivatedViewed = true;
        getCustomerTree();
    };

    $scope.hideDeactivated = function()
    {
        $scope.deactivatedViewed = false;
        getCustomerTree();
    };

    $scope.showDeactivatedPersons = function()
    {
        $scope.deactivatedPersonsViewed = true;
        getCustomerDetail();
    };

    $scope.hideDeactivatedPersons = function()
    {
        $scope.deactivatedPersonsViewed = false;
        getCustomerDetail();
    };


    $scope.collapseAll = function ()
    {
        $scope.$broadcast('angular-ui-tree:collapse-all');
        $scope.entityViewed = false;
    };

    $scope.expandAll = function ()
    {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    $scope.showEntity = function(customerId, canAddGroup)
    {
        $scope.customerId = customerId;
        $scope.canAddGroup = canAddGroup;
        getCustomerDetail();
    };

    $scope.deleteCustomer = function(customer){
        var deleteUser = $window.confirm('Are you sure you want to delete ?');
        if(deleteUser){
            if($scope.entity != null){
                $rootScope.loading = true;
                $http({
                    method : 'POST',
                    url : 'api/customers/delete',
                    data : {
                        customerId : $scope.entity.id
                    }
                }).then(function (successResponse){
                    $rootScope.loading = false;
                    $state.go('customer-directory', {}, {reload: true});
                }, function (errorResponse) {
                    // error callback
                    console.log(errorResponse);
                    $rootScope.loading = false;
                });
            }
        }

    };

    $scope.keyAccountManagerSearchTextChange = function(text)
    {
        $scope.keyAccountManagers = $scope.keyAccountManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.updateCustomer = function()
    {
        var changedRoot;
        if($scope.entity.id_root != null)
            changedRoot = $scope.entity.id_root;
            $scope.entity.id_root = $scope.entity.id_root.id;
        if($scope.entity != null){
            $rootScope.loading = true;
            $http({
                method : 'POST',
                url : 'api/customers/update',
                data : {
                    customer : $scope.entity
                }
            }).then(function (successResponse){
                $rootScope.loading = false;
                if($scope.entity.id_root != null){
                    $scope.entity.id_root = changedRoot;
                }
                getCustomerTree();
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });
        }
    };

    $scope.delete = function()
    {
        if($scope.entity != null){
            $rootScope.loading = true;

            $http({
                method : 'POST',
                url : 'api/customers/delete',
                data : {
                    customerId : $scope.entity.id
                }
            }).then(function (successResponse){
                $rootScope.loading = false;
                getCustomerTree();
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });
        }
    };

    $scope.changeCustomerForSelectedPerson = function(ev)
    {
        var selectedPersons = $scope.gridApi.selection.getSelectedRows();

        if(selectedPersons.length == 1){

            $http({
                method : 'GET',
                url : 'api/customers/groupCustomers?customerId=' + selectedPersons[0].customer_id
            }).then(function (successResponse){
                $rootScope.loading = false;

                var customers = successResponse.data;

                $mdDialog.show({
                    controller: 'ChangePersonCustomerDialogCtrl',
                    templateUrl: 'views/components/changePersonCustomerDialog/changePersonCustomerDialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:false,
                    fullscreen:true,
                    locals : {
                        customers : customers,
                        person : selectedPersons[0]
                    }
                })
                .then(function(res) {
                    console.log("res");
                }, function() {

                });


            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });


        }else{
            Flash.clear();
            $translate(["customer-directory.select-exactly-one-person"]).then(function (translations) {
                Flash.create('danger', translations["customer-directory.select-exactly-one-person"], 0, {}, true);
            });
        }
    };

    $scope.sendMailToSelectedPerson = function(ev)
    {
        var selectedPersons = $scope.gridApi.selection.getSelectedRows();

        if(selectedPersons.length == 1){

            $mdDialog.show({
                controller: 'MailToPersonDialogCtrl',
                templateUrl: 'views/components/mailToPersonDialog/mailToPersonDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    person : selectedPersons[0]
                }
            })
            .then(function(res) {
                console.log(res);
            }, function() {
                Flash.clear();
                $translate(["customer-directory.mail-sent"]).then(function (translations) {
                    Flash.create('success', translations["customer-directory.mail-sent"], 0, {}, true);
                });
            });

        }else{
            Flash.clear();
            $translate(["customer-directory.select-exactly-one-person"]).then(function (translations) {
                Flash.create('danger', translations["customer-directory.select-exactly-one-person"], 0, {}, true);
            });
        }
    };

    $scope.addCustomerGroup = function(ev, sup, root)
    {
        if(root != null)
            root = root.id;
        $mdDialog.show({
            controller: 'AddCustomerGroupDialogCtrl',
            templateUrl: 'views/components/addCustomerGroupDialog/addCustomerGroupDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                sup : sup,
                root : root,
                keyAccountManagers : $scope.keyAccountManagersAll
            }
        })
        .then(function(res) {
            console.log("res");
        }, function() {

        });
    };

    $scope.addCustomerCompany = function(ev, entity)
    {
        if(entity != null && entity.id_root != null)
            entity.id_root = entity.id_root.id;
        $mdDialog.show({
            controller: 'AddCustomerCompanyDialogCtrl',
            templateUrl: 'views/components/addCustomerCompanyDialog/addCustomerCompanyDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                entity : entity,
                keyAccountManagers : $scope.keyAccountManagersAll
            }
        })
        .then(function(res) {
            console.log("res");
        }, function() {
            getCustomerTree();
        });
    };

    $scope.addCustomerForDirectory = function (ev) {

        $mdDialog.show({
            controller: 'AddPersonToDirectoryCtrl',
            templateUrl: 'views/components/addPersonToDirectory/addPersonToDirectory.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                entity : $scope.entity,
                customerId : $scope.customerId,
                isEngineer : false
            }
        })
            .then(function(res) {
                console.log("res");
            }, function() {
                getCustomerTree();
                getCustomerDetail();
            });

    };

    function getCustomerDetail()
    {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/customers/detail?customerId=' + $scope.customerId + "&personsDeactivated=" + $scope.deactivatedPersonsViewed
        }).then(function (successResponse){
            $rootScope.loading = false;

            $scope.entity = successResponse.data;
            $scope.entityViewed = true;
            if($scope.entity.id_root != null){
                for(var i = 0; i < $scope.customerGroups.length; i++){
                    if( $scope.customerGroups[i].id ==  $scope.entity.id_root) {
                        $scope.entity.id_root = $scope.customerGroups[i];
                        break;
                    }
                }
            }

            $scope.entityPersonsGridOptions.data = $scope.entity.persons;
            window.scrollTo(0,0);

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    function getCustomerTree()
    {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/customers/directory?deactivated=' + $scope.deactivatedViewed
        }).then(function (successResponse){
            $rootScope.loading = false;

            $scope.customerTree = successResponse.data;
            $scope.customerGroups =[];
            $scope.customerTree.forEach(function (group) {
               if(group.type == "group"){
                   $scope.customerGroups.push(group);
               }
            });

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    function getKeyAccountManagers()
    {
        $http({
            method :'GET',
            url : 'api/persons/salesManagers'
        }).then(function (successResponse){
            $scope.keyAccountManagers = successResponse.data;
            $scope.keyAccountManagersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }


    //fire
    getCustomerTree();
    getKeyAccountManagers();



});
