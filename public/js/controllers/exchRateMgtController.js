tamuraApp.controller("ExchRateMgtCtrl", function ($scope, $state, $http, $rootScope, localStorageService, $translate, Flash, $mdDialog) {

    $rootScope.activeTopMenu = "exch-rate-mgt";

    $scope.currencies = [];
    $scope.currenciesAll = [];
    $scope.defaultCurrencySearchText = "";
    $scope.defaultCurrency = {};

    function getActualCurrencies()
    {
        $http({
            method :'GET',
            url : 'api/currencies/all'
        }).then(function (successResponse){
            $scope.currencies = successResponse.data;
            $scope.currenciesAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getDefaultCurrency()
    {
        $http({
            method :'GET',
            url : 'api/currencies/default'
        }).then(function (successResponse){
            $scope.defaultCurrency = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    $scope.defaultCurrencySearchTextChange = function(text)
    {
        $scope.currencies = $scope.currenciesAll.filter(function(obj){
            return obj.short_name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.changeDefaultCurrency = function()
    {
        if($scope.defaultCurrency !== undefined && $scope.defaultCurrency.id !== undefined){
            $rootScope.loading = true;
            $http({
                method : 'POST',
                url : 'api/currencies/changeDefault',
                data : {
                    currencyId : $scope.defaultCurrency.id
                }
            }).then(function (successResponse){
                $rootScope.loading = false;
            }, function (errorResponse) {
                // error callback
                $rootScope.loading = false;
            });
        }

    };

    $scope.changeLastModifiedBy = function(currencyId){

        $scope.currenciesAll.forEach(function (obj){
            if(obj.id === currencyId){
                obj.last_modified_by = $rootScope.getLoggedUser().username !== null ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
                var d = new Date();
                var curr_date = d.getDate();
                var curr_month = d.getMonth() + 1; //Months are zero based
                var curr_year = d.getFullYear();
                var curr_hour = d.getHours();
                var curr_minute = d.getMinutes();
                var curr_second = d.getSeconds();
                var date = curr_year + "-" + curr_month + "-" + curr_date + " " + curr_hour +":" +curr_minute +":" +curr_second;
                obj.last_modified_date = date;
            }
        });

    };

    $scope.saveCurrencies = function()  {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/currencies/change',
            data : {
                currencies : $scope.currenciesAll
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);

            getActualCurrencies();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    $scope.addCurrency = function(ev)
    {
        $mdDialog.show({
            controller: 'AddCurrencyDialogCtrl',
            templateUrl: 'views/components/addCurrencyDialog/addCurrencyDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true
        })
        .then(function(res) {
            console.log("res");
        }, function() {
            getActualCurrencies();
        });
    };

    $scope.deleteCurrency = function(currencyId)
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/currencies/delete',
            data : {
                currencyId : currencyId
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.currenciesAll = $scope.currenciesAll.filter(function( obj ) {
                return obj.id !== currencyId;
            });
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    getActualCurrencies();
    getDefaultCurrency();
    
});