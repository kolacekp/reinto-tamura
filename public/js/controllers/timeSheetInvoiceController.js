tamuraApp.controller("TimeSheetInvoiceCtrl", function ($scope, $http, $rootScope,$filter,$translate) {
    
    $rootScope.activeTopMenu = "time-sheet-invoice";

    $scope.allCustomers = null;
    $scope.totalHoursSelected = null;
    $scope.totalHoursInvoice = null;
    $scope.invoiceNumber = 5;
    $scope.dateOfInvoice = null;

    $scope.timeSheetInvoice = [];
    $translate(["time-sheet-invoice.sister-companies", "time-sheet-invoice.invoice-number","time-sheet-invoice.date-of-invoice",
    "time-sheet-invoice.sales-office","time-sheet-invoice.customer-name","time-sheet-invoice.project-name","time-sheet-invoice.rfq-ID",
    "time-sheet-invoice.customer-pn","time-sheet-invoice.action","time-sheet-invoice.date","time-sheet-invoice.hours",
    "time-sheet-invoice.total-rfq","time-sheet-invoice.total-customer","time-sheet-invoice.total-project","time-sheet-invoice.total-spent"]).then(function (translations){
        $scope.timeSheetInvoice = [
            {id : 0,  text : translations["time-sheet-invoice.sister-companies"]},
            {id : 1,  text : translations["time-sheet-invoice.invoice-number"]},
            {id : 2,  text : translations["time-sheet-invoice.date-of-invoice"]},
            {id : 3,  text : translations["time-sheet-invoice.sales-office"]},
            {id : 4,  text : translations["time-sheet-invoice.customer-name"]},
            {id : 5,  text : translations["time-sheet-invoice.project-name"]},
            {id : 6,  text : translations["time-sheet-invoice.rfq-ID"]},
            {id : 7,  text : translations["time-sheet-invoice.customer-pn"]},
            {id : 8,  text : translations["time-sheet-invoice.action"]},
            {id : 9,  text : translations["time-sheet-invoice.date"]},
            {id : 10,  text : translations["time-sheet-invoice.hours"]},
            {id : 11,  text : translations["time-sheet-invoice.total-rfq"]},
            {id : 12,  text : translations["time-sheet-invoice.total-project"]},
            {id : 13,  text : translations["time-sheet-invoice.total-customer"]},
            {id : 14,  text : translations["time-sheet-invoice.total-spent"]}
        ];
        // console.log( $scope.timeSheetInvoice);
    });

    var customerName = null, projectName = null, customerPn = null, rfqID = null;
    $scope.sisterCompaniesGridOptions = {
        showGridFooter : true,
        data: [

        ],
        columnDefs: [
            {
                field: 'short_name',
                displayName: 'Id',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.getCustomersByEntityId(row)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'name',
                displayName: 'Sales Office',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.getCustomersByEntityId(row)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            }
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.sisterCompaniesgridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.customersGridOptions = {
        showGridFooter : true,
        data: [

        ],
        columnDefs: [
            {
                field: 'name',
                displayName: 'Customer name',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.getProjectRfqByCustomerId(row)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            }
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.customersgridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.projectsRfqsGridOptions = {
        showGridFooter : true,
        data: [
        ],
        columnDefs: [
            {
                field: 'projectId',
                displayName: 'Pro',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.geTimeSheetByRfqId(row)" class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'name',
                displayName: 'Project name',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.geTimeSheetByRfqId(row)"  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'rfqsId',
                displayName: 'RFQ',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.geTimeSheetByRfqId(row)"  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'pn_customer',
                displayName: 'Customer PN',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div ng-click="grid.appScope.geTimeSheetByRfqId(row)"  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            }
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.projectsRfqsgridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.hoursNotInvoicedGridOptions = {
        showGridFooter : true,
        data: [
        ],
        columnDefs: [
            {
                field: 'rfqId',
                displayName: 'RFQ',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'pn_customer',
                displayName: 'Customer PN',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'hours',
                displayName: 'Hours',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            }
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.hoursNotInvoicedgridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.hoursToInvoicedGridOptions = {
        showGridFooter : true,
        data: [
        ],
        columnDefs: [
            {
                field: 'rfqId',
                displayName: 'RFQ',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'pn_customer',
                displayName: 'Customer PN',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            },
            {
                field: 'hours',
                displayName: 'Hours',
                cellEditableCondition: false,
                cellTemplate: '<div>' +
                '<div  class="ui-grid-cell-contents" title="TOOLTIP">{{COL_FIELD}}</div>' +
                '</div>'
            }
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.hoursToInvoicedgridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    function getSisterCompanies()
    {
        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/time-sheet-invoice/getSisterCompanies'
        }).then(function (successResponse){
            $scope.sisterCompaniesGridOptions.data = successResponse.data[0];
            // console.log($scope.sisterCompaniesGridOptions.data );
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    $scope.getCustomersByEntityId = function(row)
    {
        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/time-sheet-invoice/getCustomersByEntityId?id=' + row.entity.id
        }).then(function (successResponse){
            $scope.allCustomers = successResponse.data;
            $scope.customersGridOptions.data = [];
            for (var i = 0; i < successResponse.data.length; i++){
                $scope.customersGridOptions.data.push({
                    id : successResponse.data[i][0].id,
                    name : successResponse.data[i][0].name
                });
            }
            // console.log($scope.customersGridOptions.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    };

    $scope.getProjectRfqByCustomerId = function(row)
    {
        // console.log(row);
        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/time-sheet-invoice/getProjectRfqByCustomerId?id=' + row.entity.id
        }).then(function (successResponse){
            $scope.projectsRfqsGridOptions.data = [];
            $scope.projectsRfqsGridOptions.data = successResponse.data;
            for (var i = 0; i < $scope.customersGridOptions.data.length; i++){
                if($scope.customersGridOptions.data[i].id == row.entity.id){
                    customerName = $scope.customersGridOptions.data[i].name;
                }
            }
            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    };

    $scope.geTimeSheetByRfqId = function (row) {
        // console.log(row);
        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/time-sheet-invoice/geTimeSheetByRfqId?id=' + row.entity.rfqsId
        }).then(function (successResponse){
            $scope.hoursNotInvoicedGridOptions.data = [];
            $scope.hoursNotInvoicedGridOptions.data = successResponse.data;
            for (var i = 0; i <   $scope.projectsRfqsGridOptions.data.length; i++){
                if( $scope.projectsRfqsGridOptions.data[i].rfqsId == row.entity.rfqsId){
                    projectName =  $scope.projectsRfqsGridOptions.data[i].name;
                    customerPn = $scope.projectsRfqsGridOptions.data[i].pn_customer;
                    rfqID = $scope.projectsRfqsGridOptions.data[i].rfqsId;
                }
            }

            if($scope.hoursNotInvoicedGridOptions.data.length > 0)
                $scope.totalHoursSelected =  $scope.hoursNotInvoicedGridOptions.data[0].hours;
            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    };

    $scope.moveAllRowsTo = function (position){
        if(position == "right"){
            $scope.hoursToInvoicedGridOptions.data = $scope.hoursNotInvoicedGridOptions.data;
            $scope.hoursNotInvoicedGridOptions.data = [];
            countSelectedHours();
        }
        else if( position == "left"){
            $scope.hoursNotInvoicedGridOptions.data = $scope.hoursToInvoicedGridOptions.data;
            $scope.hoursToInvoicedGridOptions.data = [];
            countSelectedHours();
        }
    };

    function countSelectedHours() {
        var i;
        if ($scope.hoursNotInvoicedGridOptions.data.length < 0)
            $scope.totalHoursSelected = null;
        else{
            $scope.totalHoursSelected = null;
            for (i = 0; i < $scope.hoursNotInvoicedGridOptions.data.length ; i++) {
                $scope.totalHoursSelected += $scope.hoursNotInvoicedGridOptions.data[i].hours;
            }
        }

        if($scope.hoursToInvoicedGridOptions.data.length < 0)
            $scope.totalHoursInvoice = null;
        else{
            $scope.totalHoursInvoice = null;
            for ( i = 0; i < $scope.hoursToInvoicedGridOptions.data.length ; i++) {
                $scope.totalHoursInvoice += $scope.hoursToInvoicedGridOptions.data[i].hours;
            }
        }
    }

    $scope.moveSelectedRowsTo = function (position) {
        var selectedRows = null;
        var i, index;
        if(position == 'right' && $scope.hoursNotInvoicedgridApi.selection != null){
            selectedRows = $scope.hoursNotInvoicedgridApi.selection.getSelectedRows();
            if(selectedRows.length > 0) {
                $scope.hoursToInvoicedGridOptions.data = selectedRows;
                // console.log(selectedRows);
                for (i = 0; i < selectedRows.length; i++) {
                    index = $scope.hoursNotInvoicedGridOptions.data.indexOf(selectedRows[i]);
                    $scope.hoursNotInvoicedGridOptions.data.splice(index, 1);
                }
            }
            countSelectedHours();
            $scope.hoursNotInvoicedgridApi.selection.clearSelectedRows();
        }
        else if (position == 'left'  && $scope.hoursToInvoicedgridApi.selection != null){
            selectedRows = $scope.hoursToInvoicedgridApi.selection.getSelectedRows();
            if(selectedRows.length > 0) {
                $scope.hoursNotInvoicedGridOptions.data = selectedRows;
                console.log(selectedRows);
                for (i = 0; i < selectedRows.length; i++) {
                    index = $scope.hoursToInvoicedGridOptions.data.indexOf(selectedRows[i]);
                    $scope.hoursToInvoicedGridOptions.data.splice(index, 1);
                }
            }
            countSelectedHours();
            $scope.hoursToInvoicedgridApi.selection.clearSelectedRows();
        }
    };
    function checkErrDate (date) {
        dateCheck = false;
        var curDate = new Date();
        var inputDate = new Date(date);
        if(inputDate.getTime() > curDate.getTime()){
            alert("Invoice date must not be in the future.");
            return false;
        }
        else
            return true;
    }

    function buildTableBody(toInvoices) {
        tables = [];
        console.log(toInvoices);
        for (var i = 0; i < toInvoices.length; i++){
            Table = {
                table :
                {
                    width: ['*','*','*','*','*'],
                    body: [
                        [customerName,'       ','       ','       ','       '],
                        [projectName,'       ','       ','       ','       '],
                        ['RFQ\t'+toInvoices[i].rfqId,'       ','       ','       ','       '],
                        [toInvoices[i].rfqId,'\t|'+toInvoices[i].pn_customer,toInvoices[i].action,toInvoices[i].date,'|\t'+toInvoices[i].hours]
                    ]
                    },
                    layout: 'lightHorizontalLines'
            };
            tables.push(Table);
            Results = {
                text: $scope.timeSheetInvoice[11].text+" "+toInvoices[i].rfqId+': '+toInvoices[i].hours,
                    alignment : "right"
            };
            tables.push(Results);
            Results ={
                text: $scope.timeSheetInvoice[12].text + ' \'' + projectName + ': ' + toInvoices[i].hours,
                    alignment : "right"
            };
            tables.push(Results);
            Results ={
                text: $scope.timeSheetInvoice[13].text + ' \'' + customerName + '\': ' + toInvoices[i].hours,
                    alignment : "right",
                    margin: [0, 0, 0, 40]
            };
            tables.push(Results);
        }
        return tables;
    }

    $scope.createInvoicePdf = function ()
    {
           console.log("createInvoicePdf");
           var tamuraLogoUrl = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAREAAABdCAYAAACCeVItAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAEK9SURBVHja7J15vFXT+8c/a9jTGe/QbbgNkqNbkiQqISl9kynzPJN5lnmIyJSZzLPMlJCkQZJUJEkSVyWVxnvvGfe0ht8fFz+RsRvivF+v/VI6a61nr732Zz/7WWs9m2itUaRIkSJ/FlrsgiJFihRFpEiRIkURKVKkyKYJL3ZBkSKbJt9MmRFniz/q5Nakk9mVq5qK1elSVZspJRk3zoTkJigKJAThTPBoLG+UlNbyJo2X221aV5d0rJpXsuMu2YawgxQDq0WK/POpmfpeNPPpp53qPp3bKb+oukquWdXE8DxHuWSAUgqABuEMhFFooqAAEEYRUAYKDSI1qAihfb/+FSQSGWXEnHyrbXb8MHn4ns9He+2xoigiRYr8m0Rj4rima+Z+vlX6g7f7Zr5ZUSlX1JRHFPayDQ5JFZRBwCwTLkwQ/f+xCaIBTf6/nqQfwqcaIQUUY4CmgGZQClAgyGSWo4I3es3eauu5FWcce1vzPn3WFkWkSJFN9fXk2ec7rJzyXm9//vwOatW3TZWbH2AaEZi2DWIZ8KEQAAAlYEKDhBqUuKCUgoLg+3tZE0ARQGuNACYopWAUYFKBKg1CCAQFJCHgJgX1QsiMh5xpj97ixMMfaHPBRWOLMZEiRTYV4Xj8/h5r3v2ghztzVjeeTSdDG33DqA0j5sBxYlCgCLSAFhJaSzAKEK1BqIa2AUgDQtXXxSgFKIHWEhISGoApAjg8gkIo4REKh3IgDAFTwqMCUc9BluSRLC9HokAHVN99f6RUenbpRVeNKnoiRYr8U4Xj5ac61b7+xoDCJ592DNJeVDHen0UcMNMClIZWCtC03mNADpTyem+CUBBFoKWGEAJKKcCwQQgBQKGUAJQGpwBjBJwxhDxEIZdHzIhAKA2PajBKYfkKjFJIAYioB53nsGkciIb4Kpt+bY/b7z++ZPdea4siUqTIP4Q1M99J1o58a5/COzP7FL5d0jxgfl+nLAEIA2AUGgQhFCglYFqBSAGiJFSYg1KAVECoCUAMUG6NMkzbZ4YlpPIo5UwAgBCC6zAwSRhaNAj2UWEIJMsRUIWIxcELHgxQBJxBMxPUFQA34FsemM/gGDHUoQ5BIUBy652HdH3hkcHF15kiRf5mFo14tMuqsZP7hx/N3V55+QE0YcBqVgYtCPKKgpsKhEjIIA/Tc2EECqGkCCz7NV2SrCVtt1scKUnUlFVWLku0qFziNGuynDUqXxXv1jX4pTbTU9+Ppr9Z3rpuxcrKcNr0nvSrJanCmtVJljD7R5IO0tkMDMoRtQ24fggOA5pq5PNZkIhCo5I4cp/M7Vx8nSlS5G/ky1uH9V8+ZtI+1tdft85x0j9WVgKuFLzQh3AolPRheQJhTsFk/DVtmy5tWrHS2brdPKdjh7nxqg5zKnfcOd9wYvZ455UPvnhaftmSFmbC6M8BBEpCUQKLcVAA0vVhRg3kqQe2moxtc/M1Zzc/8KDqoogUKfIX8tHVVx1S++q4/c1sbZzE2F7EiQBCItQKSimYvoau80Aca0y8TfOF0Xbbzot03WZ65ZFHz/kr7Pv6xlv2WvLoi6c5cbJXnvmglEO5EhHbgJYKuuAjaMIQLMmhw+DB2zY95qjftKv4OlOkSAMw/+LLj1kzdtw+snZVmdM40dtqUgqdC4BMCM0otOuPsY1IEN+y7QK+43ZTrd47TW6xQ8N5Gr+XzS4ZNCbRsnLJB4Nvoo1L7P6ucOERE9IL4BGFGDWhCh4cy4SH3+dgFEWkSJEN4JPzTz5nxfQ529Fltc3KS5N9VatWcEMf+bQHeHKMNpgwt2i9oKzXTpO3Ou/Csf8Em0uPPGJuszVfPZS+6xkkYlZ/l1NwqUFtDrgMCW0g72bBShPpoogUKbLRXluuOEy+9saAfDqfTJbG+gfNY8gIASPtw8+F42mrysWVu3SdUjX0hhH/RPs7nnPlqHfHz+7hV3/ZH4YANynMnIc6O4pGiiMXdUaVNGu1pCgiRYo0dEzhzgd7rxzxyPFGIRsHnAGljVtgbe0KcN8dl7Acj2+15fyWh+7/QstDjp39Tz+XVjt1m/J19eeDTJjIeB5s24HDJOryWTRuvMWK+PadVVFEihRpIJY+92yHVcPvvCj/9eI2KK3YWSVKUSjUwVi6YHK0WaslkT4Hj+t4/dXPbErnlOy67QfyySdgaAHNTGgwCJkBlxJOxw6/O9BbFJEiRX6FVe9/4Hx5662X4ZMPutCY3T+2WVt4q7LIFWrHRDZruqTpcaeManPe2eM3xXPzy0tqTMogIGDwGGQQwnQUiLJei+/Vd2RRRIoU2UC+OHfQwMKYZ49gRqKXKGuFukIe1rcrxsY2a7K06shTH295winTNuXzk1IyJjQ0IZChD0E0ynwTNVu0+rLF//quLopIkSJ/km+eebpj9V0PDlJ+2qZNmvUKswXE65aOadq65ZKKE8++r8XBR839N5wnX7W2IggCRHgJQimgmEbtGjm+8aWHP/6H6ikOmSJF/p+PTzx1UObtabs7JWa/nBSgSwvjm3VoP6fxUYc/3uTww+b9m841/9GnnRUjEJrDMCVUPg+1Q7cZ7Q858g+JZFFEihQB8MUj9/f4avhDZ1W4rpOI0X41hfxrZS2aL4lePPCBqiMPnftvPOe1H3+6vWFbyAqJJA3hFPzxzU85cvgf9miKw6fIf533jjvpIjVtRs/SiNprLQnGxpOtH0odfsiTrU4/eeq/+bzdTz/uqGIOGlGJNbW5cc322nd0k937/eE0iUURKfKfZeWol1MLbrh5sO0rM5CK5nnJM60O6/9C+0uuGP1vP/cZQ684zAqZicBHrdSINk8t3Pruu+77M3UVRaTIf5K5l15+1LdjnjmqzDP6ZTQdU7nH/8a0G37X8P/K+fsvvTVARNC/3DLg1aoxjS8788Y/W1dRRIr856gecOxNYt4HHZlJVa5D5xs7nH/8nU12+fPZzjc1PrvgrNNEJpusaJrENyvSY9uecuLwVN99l/zZ+ooiUuQ/w9djx1d+e/PNV/jVszs78VYrK044/b5tBp057r/UB6vee7t02StjBiRbNO2X+erbyZUHHjo6ddGgDdoYWMwnUuS/8fS9Z3jPNcPvPV+66WTjHn0mdnjmiev+i/0wrU//e3hd7Rn+qm8n0V67TtzpqSev39A6iyJS5F/P3MuuPiLzxIiBtHl8RaMzBg7f8tjTp/4X+2HOJYOOy7z8yiHELsmTFi2X7DR21AUNUW/xdabIv5pZRx121dq33uvXrO+uYzs+PeK6/2o/pJ94qfOisW/3b2YxYVSWrNyugQSk6IkU+Vczee9d79ezVrRvO+TCi5sPPGn6f7kv3tqu57NNM98mM1tVzdv51dcvbMi6i55IkX8lE7t3eTpJWwSt3xiyb6POu6b/y30xc7c97o1nvk3WbNH+i16vvnphQ9dfFJEi/zqmbL3NyGSHnedu//y9g//rfTHz8KOvyi5Z0Ka8VZuFO4579dyN0QYtDrki/xZWzP6AT+uy9cjKU0+7oyggwNwzTj0rPWlcv+i23Wdt+/bE0zdWO8WYSJF/DR/u3f/WinNOu3OzDVg49a/pi7PPOcV78vGBFYcd/EzV/Q/ftlEb01oXj+KxyR+zjznukrUz5vJiX2h8cN5pZ7xZ2mzGgsFXHfJXtFf0RIps8nxzz639Wp55wbhiTwDvHX/iReT1CQPa3Hr5BU2POfkvmZEqikiRTZqV706JN9mlZ7bYE8D7e+01zFuxsnK7u247NbnTX9cnRREpUuRfwPiduz7lNK9cuvPzr1z6V7ddnJ0pUmQTZtU74yve69BvZKs9Dnzp7xAQoLhOpEiRTZZFdwzvXf3sqGPbPXjpwJY79Vn7d9lRFJEiRTZBZg665rhsWGP1nTHh2L/blmJMpEiRTYjCrBnm3LufPrdin94j2xy4X/U/waaiiBQpsolQ/fRzHcX8rzu0u+7i5/5JdhVFpEiRTYA5d9/bq3mbNtWN9tpj6T/NtqKIFCnyD6b2o/eiKxevbtLugP0W/lNtLIpIkSL/UJbOmktbdOmo/ul2FkWkSJEiG0SDTPFOn/eluXL1mkpQAwBgWZZnmtwvFApR00z4G/MElCowSqkkhEFKyTyvYBOt0KRJxfLu7VPB763nyXEfdJq/aE3HqGNnt23bZNbeO271l717Tp3zRXTyzLm7Zwo0udnmWyzutu2WU7dv5fxlT6AHRr7d45u1XqtkNJbepXPVxO7tGwcNWf8Tr37YefnSdItE0kl37tL6gx7tKt2/eqC/On1Rqy8WLWtfWpKobb955dwe7Rr9og2PjJ7R9auF36QQj6LEtGp//G+UUsUYU5zzkHMuGpeoFY7jFMoSyTXdt9os2JjnMG76l00/XrSiy6rVq5u2KnUWnXN0/0n/Ck+k28mPPjHv0/md8vl8J8OJIgwDUM5hWRbcQgGgG3dRLAGg1Xf3GyGAVICWcGKRuWVlJWvvuWC/4/frudXiXxwwr73T9bpbXx6yJK1a6wSt0p4LK6Dz+my7+dgxj11y4ca+AMMee7P/kDtHDs3TRp2poSHdHCyHzrv6jH0vvuSYPmM2ZtuzFtTSI86/7ZUvv17ZXhuRFOUURlg776RDeg2/57Jj79vQ+q+7d+SAG56Zfo0biKj2aMrQArFIdma/3bYb8+yNZwz5Kwb402/M7Hjl7U8NW1QTbmHYTir0PURNOme/nbd+YcTNp/0s0/mRF919xYtjZh0RiTZqnxcaQhfW/cH39wulIIRAwwHnFAZRc+M2TTcttZe3aVmycJeuHSft0mO7t3doUy4a4jwOPPe2oa9NmbN/GDrtAQMgorpRk/iqWwcdePox/brN2WRF5KiLHrrs+XEfHRWLxdpTw4RUGmAUUmgopcA4ASMb94FKmAkpJZQEGGOwDAYhQgShB6UUlJebO3XEpdt22WrznxnyxvSFlXudd8s7EMlUeaPGyGQWIxqxIX0H2do11QcdvOtzL1518JUby/YJ0z8r3+P8O97TRtMqkzmwdAZQAQQxERS+mT/igSv2P6RzuwUbq/2djrzl4WlfrTwxHotCCAVq2lDah6xdNG/oOQdfMOjoff70ztiRUxe0Oeb0a0e6pc06mXEOP6+QMOKQIofcqprqAbt1eumVu8/YqMu0R07+tM2B594zDpHSVCyeQJBejZhtwdMUhUxd9cF79XrmhesOXid5UXzH0z+STmlnmxFkCxoRm3z3fCLrbH//wRPWARQ4pKAg1AClHJ7nAYEPK2bP671t43HnnbTvjX23ab36T99ng2657Ok3Pzg+vlnHlPIliBQw43HUrFqN5nExefTwy/p0aVvxt8VONshNePmtjw6jJU77IEJRoAJ5FsLnEqElEZgCPpdwCTbqURfm4DGBwFTIEx9rwzzS2kNoAiRmwnMqOj71/Dsnrs/+c6566EFKSlKJEhuF/EpEzRIELiBkFo2bN0q99PLbh70+dU6LjdX5N9z/wjUKpVWQWUSsNLT0oQiFFB4oKW1/07CJV22stoc8MnL/aZ8s2tWKmtBhDlHkIXK1EAEDMZt1eOzpiadsSP1jJk0/IDCadmrKHSBXhwgE4OUhQg2nsl1q4qyF/T78evlGdVOff33ysTCcVKOyckjfRcSyoaBBoRGNJ1IT3vtojx//fuwHSytzIYv7WiOUHkwOiND/4ZAigFYCBAqUaFCiYSqGKGOwmQSRWVBVh5hTQCThwWBrO4x9f9H5+x53/cQLb3nxuD9zDg+N+bj76ElzDypt3CHl1YUg0oeWGQQ1K1DmRLBsRdjrhvueu+bv9ET+9EV8+Z3pKY85TtwqhXQJiCBwuAPlK8BTsLQBLiiYFhv1sDSHISm4AEzF4RALDnHAhQHiU8BzsXB1OvVT+y+++8VjFtaJVIlFIfw1AAkRUguS2GAWgdQFGJymhg4ffe3G6Pjrn566z6QPvuoXZSYqIhYKmRzyxEHIS8G4g9JYGebM/rLL0EdfH9DgrzEL6+iDT797tlNemjI1AVEaYSgRjUQgQx+2k8AXS912x17+8iV/to01demKUAJensCkJZAeh6kNmAYQqhC5jNt58aJv2mzMwb14ZaG1SUJ4dUtBlI+sZihQE5RpEFmACF1j1pwvfrgH+u/QYnncFFmb2KDKgsE4qBEB4Q40taCICQkDoWIIJEUgKTKUIkcJQsuEti0IyqGUBYo4iCpBJJYAS27W8ZZHxl5+3i3PD/zD4+TB0Vf7tLyz9Ck4CMIggGFY4JSBSYnS5pUY+fZHhz3+1rzOm5yIREoiBZgMvu/C5AyEAKHngREFgxMQSFAtAW1s1INRBQIFaAVoAcj6gwDgjAKcINE4uU6271nzltAHXnz/bBWzqnJEgVhxAID018IwfISSoNbTMCuaYtana7pdef9rhzV0x99+76OXWE1SqcAC8gUBmzkwmEAo0xDSR1bkQZvxqmEPPnflu9WZeIPGYe596Yplq9GLGQHCMAQMG77JkRVZWDGJnFwLs0m8/YhJ44976533K/5MG4VCxoFtIYhrZFCAjjrwlAeJNAhycCwL2tPst+r54pILj5ux/wE3fLD9Lk9Patf51fFHHXHF77XBk4Yd8AiYaYDRAEp7oFRBSA/EtpEVZuc63y/9cZmjD+rziFtXU+1mcshkViCXrkE+Uws3l4ZfyCJwcwi9PAI3h8DNwch5CFbXIqzNwJSASRiUIpCSQcECVVkEKCDSKpW647FxFz35+sxOv9f+QTfeftziZSu2sJMWAiMHYgXgtoGCEMhzghpDoJBdBZpsmrp2+PNDN7nZmf6dtlm+fRNv+qfLg1QkGgfnFgIhoQkDpYBSAoRpmIxBhgKcc8ggBOccSiloSkAIgacEiNJgjEFKCcYYhBBgjEMRQFEDUa8WlEfhsSg4/HqXjpmgUFAkBiEEOKNgjEEpUZ+yjQKBDMCyKxYc1vv8J39s+wXDX703LXmXOF+LUCWhlAuoJKgpIAIfFosAMg0RhkDzSPsHHp91Vv9enUf3aNeiQWYV9jlj6E1rwkaNYxEJP3ChTBOhAhBqmBqg3ILSIaLcQBZNu1x44xP3TH/4rAbZaPXEa293fn7C7KNLWjaGSrsACaE1h9YWKAAqCQypQagFpVB1+t3jn6jedcc9/2g7oUqaVNZACwO2jsCQATxbgIdlsGiAnAoQiUfyv1R++W0395/z1IgTjEwQjTSy+ufCHKx44xE7j3jmd3+AynLCgMoQUjuQiiLCAcgQSptQkoIoF9Dr3gLDzz9o+HH/6/jAN0tXtlKglDAKzrkwDCMwDCNkjAlKqRJCcCGEsTSTafXNF37rCe+/+7/3F9bsikizlGWkQZSEZC7MQIPCAEQBSNipYU+9fcUxe3c9+Ldsnzjzk/JbX1txeSxSklLCgmBpGMSBDkKYVEEFNpTpwzIdSK2wcGl6y4E3PXX+QxcffdsmIyIAcOmggYOPv+zx9jXp1V2gKIjj1AedgqA+ik0BEAZICcI5tBD1/9X6u5kUiWgiAYV1g1WE1AezIBX8QIBrIF9XA/AAoCGggvqLr3X99Axj9bMyIgQ4B2EEgAIhZMGQC464eO+ezX+Yrh3+3PSeM2bO72GaCUA3g8GykCICyiUCIWFbDrQQMHkcQuSgJUdGpXtcd9vz173x4AUb/NWwB0ZN6zFhxpL+ydKyVMEV4JSDKA1CNURQgME4HDuOdLoALQEnZmPOx593vnXE1H4XHLXzBqcAHPLg5KFmkqVkxoWnBRzLhFQKRCpYlgXfcwEoWIYDxGx8882yVhfdMeaYm8/d68kNaff760sIAaUUjHMYhrHe6f/Pr795ny8efPysSov1DUqTsPIhrKDRmJI7L738Z97GjHfjdrdd1pvFi1H6qzMjhBBQqJ8FJHfYukrssHXV710hugJ7YOY1Z+/+wpAnx+1/0z2vDqZGopPpmMi7aShmglABJTRiTgRfL125xVsffl7xv+3b/Wqg9Yp73ryFSy9FeQJuuAYRqwye54EzQEkblk3gBiECZYAagFXqpF4cPeeIA3t2e2aPHduu2GRE5ICdt1nY6OEhu02fPnPn6kWL2xLGYRhWwBhRjBAhgoBrw6So9zSECgWlBldaa6oJUCgUoi+8veAopVTn9Q06IQS6bdX8yV222u5tL/Adacaoqf2ASAkBzjmhQjMBFUqqtaaO4+ShNLLZTLKyedOl3bt3n9q3Q8k6F+ua+1+4gZjxjhFG4MEBl1lIQSCNAiyWhAglhM7CYuWAFHB4HG6kgPEfLOv/xOhPRhw7YJvZG9Jn1z7+5lCPlXU0Qg+cOrB5BJlcLRKlEXDDhpQS2WwajhUBNEOoJSi3O17/yLirN1RETr3+pbMWrshtGSlLwq3NIVaWhPB8SCHAOYXv5cE5B6UcmXQtTNMEZdEOD78y7Yx+fVNj+nSo+sM5KwghgP7/a/q9mFBa/4T/6e+XTRpbufLRF09r1CjaV5oaXBaQz6nxzW88/9Iteq2bxX35Pff2nHPXIxc12aHb9O2evvdnHgqnJPzhgfQjIQPqvWBCyALLpl5D3UxXHdNv1IfTPu0+4cPaTgUawLYjCBRAiA8iTFiOibWr1nZ+b9YXu/1v+3Yv/FI9Nz4xdq/pn6/YORGJQuoQlmUgyAfghoJUJiKRcmSy1bCNJCQh0AzQWiGd511uePD1q/fY8fxTNxkRAYCebWPZnm17jwUw9s8FaK88VKxv4AFQQuLAHu2fu/CE3cc2xMkefdmIy1YXgsaJeAQIfQTBalBqQDMBxzFQWFuAZUfALBuh8MG1DRn6MOMWAhFrP+TO54ceO2CbPf9s+ycMfmzQslV+C7vEgSpocK7gBXlEYlGkswWAahiGAUUppFYgWkMTA9zwUReEpYde+Ow1zw87/E99T2Xc9NlNH3jlw7PjJZFUWBCg8RIINw1PGIjYFggUlFIghMH1fCQSJQhDH6YRR627qut1d751bZ8Hq07/UyLyk4eDUvVtaa1/FpNbPmLUEY72+q+gGpXCQWZtblz56Sfet8VhR8398e8mHXTQtfasT3doUp7ol/5klrtoyustNu+59zoLBMMwtH5pCcN3IqJ6dGrfoAvf9tu75wuvzXzhImbWe+CKmqA6BNM2wtADIkksW+396ozfzY9Pusp0zBQIB7gPLaIwDA3QAFpYSNeuhhMzQLUNxgUyvou4YSPSmGPKx8v7DH1k3IDLT+w3epOY4m0QAyhV5CeDrN7NJJBSIu8Wog3RzsuTPk49P+Hjo7jtpEAEfALYZgBwA9w04NYGU9q2tB+NW5lXQs+GLyWYySGoBAIKygtYuLqw5cDrnz//z7Q/asqcNk+9+cEJUcdKqdxaUF7vdWgqIaHADRuxSAWgHJhWBAIaAiG0kNCUIM511UsTZx/68jszU3+m/aH3vzMExEuRQgiLChBCoEQAw4kh73mQYf3/84QANW34QiOXdxEIH7ZlY/LMRX2Hvzi554a8xvxUUNZHfs68zroxR4VkwPLc5Mo9jxrd6aIrfrghFj30cPeJ7Xd4xfl8YQfVpLRfoF0EmVUH+RPf7/nzmIjt/hGBawgaVSRWg7gwDQYV1ntelNWfuwgVmGHC8zz7l8ofNejxy2ozbpmpgVD6EFIj74bQnCBwCSKGi8rGFJAReKIArQRAKJgKYYRZkEgiddvTEy/5S+/hv1tEiP7/gaZI/fHDBVYKsURJg2StvvKWF28JDdk+asThexIhMWDROKQqAJIiQYzMpy8POXH/Pu1GUUGmaFhQ2oMyGYjkYMRFrLJx6tEXppw8ctrMPzw1OWT4a0OFEW1PRRpRaiHUBYBwMJPB8zw4Bkc+XQciBUQQwjAMgDMwrRAKwIQCImHV1XeMv+EPu9mPvnbIu7Ord4tGDGiuQWCCBatB7XKEUsAxTRAdgjMCxhhATXiBj0QiAUkKMFUcTqmVuun+twb/GRH5qZB8Hxf56etMzSfvO6quplRqCeWpUZk+249vd/e19wHA4tlvl8478OChy264Zmi8LD+AOWJ/5XmwiIEyx0bw8YJOP39AcfVrIqa1xqzPvmzQe2Btbb4CwkUUHFGYoEqAwIQiCpQbkIUcIoZYb0D5mTHTOzw3btYxjcorUpoSEAgwWLATHJoAKu9PP/6ALoffeOFR7XUmmEINASklotyBFC6IMmAaLmq8oNHB5z967X9GRH46yH7qfhK14dsRzrvt0YELV3mpeCRSv5KVEBgEyBd8OJYBP7Nm2hnH7D4cAIZfcvKTlloVJCP1DpDwJAgj0IpCBlkwJ1k1+NZXbvoj7Q9+6LVDPl64envbdgBqghoOiA6hKYEICaKWCeGm0ayMI8oLMFQBMgghwvpl/FY0ipySiDsmPvs62/Gye0ce8Xvbfv+zlc5dT0y+yCyzUhaNQRlAwdeIMAbPVyChD64D2ERB+3kwrQElEIs6CNwCiCLQICBU4pu1+VYnDnls0J95nfnpSs/1iUhmbW2pZbL+1A3BW7Vc1PuJp35Ylq5en9HLz7jxoEnFqswad1yuJgvJFVZyhkyEwg0KP3s156q+/p+OqR/Z0l6pht2X8drE2QdQIwqvECAkLgyEkIJCcYAwBggP7VMVn61X7IePHiYdXQXBEcj6rlG+Cy0k3JyPLVo41cPOOvi5I3fd6vPe3Teb7BcEYBBwIUB4HAXGwamHuB1NvTLh4wMfnzir839CRH7Jtfz+79TYsI1o73xSnXzoxU/PUA46UM8AZRKcE1Dpw46YyNW52Kpt5WdXn9bvze/LDDrvgGHp1V9PN2kUUTMGX3lQwgLXPiKRCOYv1h0vH/7S776R73lq0nlWMppiQYgQUWSFC6IsSC1hEAtBwUVl4/iYxx+8vtFmzRPP6DAHgzIY1IFBHLgij8Ci0AUDZoJU3fncjIt+twc2/MVhOdfrQjVDXrkIAxtWzEchTMIwJGyDInBzc6687MLddum2wy2FfBoEAm4uB9ukMEkSLmoB5cBpxFKPvjTzlDfe/6byj1zXHwvJd7GQ7/9tnWtbakXyimA0IQShIuyLh57q/v2/tbnyklHbjX/97N7vf3R421vvPqnVqSfvG/ccNMoxGAUJVhLL/azt9QjIT4UkFNJoqDF898sTer02Zd7+PF6GkHIETgCDaIQhBSgBQBFxjDmdt95y5k/LnnTNi+d/tXr1luVljZEVLkxtgRgmojyAzZNQnvfhGafsOfyHmdEL9x/qsJKpUiuYoMi5NnKGgFISRsDA4rT9lXe/est/wxMh/28D1QBR6+5PENrfoODvRXe+cZevaCdqJJE3C5DEA4MAJRxSeFCkZtrVpw5Yx02/6vB+b+2wZeLDtYECkxIKIYhhgiOKgloGmTSqho/44HfFRnY+5cGH064utTRDgAIMlgdTDASASXxYzIMfqupLjtpjcJ+WWHveSQfe6EmygENDkQA56sJQFJayIKkPhygUJI/ufu5j9/xW27c+/06/iTPm94tFmoArQDMfSvtggQnTchESC9lsUL1v7+4jz9qjxeRLj+lytcEj8wiloLaEKzgCiPpXKZ0FUaVgsdLUxbfef+dvtZ2MBmktGKhQYERBcgrDi8JgHjzTgpQGRCGzzrVN9uiVzsacrB9zIFctP2fpzVcMmdK597Ppzz5Y53et9t57qbu2przWLiDkBCbLQDTd4me7rgPimwYxIJUPYlJI7UAEDJT6YEyAaA7BWIPsZD/rhhdOu2LY67cQI9IeREGpDEwhkaYWYioDQ1LkcwKdU8kPe3Vqt87ix1HvfdXmhXGTjrJiZal0LgOuBQwT0MJDyEykCzl0277NzDMH7PbDF+16tGkVnHto19v9HPswpBkkk1mQjIQDBx7LgpgxrPg63ezsG1885V8vIhuTu5+Y1GvmR7N7xONRSDcLWwM6BJSyIbUNz1fTDt+j57MH7LrN8p+Wvezsk4Yiv3x6yAksw4QKPbi+DwYHMdNE3pWRfqfcdsevtX/fyEk7z5o9v6sT4VWen0XEKUEYUDCDQVNAawdrs2H1MXt2fOSEA3eeBQBH7bbN3IP6dHqmxverDUIRtwwoESIUCtSwIEOBWMRITZwyrd8zb8zu8Gvt3/X4mxc4kVgqEHkQboDrKBihgKWQdwsgoUSTErLi2RuOGwIAO227df6sg3a6xa1dW82pAYNRSOWBkAi0MqBUAfG4gy++WtH+intG/uoqXqFMSnkIBQDqO8+D1Q84CgUlQ/ih+lmAsWXLDkviaz0wLhARERXfrcOU5FY7/PDaUz3u6Y7zenR9ynvhlcMiKoZk1EbOVVOb9O42/qd1SZdRhfpAvZYKWgmYVv0Utlak3iDI9do/af7q0ufe+rj94+M/7vzw2JldHxwzrfvjE2Z2fvbdOR2emjS70+0vTOx7xX0vHdbrlNvuL+9zwfv3vDLt/NC0uziGB0NkYZol8PkWiCKAiDaCtBNQ2eXVV19w/MU/beu6W+6/1lO8swKD7URAKEcQCkgFMC0RcTNTbjr7gJ+Vu/7svUemmpVV12UYsnmOaDyCtJQwzAhImIWVpO2ffW3iMZPmLCn9R0/x/pO5/ok3rzFKK1JCCEQYACFgmXGAxBAUckhGeN2Ia05c7xN9v53arjiiX/dnn5nwOeJxuzulAnY0iiDwEGbrECuPtX/rw2+M+0ZOfum0A3pNXV8dlz8w+lbCox0V8ohGLLiFAKFkYCaBhoZSFkojYc0T151844/LPXfTmUNe63nGAdBJ+G4ahmFAa45AAVxKGDIAt+KpKx8YNeyIPTuvd8r5mIsevGTJCn/zeMKGdgQ8X8AkETDiIee5cJw4CrV1C64485B1FnDdMuiAx8fMmLnfF8tFKgIBw2JQgsAyHGS9dP1CsUh5h0eefeu0Pn13HrdbVePa9Q4sK6EIFQC1oSWglQSjJqApqNZQOkCmECQBrLOOZ/Njj3lg1jkzuiSI7Jfu1nla99vu/iElwezLrjxixYvPHxFL8L2ciAMLFEuXL0O8245TN99nv8U/taGuJixVlIGx+melVAEINyEkAGhoqhdYXP1swVv/gbfeOuXThX1c3+2kRQJgBJT+f8xOKQUoAAowIyXg3IZjutC2RJ5YUD5gpl2UM4GCIZAnCkhXVw8+88BLd++yxTprbS68d8xxXyzPtLfjzZB18+CEgoJBKI1IMoHaVTUfnntA74d27bhZbr2e9sk7Djv96qdaxxLl3XNeGpI6CD0DUcNArdbIC9lj6F2vX9v7kdPPLHoif5Ajzn/0ihXpQqXSFEpLaC3r10D4CqHvIkgvmXbhSXsO+7U6nr7x5LtKTVGnAgPgUaRzdTAoA2caATJgsUhq6MOT1hsFP+qaRy+rrTPKDNsCJSZ8PwQAOI4Dzw1hcQteZnX19Wfuf976yt94xsHn5mqWVjM7gXwhgMUACg1qOgiCAMlkCRZ+vXbLs28fecrPp7M/Tz0z5YvjonErpUMCUAJJFJQMAK3AeQn8fIBdd2g54cxDd57ys0DwKftcCiEWcNOuTxFAAder3+8CwkB5BLUB73nt3c/94n4Ny6SeEKL+96DQWta/r4KCqhBEKayoKfwsthLbt/eSshMOf2Ax2OjO1wy6DgDSE98u/3yXvR4sjHr5ELsiuZctY6Dl5VibXw2vsskjO1x+xeXrs6HO88soZ1CgYISCQEJKAQUOTRhsk3rd2rdbJ3K/8yGXPjzuw+p9A7usk1ORQqTMRKzUQKzUQCTJEEkyxEoNJBtHUFqZgGWvBmEeiI7D8ktgpRVKCYGTNJCJaAg/jxKWnXnj+Qede/Wp+77047amz1li3jti9Lk+iXUuuCHi0Qg4UdAqgGEwZNK1aNm0dOntV+w/4pf6eeBePT/qt0P7t+q+XQbTiMOgAHQOXlgHxkPY8VJMnvXV7ne/PKFXUUT+AE+88X7n5yfNO6qkvCQVMy0ooQBuIaAcihEokcPuO7adcMkRvSb/Vl3nHtP79nxm1XRtMtiWgvB9aG6jUHBRZgLLa9DitEvvOuvHZUa/O7v1s69/ckzSiaXcMAcQE0pyUCYhdQDHjMJL12K3ndqNP/Wgnaat9x370F6Te3VrPT6X9xGNlyD0CuBEQTEDoQLCwIddkkjd/8Lkc6Z8+Pk6G/SuuOuJW6TtVJmsAMYYPFfAMBgo80GIhokYokrOvOKc/debK+Ww3Xeaf+AuWz1fl81VO0YM0AEYl1CSgcGCG+RhJUrw9nuf9733xWk7r6+Otq3L5kPx7wKcDBQKEhJak/rYF2eoXlJbtb6yHa+8atT+n83bL1nVRX1yzmlnzTz+9Bdq08sH8rgxoCyThRBprFy6ZGzTllsM73fr9efxjl3WG3xP+7lSalCoUKJ+1XS9J2FwG0wDCYetE5t49JWpXWZ8saZHorIyZRgSYcED0xw6pBAegQooiDQAYSB0FdysB0XjoGYcIVHwZQGhLiBXWIO6utXVQoh5++3a5coxD17X6+Jjf55g6qq7n7vFU2aneKIM0UgEXj6DwM+DMwpOCFTgfnjhwH2G/dYYvXjggJtK43pCUAgRVRIWDyEMDh0K0MAFHLPq5gcmXVkUkT/AjXe/NpgkIlUIPAS5Ahjn8IWAIBqWZUCHHk447ujHfk9dV52631utU40Xe2EGnAGhUpAkglisCUTOR9TRqUfHVp8y7oN5TX+InN/54h2cJqu0KsAwCaTUsC0HXugBWkCGeZRawZQrzzrs8l9r+9ozD7rQlpm5AEA5gwryCKUAMWwQCBhaIQx4+8H3v/rDlPPF97x6zOdL0h1MBNAMEESCEQ6qJKhRH5vIrV66YOBh/YbvtlWq9pfafuGWkweXxmlN4HpQWtT3myIoFApwIiYCKWAnKlI3PzByvTlPOraumANFILQCAwOgoLSGJhxQGtQw8En18t/c0WrmsvHolpvNd82SZ7w6a0yel79AWlXdtvnAk+7rNOGNM63u698389LUj1LZXDZuGRRaa0hZf+6SAoQwhG4OzRtH1llGX/BEVEizfd4tgIZZmFpAQkFTAIzUT8lrBQkFajBYERuedCH8DGwq4NjmzJKSkmldOqYev+GsAef6U27e+ulhp1/XY8vIzxa93f7UhL7j5yztb0di8H0fbrYOnGnEIlGEYQjfd9Fjx+7Tzhqww7Tf6qNdOm9ROGBAv5eVFCBhiCAIQY0EqLBgUQk7SrGshrU6bvDDF22M++1fFxM568anT/t6rWxjxQBfCHDuIFQSTiyCnJuGFhqxeBkuGfrADTcn/ZqMLEmEEiZjllDCo4bBhaYESmqqQkWTViZT8MyIpYEwAEzLQtYrgFoUYIDBAuScRIdLbnn+9n7PDzl80N2vHjd/Ua5DMlkOpev3BBpMwgsDmEYUpuGgULt4/vEn9n/gl+IJP7jWnary5xzU8+abnntncKyiMoXcGhBImJaFQjYH03JQlohi8ozqvtc+OWn/K4/pPerOR1+5KFK+RYqGObiehqQBotxC4LlQto0w8LHDNmUzbjq//29uqLvixH5XXHrHa7eb3OpQcCXsCIMueCCK1b+ZwMeS2mDzY65+6pInrz56nbhOqmnJgljMme0L0dkm5nczbQomtaF0CMpNfLZoRcffsqHdIyN+WCuy6sOZvPH2XX9XusGJ07/oj1ClCAQY+U5IKIUmtH6lsO+ie6e269yg7VtVzGM0XJBwklVhNg8wDUHrBUhr/d2+IgatNQIhoEIPcbsC2UIelIbYt++2Lz9+8aE3/x77hj748hDilKe0DsE5gaEYTJuh4AcAs6AFsPCrRW26HD30ARdWRAQFHmWxnCIB9YVvO7xRQdA6brvSY46t1hZkmUEEAoOAcQcFdw0ijg3XFYhH4whslXpi5PSB++/V+4UBXdssbtDlGX93tveyvlfNcF23KzeMH9YQEELACEM2ncbNgw7Y88Ije/+uvTOTZn9WutcZj76jYHWMWgIFpsECAkkICFFgNAQNOSBj8IhCqHMwlQShBpTQsI36FAb5MIRlR6GlgkQUTAsYXIOy+hW1QodQJAQYBQ8NSAcIM2H1Qbv1eGbmjFk7rvaNviHPgMEBJRxapgFKQGg5fM9DVVM9at7IGw74vX2UOvCGN75ame+fMMP6VJDUAAOBpyliSiIPhUalfEqzqLn0s/nfdKTxFh2l1rBYiAITcAINRigKsBG4meoRtx57wJE9t537e9ruMfDBx2bO/vy4WEkCeb8GtmFChRyUEGjqIkAJmLt67ovXHrnn3v/rsc4069ZH3jly/qKv90/yOHyahQcDMUQhdQaCOPAzmer7Bh1w7KlH7DqtocdVqz3On1iTT/QWrBYmYtBKQXGNkHBwycFzy2Y/cPtFRx++y5bzflzu8Ctfvuq5kZOPjpUaKRgOANQHUn+yvoTS+tQTvgjACQOjBFrkZm/TttlHB+6+/fPnH953/C/Ztu/Zw296/cMlB8QMpEIq61dEMwY3zMG0ohDSANMCnHoQOQ3PBJguga0zyGsBsDIwZKBEHmAUmpTChA+D+/BYHIYyQUUBghEYnCF0PdgRC64XQVUT9dLcl686uCH7+u9f9k6Iooz9cKEopcB3QoL1rGD9Na6+ZdRNvuIdpSEQMED7BiQPoWkIDQnIeqEipgvDyCMS5TBtC4ZJYUUYtMFALAuxWAwGJzAtBtuS4HYIQXxog8KneSgzC8YYmFdWP5hcwGQs9eqUGVetDGRfsACGtkG1BtMUAbXBeAKgLmRuZfXNF5x19h/po9vO6HMq/Gy1KyhsWgKXhPBJgKikCGm9O1lbE/b87Jv8ETTRqCOIB84CSGgIHYMRBrBshiC3pvqYfl0e+b0CAgC3n933FMcKZ+dEASZikCoGQRRACELmIErS8Ljd8ZKH3r/rp2UP7tHyGRX41dqi8DwbjplAgeQhqYJJQjBuph4bO7nB1zHc8fTU3itXrGmiuQbVJoim8LQEIVHwwEMgVqNlm+aLfyogAPDstQcOuevKQwa2KHNGe9n0/HzgghgchBlgxARVtH7dmCngsRwsRgF4CGUBipd2nvmJd+IF175+b9v9Lnv9lDuePGfmoiXrePsPvjqz+1szPt7L5iylpQMuORgALQQcFgVTBFSFEDKAhAUjZiJumohYBVCbI+7YiJsFREyOWCSJMsNBjAUwDA5FHbAgC4ZaMIuCKo2CD8TtOEiQB7cz+HRx7bZXPTz5kIbsb3b11Vf/rSJy05NvnyylbP4jUQG0BqUMvu/jfz3aP73TNpv/5oeLh414q/+jo987PVbSuMygAoGXh2lYIKEDbthQWkNyCmaaUCD103yaQtTLyy8ekBKWaYEqE4Gr4BgxaJ+AawOmNqBY+IPd3+8JWXe9hAeTRKCohFubqT7xgF3vG3R0jz+0pb9q8+bpr5cvcz7+NNuKWPlGpgIoNSFCD+S7rOM/CPBPMKSENBVcD2hWYn085bELTv4jbbdoUioLYZCeMnPRttxxygyZR8SIICdyoLCgCIPDIli5fDFcX9X26d7uB4Hq1bX9Z3eMmHqsErlKywHybh4WOCxFEAoBnohgcXVttLQ8Ma/71q0WNdjM3IV33ZcJoztaNoeUPhQRsBCBVHUQhgntYsEJ+2z3wO7dOnyyvvLdtt5s8ZmH93muXdumE75atKps5ddLaChlYxozEFghFJHQOY14kEDW0AC1wMFBpA/b9GEnzLK1WdV25off7PHSqI/2mDF33hZlTZKfbNGsonDQWcMfXuvGdjTiNkK2CoLYUJCgVEBqgeC71yaHAzQswCf8V8dnqC0Qw4AnJQh1EIuUw3c1FNEgBmAxDVAKN6iPTVmmXTbnk1nNd9l5+xEtymPyX+GJ/KanQX4zgx5mfrWGD37glZtYaYuU77uADmEYDEoGcEQWNL8WUSrhQIMKASYVEoQirhVixPjVw4xqhDKAQgjHIYAKYHITUhUgzZXr5MdY301MOKl3/QVBi1JnycNXHfGnMk89dvVJN7cs00vyvkSUGdBCgpnGOu3/dJOZ1hpRBJBWCbx8tvqa0w/8U4G1607d77mtW5V+7AY+CBSCQg6mYYNIgVAZMEIBw7Hb3zXizYumzV3urDPVvW+PR/xC7TzGTUQ4haVCWJYFTRlCGcJsaqeG3PPC0Hfnrm2QFJD7XXD7DUuzaMUiMYTCA6NW/TVADrbpQGoTLUr4khvOOmTEb9V1SK/tFswacdGRzw6/YN9t27UaUViRrUadATtwEItFETghbOnDUApCUYSUwedAAAFGLcSilfCceNfR7y2/ZP/Tnxzf7H+D31nhZisjCRe+n4aSSQAahvldNj/DghNNIFQESnNwZn+3ke+XD5szUOkjSkNEpA9Vl0WcGEgaDFRkQKWHjJsDj5SAUxuAwuo60WPYAyMHN9Q9/E8QEfZ9HGR9oiK0+s3g79W3PXmT61kdo9EoKEKEAqDMAXSIQiIC37GR9kOk1+aQzUgUCho1OQ+12Qxq6n79yK3xUUhL5DM+3HwG2bo0XFeDOeUoML6OvT/9pAAhBAaxIUgAry5TfeFJ+27QzspLT+95NVxeXRASnCr4Uq7jBf20/wghUETAqwuxR69tXj1+v66z/mzb1523/wXUr1sQsiiEqQBhgLPvzpkpWJEo8jTS6Yo7n7r1x+XuGbTPfa2aVSyuyxPYZgyel0NeehCMw5QEtsqgJuDdj7h0+CszqleZG9I/5940euCr46sPIobZnpgBFFFQkkEFFK7KQZESqJqa6stO3+MPZdHfv0e7hR88OejosfeeussuHRrdlytkq2uyHgpSwFYaXEsQHUASBU0MQFIw6SJC0wDysJMWfI5OrtI9LR7rqPICSR6BrSkcX4D6BCaLgQiC7LfLoNM1yNemUZvxUZvO/uqRztai4PrI5X1k8xlkvTpkvBxWrs1BWhUQ0gRnEqGfR6iBECFiFY0xZuJHAx4aO7v7vyKwmux9+YehEF0opf9/A2gNg5tI19XhqjP2PvSaE/v9Yhaoe0d9tPPZ19z9cKRx66ogn0bEMZDxAMuyQGQOvlEC6q6Z13mLylm7du0yMZvOxikzFDWI8oO8bRn41cxWWhgUxIQXuo5jq7yCQd95/6ve875eeZCOm3CExPqyZ33vGTBiIe3VotfWre97+8FzT9/Q/trj1AfuGP/J1+dEmQ/fsMCE/MED+rF4ff93xgmUF8wZ+8AFu+yydeUGpVU46Zonzn9k3OenccdIRYSG0AHAGYj2EboasbIS1CxZXH3zBcecfeHxvX4Iho9+f2HrA8+48U0abVoVT9hwc2tBCAM0gyUovLgNN5tFC4tMuuOyE045cPeq6j9q24AL7rvp1bfmHlDaqHkqlFloJhFqCq5smEQBUY665aurj+zf6bERNwy8fkP6YdwHs5re8vi7l034YE1/ZnipqB0FhUYY+KBMAVQjFAqh1IhQCskFBJGg2oR2ORJOEl6YhQ8PUdgIqAFCKRLcn3xY/+4j4pZO5wLEsspKWiT7q+NTSUYB/l0YwAdlUklq0kXLvTZvTvpwn2hJ0yqKlRASyIcGrCjAfQnqEZQ3jY1bNHrwHpu8iCR2u+wjIWVnSul3mbXId5naDaTr6nD+cbuddOvZBzzyS+Vb73fNW2trvL5hGMAyBSRhEIgCmsLkHsI0QbtKPDf71RsPbyibX522sNVRZ173Ckm26iy1v14P6nsR8bN5OBFj1sh7T+3bu8MWtRva9rT5C53dT7p/hslJR9+koIFe58NKP46NKKXgufnqi0/oc80Np+0/oiHOvdk+V7yzYi16RlgGhJaAEBc+fBjKgUKIkMZQqQuTlrxzS58fl7t/1Ds9Th/20hOalKZKeQCKAGliwhAMmodQWsAmDgp1q+b37tn2rTOP3f+2fbqklvzmNPTdrx326IsTT1vhoZI7PGVBwVQWhOYImALREjHKsSYdVnfbITH1/QcvP76hxsGUmbPiV414d9j778/fOXCtDqVNm8NTOfiyANM0IUKNuKmQczUMOwnfzcPgElqG0ITBcOJQASBViDC/snroeUedf+mRu77WUPb1PfTKuyZUi7PKSkJIKBRgQSkFR4WwWAxra/PV5xzR9eY7Ljz0oU06sHr94xNO0Vo3+3EukfoAJYPvedhx281f7det/Xrzmo6dOqvy3hemnWdyu8w2OAIoCAAR7iD0XFBGYASZWffecuXxW1ZGsg1lc1XL0vSnX66u+HT+it7Upj+dbVrnplZhdt6ZB/e57eR9d5zREG23rCgVnh/UTp7xaQdiGBUMP2//e69OSoltW8fefOGmMxrsIkcjdvX4idO6RUrLGru+BJQCYyYMg0EIiWg0gVWrV0nH5gt37rzll9+X275962+i0cgXk99+pwthVgW3HChCQSMS8DxEDAZPB9B2vGLholz3US9P3n3k+Jk7T/l44VbvfrKk6uMvVzd7b+7XbcdMndftqddn7nnjo68OvOT2py9++/3P+oVW6fY0YpdRQ4JRQGkOITUMk8PgQL7u23m7bN3i7Xcfu+zkhhy7mzWvDI7r3/31Pj13fKzgZoLZn35SGYSyUSQSBwklTAqkc0DEcSC8HIhWYNwGM2yEIoTWPgJiggdr5+3Vrc0rwy8+8t6GtK+kUdm8FyfM2NMwoo2yuTwS0SiEKwHNoIgEMY2yufM+r7z0pL0f3JB2/vbFZt/n2fylVHW2/csp7j5f8FUHQKY4BULN4FMLlCho4SJhMqzJZaqvPKnnzXt2KV/e0Hafcua+d74xY8g+IeJd15f05vs8olu2bfPFTecNeLIh27729L2eGzXlk0M+W7y2PbfZOq8vP/6zlHLB5acf1KDLnU/br8fUMW9NHjtmZk2HWIkDLSgQKvjahUYEys0DJlKfLXM7Alhnqfegw3qN27xxyb6n3PL8U2tXBY3Kok6q4BpgREOCIgTAGEEkqoBIWcePVquOH309B9AKjGhoLaEUBZgBODGANUe0QkOpHLQoADKOQDNoeHCiBG4+i6DgVh9yUP+Xn7/i4MEbawz3aNfI7XHD8UPOPm73YcMfm3jeaxPnHpCVuosdM5AoicPLrkbcodCKouAF4FYENjFg6BBZrdDUwfJX7rqowT8puu9u2y7ZZcd2kyZP+6oqXtYYbnotEkYFfA54uhaOIeCGTuTF8Z9UHdx3mz/9uda/PbAat5y0SQNomQfRBJQaIAwIQh+URhZsVUF/cU1D8yblS0BUteYOtKKISAUrlNCKI+O76Nhxs4+HnHr4CxvD7p6tG2fPG9D+xnxtodq2QlBWQOBraMVhmD4M7kF4esFdp/Y6cWO0f9U5B1xqy8JcYnoIXQFLMEBkQWgIomzQnI/D+2z95P49t13Y0G0PPvuES0uNupmSRBCqEIYTgmsfYAF80wakqu68eWzm+soe2Hvb6jVv3LDjUXu0fSxXt2xe4Kbrp9IJR5xyGEJDag5FCEwSoiQWQ2kiiWSiFMlEOUpLSpGIRxFnClFVAAlc6JCAUQeGCTDDR+hlkc/Uzm/euHTca8MO321jCsiP2bGqpTvixuOuf/Ops3Y5eb/OZ0e9/LTMqtpq00pA0SgKIcAMCkpCKChowwBPL5o39LLTL9hYNl11wuGXl+iamWHggUUr4KksmLcKpZRBSQ4r9L1kZeOaTfp15qOFK1rMmfN5MyNaXsETERTCALYRR5jPojwhPn/yhjNv/KWyHdq2Wfv8mOl9li1byXmypMznGjAZEPiIyey0Z+65Yf+WJURsLNt37dH18xGTP+i/fEndlslkc3BDQ0NChTbyOa+6X/9ur195/B4bJet2h5aN1s79fHHF7C/qWibLSssCVgCNWyiIGLiKwyGrpt112/EDm8cSDf6h5+aNYurbrCBT35nSPpZsXqZCA1KaiNpxFOpWo1WpMeu5YWcO+bU6Dujd5d2uu3QdufTbTLRm7WpZt/xbzwcpM836D7RDC0RtAz4RkFT9cCimoZkGGEA4UL/ShyMIGfy0DwT+/O1aNxp3wVH/u/7ZG064tu0WrTN/9Zhu2ahE7L1rp5kXDdzz0YVrV0YXfPppebrObxYrawpfhiAMsKiFuiU11ReevOfQ8w7fdcLGsmXzpo5XWwiDd96f3xFWogwWAY9ayPkUbo5V99qmdMLVA/ccuUkHVgHg4HOHXTt26lf75AukEywLpvbmddjcnnvzdaefuXuHtr/5rZOuB1301KdLC51cSTtSohekGllf3D/0jGN367x57ca2febX3/Izzhv+2Idf1nWHRVLgHNSVCzq3ic/6cNTQIzd2+636XTlx+bJ8c2lYVaA+YADc9effeeXJJ59+UKepG7PtvU4bOuyNdxfuF4mWp7Ti8AtrFlRtFvn8sbsuP6RbquIPJce9/tE39pnwwcL+c79a1ak2UyhXgajSIgAx6pesA/pHs1CyficwISBUzE9EWbp18+TCHbdpM3Xv3t1G9ev613686beYUb3KvPm+UYNfeevjAw07WeUXaqubJOSKww/p9+Tt5x380F9hw1k3Pn3aIyMnn+aGRkfCObgozN++41Yzpj15/gYHmv8RIgIA78xdlJw1b0lXIQRvu1mj+fvt0mnxHyk/buYXTb9ZtrRVRaPyVQP+YNmGYPjLM3pOnvVV32w2G992i/IPbzz3oBF/VduXDht9VPWiTJVLM3azSnx74J67PrPHtlv/JTfS1Q+OOeiD2Z93Y5ajunduN/WyY3tv8OzChBnV5fO+XtNp8cqaLdZk6yry+XzUdV0bSsKyrCAZj6bLy8tXlyZLatpv3mjuQTu2q8YmwDvzlyQ/nlvdJcrt3EkH9Jj5V7f/1qzqihlzFu/sFVyn61atpg3o3TD3yT9GRIoUKbJpQotdUKRIkaKIFClSpCgiRYoUKYpIkSJFiiJSpEiRIkURKVKkSFFEihQpUhSRIkWK/Cf4vwEAzXIYYpkNqVoAAAAASUVORK5CYII='
        if (checkErrDate($scope.dateOfInvoice)) {
            if ($scope.hoursToInvoicedGridOptions.data.length > 0) {
                var toInvoices = $scope.hoursToInvoicedGridOptions.data;
                var allHours = 0;
                for (var i = 0; i < toInvoices.length; i++) {
                    allHours += toInvoices[i].hours;
                }

                var docDefinition = {
                    content: [
                        {
                            alignment: 'justify',
                            columns: [
                                {
                                    // text: 'Tamura'
                                    image:tamuraLogoUrl
                                },
                                {
                                    text: $scope.timeSheetInvoice[0].text + '    ' + $scope.timeSheetInvoice[1].text + '\n' +
                                    $scope.timeSheetInvoice[1].text + '\t   ' + $scope.invoiceNumber
                                    + '\n' + $scope.timeSheetInvoice[2].text + '      ' + $filter('date')($scope.dateOfInvoice, "dd/MM/yyyy")

                                }
                            ],
                            margin: [0, 0, 0, 40]
                        },
                        {
                            table: {
                                body: [
                                    [$scope.timeSheetInvoice[4].text, '       ', '       ', '       ', '       '],
                                    [$scope.timeSheetInvoice[5].text, '       ', '       ', '       ', '       '],
                                    [$scope.timeSheetInvoice[6].text + '       ', '       ', '       ', '       ', '       '],
                                    [$scope.timeSheetInvoice[6].text + '       ', $scope.timeSheetInvoice[7].text,
                                        $scope.timeSheetInvoice[8].text, $scope.timeSheetInvoice[9].text, $scope.timeSheetInvoice[10].text]
                                ]
                            },
                            layout: 'lightHorizontalLines',
                            margin: [0, 0, 0, 40]
                        },
                        buildTableBody(toInvoices),
                        {
                            text: $scope.timeSheetInvoice[14].text + ': ' + allHours,
                            alignment: 'right'
                        }
                    ],
                    styles: {
                        table: {
                            width: '100%'
                        }
                    }
                };
                pdfMake.createPdf(docDefinition).open();
                // pdfMake.createPdf(docDefinition).print();
                // }

            }
            else {
                alert("One TimeSheet must be selected for invoice.");
            }
        }

    };

    getSisterCompanies();
});
