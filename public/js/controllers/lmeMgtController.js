tamuraApp.controller("LmeMgtCtrl", function ($scope, $http, $rootScope) {
    
    $rootScope.activeTopMenu = "lme-mgt";

    $scope.lme = {
        id: null,
        cu : null,
        al : null
    };

    function getLmes()
    {
        $http({
            method :'GET',
            url : 'api/lme/getAll'
        }).then(function (successResponse){
            $scope.lmes = successResponse.data;
            $scope.lme.id = $scope.lmes[0].id;
            $scope.lme.cu = $scope.lmes[0].cu;
            $scope.lme.al = $scope.lmes[0].al;
            console.log($scope.lmes);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    $scope.saveLme = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/lme/update',
            data : {
                id :  $scope.lme.id,
                cu : $scope.lme.cu,
                al : $scope.lme.al
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            Flash.clear();
            Flash.create('success', "Saved", 0, {}, true);
            getLmes();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
            Flash.clear();
            Flash.create('danger', "Error in saving.", 0, {}, true);
        });
    };

    getLmes();
    
});
