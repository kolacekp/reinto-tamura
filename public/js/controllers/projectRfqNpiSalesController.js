tamuraApp.controller("ProjectRfqNpiSalesCtrl", function ($scope, $http, $rootScope, $translate, $mdDialog, Flash) {

    $rootScope.activeTopMenu = "project-rfq-npi-sales";

    // initializations
    $scope.projects = [];
    $scope.project = null;
    $scope.projectExport = [];
    $scope.projectExportHeader = ['id','name','status','market_id','standards','description','remarks','estimated_value','estimated_value_currency_id','item_to_design','customer_id','end_customer','customer_purchase_manager_id','manager_id','business_location_iso2','creation_date','last_modified_on','last_modified_on','last_modified_by','life','spot_order'];
    $scope.rfq = null;
    $scope.rfqExport = [];
    $scope.rfqExportHeader = ['id', 'father', 'is_std_item', 'project_id', 'status', 'folder', 'priority', 'design_office_id', 'customer_engineer_id', 'tamura_elec_engineer_id', 'tamura_mec_engineer_id', 'product_id', 'pn_customer', 'pn_desc', 'pn_category_id', 'pn_format', 'pn_new_dev', 'pn_new_biz', 'com_currency_id', 'com_estimated_price', 'com_target_price', 'com_annual_qty', 'com_annual_value', 'com_confidence_level', 'com_market_share', 'com_incoterms', 'com_delivery_location', 'com_teu_group', 'qualif_quotation_required_date', 'qualif_sample_date', 'qualif_mass_prod_date', 'comments', 'duplicate', 'duplicate_from', 'creation_date', 'last_modified_on', 'last_modified_by', 'status_10', 'status_10_who', 'status_10_when', 'status_20', 'status_20_who', 'status_20_when', 'status_30', 'status_30_who', 'status_30_when', 'status_dead', 'status_dead_who', 'status_dead_when', 'status_dead_reason_id', 'status_rejected', 'status_rejected_who', 'status_rejected_when', 'eng_action_costing_planned_date', 'eng_action_costing_complete', 'eng_action_costing_complete_who', 'eng_action_costing_complete_when', 'eng_action_design_required', 'eng_action_design_required_who', 'eng_action_design_required_when', 'eng_action_design_complete_planned_date', 'eng_action_design_complete', 'eng_action_design_complete_who', 'eng_action_design_complete_when', 'eng_action_mass_prod_auth', 'eng_action_mass_prod_auth_who', 'eng_action_mass_prod_auth_when', 'product_type', 'product_length', 'product_width', 'product_height', 'product_weight', 'product_field1', 'product_field2', 'product_field3', 'product_field4', 'product_field5', 'product_field6', 'product_other_materials', 'product_other_materials_value', 'product_handling_scrap', 'product_handling_scrap_value', 'mat_currency_id', 'mat_total_value', 'pallet_length', 'pallet_width', 'pallet_space', 'pallet_layers', 'pallet_quantity', 'labour1', 'labour2', 'labour3', 'labour_time1', 'labour_time2', 'labour_time3', 'labour_time4'];
    $scope.npiExport = [];
    $scope.npiExportHeader = ['id', 'rfq_id', 'tamura_entity_id_factory', 'tamura_entity_id_sales_office', 'currency_id', 'rfq_material_cost_value', 'rfq_profit_factory', 'rfq_profit_value_factory', 'rfq_labour', 'rfq_labour_time', 'rfq_labour_value', 'rfq_freight', 'rfq_freight_value', 'rfq_admin', 'rfq_admin_value', 'rfq_selling_price_factory', 'rfq_profit_sales_office', 'rfq_profit_value_sales_office', 'rfq_selling_price_sales_office', 'material_cost_value', 'profit_factory', 'profit_value_factory', 'labour', 'labour_time', 'labour_value', 'freight', 'freight_value', 'admin', 'admin_value', 'selling_price_factory', 'profit_sales_office', 'profit_value_sales_office', 'selling_price_sales_office', 'sample_quantity_required', 'sample_delivery_date', 'sample_chargeable', 'sample_price', 'sample_incoterms_id', 'sample_manufactured_purchased', 'sample_lead_time', 'sample_teu_pn', 'sample_customer_address', 'minimum_order_quantity', 'minimum_package_quantity', 'comment', 'creation_date', 'created_by', 'last_modified_on', 'last_modified_by'];
    $scope.saleExport = [];
    $scope.saleExportHeader = ['id', 'npi_id', 'valid', 'valid_last_update_date', 'currency_id', 'sales_manager_id', 'tamura_entity_id_sales_office', 'buy_price_engineering', 'buy_price_factory', 'buy_price', 'tamura_entity_fob_charge', 'tamura_entity_fob_charge_value', 'tamura_entity_freight', 'tamura_entity_freight_value', 'cif_cost', 'tamura_entity_terminal_handling', 'tamura_entity_terminal_handling_value', 'tamura_entity_delivery_order', 'tamura_entity_delivery_order_value', 'tamura_entity_customs_clearance', 'tamura_entity_customs_clearance_value', 'advance_fees', 'advance_fees_value', 'lfr', 'lfr_value', 'tamura_entity_haulage_to_warehouse', 'tamura_entity_haulage_to_warehouse_value', 'total_inland_charges_value', 'unloading', 'unloading_value', 'order_picking', 'order_picking_value', 'storage', 'storage_value', 'admin', 'admin_value', 'total_hub_costs', 'total_oversea_costs', 'total_oversea_costs_value', 'total_oversea_cost_flag', 'import_duty', 'import_duty_value', 'delivery_to_customer', 'delivery_to_customer_value', 'cost_price', 'cost_price_percentage', 'gand_a', 'gand_a_value', 'total_cost', 'total_cost_percentage', 'profit', 'profit_value', 'selling_price', 'comment', 'special_req', 'incoterms_id', 'creation_date', 'last_update_date', 'created_by', 'pallet_qty', 'customer_id', 'customer_payment_term_nr', 'customer_payment_term_type', 'purchaser_id', 'fcl_lcl', 'lme_cu', 'lme_cu_at_creation', 'lme_al', 'lme_al_at_creation'];
    $scope.allProjectsExport = [];
    $scope.allRFQsExport = [];
    $scope.allNPIsExport = [];
    $scope.allSalesExport = [];
    $scope.projectManagers = [];
    $scope.projectManagersAll = [];
    $scope.countries = [];
    $scope.countriesAll = [];
    $scope.deliveryLocations = [];
    $scope.deliveryLocationsAll = [];
    $scope.currencies = [];
    $scope.currenciesAll = [];
    $scope.rfqCurrencies = [];
    $scope.rfqCurrenciesAll = [];
    $scope.customerEngineers = [];
    $scope.customerEngineersAll = [];
    $scope.designOffices = [];
    $scope.designOfficesAll = [];
    $scope.customers = [];
    $scope.customersAll = [];
    $scope.purchasers = [];
    $scope.purchasersAll = [];
    $scope.salesManager = [];
    $scope.salesManagerAll = [];
    $scope.marketParents = [];
    $scope.marketParentsAll = [];
    $scope.marketChildren = [];
    $scope.marketChildrenAll = [];
    $scope.currenciesShortNames = [];
    $scope.incotermsFormated = [];

    $scope.projectExists = false;
    $scope.rfqExists = false;

    const PERCENT = 100;

    var PROJECT_INIT = {
        'id' : null,
        'business_location': null,
        'creation_date': null,
        'customer': null,
        'customer_id': null,
        'customer_purchase_manager_id': null,
        'description': null,
        'end_customer': null,
        'estimated_value': null,
        'estimated_value_currency': null,
        'estimated_value_currency_id': null,
        'item_to_design': null,
        'last_modified_by': null,
        'last_modified_on': null,
        'life': null,
        'manager': null,
        'manager_id': null,
        'market': null,
        'marketParent': null,
        'market_id': null,
        'name': null,
        'purchase_manager': null,
        'remarks': null,
        'rfqs': [],
        'spot_order': null,
        'standards': null,
        'status': "INIT"
    };

    var RFQ_INIT = {
        category: null,
        com_annual_qty: null,
        com_annual_value: null,
        com_confidence_level: null,
        com_currency: null,
        com_currency_id: null,
        com_delivery_location: null,
        com_estimated_price: null,
        com_incoterms: null,
        com_market_share: null,
        com_target_price: null,
        com_teu_group: null,
        comments: null,
        creation_date: null,
        customer_engineer: null,
        customer_engineer_id: null,
        design_office: null,
        design_office_id: null,
        duplicate: null,
        duplicate_from: null,
        electrical_engineer: null,
        eng_action_costing_complete: null,
        eng_action_costing_complete_when: null,
        eng_action_costing_complete_who: null,
        eng_action_costing_planned_date: null,
        eng_action_design_complete: null,
        eng_action_design_complete_planned_date: null,
        eng_action_design_complete_when: null,
        eng_action_design_complete_who: null,
        eng_action_design_required: null,
        eng_action_design_required_when: null,
        eng_action_design_required_who: null,
        eng_action_mass_prod_auth: null,
        eng_action_mass_prod_auth_when: null,
        eng_action_mass_prod_auth_who: null,
        estimated_factory_cost: null,
        father: null,
        folder: null,
        id: null,
        is_std_item: null,
        labour1: null,
        labour2: null,
        labour2Min: null,
        labour3: null,
        labour3Min: null,
        labourMax: null,
        labour_time1: null,
        labour_time2: null,
        labour_time3: null,
        labour_time4: null,
        last_modified_by: null,
        last_modified_on: null,
        mat_currency_id: null,
        mat_currency: null,
        mat_total_value: null,
        materials: null,
        mechanical_engineer: null,
        npis: null,
        pallet_layers: null,
        pallet_length: null,
        pallet_quantity: null,
        pallet_space: null,
        pallet_width: null,
        pn_category_id: null,
        pn_customer: null,
        pn_desc: null,
        pn_format: null,
        pn_new_biz: null,
        pn_new_dev: null,
        priority: null,
        product: null,
        product_field1: null,
        product_field2: null,
        product_field3: null,
        product_field4: null,
        product_field5: null,
        product_field6: null,
        product_handling_scrap: null,
        product_handling_scrap_value: null,
        product_height: null,
        product_id: null,
        product_length: null,
        product_other_materials: null,
        product_other_materials_value: null,
        product_type: null,
        product_weight: null,
        product_width: null,
        project: null,
        project_id: null,
        qualif_mass_prod_date: null,
        qualif_quotation_required_date: null,
        qualif_sample_date: null,
        status: "INIT",
        status_10: null,
        status_10_when: null,
        status_10_who: null,
        status_20: null,
        status_20_when: null,
        status_20_who: null,
        status_30: null,
        status_30_when: null,
        status_30_who: null,
        status_dead: null,
        status_dead_reason_id: null,
        status_dead_when: null,
        status_dead_who: null,
        status_rejected: null,
        status_rejected_when: null,
        status_rejected_who: null,
        tamuraEntityLabourValueFactory1: null,
        tamuraEntityLabourValueFactory2: null,
        tamuraEntityLabourValueFactory3: null,
        tamura_elec_engineer_id: null,
        tamura_mec_engineer_id: null,
        totalMaterialsValue: null
    };

    $scope.projectViewed = false;
    $scope.rfqViewed = false;
    $scope.npiViewed = false;
    $scope.saleViewed = false;

    $scope.projectCustomerSelected = false;
    $scope.projectMarketParentSelected = false;

    $scope.rfqGeneralTab = true;
    $scope.rfqTechnicalTab = false;
    $scope.rfqCostingTab = false;
    $scope.rfqStatusLogTab = false;

    $scope.rfqDeadReasons = [];
    $scope.rfqDeadReasonsAll = [];
    $scope.rfqDeadReasonSearchText = "";

    $scope.rfqStatusLogs = [];

    // search texts
    $scope.projectManagerSearchText = "";
    $scope.businessLocationSearchText = "";
    $scope.customerEngineerSearchText = "";
    $scope.designOfficeSearchText = "";
    $scope.estimatedValueCurrencySearchText = "";
    $scope.rfqComCurrencySearchText = "";
    $scope.rfqMaterialCurrencySearchText = "";
    $scope.rfqComDeliveryLocationSearchText = "";
    $scope.projectCustomerSearchText = "";
    $scope.marketParentSearchText = "";
    $scope.marketChildSearchText = "";

    $scope.rfqByNpi = null;
    $scope.npiProject = null;
    $scope.generateSaleShow = false;
    $scope.lclFcl = null;
    $scope.estimatedEXW1OldCurrency = null;
    $scope.estimatedEXW2OldCurrency = null;
    $scope.estimatedEXW3OldCurrency = null;
    $scope.totalUpdated = false;

    $scope.generateSale = {
        "lcl_fcl": null,
        "customer": null,
        "saleOffice": null,
        "customer_payment_term_nr": null,
        "customer_payment_term_type": null,
        "purchaseManager": null,
        "salesManager": null,
        "pallet_qty": 5,
        "currency": null,
        "incoterms": null,
        "valid": 1,
        "lmeCu": '',
        "lmeAl": '',
        "buyPriceEngineering": 0,
        "buyPriceFactory": 0,
        "buyPrice": 0,
        "overseaCost": 0,
        "overseaCostValue": 0,
        "importDuty" : 0,
        "importDutyValue" : 0,
        "deliveryToCustomer" : 0,
        "deliveryToCustomerValue" : 0,
        "costPrice" : 0,
        "costPriceValue" : 0,
        "generalAdministration" : 0,
        "generalAdministrationValue" : 0,
        "totalCost" : 0,
        "totalCostValue" : 0,
        "profit" : 0,
        "profitValue" : 0,
        "sellingPrice" : 0,
        "comment" : null,
        "specialReq" : null,
        "creationDate" :null,
        "updatedDate" : null,
        "createdBy" : null
    };
    $scope.salesOffices = [];
    $scope.allCustomers = [];
    $scope.allCustomersNPI = [];
    var CUSTOMER_TYPE_GROUP = "group";
    $scope.paymentTermNr = ["30", "45", "60", "90", "120", "150"];
    $scope.paymentTermType = ["NET", "EOM"];

    $scope.categories = [];

    // rfq technical - product
    $scope.productParentSearchText = "";
    $scope.productChildOneSearchText = "";
    $scope.productChildTwoSearchText = "";
    $scope.productProductSearchText = "";

    $scope.productParentSelected = ($scope.rfq !== null && $scope.rfq.productParent !== null);
    $scope.productChildOneSelected = ($scope.rfq !== null && $scope.rfq.productChildOne !== null);
    $scope.productChildTwoSelected = ($scope.rfq !== null && $scope.rfq.productChildTwo !== null);

    $scope.productProductSelected = ($scope.rfq !== null && $scope.rfq.product !== null);

    $scope.productChildOneShown = true;
    $scope.productChildTwoShown = true;
    $scope.productProductShown = true;

    $scope.PNFormats = [];
    $scope.priorities = [];

    $scope.allIncoterms = null;
    $scope.materialCost = null;
    $scope.npiElectEngineer = null;

    $scope.lmeCu = null;
    $scope.lmeAl = null;
    var GBPCurrency = null;

    $scope.specifications = [];
    $scope.specificationsStatusText = "";

    var actualCurrency = null;
    $scope.viewedDetails = false;

    $translate(["data-sheet.pn-formats.linear", "data-sheet.pn-formats.toroid"]).then(function (translations) {
        $scope.PNFormats = [
            {id: 1, value: "Linear", text: translations["data-sheet.pn-formats.linear"]},
            {id: 2, value: "Toroid", text: translations["data-sheet.pn-formats.toroid"]}
        ];
    });

    $translate(["data-sheet.priorities.high", "data-sheet.priorities.medium", "data-sheet.priorities.price-checking","data-sheet.choose-product","data-sheet.choose-market"]).then(function (translations) {
        $scope.priorities = [
            {id: 1, value: "HIGH", text: translations["data-sheet.priorities.high"]},
            {id: 2, value: "MEDIUM", text: translations["data-sheet.priorities.medium"]},
            {id: 3, value: "PRICE-CHECKING", text: translations["data-sheet.priorities.price-checking"]}
        ];
        $scope.chooseProduct = [
            {id:1 , text: translations["data-sheet.choose-product"]}
        ];
        $scope.chooseMarket = [
            {id : 1, text: translations["data-sheet.choose-market"]}
        ]
    });

    $scope.productChildOneViewed = false;
    $scope.productChildTwoViewed = false;
    $scope.productProductViewed = false;

    $scope.rfqMaterialsGridOptions = {};
    $scope.rfqMaterialsGridApi = {};

    $scope.projectEditing = false;
    $scope.projectAdding = false;

    var oldCurrencyId = 0;
    var newCurrencyId = 0;
    var DAT = "DAT - Delivered At Terminal";
    var DAP = "DAP - Delivered At Place";
    var DPP = "DDP - Delivered Duty Paid"

    $translate(
        [
            "data-sheet.type",
            "data-sheet.part-no",
            "data-sheet.description",
            "data-sheet.qty",
            "data-sheet.unitary-price",
            "data-sheet.total"
        ]
    ).then(function (translations) {

        $scope.rfqMaterialsGridOptions = {
            showGridFooter: true,
            data: [],
            columnDefs: [
                {name: 'id', visible: false},
                {name: 'mat_id', visible: false},
                {field: 'type', displayName: translations["data-sheet.type"], cellEditableCondition: false},
                {field: 'part_no', displayName: translations["data-sheet.part-no"], cellEditableCondition: false},
                {
                    field: 'description',
                    displayName: translations["data-sheet.description"],
                    cellEditableCondition: false
                },
                {field: 'qty', displayName: translations["data-sheet.qty"], cellEditableCondition: false},
                {
                    field: 'unitary_price',
                    displayName: translations["data-sheet.unitary-price"],
                    cellEditableCondition: false
                },
                {field: 'total_price', displayName: translations["data-sheet.total"], cellEditableCondition: false}
            ],
            onRegisterApi: function (gridApi) {
                //set gridApi on scope
                $scope.rfqMaterialsGridApi = gridApi;
            }
        };

    });

    var pdfSampleRequestFormText = null;
    $translate([
            "data-sheet.enquiry-no",
            "data-sheet.request-date",
            "data-sheet.sales-person-responsible",
            "data-sheet.customer-name",
            "data-sheet.customer-part-no",
            "data-sheet.description",
            "data-sheet.quality-required",
            "data-sheet.contact-name",
            "data-sheet.phone-no",
            "data-sheet.email",
            "data-sheet.address",
            "data-sheet.chargeable",
            "data-sheet.delivery-terms",
            "data-sheet.customer-spec",
            "data-sheet.specification",
            "data-sheet.manufactured-purchased",
            "data-sheet.supplier",
            "data-sheet.buying-price",
            "data-sheet.lead-time",
            "data-sheet.tamid-reference",
            "data-sheet.delivery-date",
            "data-sheet.mobile-no",
            "data-sheet.currency-price",
            "data-sheet.npi-tamura-header",
            "data-sheet.manufactured",
            "data-sheet.purchased",
            "data-sheet.sales-department",
            "data-sheet.engineering-department",
            "data-sheet.logistics-department",
            "data-sheet.customer-spec-from-sales",
            "data-sheet.engineer-responsibile",
            "data-sheet.eng-promise-quality",
            "data-sheet.eng-promise-date",
            "data-sheet.teu-top-level-no",
            "data-sheet.teu-contacts-no",
            "data-sheet.teu-purchase-order-no",
            "data-sheet.comments"
        ]
    ).then(function (translations) {

        pdfSampleRequestFormText = {
            "enquiryNo": translations['data-sheet.enquiry-no'],
            "requestDate": translations['data-sheet.request-date'],
            "salesPersonResponsible": translations["data-sheet.sales-person-responsible"],
            "customerName": translations["data-sheet.customer-name"],
            "customerPartNo": translations["data-sheet.customer-part-no"],
            "description": translations ["data-sheet.description"],
            "quantityRequired": translations["data-sheet.quality-required"],
            "contactName": translations ["data-sheet.contact-name"],
            "phoneNo": translations["data-sheet.phone-no"],
            "email": translations["data-sheet.email"],
            "address": translations["data-sheet.address"],
            "chargeable": translations["data-sheet.chargeable"],
            "deliveryTerms": translations["data-sheet.delivery-terms"],
            "customerSpec": translations["data-sheet.customer-spec"],
            "specification": translations ["data-sheet.specification"],
            "manufacturedPurchased": translations["data-sheet.manufactured-purchased"],
            "supplier": translations["data-sheet.supplier"],
            "buyingPrice": translations["data-sheet.buying-price"],
            "leadTime": translations["data-sheet.lead-time"],
            "tamidReference": translations["data-sheet.tamid-reference"],
            "deliveryDate": translations["data-sheet.delivery-date"],
            "mobileNo": translations["data-sheet.mobile-no"],
            "currencyPrice": translations["data-sheet.currency-price"],
            "tamuraSupplier": translations["data-sheet.npi-tamura-header"],
            "purchased": translations["data-sheet.purchased"],
            "manufactured": translations["data-sheet.manufactured"],
            "salesDepartment": translations["data-sheet.sales-department"],
            "engineeringDepartment": translations["data-sheet.engineering-department"],
            "logisticsDepartmnet": translations ["data-sheet.logistics-department"],
            "customerSpecFrom": translations["data-sheet.customer-spec-from-sales"],
            "engineerResponsible": translations["data-sheet.engineer-responsibile"],
            "engPromiseQuality": translations["data-sheet.eng-promise-quality"],
            "engPromiseDate": translations ["data-sheet.eng-promise-date"],
            "teuTopLevelNo": translations["data-sheet.teu-top-level-no"],
            "teuContactsNo": translations ["data-sheet.teu-contacts-no"],
            "teuPurchaseOrderNo": translations ["data-sheet.teu-purchase-order-no"],
            "comments": translations ["data-sheet.comments"]
        };
    });

    $scope.factories = [];
    $scope.factoriesAll = [];

    $scope.entitySearchTextChange = function (text, array, initialArray, comparedProperty) {
        $scope[array] = $scope[initialArray].filter(function (obj) {
            return obj[comparedProperty].toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    var changeDateFormats = function (project){
        // LOGS
        if ( project.last_modified_on != null) {
            var lastModifiedDateSplit1 = project.last_modified_on.split(" ");
            var lastModifiedDateSplit2 = lastModifiedDateSplit1[0].split("/");
            project.last_modified_on = lastModifiedDateSplit2[1] + "/" + lastModifiedDateSplit2[0] + "/" + lastModifiedDateSplit2[2] + " " + lastModifiedDateSplit1[1];
        }
        if ( project.creation_date != null) {
            var creationDateSplit1 = project.creation_date.split(" ");
            var creationDateSplit2 = creationDateSplit1[0].split("/");
            project.creation_date = creationDateSplit2[1] + "/" + creationDateSplit2[0] + "/" + creationDateSplit2[2] + " " + creationDateSplit1[1];
        }


    };


    $scope.viewProject = function (project) {

        $rootScope.loading = true;

        $scope.projectEditing = true;
        $scope.projectAdding = false;

        $scope.projectExists = false;

        $http({
            method: 'GET',
            url: 'api/projects/get?id=' + project.id
        }).then(function (successResponse) {

            $scope.project = successResponse.data;

            $rootScope.loading = false;
            $scope.projectViewed = true;
            $scope.rfqViewed = false;
            $scope.npiViewed = false;

            if ($scope.project.customer !== null) {
                $scope.projectCustomerSelected = true;
            }

            if ($scope.project.market !== null && ($scope.project.market.parent !== null || $scope.project.market.parentNew !== null)) {
                $scope.projectMarketParentSelected = true;
                if ($scope.project.market.parent != null)
                    $scope.project.marketParent = $scope.project.market.parent;
                if($scope.project.market.parentNew != null)
                    $scope.project.marketParent = $scope.project.market.parentNew;
                getMarketChildren();
            }
            changeDateFormats($scope.project);
            $scope.createProjectExport($scope.project);
            console.log($scope.project);
            window.scrollTo(0,0);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };

    $scope.createProjectExport = function (project) {
        $scope.projectExport = [];
        pushProjectExportToArray(project,$scope.projectExport);

    };

    var pushProjectExportToArray = function (project,exportVar) {
        var business_location_iso2;
        if(project.business_location != null)
            business_location_iso2 = project.business_location.iso2;
        else
            business_location_iso2 = null;
        exportVar.push({id: project.id,
            name: project.name,
            status: project.status,
            market_id: project.market_id,
            standards: project.standards,
            description: project.description,
            remarks: project.remarks,
            estimated_value: project.estimated_value,
            estimated_value_currency_id: project.estimated_value_currency_id,
            item_to_design: project.item_to_design,
            customer_id: project.customer_id,
            end_customer: project.end_customer,
            customer_purchase_manager_id: project.customer_purchase_manager_id,
            manager_id: project.manager_id,
            business_location_iso2: business_location_iso2,
            creation_date: project.creation_date,
            last_modified_on: project.last_modified_on,
            last_modified_by: project.last_modified_by,
            life: project.life,
            spot_order : project.spot_order
        });
    };

    $scope.prepareNewProject = function () {
        $scope.projectEditing = false;
        $scope.projectAdding = true;

        $scope.project = PROJECT_INIT;

        $scope.projectExists = false;

        $scope.projectViewed = true;
        $scope.rfqViewed = false;
        $scope.npiViewed = false;

        $scope.projectCustomerSelected = false;
        $scope.projectMarketParentSelected = false;
    };

    $scope.checkProjectName = function()
    {
        $http({
            method: 'GET',
            url: 'api/projects/checkName?name='+$scope.project.name+'&id='+$scope.project.id
        }).then(function (successResponse) {
            $scope.projectExists = successResponse.data === true;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    };

    $scope.checkRfqCustomerPN = function()
    {
        $http({
            method: 'GET',
            url: 'api/rfqs/checkCustomerPn?customerPN='+$scope.rfq.pn_customer+'&id='+$scope.rfq.id
        }).then(function (successResponse) {
            $scope.rfqExists = successResponse.data === true;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    };

    $scope.prepareNewRfqWithoutProject = function(){

        var d = new Date();
        var h = d.getHours();
        var m = d.getMinutes();

        $scope.project = PROJECT_INIT;
        $scope.project.name = "undefinedproject" + h + "-" + m;

        $scope.createNewProjectAndRfq();

    };

    $scope.prepareNewRfq = function () {
        $scope.projectEditing = false;
        $scope.projectAdding = false;

        $scope.rfq = RFQ_INIT;
        $scope.specificationsStatusText = "";

        $scope.rfqMaterialsGridOptions.data = [];

        $scope.rfqGeneralTab = true;
        $scope.rfqTechnicalTab = false;
        $scope.rfqCostingTab = false;
        $scope.rfqStatusLogTab = false;

        $scope.projectViewed = false;
        $scope.rfqViewed = true;
        $scope.npiViewed = false;

        $scope.createNewRfq();

    };

    $scope.createNewProject = function () {
        $rootScope.loading = true;
        $http({
            method: 'POST',
            url: 'api/projects/create',
            data: {
                name: $scope.project.name,
                status: $scope.project.status,
                marketId: $scope.project.market !== null ? $scope.project.market.no_entity : null,
                standards: $scope.project.standards,
                description: $scope.project.description,
                remarks: $scope.project.remarks,
                estimatedValue: $scope.project.estimated_value,
                estimatedValueCurrencyId: $scope.project.estimated_value_currency !== null ? $scope.project.estimated_value_currency.id : null,
                itemToDesign: $scope.project.item_to_design,
                customerId: $scope.project.customer !== null ? $scope.project.customer.id : null,
                endCustomer: $scope.project.end_customer,
                customerPurchaseManagerId: $scope.project.purchase_manager !== null ? $scope.project.purchase_manager.id : null,
                managerId: $scope.project.manager !== null ? $scope.project.manager.id : null,
                businessLocation: $scope.project.business_location !== null ? $scope.project.business_location.iso2 : null,
                lastModifiedBy: $rootScope.getLoggedUser().username !== null ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                life: $scope.project.life,
                spotOrder: $scope.project.spot_order,
                managerPhone : $scope.project.manager !== null ?  $scope.project.manager.phone : null,
                managerMobile : $scope.project.manager !== null ?  $scope.project.manager.mobile : null,
                purchaseManager :  $scope.project.purchase_manager
            }
        }).then(function (successResponse) {
            getData();
            $scope.project = successResponse.data;
            $rootScope.loading = false;
            $scope.viewProject($scope.project);
            Flash.clear();
            $translate(["data-sheet.project-successfully-created", {projectName : $scope.project.name}]).then(function (translations) {
                Flash.create('success', translations["data-sheet.project-successfully-created"], 0, {}, true);
            });
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.createNewProjectAndRfq = function () {
        $rootScope.loading = true;
        $http({
            method: 'POST',
            url: 'api/projects/create',
            data: {
                name: $scope.project.name,
                status: $scope.project.status,
                marketId: $scope.project.market !== null ? $scope.project.market.id : null,
                standards: $scope.project.standards,
                description: $scope.project.description,
                remarks: $scope.project.remarks,
                estimatedValue: $scope.project.estimated_value,
                estimatedValueCurrencyId: $scope.project.estimated_value_currency !== null ? $scope.project.estimated_value_currency.id : null,
                itemToDesign: $scope.project.item_to_design,
                customerId: $scope.project.customer !== null ? $scope.project.customer.id : null,
                endCustomer: $scope.project.end_customer,
                customerPurchaseManagerId: $scope.project.purchase_manager !== null ? $scope.project.purchase_manager.id : null,
                managerId: $scope.project.manager !== null ? $scope.project.manager.id : null,
                businessLocation: $scope.project.business_location !== null ? $scope.project.business_location.iso2 : null,
                lastModifiedBy: ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                life: $scope.project.life,
                spotOrder: $scope.project.spot_order
            }
        }).then(function (successResponse) {
            getData();
            $scope.project = successResponse.data;
            $rootScope.loading = false;
            Flash.clear();
            $translate("data-sheet.project-successfully-created", {projectName : $scope.project.name}).then(function (translation) {
                Flash.create('success', translation, 0, {}, true);
            });

            $scope.projectEditing = false;
            $scope.projectAdding = false;

            $scope.rfq = RFQ_INIT;
            $scope.specificationsStatusText = "";

            $scope.rfqMaterialsGridOptions.data = [];

            $scope.rfqGeneralTab = true;
            $scope.rfqTechnicalTab = false;
            $scope.rfqCostingTab = false;
            $scope.rfqStatusLogTab = false;

            $scope.projectViewed = false;
            $scope.rfqViewed = true;
            $scope.npiViewed = false;

            $scope.createNewRfq(true);

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };


    $scope.createNewSale = function (npi,rfq) {
        $rootScope.loading = true;
        console.log(npi);
        $http({
            method: 'POST',
            url: 'api/sale/createSale',
            data: {
                npi: npi,
                rfq: rfq,
                createdBy : ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name
            }
        }).then(function (successResponse) {
            $rootScope.loading = false;
            $scope.generateSale = successResponse.data;
            $scope.incotermDisable = false;
            $scope.incotermDisableDPP = false;
            console.log($scope.generateSale );
            $scope.viewSale($scope.generateSale);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.saleOfficeChange = function(sale_office,sale)
    {
        $http({
            method: 'POST',
            url: 'api/sale/changeSaleOffice',
            data: {
                sale : sale,
                sale_office: sale_office
            }
        }).then(function (successResponse) {
            $rootScope.loading = false;
            $scope.generateSale = successResponse.data;
            $scope.getSaleOffice($scope.generateSale.tamura_entity_id_sales_office);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.getSaleOffice = function(id)
    {
        $http({
            method: 'GET',
            url: 'api/tamura-directory/getEntityById?id='+id
        }).then(function (successResponse) {
            $rootScope.loading = false;
            $scope.generateSale.sale_office = {
                "id" : successResponse.data[0].id,
                "name" :  successResponse.data[0].country.name +" - " + successResponse.data[0].short_name + " - " +successResponse.data[0].name
            };
            var containerQty = 1;
            if($scope.generateSale.fcl_lcl == "LCL") {
                containerQty = 3 * $scope.generateSale.pallet_qty;
            }else if($scope.generateSale.fcl_lcl == "LCL"){
                containerQty =  20 * $scope.generateSale.pallet_qty;
            }

            $scope.generateSale.tamura_entity_customs_clearance = !empty(successResponse.data[0].customs_clearance)? successResponse.data[0].customs_clearance : 0;
            $scope.generateSale.tamura_entity_delivery_order = !empty(successResponse.data[0].delivery_order)? successResponse.data[0].delivery_order : 0;
            $scope.generateSale.tamura_entity_fob_charge = !empty(successResponse.data[0].fob_charge) ? successResponse.data[0].fob_charge : 0;
            $scope.generateSale.tamura_entity_freight  = !empty( successResponse.data[0].freight)? successResponse.data[0].freight : 0;
            $scope.generateSale.tamura_entity_haulage_to_warehouse = !empty(successResponse.data[0].haulage_to_warehouse)? successResponse.data[0].haulage_to_warehouse : 0;
            $scope.generateSale.tamura_entity_terminal_handling = !empty(successResponse.data[0].terminal_handling)? successResponse.data[0].terminal_handling : 0;

            $scope.generateSale.tamura_entity_customs_clearance_value = parseFloat(( $scope.generateSale.tamura_entity_customs_clearance / containerQty ).toFixed(3));
            $scope.generateSale.tamura_entity_delivery_order_value = parseFloat(($scope.generateSale.tamura_entity_delivery_order / $scope.generateSale.pallet_qty).toFixed(3));
            $scope.generateSale.tamura_entity_fob_charge_value = parseFloat(($scope.generateSale.tamura_entity_fob_charge / $scope.generateSale.pallet_qty).toFixed(3));
            $scope.generateSale.tamura_entity_freight_value = parseFloat(($scope.generateSale.tamura_entity_freight / $scope.generateSale.pallet_qty).toFixed(3));
            $scope.generateSale.tamura_entity_haulage_to_warehouse_value = parseFloat(($scope.generateSale.tamura_entity_haulage_to_warehouse / containerQty).toFixed(3));
            $scope.generateSale.tamura_entity_terminal_handling_value = parseFloat(($scope.generateSale.tamura_entity_terminal_handling / $scope.generateSale.pallet_qty).toFixed(3));
            if($scope.generateSale.incoterms_id == "DDP"){
                $scope.generateSale.import_duty_value = parseFloat(($scope.generateSale.cif_cost * ($scope.generateSale.import_duty / PERCENT)).toFixed(3));
            }
            else{
                $scope.generateSale.import_duty_value = 0;
            }
            $scope.generateSale.advance_fees_value = parseFloat(($scope.generateSale.import_duty_value * ($scope.generateSale.advance_fees / PERCENT)).toFixed(3));
            $scope.generateSale.cif_cost = parseFloat(($scope.generateSale.buy_price + $scope.generateSale.tamura_entity_fob_charge_value + $scope.generateSale.tamura_entity_freight_value).toFixed(3));
            $scope.generateSale.total_oversea_cost_flag = false;
            for(var i = 0; i < $scope.currenciesAll.length; i++){
                if(empty($scope.generateSale.currency)) {
                    if ($scope.currenciesAll[i].id == successResponse.data[0].currency_id) {
                        $scope.generateSale.currency = $scope.currenciesAll[i];
                        $scope.generateSale.currency_symbol = $scope.currenciesAll[i].symbol;
                        $scope.changeNewSaleCurrency();
                        break;
                    }
                }
                else{
                    if ($scope.currenciesAll[i].id == $scope.generateSale.currency.id) {
                        $scope.changeNewSaleCurrency();
                        break;
                    }
                }
            }

            updateTotalSaleOversea();
            updateTotalSale();
            console.log($scope.generateSale);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.lclFclchange = function () {
        var containerQty = 1;
        if($scope.generateSale.fcl_lcl == "LCL") {
            containerQty = 3 * $scope.generateSale.pallet_qty;
        }else if($scope.generateSale.fcl_lcl == "LCL"){
            containerQty =  20 * $scope.generateSale.pallet_qty;
        }

        $scope.generateSale.tamura_entity_customs_clearance_value = parseFloat(( $scope.generateSale.tamura_entity_customs_clearance / containerQty ).toFixed(3));
        $scope.generateSale.lfr_value = parseFloat(($scope.generateSale.lfr / containerQty).toFixed(3));
        $scope.generateSale.tamura_entity_haulage_to_warehouse_value = parseFloat(($scope.generateSale.tamura_entity_haulage_to_warehouse / containerQty).toFixed(3));

        if (!$scope.generateSale.total_oversea_cost_flag){
            updateTotalSaleOversea();
        }
        updateTotalSale();
    };

    $scope.palletQtyChange = function () {
        var containerQty = 1;
        if($scope.generateSale.fcl_lcl == "LCL") {
            containerQty = 3 * $scope.generateSale.pallet_qty;
        }else if($scope.generateSale.fcl_lcl == "LCL"){
            containerQty =  20 * $scope.generateSale.pallet_qty;
        }
        $scope.generateSale.tamura_entity_customs_clearance_value = parseFloat(( $scope.generateSale.tamura_entity_customs_clearance / containerQty ).toFixed(3));
        $scope.generateSale.tamura_entity_delivery_order_value = parseFloat(($scope.generateSale.tamura_entity_delivery_order / $scope.generateSale.pallet_qty).toFixed(3));
        $scope.generateSale.tamura_entity_fob_charge_value = parseFloat(($scope.generateSale.tamura_entity_fob_charge / $scope.generateSale.pallet_qty).toFixed(3));
        $scope.generateSale.tamura_entity_freight_value = parseFloat(($scope.generateSale.tamura_entity_freight / $scope.generateSale.pallet_qty).toFixed(3));
        $scope.generateSale.tamura_entity_haulage_to_warehouse_value = parseFloat(($scope.generateSale.tamura_entity_haulage_to_warehouse / containerQty).toFixed(3));
        $scope.generateSale.tamura_entity_terminal_handling_value = parseFloat(($scope.generateSale.tamura_entity_terminal_handling / $scope.generateSale.pallet_qty).toFixed(3));
        $scope.generateSale.lfr_value = parseFloat(($scope.generateSale.lfr / containerQty).toFixed(3));
        saleConvertValuesToPounds(GBPCurrency,$scope.generateSale.currency.id);
        switch ($scope.generateSale.incoterms_id){
            case "DAT" :
            case "DAP" :
            case "DDP" :
                $scope.generateSale.delivery_to_customer_value = parseFloat(($scope.generateSale.delivery_to_customer / $scope.generateSale.pallet_qty).toFixed(3));
                break;
            default:
                $scope.generateSale.delivery_to_customer_value = 0;
                break;
        }
        if (!$scope.generateSale.total_oversea_cost_flag){
            updateTotalSaleOversea();
        }
        updateTotalSale();
    };

    var saleConvertValuesToPounds = function(currency, oldCurrencyId)
    {
        var estimatedValue = {
            "unloading" : $scope.generateSale.unloading,
            "orderPicking" : $scope.generateSale.order_picking,
            "storage" : $scope.generateSale.storage,
            "admin" : $scope.generateSale.admin
        };
        if(currency !== undefined && currency.id !== oldCurrencyId){
            $http({
                method : 'POST',
                url : 'api/currencies/saleConvertToPound',
                data : {
                    currencyId : currency.id,
                    oldCurrencyId : oldCurrencyId,
                    estimatedValue : estimatedValue
                }
            }).then(function (successResponse){
                $scope.generateSale.unloading_value = parseFloat((successResponse.data["unloading"] / $scope.generateSale.pallet_qty).toFixed(3));
                $scope.generateSale.order_picking_value = parseFloat((successResponse.data["orderPicking"] / $scope.generateSale.pallet_qty).toFixed(3));
                $scope.generateSale.storage_value = parseFloat((successResponse.data["storage"] / $scope.generateSale.pallet_qty).toFixed(3));
                $scope.generateSale.admin_value = parseFloat((successResponse.data["admin"] / $scope.generateSale.pallet_qty).toFixed(3));
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    };

    $scope.checkProjectMarket = function(parent,product){
        if(parent !== undefined) {
            if (parent !== null) {
                if (product == null) {
                    alert($scope.chooseMarket[0].text);
                    return 1;
                }
                else
                    return 0;
            }
            else
                return 0;
        }
        else
            return 0;
    };


    $scope.updateProject = function () {
        $rootScope.loading = true;
        console.log($scope.project);
        var checkMarket = $scope.checkProjectMarket($scope.project.marketParent,$scope.project.market);
        if(checkMarket == 1){
            $rootScope.loading = false;
            return false;
        }else {
            $http({
                method: 'POST',
                url: 'api/projects/update',
                data: {
                    id: $scope.project.id,
                    name: $scope.project.name,
                    status: $scope.project.status,
                    marketId: $scope.project.market !== null ? $scope.project.market.no_entity : null,
                    standards: $scope.project.standards,
                    description: $scope.project.description,
                    remarks: $scope.project.remarks,
                    estimatedValue: $scope.project.estimated_value,
                    estimatedValueCurrencyId: $scope.project.estimated_value_currency !== null ? $scope.project.estimated_value_currency.id : null,
                    itemToDesign: $scope.project.item_to_design,
                    customerId: $scope.project.customer !== null ? $scope.project.customer.id : null,
                    endCustomer: $scope.project.end_customer,
                    customerPurchaseManagerId: $scope.project.purchase_manager !== null ? $scope.project.purchase_manager.id : null,
                    managerId: $scope.project.manager !== null ? $scope.project.manager.id : null,
                    businessLocation: $scope.project.business_location !== null ? $scope.project.business_location.iso2 : null,
                    lastModifiedBy: ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                    life: $scope.project.life,
                    spotOrder: $scope.project.spot_order,
                    managerPhone : $scope.project.manager !== null ?  $scope.project.manager.phone : null,
                    managerMobile : $scope.project.manager !== null ?  $scope.project.manager.mobile : null,
                    purchaseManager :  $scope.project.purchase_manager
                }
            }).then(function (successResponse) {
                getData();
                $scope.createProjectExport($scope.project);
                $rootScope.loading = false;
                $scope.viewProject($scope.project);
                Flash.clear();
                $scope.viewProject(successResponse.data);
                $translate(["data-sheet.project-successfully-updated"]).then(function (translations) {
                    Flash.create('success', translations["data-sheet.project-successfully-updated"], 0, {}, true);
                });
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });
        }
    };

    $scope.createNewRfq = function (flashAlreadyCleared = false) {
        $rootScope.loading = true;
        $http({
            method: 'POST',
            url: 'api/rfqs/create',
            data: {
                projectId: $scope.project.id,
                lastModifiedBy: ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name
            }
        }).then(function (successResponse) {

            if (successResponse.data !== null) {
                getData();

                $scope.rfq.id = successResponse.data.id;
                $scope.rfq.project_id = $scope.project.id;

                $scope.viewRfq($scope.rfq);

                if(!flashAlreadyCleared){
                    Flash.clear();
                }

                $translate("data-sheet.rfq-created-successfully", {rfqId : $scope.rfq.id}).then(function (translation) {
                    Flash.create('success', translation, 0, {}, true);
                });

            }

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.updateRfq = function () {
        $rootScope.loading = true;
        console.log($scope.rfq);
        var checkProduct = $scope.checkRfqProduct($scope.productParent,$scope.productProduct);
        if (checkProduct == 1) {
            $rootScope.loading = false;
            return false;
        }
        $scope.rfq.product = $scope.productProduct;

        changeDateFormatsMonthsDays($scope.rfq);
        $http({
            method: 'POST',
            url: 'api/rfqs/update',
            data: {
                rfq: $scope.rfq,
                lastModifiedBy: ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name
            }
        }).then(function (successResponse) {
            if (successResponse.data !== null) {
                getData();
                $scope.createRfqExport($scope.rfq);
                $scope.viewRfq(successResponse.data);
                Flash.clear();
                $translate(["data-sheet.rfq-successfully-updated"]).then(function (translations) {
                    Flash.create('success', translations["data-sheet.rfq-successfully-updated"], 0, {}, true);
                });
            }
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.checkRfqProduct = function(parent,product){
        if (parent !== undefined) {
            if (parent !== null) {
                if (product == null) {
                    alert($scope.chooseProduct[0].text);
                    return 1;
                }
                else
                    return 0;
            }
            else
                return 0;
        }
        else
            return 0;
    };

    $scope.projectCustomerSelectedItemChange = function (customer) {
        if (customer !== undefined) {
            $scope.projectCustomerSelected = true;
            getPurchasers();
        }
    };

    $scope.marketParentSelectedItemChange = function (marketParent) {
        if (marketParent !== undefined) {
            $scope.projectMarketParentSelected = true;
            $scope.project.marketParent = marketParent;
            getMarketChildren();
        } else {
            $scope.project.market = null;
        }
    };
    var changeDateFormats = function (rfq){
        // LOGS
        if ( rfq.last_modified_on != null) {
            var lastModifiedDateSplit1 = rfq.last_modified_on.split(" ");
            var lastModifiedDateSplit2 = lastModifiedDateSplit1[0].split("/");
            rfq.last_modified_on = lastModifiedDateSplit2[1] + "/" + lastModifiedDateSplit2[0] + "/" + lastModifiedDateSplit2[2] + " " + lastModifiedDateSplit1[1];
        }
        if ( rfq.creation_date != null) {
            var creationDateSplit1 = rfq.creation_date.split(" ");
            var creationDateSplit2 = creationDateSplit1[0].split("/");
            rfq.creation_date = creationDateSplit2[1] + "/" + creationDateSplit2[0] + "/" + creationDateSplit2[2] + " " + creationDateSplit1[1];
        }
    };
    var changeDateFormatsMonthsDays = function (rfq){

        if ( rfq.qualif_mass_prod_date != null ) {
            var qualifMassProdSplit1 = rfq.qualif_mass_prod_date.split(" ");
            var qualifMassProdSplit2 = qualifMassProdSplit1[0].split("/");
            rfq.qualif_mass_prod_date = qualifMassProdSplit2[1] + "/" + qualifMassProdSplit2[0] + "/" + qualifMassProdSplit2[2];
        }
        if ( rfq.qualif_quotation_required_date != null ) {
            var qualifQuotationSplit1 = rfq.qualif_quotation_required_date.split(" ");
            var qualifQuotationSplit2 = qualifQuotationSplit1[0].split("/");
            rfq.qualif_quotation_required_date = qualifQuotationSplit2[1] + "/" + qualifQuotationSplit2[0] + "/" + qualifQuotationSplit2[2];
        }
        if ( rfq.qualif_sample_date != null ) {
            var qualifSampleSplit1 = rfq.qualif_sample_date.split(" ");
            var qualifSampleSplit2 = qualifSampleSplit1[0].split("/");
            rfq.qualif_sample_date = qualifSampleSplit2[1] + "/" + qualifSampleSplit2[0] + "/" + qualifSampleSplit2[2];
        }

        if ( rfq.eng_action_costing_planned_date != null ) {
            var costComplSplit2 = rfq.eng_action_costing_planned_date.split("/");
            rfq.eng_action_costing_planned_date = costComplSplit2[1] + "/" + costComplSplit2[0] + "/" + costComplSplit2[2];
        }

        if ( rfq.eng_action_design_complete_planned_date != null ) {
            var desComplSplit2 = rfq.eng_action_design_complete_planned_date.split("/");
            rfq.eng_action_design_complete_planned_date = desComplSplit2[1] + "/" + desComplSplit2[0] + "/" + desComplSplit2[2];
        }


    };

    var changeNormalDateFormat = function(rfq){
        if ( rfq.eng_action_costing_complete_when != null) {
            var costingCompleteSplit1 = rfq.eng_action_costing_complete_when.split(" ");
            var costingCompleteSplit2 = costingCompleteSplit1[0].split("/");
            rfq.eng_action_costing_complete_when = costingCompleteSplit2[2] + "-" + costingCompleteSplit2[0] + "-" + costingCompleteSplit2[1] + " " + costingCompleteSplit1[1];
        }
        if ( rfq.eng_action_design_complete_when != null) {
            var designCompleteSplit1 = rfq.eng_action_design_complete_when.split(" ");
            var designCompleteSplit2 = designCompleteSplit1[0].split("/");
            rfq.eng_action_design_complete_when = designCompleteSplit2[2] + "-" + designCompleteSplit2[0] + "-" + designCompleteSplit2[1] + " " + designCompleteSplit1[1];
        }
        if ( rfq.eng_action_design_required_when != null ) {
            var designRequiredSplit1 = rfq.eng_action_design_required_when.split(" ");
            var designRequiredSplit2 = designRequiredSplit1[0].split("/");
            rfq.eng_action_design_required_when = designRequiredSplit2[2] + "-" + designRequiredSplit2[0] + "-" + designRequiredSplit2[1] + " " + designRequiredSplit1[1];
        }
        if ( rfq.eng_action_mass_prod_auth_when != null) {
            var massProdSplit1 = rfq.eng_action_mass_prod_auth_when.split(" ");
            var massProdSplit2 = massProdSplit1[0].split("/");
            rfq.eng_action_mass_prod_auth_when = massProdSplit2[2] + "-" + massProdSplit2[0] + "-" + massProdSplit2[1] + " " + massProdSplit1[1];
        }
    };

    $scope.viewRfq = function (rfq) {
        $rootScope.loading = true;

        $scope.projectEditing = false;
        $scope.projectAdding = false;

        $scope.rfqExists = false;

        $http({
            method: 'GET',
            url: 'api/rfqs/get?id=' + rfq.id
        }).then(function (successResponse) {

            $scope.rfq = successResponse.data;

            $rootScope.loading = false;
            $scope.projectViewed = false;
            $scope.rfqViewed = true;
            $scope.npiViewed = false;
            console.log($scope.rfq);
            window.scrollTo(0,0);
            changeDateFormats($scope.rfq);
            changeDateFormatsMonthsDays($scope.rfq);
            changeNormalDateFormat($scope.rfq);
            getCustomerEngineers();
            getDesignOffices();
            getRfqStatusLogs($scope.rfq.id);
            getSpecifications($scope.rfq.id);
            getFactories();
            getProduction($scope.rfq.id);

            if ($scope.rfq.product !== null) {

                $scope.productProduct = $scope.rfq.product;

                if ($scope.rfq.product.parent.type_entity === 1) {
                    $scope.productChildOneViewed = false;
                    $scope.productChildTwoViewed = false;
                    $scope.productProductViewed = true;

                    $scope.productParent = $scope.rfq.product.parent;
                    getProductParents();
                    getProductProducts($scope.rfq.product.parent);
                }

            }

            $scope.rfqMaterialsGridOptions.data = $scope.rfq.materials;

            // pre-count total materials value
            $scope.rfq.totalMaterialsValue = 0.0;
            angular.forEach($scope.rfq.materials, function (value) {
                $scope.rfq.totalMaterialsValue += value.total_price;
            });

            if($scope.rfq.product_other_materials != null && $scope.rfq.totalMaterialsValue != null)
                $scope.rfq.product_other_materials_value = parseFloat((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue).toFixed(3));

            if($scope.rfq.product_handling_scrap != null && $scope.rfq.totalMaterialsValue != null)
                $scope.rfq.product_handling_scrap_value = parseFloat((($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue).toFixed(3));

            var totalMatValue = parseFloat(((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue) + (($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue) + $scope.rfq.totalMaterialsValue).toFixed(3));
            $scope.rfq.estimated_factory_cost.rfq_material_factory1 =  totalMatValue;
            $scope.rfq.estimated_factory_cost.rfq_material_factory2 =  totalMatValue;
            $scope.rfq.estimated_factory_cost.rfq_material_factory3 =  totalMatValue;

            // labour costs
            $scope.rfq.labour2Min = $scope.rfq.labour1 + 1;
            $scope.rfq.labour3Min = $scope.rfq.labour2 + 1;
            $scope.rfq.labourMax = $scope.rfq.labour3 + 1;

            // estimated EXW Factory Cost
            $scope.rfq.tamuraEntityLabourValueFactory1 =
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1;

            $scope.rfq.tamuraEntityLabourValueFactory2 =
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2;

            $scope.rfq.tamuraEntityLabourValueFactory3 =
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3;

            $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1 = $scope.rfq.labour_time1;
            $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2 = $scope.rfq.labour_time3;
            $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3 = $scope.rfq.labour_time4;
            $scope.createRfqExport($scope.rfq);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };

    $scope.RFQLabourTimeCategoryChange = function (number,category) {
        switch (number){
            case 1:
                switch (category){
                    case 1:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1 =  parseFloat($scope.rfq.labour_time1);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1.toFixed(3));                        break;
                        break;
                    case 2:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1 =  parseFloat($scope.rfq.labour_time2);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1.toFixed(3));                        break;
                        break;
                    case 3:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1 =  parseFloat($scope.rfq.labour_time3);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1.toFixed(3));
                        break;
                    case 4:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1 =  parseFloat($scope.rfq.labour_time4);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1.toFixed(3));
                        break;
                    default:
                        break;
                }
                break;
            case 2:
                switch (category){
                    case 1:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2 =  parseFloat($scope.rfq.labour_time1);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2.toFixed(3));                        break;
                        break;
                    case 2:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2 = parseFloat( $scope.rfq.labour_time2);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2.toFixed(3));
                        break;
                    case 3:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2 = parseFloat( $scope.rfq.labour_time3);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2.toFixed(3));
                        break;
                    case 4:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2 = parseFloat( $scope.rfq.labour_time4);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2.toFixed(3));
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                switch (category){
                    case 1:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3 = parseFloat( $scope.rfq.labour_time1);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3.toFixed(3));
                        break;
                    case 2:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3 = parseFloat( $scope.rfq.labour_time2);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3.toFixed(3));
                        break;
                    case 3:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3 = parseFloat( $scope.rfq.labour_time3);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3.toFixed(3));
                        break;
                    case 4:
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3 = parseFloat( $scope.rfq.labour_time4);
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3;
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3.toFixed(3));
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    };

    $scope.RFQLabourRateUpdate = function (number) {
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory1;
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1.toFixed(3));
                $scope.RFQAdminUpdate(1);
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory2;
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2.toFixed(3));
                $scope.RFQAdminUpdate(2);
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 * $scope.rfq.estimated_factory_cost.tamura_entity_labour_time_value_factory3;
                $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3.toFixed(3));
                $scope.RFQAdminUpdate(3);
                break;
            default:
                break;
        }
    };

    $scope.RFQAdminUpdate = function (number) {
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 + $scope.rfq.estimated_factory_cost.rfq_material_factory1) * ($scope.rfq.estimated_factory_cost.tamura_entity_admin_factory1 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1.toFixed(3));
                $scope.RFQFreightUpdate(1);
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 + $scope.rfq.estimated_factory_cost.rfq_material_factory2) * ($scope.rfq.estimated_factory_cost.tamura_entity_admin_factory2 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2.toFixed(3));
                $scope.RFQFreightUpdate(2);
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 + $scope.rfq.estimated_factory_cost.rfq_material_factory3) * ($scope.rfq.estimated_factory_cost.tamura_entity_admin_factory3 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3.toFixed(3));
                $scope.RFQFreightUpdate(3);
                break;
            default:
                break;
        }
    };

    $scope.RFQFreightUpdate = function (number) {
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 + $scope.rfq.estimated_factory_cost.rfq_material_factory1 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1) * ($scope.rfq.estimated_factory_cost.tamura_entity_freight_factory1 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1.toFixed(3));
                $scope.RFQProfitUpdate(1);
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 + $scope.rfq.estimated_factory_cost.rfq_material_factory2 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2) * ($scope.rfq.estimated_factory_cost.tamura_entity_freight_factory2 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2.toFixed(3));
                $scope.RFQProfitUpdate(2);
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 + $scope.rfq.estimated_factory_cost.rfq_material_factory3 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3) * ($scope.rfq.estimated_factory_cost.tamura_entity_freight_factory3 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3.toFixed(3));
                $scope.RFQProfitUpdate(3);
                break;
            default:
                break;
        }
    };

    $scope.RFQProfitUpdate = function(number){
        var profitConst = 1;
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory1 =
                    ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 + $scope.rfq.estimated_factory_cost.rfq_material_factory1 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1 +
                    $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1) * ($scope.rfq.estimated_factory_cost.tamura_entity_profit_factory1 / PERCENT) / (profitConst - ($scope.rfq.estimated_factory_cost.tamura_entity_profit_factory1 / PERCENT));
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory1.toFixed(3));
                $scope.RFQProfitSellingPriceUpdate(1);
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory2 =
                    ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 + $scope.rfq.estimated_factory_cost.rfq_material_factory2 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2 +
                     $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2) * ($scope.rfq.estimated_factory_cost.tamura_entity_profit_factory2 / PERCENT) / (profitConst - ($scope.rfq.estimated_factory_cost.tamura_entity_profit_factory2 / PERCENT));
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory2.toFixed(3));
                $scope.RFQProfitSellingPriceUpdate(2);
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory3 =
                    ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 + $scope.rfq.estimated_factory_cost.rfq_material_factory3 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3 +
                    $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3) * ($scope.rfq.estimated_factory_cost.tamura_entity_profit_factory3 / PERCENT) / (profitConst - ($scope.rfq.estimated_factory_cost.tamura_entity_profit_factory3 / PERCENT));
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory3.toFixed(3));
                $scope.RFQProfitSellingPriceUpdate(3);
                break;
            default:
                break;
        }
    };

    $scope.RFQProfitSellingPriceUpdate = function (number) {
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 + $scope.rfq.estimated_factory_cost.rfq_material_factory1 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1 + $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1 + $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory1) ;
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1.toFixed(3));
                $scope.RFQProfitSellOfficeUpdate(1);
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 + $scope.rfq.estimated_factory_cost.rfq_material_factory2 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2 + $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2 + $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory2) ;
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2.toFixed(3));
                $scope.RFQProfitSellOfficeUpdate(2);
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3 = ($scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 + $scope.rfq.estimated_factory_cost.rfq_material_factory3 +  $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3 + + $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3 + $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory3);
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3.toFixed(3));
                $scope.RFQProfitSellOfficeUpdate(3);
                break;
            default:
                break;
        }
    };

    $scope.RFQProfitSellOfficeUpdate = function (number) {
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1 = $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1 * ($scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office1 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office1 = $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1 + $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1;
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1.toFixed(3));
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office1 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office1.toFixed(3));
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2 = $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2 * ($scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office2 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office2 = $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2 + $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2;
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2.toFixed(3));
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office2 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office2.toFixed(3));
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3 = $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3 * ($scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office3 / PERCENT);
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office3 = $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3 + $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3;
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3.toFixed(3));
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office3 = parseFloat($scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office3.toFixed(3));
                break;
            default:
                break;
        }
    };

    $scope.changeProductOtherMaterialsValue = function () {
        if($scope.rfq.product_other_materials != null && $scope.rfq.totalMaterialsValue != null)
            $scope.rfq.product_other_materials_value = parseFloat((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue).toFixed(3));
        else
            $scope.rfq.product_other_materials_value = 0;
    };

    $scope.changeProductHandlingScrapValue = function () {
        if($scope.rfq.product_handling_scrap != null && $scope.rfq.totalMaterialsValue != null)
            $scope.rfq.product_handling_scrap_value = parseFloat((($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue).toFixed(3));
        else
            $scope.rfq.product_handling_scrap_value = 0;
    };

    $scope.createRfqExport = function (rfq) {
        $scope.rfqExport = [];
        pushRfqExportToArray(rfq,$scope.rfqExport);
    };

    $scope.efcFactoryChange = function (num) {
        var totalMaterialValue = parseFloat(((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue) + (($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue) + $scope.rfq.totalMaterialsValue).toFixed(3));
        switch (num){
            case 1:
                $scope.rfq.estimated_factory_cost.rfq_material_factory1 = totalMaterialValue;
                if($scope.rfq.estimated_factory_cost.factory1 != null) {
                    $scope.currenciesAll.forEach(function (currency) {
                        if (currency.id == $scope.rfq.estimated_factory_cost.factory1.currency_id)
                            $scope.rfq.estimated_factory_cost.currency_factory1 = currency;
                    });
                    $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 =  $scope.rfq.estimated_factory_cost.factory1.labour_rate;
                    $scope.rfq.estimated_factory_cost.tamura_entity_admin_factory1 =  $scope.rfq.estimated_factory_cost.factory1.admin;
                    $scope.rfq.estimated_factory_cost.tamura_entity_profit_factory1 =  $scope.rfq.estimated_factory_cost.factory1.profit;
                    $scope.rfq.estimated_factory_cost.tamura_entity_freight_factory1 =  $scope.rfq.estimated_factory_cost.factory1.freight;
                    $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office1 =  $scope.rfq.estimated_factory_cost.factory1.profit;
                }
                $scope.RFQLabourRateUpdate(1);
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.rfq_material_factory2 = totalMaterialValue;
                if($scope.rfq.estimated_factory_cost.factory2 != null) {
                    $scope.currenciesAll.forEach(function (currency) {
                        if (currency.id == $scope.rfq.estimated_factory_cost.factory2.currency_id)
                            $scope.rfq.estimated_factory_cost.currency_factory2 = currency;
                    });
                    $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 =  $scope.rfq.estimated_factory_cost.factory2.labour_rate;
                    $scope.rfq.estimated_factory_cost.tamura_entity_admin_factory2 =  $scope.rfq.estimated_factory_cost.factory2.admin;
                    $scope.rfq.estimated_factory_cost.tamura_entity_profit_factory2 =  $scope.rfq.estimated_factory_cost.factory2.profit;
                    $scope.rfq.estimated_factory_cost.tamura_entity_freight_factory2 =  $scope.rfq.estimated_factory_cost.factory2.freight;
                    $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office2 =  $scope.rfq.estimated_factory_cost.factory2.profit;
                }
                $scope.RFQLabourRateUpdate(2);
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.rfq_material_factory1 = totalMaterialValue;
                if($scope.rfq.estimated_factory_cost.factory3 != null) {
                    $scope.currenciesAll.forEach(function (currency) {
                        if (currency.id == $scope.rfq.estimated_factory_cost.factory3.currency_id)
                            $scope.rfq.estimated_factory_cost.currency_factory3 = currency;
                    });
                    $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 =  $scope.rfq.estimated_factory_cost.factory3.labour_rate;
                    $scope.rfq.estimated_factory_cost.tamura_entity_admin_factory3 =  $scope.rfq.estimated_factory_cost.factory3.admin;
                    $scope.rfq.estimated_factory_cost.tamura_entity_profit_factory3 =  $scope.rfq.estimated_factory_cost.factory3.profit;
                    $scope.rfq.estimated_factory_cost.tamura_entity_freight_factory3 =  $scope.rfq.estimated_factory_cost.factory3.freight;
                    $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office3 =  $scope.rfq.estimated_factory_cost.factory3.profit;
                }
                $scope.RFQLabourRateUpdate(3);
                break;
            default:
                break;
        }
    };

    var pushRfqExportToArray = function (rfq,exportVar) {
        exportVar.push({id: rfq.id,
            father: rfq.father,
            is_std_item: rfq.is_std_item,
            project_id: rfq.project_id,
            status: rfq.status,
            folder: rfq.folder,
            priority: rfq.priority,
            design_office_id: rfq.design_office_id,
            customer_engineer_id: rfq.customer_engineer_id,
            tamura_elec_engineer_id: rfq.tamura_elec_engineer_id,
            tamura_mec_engineer_id: rfq.tamura_mec_engineer_id,
            product_id: rfq.product_id,
            pn_customer: rfq.pn_customer,
            pn_desc: rfq.pn_desc,
            pn_category_id: rfq.pn_category_id,
            pn_format: rfq.pn_format,
            pn_new_dev: rfq.pn_new_dev,
            pn_new_biz: rfq.pn_new_biz,
            com_currency_id: rfq.com_currency_id,
            com_estimated_price : rfq.com_estimated_price,
            com_target_price: rfq.com_target_price,
            com_annual_qty: rfq.com_annual_qty,
            com_annual_value: rfq.com_annual_value,
            com_confidence_level: rfq.com_confidence_level,
            com_market_share: rfq.com_market_share,
            com_incoterms: rfq.com_incoterms,
            com_delivery_location: rfq.com_delivery_location,
            com_teu_group: rfq.com_teu_group,
            qualif_quotation_required_date: rfq.qualif_quotation_required_date,
            qualif_sample_date: rfq.qualif_sample_date,
            qualif_mass_prod_date: rfq.qualif_mass_prod_date,
            comments: rfq.comments,
            duplicate : rfq.duplicate,
            duplicate_from: rfq.duplicate_from,
            creation_date: rfq.creation_date,
            last_modified_on: rfq.last_modified_on,
            last_modified_by: rfq.last_modified_by,
            status_10: rfq.status_10,
            status_10_who: rfq.status_10_who,
            status_10_when: rfq.status_10_when,
            status_20: rfq.status_20,
            status_20_who: rfq.status_20_who,
            status_20_when: rfq.status_20_when,
            status_30: rfq.qualif_mass_prod_date,
            status_30_who: rfq.status_30_who,
            status_30_when : rfq.status_30_when,
            status_dead: rfq.status_dead,
            status_dead_reason: rfq.status_dead_reason,
            status_dead_reason_id: rfq.status_dead_reason_id,
            status_dead_who: rfq.status_dead_who,
            status_dead_when: rfq.status_dead_when,
            status_rejected: rfq.status_rejected,
            status_rejected_who: rfq.status_rejected_who,
            status_rejected_when: rfq.status_rejected_when,
            eng_action_costing_complete: rfq.eng_action_costing_complete,
            eng_action_costing_complete_who: rfq.eng_action_costing_complete_who,
            eng_action_costing_complete_when: rfq.eng_action_costing_complete_when,
            eng_action_costing_planned_date: rfq.eng_action_costing_planned_date,
            eng_action_design_complete : rfq.eng_action_design_complete,
            eng_action_design_complete_planned_date: rfq.eng_action_design_complete_planned_date,
            eng_action_design_complete_who: rfq.eng_action_design_complete_who,
            eng_action_design_complete_when : rfq.eng_action_design_complete_when,
            eng_action_design_required: rfq.eng_action_design_required,
            eng_action_design_required_who: rfq.eng_action_design_required_who,
            eng_action_design_required_when: rfq.eng_action_design_required_when,
            eng_action_mass_prod_auth: rfq.eng_action_mass_prod_auth,
            eng_action_mass_prod_auth_who: rfq.eng_action_mass_prod_auth_who,
            eng_action_mass_prod_auth_when: rfq.eng_action_mass_prod_auth_when,
            product_type: rfq.product_type,
            product_length: rfq.product_length,
            product_width: rfq.product_width,
            product_height: rfq.product_height,
            product_weight: rfq.product_weight,
            product_field1: rfq.product_field1,
            product_field2 : rfq.product_field2,
            product_field3: rfq.product_field3,
            product_field4: rfq.product_field4,
            product_field5 : rfq.product_field5,
            product_field6: rfq.product_field6,
            product_other_materials: rfq.product_other_materials,
            product_other_materials_value: rfq.product_other_materials_value,
            product_handling_scrap: rfq.product_handling_scrap,
            product_handling_scrap_value: rfq.product_handling_scrap_value,
            mat_currency_id: rfq.mat_currency_id,
            mat_total_value: rfq.mat_total_value,
            pallet_length: rfq.pallet_length,
            pallet_width: rfq.pallet_width,
            pallet_space: rfq.pallet_space,
            pallet_layers: rfq.pallet_layers,
            pallet_quantity: rfq.pallet_quantity,
            labour1 : rfq.labour1,
            labour2 : rfq.labour2,
            labour3: rfq.labour3,
            labour_time1: rfq.labour_time1,
            labour_time2: rfq.labour_time2,
            labour_time3: rfq.labour_time3,
            labour_time4: rfq.labour_time4

        });
    };

    $scope.computeRfqAnnualValue = function () {
        if ($scope.rfq !== null) {
            if ($scope.rfq.com_currency !== null
                && $scope.rfq.com_annual_qty !== null
                && $scope.rfq.com_confidence_level !== null
                && ($scope.rfq.com_target_price !== null || $scope.rfq.com_estimated_price !== null)
            ) {
                if ($scope.rfq.com_target_price === null) {
                    $scope.rfq.com_annual_value = $scope.rfq.com_estimated_price * $scope.rfq.com_annual_qty * ($scope.rfq.com_confidence_level / 100) * ($scope.rfq.com_market_share / 100);
                } else {
                    $scope.rfq.com_annual_value = $scope.rfq.com_target_price * $scope.rfq.com_annual_qty * ($scope.rfq.com_confidence_level / 100) * ($scope.rfq.com_market_share / 100);
                }
            }
            else{
                if( $scope.rfq.com_annual_qty !== null
                    && $scope.rfq.com_confidence_level !== null
                    && ($scope.rfq.com_target_price !== null || $scope.rfq.com_estimated_price !== null)
                ){
                    if ($scope.rfq.com_target_price === null) {
                        $scope.rfq.com_annual_value = $scope.rfq.com_estimated_price * $scope.rfq.com_annual_qty * ($scope.rfq.com_confidence_level / 100) * ($scope.rfq.com_market_share / 100);
                    } else {
                        $scope.rfq.com_annual_value = $scope.rfq.com_target_price * $scope.rfq.com_annual_qty * ($scope.rfq.com_confidence_level / 100) * ($scope.rfq.com_market_share / 100);
                    }
                }
            }
        }
    };

    $scope.toggleStatusT10 = function (status) {
        if (status === true) {
            $scope.rfq.status_10 = true;
            $scope.rfq.status_10_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.status_10_when = $rootScope.getDateTime();
            $scope.rfq.status = "T10";
        } else {
            $scope.rfq.status_10 = false;
            $scope.rfq.status_10_who = null;
            $scope.rfq.status_10_when = null;
            $scope.rfq.status = $scope.returnPreviousStatus();
        }
    };

    $scope.toggleStatusT10CostingComplete = function (status) {
        if (status === true) {
            $scope.rfq.eng_action_costing_complete = true;
            $scope.rfq.eng_action_costing_planned_date = null;
            $scope.rfq.eng_action_costing_complete_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.eng_action_costing_complete_when = $rootScope.getDateTime();
        } else {
            $scope.rfq.eng_action_costing_complete = false;
            $scope.rfq.eng_action_costing_complete_who = null;
            $scope.rfq.eng_action_costing_complete_when = null;
        }
    };

    $scope.toggleStatusT10DesignRequired = function (status) {
        if (status === true) {
            $scope.rfq.eng_action_design_required = true;
            $scope.rfq.eng_action_design_required_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.eng_action_design_required_when = $rootScope.getDateTime();
        } else {
            $scope.rfq.eng_action_design_required = false;
            $scope.rfq.eng_action_design_required_who = null;
            $scope.rfq.eng_action_design_required_when = null;
        }
    };

    $scope.toggleStatusT10DesignComplete = function (status) {
        if (status === true) {
            $scope.rfq.eng_action_design_complete = true;
            $scope.rfq.eng_action_design_complete_planned_date = null;
            $scope.rfq.eng_action_design_complete_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.eng_action_design_complete_when = $rootScope.getDateTime();
        } else {
            $scope.rfq.eng_action_design_complete = false;
            $scope.rfq.eng_action_design_complete_who = null;
            $scope.rfq.eng_action_design_complete_when = null;
        }
    };

    $scope.toggleStatusT10MassProdAuthorized = function (status) {
        if (status === true) {
            $scope.rfq.eng_action_mass_prod_auth = true;
            $scope.rfq.eng_action_mass_prod_auth_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.eng_action_mass_prod_auth_when = $rootScope.getDateTime();
        } else {
            $scope.rfq.eng_action_mass_prod_auth = false;
            $scope.rfq.eng_action_mass_prod_auth_who = null;
            $scope.rfq.eng_action_mass_prod_auth_when = null;
        }
    };

    $scope.toggleStatusT20 = function (status) {
        if (status === true) {
            $scope.rfq.status_20 = true;
            $scope.rfq.status_20_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.status_20_when = $rootScope.getDateTime();
            $scope.rfq.status = "T20";
        } else {
            $scope.rfq.status_20 = false;
            $scope.rfq.status_20_who = null;
            $scope.rfq.status_20_when = null;
            $scope.rfq.status = $scope.returnPreviousStatus();
        }
    };

    $scope.toggleStatusT30 = function (status) {
        if (status === true) {
            $scope.rfq.status_30 = true;
            $scope.rfq.status_30_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.status_30_when = $rootScope.getDateTime();
            $scope.rfq.status = "T30";
        } else {
            $scope.rfq.status_30 = false;
            $scope.rfq.status_30_who = null;
            $scope.rfq.status_30_when = null;
            $scope.rfq.status = $scope.returnPreviousStatus();
        }
    };

    $scope.toggleStatusDead = function (status)
    {
        if (status === true) {
            $scope.rfq.status_dead = true;
            $scope.rfq.status_dead_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.status_dead_when = $rootScope.getDateTime();
            $scope.rfq.status = "DEAD";
        } else {
            $scope.rfq.status_dead = false;
            $scope.rfq.status_dead_who = null;
            $scope.rfq.status_dead_when = null;
            $scope.rfq.status = $scope.returnPreviousStatus();
        }
    };

    $scope.toggleStatusRejected = function (status)
    {
        if (status === true) {
            $scope.rfq.status_rejected = true;
            $scope.rfq.status_rejected_who = ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name;
            $scope.rfq.status_rejected_when = $rootScope.getDateTime();
            $scope.rfq.status = "REJECTED";
        } else {
            $scope.rfq.status_rejected = false;
            $scope.rfq.status_rejected_who = null;
            $scope.rfq.status_rejected_when = null;
            $scope.rfq.status = $scope.returnPreviousStatus();
        }
    };

    $scope.returnPreviousStatus = function()
    {
        var status = "INIT";

        if($scope.rfq.status_10 === true){
            status = "T10";
        }
        if($scope.rfq.status_20 === true){
            status = "T20";
        }
        if($scope.rfq.status_30 === true){
            status = "T30";
        }
        if($scope.rfq.status_dead === true){
            status = "DEAD";
        }
        if($scope.rfq.status_rejected === true){
            status = "REJECTED";
        }
        return status;
    };

    $scope.changeRfqProject = function(ev)
    {
        if($scope.rfq !== null){
            $mdDialog.show({
                controller: 'RfqChangeProjectDialogCtrl',
                templateUrl: 'views/components/rfqChangeProjectDialog/rfqChangeProjectDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    rfq : $scope.rfq
                }
            })
            .then(function(res) {
                console.log("res");
            }, function() {
                getData();
            });
        }
    };

    $scope.copyRfq = function()
    {
        if($scope.rfq !== null){
            $rootScope.loading = true;
            $http({
                method : 'POST',
                url : 'api/rfqs/copy',
                data : {
                    rfqId : $scope.rfq.id
                }
            }).then(function (successResponse){
                console.log(successResponse.data);
                $rootScope.loading = false;
                getData();

            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });

        }
    };

    $scope.cloneRfq = function(ev)
    {
        if($scope.rfq !== null){
            $mdDialog.show({
                controller: 'RfqCloneDialogCtrl',
                templateUrl: 'views/components/rfqCloneDialog/rfqCloneDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    rfq : $scope.rfq
                }
            })
            .then(function(res) {
                console.log("res");
            }, function() {
                getData();
            });
        }
    };

    $scope.manageTimeSheet = function(ev)
    {
        if($scope.rfq !== null){
            $mdDialog.show({
                controller: 'RfqManageTimeSheetDialogCtrl',
                templateUrl: 'views/components/rfqManageTimeSheetDialog/rfqManageTimeSheetDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    rfq : $scope.rfq
                }
            })
            .then(function(res) {
                console.log("res");
            }, function() {
            });
        }
    };

    $scope.labour1Changed = function()
    {
        var labour2Min = parseInt($scope.rfq.labour1);
        $scope.rfq.labour2Min = isNaN(labour2Min) ? 0 : labour2Min + 1;

        labour2Min = parseInt($scope.rfq.labour2Min);
        var labour2 = parseInt($scope.rfq.labour2);

        if(labour2 < labour2Min){
            $scope.rfq.labour2 = labour2Min + 1;
        }

        labour2 = parseInt($scope.rfq.labour2);
        var labour3Min = parseInt($scope.rfq.labour3Min);

        if(labour3Min < labour2){
            $scope.rfq.labour3Min = labour2 + 1;
        }

        var labour3 = parseInt($scope.rfq.labour3);
        labour3Min = parseInt($scope.rfq.labour3Min);
        if(labour3 < labour3Min){
            $scope.rfq.labour3 = labour3Min + 1;
        }

        labour3 = parseInt($scope.rfq.labour3);
        $scope.rfq.labourMax = labour3 + 1;

    };

    $scope.labour2Changed = function()
    {

        var labour2 = parseInt($scope.rfq.labour2);
        var labour2Min = parseInt($scope.rfq.labour2Min);

        if(isNaN(labour2) || labour2 < labour2Min){
            $scope.rfq.labour2 = labour2Min + 1;
            $scope.rfq.labour3Min = labour2Min + 2;
        }else{

            var labour3Min = parseInt($scope.rfq.labour2);
            $scope.rfq.labour3Min = isNaN(labour3Min) ? 0 : labour3Min + 1;

            labour3Min = parseInt($scope.rfq.labour3Min);
            var labour3 = parseInt($scope.rfq.labour3);

            if(labour3 < labour3Min){
                $scope.rfq.labour3 = labour3Min + 1;
            }

            labour3 = parseInt($scope.rfq.labour3);
            $scope.rfq.labourMax = labour3 + 1;

        }

    };

    $scope.labour3Changed = function()
    {
        var labour3 = parseInt($scope.rfq.labour3);
        var labour3Min = parseInt($scope.rfq.labour3Min);

        if(isNaN(labour3) || labour3 < labour3Min){
            $scope.rfq.labour3 = labour3Min + 1;
            $scope.rfq.labourMax = labour3Min + 2;
        }else{

            labour3 = parseInt($scope.rfq.labour3);
            $scope.rfq.labourMax = labour3 + 1;

        }
    };

    $scope.computePalletQuantity = function()
    {
        if($scope.rfq.pallet_length !== null
            && $scope.rfq.pallet_length > 0
            && $scope.rfq.pallet_width !== null
            && $scope.rfq.pallet_width > 0
            && $scope.rfq.pallet_space !== null
            && $scope.rfq.pallet_layers !== null
            && $scope.rfq.pallet_layers !== 0
            && $scope.rfq.product_length !== null
            && $scope.rfq.product_length !== 0
            && $scope.rfq.product_width !== null
            && $scope.rfq.product_width !== 0
        ){

        $scope.rfq.pallet_quantity = Math.round(($scope.rfq.pallet_length / ($scope.rfq.product_length + $scope.rfq.pallet_space)) * ($scope.rfq.pallet_width / ($scope.rfq.product_width + $scope.rfq.pallet_space)) * $scope.rfq.pallet_layers, 0);

        }else{
            console.log("nok");
            $scope.rfq.pallet_quantity = 0;
        }
    };

    $scope.viewNpi = function(npi)
    {

        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/npi/get?id=' + npi.id
        }).then(function (successResponse){

            $scope.npi = successResponse.data;
            console.log($scope.npi);
            $rootScope.loading = false;
            $scope.projectViewed = false;
            $scope.rfqViewed = false;
            $scope.npiViewed = true;
            $scope.generateSaleShow = false;
            getCurrencies();
            getIncoterms($scope.npi.sample_incoterms_id);
            getRFQMaterialCost($scope.npi.rfq_id);
            getRfqForNpi($scope.npi.rfq_id);
            $scope.createNpiExport($scope.npi);
            $scope.materialCost = $scope.npi.material_cost_value;
            for(var i = 0; i < $scope.currenciesAll.length;i++){
                $scope.currenciesShortNames.push($scope.currenciesAll[i].short_name);
            }
            for(var i = 0; i < $scope.currenciesAll.length;i++){
                if($scope.currenciesAll[i].id ==  $scope.npi.currency_id){
                    $scope.npi.currency_id  = $scope.currenciesAll[i].short_name;
                    $scope.npi.currency_symbol = $scope.currenciesAll[i].symbol;
                    $scope.npi.sample_delivery_date = new Date($scope.npi.sample_delivery_date);
                    $scope.npi.creation_date = new Date($scope.npi.creation_date);
                    actualCurrency = $scope.currenciesAll[i].short_name;
                    oldCurrencyId = $scope.currenciesAll[i].id;
                    newCurrencyId =  $scope.currenciesAll[i].id;
                    // $scope.currencyRfqMaterial =  $scope.currenciesAll[i].exch_rate * $scope.npi.rfq_material_cost_value;
                    break;
                }
            }
            window.scrollTo(0,0);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };

    $scope.createNpiExport = function (npi) {
        $scope.npiExport = [];

        pushNPIExportToArray(npi,$scope.npiExport);
    };

    var pushNPIExportToArray = function (npi,exportVar) {
        exportVar.push({id: npi.id,
            rfq_id: npi.rfq_id,
            tamura_entity_id_factory: npi.tamura_entity_id_factory,
            tamura_entity_id_sales_office: npi.tamura_entity_id_sales_office,
            currency_id: npi.currency_id,
            rfq_material_cost_value: npi.rfq_material_cost_value,
            rfq_profit_factory: npi.rfq_profit_factory,
            rfq_profit_value_factory: npi.rfq_profit_value_factory,
            rfq_labour: npi.rfq_labour,
            rfq_labour_time: npi.rfq_labour_time,
            rfq_labour_value: npi.rfq_labour_value,
            rfq_freight: npi.rfq_freight,
            rfq_freight_value: npi.rfq_freight_value,
            rfq_admin: npi.rfq_admin,
            rfq_admin_value: npi.rfq_admin_value,
            rfq_selling_price_factory: npi.rfq_selling_price_factory,
            rfq_profit_sales_office: npi.rfq_profit_sales_office,
            rfq_profit_value_sales_office: npi.rfq_profit_value_sales_office,
            rfq_selling_price_sales_office: npi.rfq_selling_price_sales_office,
            material_cost_value: npi.material_cost_value,
            profit_factory: npi.profit_factory,
            profit_value_factory: npi.profit_value_factory,
            labour: npi.labour,
            labour_time: npi.labour_time,
            labour_value: npi.labour_value,
            freight: npi.freight,
            freight_value: npi.freight_value,
            admin: npi.admin,
            admin_value: npi.admin_value,
            selling_price_factory: npi.selling_price_factory,
            profit_sales_office: npi.profit_sales_office,
            profit_value_sales_office: npi.profit_value_sales_office,
            selling_price_sales_office: npi.selling_price_sales_office,
            sample_quantity_required: npi.sample_quantity_required,
            sample_delivery_date: npi.sample_delivery_date,
            sample_chargeable: npi.sample_chargeable,
            sample_price: npi.sample_price,
            sample_incoterms_id: npi.sample_incoterms_id,
            sample_manufactured_purchased: npi.sample_manufactured_purchased,
            sample_lead_time: npi.sample_lead_time,
            sample_teu_pn: npi.sample_teu_pn,
            sample_customer_address: npi.sample_customer_address,
            minimum_order_quantity: npi.minimum_order_quantity,
            minimum_package_quantity: npi.minimum_package_quantity,
            comment: npi.comment,
            creation_date: npi.creation_date,
            created_by: npi.created_by,
            last_modified_on: npi.last_modified_on,
            last_modified_by: npi.last_modified_by
        });
    };

    $scope.NPILabourTimeUpdate = function () {
        $scope.npi.labour_value = $scope.npi.labour * $scope.npi.labour_time;
        $scope.npi.labour_value = parseFloat($scope.npi.labour_value.toFixed(3));
        $scope.NPIAdminUpdate();
    };

    $scope.NPIAdminUpdate = function () {
        $scope.npi.admin_value = (parseFloat($scope.npi.material_cost_value) + parseFloat($scope.npi.labour_value)) * (parseFloat($scope.npi.admin) / PERCENT);
        $scope.npi.admin_value = parseFloat($scope.npi.admin_value.toFixed(3));
        $scope.NPIFreightUpdate();
    };

    $scope.NPIFreightUpdate = function () {
        $scope.npi.freight_value = (parseFloat($scope.npi.material_cost_value) + parseFloat($scope.npi.labour_value) + parseFloat($scope.npi.admin_value)) * (parseFloat($scope.npi.freight) / PERCENT);
        $scope.npi.freight_value = parseFloat($scope.npi.freight_value.toFixed(3));
        $scope.NPIProfitUpdate();
    };

    $scope.NPIProfitUpdate = function(){
        var profitConst = 1;
        $scope.npi.profit_value_factory = ((parseFloat($scope.npi.material_cost_value) + parseFloat($scope.npi.labour_value) + parseFloat($scope.npi.admin_value) + parseFloat($scope.npi.freight_value))
            * (parseFloat($scope.npi.profit_factory) / PERCENT)) / (profitConst - (parseFloat($scope.npi.profit_factory) / PERCENT));
        $scope.npi.profit_value_factory = parseFloat($scope.npi.profit_value_factory.toFixed(3));
        console.log($scope.npi.profit_value_factory);
        $scope.NPIProfitSellingPriceUpdate();
    };

    $scope.NPIProfitSellingPriceUpdate = function () {
        $scope.npi.profit_value_sales_office = parseFloat($scope.npi.selling_price_factory) * (parseFloat($scope.npi.profit_sales_office) / PERCENT);
        $scope.npi.selling_price_sales_office = parseFloat($scope.npi.selling_price_factory) + parseFloat($scope.npi.profit_value_sales_office);
        $scope.npi.profit_value_sales_office = parseFloat($scope.npi.profit_value_sales_office.toFixed(3));
        $scope.npi.selling_price_sales_office = parseFloat($scope.npi.selling_price_sales_office.toFixed(3));
    };

    $scope.viewSale = function(sale)
    {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/npi/get?id=' + sale.npi_id
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.npi = successResponse.data;
            $scope.projectViewed = false;
            $scope.rfqViewed = false;
            $scope.npiViewed = true;
            $scope.generateSaleShow = true;

            $scope.generateSale = sale;
            getSalesOffices();
            getCustomers();
            getLME();
            getCurrencies();
            getSaleIncoterms($scope.generateSale.incoterms_id);
            getRFQMaterialCost($scope.npi.rfq_id);
            getRfqForNpi($scope.npi.rfq_id);
            $scope.lclFcl = ["LCL","FCL"];
            $scope.createSaleExport($scope.generateSale);

            for(var i = 0; i < $scope.currenciesAll.length;i++){
                $scope.currenciesShortNames.push($scope.currenciesAll[i].short_name);
            }
            for(var i = 0; i < $scope.currenciesAll.length;i++){
                if(empty($scope.generateSale.currency_id)){
                    if($scope.currenciesAll[i].id ==  $scope.npi.currency_id){
                        $scope.generateSale.currency = $scope.currenciesAll[i];
                        $scope.generateSale.currency_symbol = $scope.currenciesAll[i].symbol;
                        actualCurrency = $scope.currenciesAll[i].short_name;
                        oldCurrencyId = $scope.currenciesAll[i].id;
                        newCurrencyId =  $scope.currenciesAll[i].id;
                        break;
                    }
                }
                else{
                    if($scope.currenciesAll[i].id ==  $scope.generateSale.currency_id){
                        $scope.generateSale.currency = $scope.currenciesAll[i];
                        $scope.generateSale.currency_symbol = $scope.currenciesAll[i].symbol;
                        actualCurrency = $scope.currenciesAll[i].short_name;
                        oldCurrencyId = $scope.currenciesAll[i].id;
                        newCurrencyId =  $scope.currenciesAll[i].id;
                        console.log( $scope.generateSale.currency);
                        break;
                    }
                }
            }
            for(var i = 0; i < $scope.currenciesAll.length;i++) {
                if ($scope.currenciesAll[i].short_name == "GBP") {
                    GBPCurrency = $scope.currenciesAll[i];
                    break;
                }
            }
            if(!empty($scope.generateSale.tamura_entity_id_sales_office)){
                $scope.getSaleOffice($scope.generateSale.tamura_entity_id_sales_office)
            }
            console.log($scope.generateSale);
            getData();
            window.scrollTo(0,0);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };
    $scope.saleIncotermChange = function () {
        for(var i = 0; i < $scope.allIncoterms.length;i++) {
            var incot = $scope.allIncoterms[i].short + " - " + $scope.allIncoterms[i].label;
            if (incot == $scope.generateSale.sample_incoterms_id) {
                $scope.generateSale.incoterms_id = $scope.allIncoterms[i].short;
                break;
            }
        }
        if($scope.generateSale.sample_incoterms_id == DPP){
            $scope.incotermDisable = true;
            $scope.incotermDisableDPP = true;
            $scope.generateSale.import_duty_value = $scope.generateSale.cif_cost * $scope.generateSale.import_duty;
            $scope.generateSale.delivery_to_customer_value = $scope.generateSale.delivery_to_customer / $scope.generateSale.pallet_qty;
        }
        else if($scope.generateSale.sample_incoterms_id == DAP || $scope.generateSale.sample_incoterms_id == DAT){
            $scope.incotermDisable = true;
            $scope.incotermDisableDPP = false;
            $scope.generateSale.import_duty_value = 0;
            $scope.generateSale.delivery_to_customer_value = $scope.generateSale.delivery_to_customer / $scope.generateSale.pallet_qty;
        }
        else{
            $scope.incotermDisable = false;
            $scope.incotermDisableDPP = false;
            $scope.generateSale.import_duty_value = 0;
            $scope.generateSale.delivery_to_customer_value = 0;
            $scope.generateSale.delivery_to_customer = 0;
            $scope.generateSale.import_duty = 0;
        }
        $scope.generateSale.advance_fees_value = $scope.generateSale.import_duty_value * $scope.generateSale.advance_fees;

        if (!$scope.generateSale.total_oversea_cost_flag){
            updateTotalSaleOversea();
        }
        updateTotalSale();
    };

    $scope.createSaleExport = function (sale) {
        $scope.saleExport = [];

        pushSaleExportToArray(sale,$scope.saleExport);
    };

    var pushSaleExportToArray = function(sale,exportVar){
        exportVar.push({id: sale.id,
            npi_id: sale.npi_id,
            valid: sale.valid,
            valid_last_update_date: sale.valid_last_update_date,
            currency_id: sale.currency_id,
            sales_manager_id: sale.sales_manager_id,
            tamura_entity_id_sales_office: sale.tamura_entity_id_sales_office,
            buy_price_engineering: sale.buy_price_engineering,
            buy_price_factory: sale.buy_price_factory,
            buy_price: sale.buy_price,
            tamura_entity_fob_charge: sale.tamura_entity_fob_charge,
            tamura_entity_fob_charge_value: sale.tamura_entity_fob_charge_value,
            cif_cost: sale.cif_cost,
            tamura_entity_terminal_handling: sale.tamura_entity_terminal_handling,
            tamura_entity_terminal_handling_value: sale.tamura_entity_terminal_handling_value,
            tamura_entity_delivery_order: sale.tamura_entity_delivery_order,
            tamura_entity_delivery_order_value: sale.tamura_entity_delivery_order_value,
            tamura_entity_customs_clearance: sale.tamura_entity_customs_clearance,
            tamura_entity_customs_clearance_value: sale.tamura_entity_customs_clearance_value,
            advance_fees: sale.advance_fees,
            advance_fees_value: sale.advance_fees_value,
            lfr: sale.lfr,
            lfr_value: sale.lfr_value,
            tamura_entity_haulage_to_warehouse: sale.tamura_entity_haulage_to_warehouse,
            tamura_entity_haulage_to_warehouse_value: sale.tamura_entity_haulage_to_warehouse_value,
            total_inland_charges_value: sale.total_inland_charges_value,
            unloading: sale.unloading,
            unloading_value: sale.unloading_value,
            order_picking: sale.order_picking,
            order_picking_value: sale.order_picking_value,
            storage: sale.storage,
            storage_value: sale.storage_value,
            admin: sale.admin,
            admin_value: sale.admin_value,
            total_hub_costs: sale.total_hub_costs,
            total_oversea_costs: sale.total_oversea_costs,
            total_oversea_costs_value: sale.total_oversea_costs_value,
            total_oversea_cost_flag: sale.total_oversea_cost_flag,
            import_duty: sale.import_duty,
            import_duty_value: sale.import_duty_value,
            delivery_to_customer: sale.delivery_to_customer,
            delivery_to_customer_value: sale.delivery_to_customer_value,
            cost_price: sale.cost_price,
            cost_price_percentage: sale.cost_price_percentage,
            gand_a: sale.gand_a,
            gand_a_value: sale.gand_a_value,
            total_cost: sale.total_cost,
            total_cost_percentage: sale.total_cost_percentage,
            profit: sale.profit,
            profit_value: sale.profit_value,
            selling_price: sale.selling_price,
            comment: sale.comment,
            special_req: sale.special_req,
            incoterms_id: sale.incoterms_id,
            creation_date: sale.creation_date,
            last_update_date: sale.last_update_date,
            created_by: sale.created_by,
            pallet_qty: sale.pallet_qty,
            customer_id: sale.customer_id,
            customer_payment_term_nr: sale.customer_payment_term_nr,
            customer_payment_term_type: sale.customer_payment_term_type,
            purchaser_id: sale.purchaser_id,
            fcl_lcl: sale.fcl_lcl,
            lme_cu: sale.lme_cu,
            lme_cu_at_creation: sale.lme_cu_at_creation,
            lme_al: sale.lme_al,
            lme_al_at_creation: sale.lme_al_at_creation,
        });
    };console.log($scope.generateSale);

    $scope.updateSale = function () {
        $rootScope.loading = true;
        console.log($scope.generateSale);
        for(var i = 0; i < $scope.allIncoterms.length;i++) {
            var incot = $scope.allIncoterms[i].short + " - " + $scope.allIncoterms[i].label;
            if ($scope.generateSale.sample_incoterms_id == incot){
                $scope.generateSale.incoterms_id = $scope.allIncoterms[i].short;
                break;
            }
        }

        $rootScope.loading = false;
        $http({
        method: 'POST',
        url: 'api/sale/updateSale',
        data: {
            sale: $scope.generateSale,
            npi : $scope.npi
        }
        }).then(function (successResponse) {
            getData();
            $scope.createSaleExport($scope.generateSale);
            $rootScope.loading = false;
            Flash.clear();
            $translate(["data-sheet.sale-successfully-updated"]).then(function (translations) {
                Flash.create('success', translations["data-sheet.sale-successfully-updated"], 0, {}, true);
            });
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };


    function getProductParents()
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/parentProductEntities'
        }).then(function (successResponse){
            $scope.productParents = successResponse.data;
            $scope.productParentsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getRFQMaterialCost(rfqId){
        $http({
            method :'GET',
            url : 'api/rfqMaterial/getCost?rfqId=' + rfqId
        }).then(function (successResponse){
            $scope.materialCost = successResponse.data["mat_total_value"];
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getProductProducts(parentProduct)
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/childProductEntities?level=4&parent='+parentProduct.entity_path+parentProduct.entity+"/"
        }).then(function (successResponse){
            console.log(successResponse);
            $scope.productProducts = successResponse.data.childProducts;
            $scope.productProductsAll = successResponse.data.childProducts;
            $scope.productProductViewed = ($scope.productProducts.length > 0);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getProductChildrenOne()
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/childProductEntities?level=2&parent='+$scope.productParent.entity_path+$scope.productParent.entity+"/"
        }).then(function (successResponse){
            $scope.productChildrenOne = successResponse.data.childProductEntities;
            $scope.productChildrenOneAll = successResponse.data.childProductEntities;
            $scope.productChildOneViewed = ($scope.productChildrenOne.length > 0);

            $scope.productProducts = successResponse.data.childProducts;
            $scope.productProductsAll = successResponse.data.childProducts;
            $scope.productProductViewed = ($scope.productProducts.length > 0);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getProductChildrenTwo()
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/childProductEntities?level=3&parent='+$scope.productChildOne.entity_path+$scope.productChildOne.entity+"/"
        }).then(function (successResponse){
            $scope.productChildrenTwo = successResponse.data.childProductEntities;
            $scope.productChildrenTwoAll = successResponse.data.childProductEntities;
            $scope.productChildTwoViewed= ($scope.productChildrenTwo.length > 0);

            $scope.productProducts = successResponse.data.childProducts;
            $scope.productProductsAll = successResponse.data.childProducts;
            $scope.productProductViewed = ($scope.productProducts.length > 0);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    $scope.productParentSelectedItemChange = function(item)
    {

        if(item === undefined){
            $scope.productChildOne = null;
            $scope.productProduct = null;
        }

        if(item !== undefined){
            getProductChildrenOne();
        }

    };

    $scope.productChildOneSelectedItemChange = function(item)
    {
        if(item === undefined) {
            $scope.productChildTwo = null;
            $scope.productProduct = null;
        }

        if(item !== undefined){
            getProductChildrenTwo();
        }

    };

    $scope.productProductSearchTextChange = function(text)
    {
        $scope.productProducts = $scope.productProductsAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.productParentSearchTextChange = function(text)
    {
        $scope.productParents = $scope.productParentsAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.productChildOneSearchTextChange = function(text)
    {
        $scope.productChildrenOne = $scope.productChildrenOneAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.productChildTwoSearchTextChange = function(text)
    {
        $scope.productChildrenTwo = $scope.productChildrenTwoAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.viewRfqTab = function(tab){
        $scope.rfqGeneralTab = false;
        $scope.rfqTechnicalTab = false;
        $scope.rfqCostingTab = false;
        $scope.rfqProductionTab = false;
        $scope.rfqStatusLogTab = false;
        $scope[tab] = true;
    };
    
    $scope.estimatedValueCurrencyChange = function(currency, oldCurrencyId, estimatedValue)
    {
        if(currency !== undefined && currency.id !== oldCurrencyId){
            $http({
                method : 'POST',
                url : 'api/currencies/convert',
                data : {
                    currencyId : currency.id,
                    oldCurrencyId : oldCurrencyId,
                    estimatedValue : estimatedValue
                }
            }).then(function (successResponse){
                $scope.project.estimated_value = successResponse.data;
                $scope.project.estimated_value_currency = currency;
                $scope.project.estimated_value_currency_id = currency.id;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    };

    $scope.npiCurrencyChange = function(currency, oldCurrencyId, estimatedValue)
    {

        if(currency !== undefined && currency !== oldCurrencyId){
            $http({
                method : 'POST',
                url : 'api/currencies/npiConvert',
                data : {
                    currencyId : currency,
                    oldCurrencyId : oldCurrencyId,
                    estimatedValue : estimatedValue
                }
            }).then(function (successResponse){
                $scope.npi.rfq_material_cost_value = parseFloat(successResponse.data["materialCost"].toFixed(3));
                $scope.npi.material_cost_value = parseFloat(successResponse.data["materialCostValue"].toFixed(3));
                $scope.npi.labour = parseFloat(successResponse.data["npiLabour"].toFixed(3));
                $scope.npi.rfq_labour_value = parseFloat(successResponse.data["rfqLabourValue"].toFixed(3));
                $scope.npi.admin_value = parseFloat(successResponse.data[ "adminValue"].toFixed(3));
                $scope.npi.freight_value = parseFloat(successResponse.data[ "freightValue" ].toFixed(3));
                $scope.npi.profit_value_factory = parseFloat(successResponse.data["profitValueFactory"].toFixed(3));
                $scope.npi.rfq_selling_price_factory = parseFloat(successResponse.data[ "rfqSellingPriceFactory"].toFixed(3));
                $scope.npi.rfq_profit_value_sales_office = parseFloat(successResponse.data["rfqProfitValuesSalesOffice"].toFixed(3));
                $scope.npi.rfq_selling_price_sales_office = parseFloat(successResponse.data["rfqSellingPriceSalesOffice"].toFixed(3));

            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    };

    $scope.newSaleCurrencyChange = function(currency, oldCurrencyId, estimatedValue){
        if(currency !== undefined && currency !== oldCurrencyId){
            $http({
                method : 'POST',
                url : 'api/currencies/newSaleConvert',
                data : {
                    currencyId : currency,
                    oldCurrencyId : oldCurrencyId,
                    estimatedValue : estimatedValue
                }
            }).then(function (successResponse){
                $scope.lmeAl = parseFloat(successResponse.data["lmeAl"].toFixed(3));
                $scope.lmeCu = parseFloat(successResponse.data["lmeCu"].toFixed(3));
                $scope.generateSale.lme_al = parseFloat($scope.lmeAl.toFixed(3));
                $scope.generateSale.lme_cu = parseFloat($scope.lmeCu.toFixed(3));
                $scope.generateSale.lme_al_at_creation = parseFloat($scope.lmeAl.toFixed(3));
                $scope.generateSale.lme_cu_at_creation = parseFloat($scope.lmeCu.toFixed(3));
                $scope.generateSale.buy_price_engineering = parseFloat(successResponse.data["buyPriceEngineering" ].toFixed(3));
                $scope.generateSale.buy_price_factory = parseFloat(successResponse.data["buyPriceFactory"].toFixed(3));
                $scope.generateSale.buy_price = parseFloat(successResponse.data["buyPrice"].toFixed(3));
                $scope.generateSale.tamura_entity_fob_charge = parseFloat(successResponse.data["fobCharge"].toFixed(3));
                $scope.generateSale.tamura_entity_fob_charge_value = parseFloat(successResponse.data["fobChargeValue" ].toFixed(3));
                $scope.generateSale.tamura_entity_freight = parseFloat(successResponse.data["freight"].toFixed(3));
                $scope.generateSale.tamura_entity_freight_value = parseFloat(successResponse.data["freightValue"].toFixed(3));
                $scope.generateSale.cif_cost = parseFloat(successResponse.data["cifCost"].toFixed(3));
                $scope.generateSale.tamura_entity_terminal_handling = parseFloat(successResponse.data["terminalHandling"].toFixed(3));
                $scope.generateSale.tamura_entity_terminal_handling_value = parseFloat(successResponse.data["terminalHandlingValue"].toFixed(3));
                $scope.generateSale.tamura_entity_delivery_order = parseFloat(successResponse.data["deliveryOrder"].toFixed(3));
                $scope.generateSale.tamura_entity_delivery_order_value = parseFloat(successResponse.data["deliveryOrderValue"].toFixed(3));
                $scope.generateSale.tamura_entity_customs_clearance = parseFloat(successResponse.data["customerClearance"].toFixed(3));
                $scope.generateSale.tamura_entity_customs_clearance_value = parseFloat(successResponse.data["customerClearanceValue"].toFixed(3));
                $scope.generateSale.advance_fees_value = parseFloat(successResponse.data["advanceFeesValue"].toFixed(3));
                $scope.generateSale.lfr_value = parseFloat(successResponse.data["lfrValue"].toFixed(3));
                $scope.generateSale.tamura_entity_haulage_to_warehouse = parseFloat(successResponse.data["haulageWarehouse"].toFixed(3));
                $scope.generateSale.tamura_entity_haulage_to_warehouse_value = parseFloat(successResponse.data["haulageWarehouseValue"].toFixed(3));
                $scope.generateSale.total_inland_charges_value = parseFloat(successResponse.data["totalInlandChargesValue"].toFixed(3));
                $scope.generateSale.unloading_value = parseFloat(successResponse.data["unloadingValue"].toFixed(3));
                $scope.generateSale.order_picking_value = parseFloat(successResponse.data["orderPickingValue"].toFixed(3));
                $scope.generateSale.storage_value = parseFloat(successResponse.data["storageValue"].toFixed(3));
                $scope.generateSale.admin_value = parseFloat(successResponse.data["adminValue"].toFixed(3));
                $scope.generateSale.total_hub_costs = parseFloat(successResponse.data["totalHubCosts"].toFixed(3));
                $scope.generateSale.total_oversea_costs_value = parseFloat(successResponse.data["totalOverseaCostsValue"].toFixed(3));
                $scope.generateSale.import_duty_value = parseFloat(successResponse.data["totalOverseaCostsValue"].toFixed(3));
                $scope.generateSale.delivery_to_customer = parseFloat(successResponse.data["deliveryToCustomer"].toFixed(3));
                $scope.generateSale.delivery_to_customer_value = parseFloat(successResponse.data["deliveryToCustomerValue"].toFixed(3));
                $scope.generateSale.cost_price = parseFloat(successResponse.data["costPrice"].toFixed(3));
                $scope.generateSale.gand_a_value = parseFloat(successResponse.data["gandaValue"].toFixed(3));
                $scope.generateSale.total_cost = parseFloat(successResponse.data["totalCost"].toFixed(3));
                $scope.generateSale.profit_value = parseFloat(successResponse.data["profitValue"].toFixed(3));
                $scope.generateSale.selling_price = parseFloat(successResponse.data["sellingPrice"].toFixed(3));
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    };

    $scope.rfqMaterialCurrencyChange = function(currency, oldCurrencyId)
    {
        if(currency !== undefined && currency.id !== oldCurrencyId){

            $scope.rfq.totalMaterialsValue = 0.0;

            angular.forEach($scope.rfqMaterialsGridOptions.data, function(value, index){
                $http({
                    method : 'POST',
                    url : 'api/currencies/convert',
                    data : {
                        currencyId : currency.id,
                        oldCurrencyId : oldCurrencyId,
                        estimatedValue : value.unitary_price
                    }
                }).then(function (successResponse){
                    $scope.rfqMaterialsGridOptions.data[index].unitary_price = successResponse.data;
                    $scope.rfqMaterialsGridOptions.data[index].total_price = successResponse.data * $scope.rfqMaterialsGridOptions.data[index].qty;

                    // re-count total materials value after currency change
                    $scope.rfq.totalMaterialsValue += successResponse.data * $scope.rfqMaterialsGridOptions.data[index].qty;

                }, function (errorResponse) {
                    // error callback
                    console.log(errorResponse);
                });
            });

            $scope.oldRfqMaterialCurrency = currency;

        }

    };

    $scope.rfqComCurrencyChange = function(currency, oldCurrencyId, estimatedPrice, targetPrice)
    {
        if(currency !== undefined && currency.id !== oldCurrencyId && $scope.rfqEditing === true){
            $scope.rfq.com_currency_id = currency.id;

            $http({
                method : 'POST',
                url : 'api/currencies/convertRfqComValues',
                data : {
                    currencyId : currency.id,
                    oldCurrencyId : oldCurrencyId,
                    estimatedPrice : estimatedPrice,
                    targetPrice : targetPrice
                }
            }).then(function (successResponse){
                $scope.rfq.com_estimated_price = successResponse.data.estimated_price;
                $scope.rfq.com_target_price = successResponse.data.target_price;
                $scope.computeRfqAnnualValue();
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    };

    $scope.estimatedEXWCurrencyChange = function(currency,EXWnumber)
    {
        var allCurrenciesToConvert;
        switch (EXWnumber){
            case 1:
                allCurrenciesToConvert = {
                    "materialCost" : $scope.rfq.estimated_factory_cost.rfq_material_factory1,
                    "materialCostValue" : $scope.rfq.estimated_factory_cost.rfq_material_factory1,
                    "npiLabour" : $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1,
                    "rfqLabourValue" : $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1,
                    "adminValue" :$scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1,
                    "freightValue" : $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1,
                    "profitValueFactory" : $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory1,
                    "rfqSellingPriceFactory" : $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1,
                    "rfqProfitValuesSalesOffice" : $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1,
                    "rfqSellingPriceSalesOffice" : $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office1
                };
                EXWCurrencyChange(currency.id, $scope.estimatedEXW1OldCurrency.id, allCurrenciesToConvert,EXWnumber);
                $scope.estimatedEXW1OldCurrency = currency;
                break;
            case 2:
                allCurrenciesToConvert = {
                    "materialCost" : $scope.rfq.estimated_factory_cost.rfq_material_factory2,
                    "materialCostValue" : $scope.rfq.estimated_factory_cost.rfq_material_factory2,
                    "npiLabour" : $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2,
                    "rfqLabourValue" : $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2,
                    "adminValue" :$scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2,
                    "freightValue" : $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2,
                    "profitValueFactory" : $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory2,
                    "rfqSellingPriceFactory" : $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2,
                    "rfqProfitValuesSalesOffice" : $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2,
                    "rfqSellingPriceSalesOffice" : $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office2
                };
                EXWCurrencyChange(currency.id, $scope.estimatedEXW2OldCurrency.id, allCurrenciesToConvert,EXWnumber);
                $scope.estimatedEXW2OldCurrency = currency;
                break;
            case 3:
                allCurrenciesToConvert = {
                    "materialCost" : $scope.rfq.estimated_factory_cost.rfq_material_factory3,
                    "materialCostValue" : $scope.rfq.estimated_factory_cost.rfq_material_factory3,
                    "npiLabour" : $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3,
                    "rfqLabourValue" : $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3,
                    "adminValue" :$scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3,
                    "freightValue" : $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3,
                    "profitValueFactory" : $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory3,
                    "rfqSellingPriceFactory" : $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3,
                    "rfqProfitValuesSalesOffice" : $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3,
                    "rfqSellingPriceSalesOffice" : $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office3
                };
                EXWCurrencyChange(currency.id, $scope.estimatedEXW3OldCurrency.id, allCurrenciesToConvert,EXWnumber);
                $scope.estimatedEXW3OldCurrency = currency;
                break;
            default: break;
        }
    };

    var EXWCurrencyChange = function(currency, oldCurrencyId, estimatedValue,EXWnumber)
    {
        if(currency !== undefined && currency !== oldCurrencyId){
            $http({
                method : 'POST',
                url : 'api/currencies/npiConvert',
                data : {
                    currencyId : currency,
                    oldCurrencyId : oldCurrencyId,
                    estimatedValue : estimatedValue
                }
            }).then(function (successResponse){
                switch (EXWnumber)
                {
                    case 1:
                        $scope.rfq.estimated_factory_cost.rfq_material_factory1 = parseFloat(successResponse.data["materialCost"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory1 = parseFloat(successResponse.data["npiLabour"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory1 = parseFloat(successResponse.data["rfqLabourValue"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory1 = parseFloat(successResponse.data[ "adminValue"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory1 = parseFloat(successResponse.data[ "freightValue" ].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory1 = parseFloat(successResponse.data["profitValueFactory"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1 = parseFloat(successResponse.data[ "rfqSellingPriceFactory"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1 = parseFloat(successResponse.data["rfqProfitValuesSalesOffice"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office1 = parseFloat(successResponse.data["rfqSellingPriceSalesOffice"].toFixed(3));
                        break;
                    case 2:
                        $scope.rfq.estimated_factory_cost.rfq_material_factory2 = parseFloat(successResponse.data["materialCost"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory2 = parseFloat(successResponse.data["npiLabour"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory2 = parseFloat(successResponse.data["rfqLabourValue"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory2 = parseFloat(successResponse.data[ "adminValue"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory2 = parseFloat(successResponse.data[ "freightValue" ].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory2 = parseFloat(successResponse.data["profitValueFactory"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2 = parseFloat(successResponse.data[ "rfqSellingPriceFactory"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2 = parseFloat(successResponse.data["rfqProfitValuesSalesOffice"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office2 = parseFloat(successResponse.data["rfqSellingPriceSalesOffice"].toFixed(3));
                        break;
                    case 3:
                        $scope.rfq.estimated_factory_cost.rfq_material_factory3 = parseFloat(successResponse.data["materialCost"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_factory3 = parseFloat(successResponse.data["npiLabour"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_labour_rate_value_factory3 = parseFloat(successResponse.data["rfqLabourValue"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_admin_value_factory3 = parseFloat(successResponse.data[ "adminValue"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_freight_value_factory3 = parseFloat(successResponse.data[ "freightValue" ].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_factory3 = parseFloat(successResponse.data["profitValueFactory"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3 = parseFloat(successResponse.data[ "rfqSellingPriceFactory"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3 = parseFloat(successResponse.data["rfqProfitValuesSalesOffice"].toFixed(3));
                        $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_sales_office3 = parseFloat(successResponse.data["rfqSellingPriceSalesOffice"].toFixed(3));
                        break;
                    default: break;

                }
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    };


    $scope.manageSpecification = function(ev)
    {
        $mdDialog.show({
            controller: 'RfqSpecificationDialogCtrl',
            templateUrl: 'views/components/rfqSpecificationDialog/rfqSpecificationDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                rfq : $scope.rfq
            }
        })
        .then(function(res) {
            console.log("res");
        }, function() {

        });
    };

    $scope.addRfqMaterial = function(ev)
    {
        $mdDialog.show({
            controller: 'NewRfqMaterialDialogCtrl',
            templateUrl: 'views/components/rfqMaterialDialog/newRfqMaterialDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                rfq : $scope.rfq
            }
        })
        .then(function(res) {
            console.log("res");
        }, function() {
            refreshRfqMaterials();
        });
    };

    $scope.editSelectedRfqMaterial = function (ev)
    {
        var selectedRfqMaterials = $scope.rfqMaterialsGridApi.selection.getSelectedRows();

        if(selectedRfqMaterials.length === 1){

            $mdDialog.show({
                controller: 'EditRfqMaterialDialogCtrl',
                templateUrl: 'views/components/rfqMaterialDialog/editRfqMaterialDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    rfqMaterial : selectedRfqMaterials[0]
                }
            })
            .then(function(res) {
                console.log("res");
            }, function() {
                refreshRfqMaterials();
            });

        }else{
            Flash.clear();
            $translate(["data-sheet.select-exactly-one-material"]).then(function (translations) {
                Flash.create('danger', translations["data-sheet.select-exactly-one-material"], 0, {}, true);
            });
        }
    };

    $scope.deleteSelectedRfqMaterial = function ()
    {
        var selectedRfqMaterials = $scope.rfqMaterialsGridApi.selection.getSelectedRows();

        if(selectedRfqMaterials.length === 1){

            $http({
                method : 'POST',
                url : 'api/rfqMaterial/delete',
                data : {
                    id : selectedRfqMaterials[0].id
                }
            }).then(function (successResponse){
                $rootScope.loading = false;
                $scope.rfqMaterialsGridOptions.data = $scope.rfqMaterialsGridOptions.data.filter(function( obj ) {
                    return obj.id !== selectedRfqMaterials[0].id;
                });
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });

        }else{
            Flash.clear();
            $translate(["data-sheet.select-exactly-one-material"]).then(function (translations) {
                Flash.create('danger', translations["data-sheet.select-exactly-one-material"], 0, {}, true);
            });
        }
    };

    $scope.addRfqProduction = function(ev){

        $mdDialog.show({
            controller: 'addRfqProductionCtrl',
            templateUrl: 'views/components/rfqProduction/addRfqProduction.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                "rfqId" : $scope.rfq.id
            }
        })
            .then(function(res) {
                getProduction($scope.rfq.id);

            }, function() {
                getProduction($scope.rfq.id);
            });

    };

    $scope.deleteRfqProduction = function (productionId)
    {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/rfqs/deleteProduction?id='+productionId
        }).then(function (successResponse){
            $rootScope.loading = false;
            getProduction($scope.rfq.id);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.editRfqProduction = function (ev,productionId) {
        $mdDialog.show({
            controller: 'editRfqProductionCtrl',
            templateUrl: 'views/components/rfqProduction/editRfqProduction.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                "productionId" : productionId
            }
        })
            .then(function(res) {
                getProduction($scope.rfq.id);

            }, function() {
                getProduction($scope.rfq.id);
            });
    };

    $scope.updateComment = function(comment){
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/npiUpdateComment/updateComment',
            data : {
                id : $scope.npi.id,
                comment : comment
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse.data);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };
    function checkNPI (NPINumber){
        switch (NPINumber){
            case 1:
                $scope.rfq.estimated_factory_cost.rfq_material_factory1 = parseFloat(((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue) + (($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue) + $scope.rfq.totalMaterialsValue).toFixed(3));
                if ($scope.rfq.estimated_factory_cost.factory1 == null || $scope.rfq.estimated_factory_cost.currency_factory1 == null){
                    return 1;
                }
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.rfq_material_factory2 = parseFloat(((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue) + (($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue) + $scope.rfq.totalMaterialsValue).toFixed(3));
                if ($scope.rfq.estimated_factory_cost.factory2 == null || $scope.rfq.estimated_factory_cost.currency_factory2 == null){
                    return 2;
                }
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.rfq_material_factory3 = parseFloat(((($scope.rfq.product_other_materials / 100) * $scope.rfq.totalMaterialsValue) + (($scope.rfq.product_handling_scrap / 100) * $scope.rfq.totalMaterialsValue) + $scope.rfq.totalMaterialsValue).toFixed(3));
                if ($scope.rfq.estimated_factory_cost.factory3 == null || $scope.rfq.estimated_factory_cost.currency_factory3 == null){
                    return 3;
                }
                break;
            default:
                return 0;
                break;
        }
        return 0;
    }
    $scope.generateNpi = function (NPINumber){
        $rootScope.loading = true;
        console.log("generating NPI"+NPINumber);
        var NPIok = checkNPI(NPINumber);
        if (NPIok != 0){
            alert("Please fill Factory "+NPIok+" or Currency "+NPIok);
            $rootScope.loading = false;
            return false;
        }
        var customerAddress = null;
        if ($scope.rfq.project.id != null){
            for (var i = 0; i < $scope.allCustomersNPI.length ; i ++) {
                if ($scope.allCustomersNPI[i].id == $scope.rfq.project.customer_id){
                    customerAddress = $scope.allCustomersNPI[i].address;
                    break;
                }
            }
        }
        $http({
            method : 'POST',
            url : 'api/rfq/generateNPI',
            data : {
                rfq : $scope.rfq,
                NPINumber : NPINumber,
                userName : ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                customerAddress : customerAddress
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            var npi = successResponse.data;
            console.log(npi);
            getData();
            $scope.viewNpi(npi);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.copyEngineeringCostsToMaterial = function (){
        $scope.npi.material_cost_value = $scope.npi.rfq_material_cost_value;
        $scope.npi.labour = $scope.npi.rfq_labour;
        $scope.npi.labour_time = $scope.npi.rfq_labour_time;
        $scope.npi.labour_value = $scope.npi.rfq_labour_value;
        $scope.npi.admin = $scope.npi.rfq_admin;
        $scope.npi.admin_value = $scope.npi.rfq_admin_value;
        $scope.npi.freight = $scope.npi.rfq_freight;
        $scope.npi.freight_value = $scope.npi.rfq_freight_value;
        $scope.npi.profit_factory = $scope.npi.rfq_profit_factory;
        $scope.npi.profit_value_factory = $scope.npi.rfq_profit_value_factory;
        $scope.npi.selling_price_factory = $scope.npi.rfq_selling_price_factory;
        $scope.npi.profit_sales_office = $scope.npi.rfq_profit_sales_office;
        $scope.npi.profit_value_sales_office = $scope.npi.rfq_profit_value_sales_office;
        $scope.npi.selling_price_sales_office = $scope.npi.rfq_selling_price_sales_office;
    };

    $scope.changeSellingPriceFactory = function(number,price){
        switch (number) {
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1 = price;
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2 = price;
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3 = price;
                break;
            default:
                break;
        }
    };

    $scope.profitValueSaleOfficeUpdate = function(number,profit){
        switch (number){
            case 1:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office1 = profit;
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office1 = $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office1 * $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory1;
                break;
            case 2:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office2 = profit;
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office2 = $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office2 * $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory2;
                break;
            case 3:
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office3 = profit;
                $scope.rfq.estimated_factory_cost.tamura_entity_profit_value_sales_office3 = $scope.rfq.estimated_factory_cost.tamura_entity_profit_sales_office3 * $scope.rfq.estimated_factory_cost.tamura_entity_selling_price_factory3;
                break;
            default:
                break;
        }
    };

    function getRfqStatusLogs(rfqId)
    {

        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/rfqs/statusLogs?id='+rfqId
        }).then(function (successResponse){
            $scope.rfqStatusLogs = successResponse.data;
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    }
    
    function getData()
    {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/projects/fetch?projects=' + JSON.stringify($rootScope.search.projects)
                    + '&rfqs=' + JSON.stringify($rootScope.search.rfqs)
                    + '&sales=' + JSON.stringify($rootScope.search.sales)
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.projects = successResponse.data;
            checkProjectsStatuses($scope.projects);
            $scope.createAllExports($scope.projects);
            // console.log($scope.projects);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });   
    }

    function checkProjectsStatuses(projects){
        var t30 = "T30",dead ="DEAD",rej = "REJECTED";
        var closed = "CLOSED" , opened = "OPENED";
        projects.forEach(function (project) {
            var closedProject = false;
            if(project.rfqs.length > 0){
                for(var i = 0; i < project.rfqs.length; i++){
                    var rfq = project.rfqs[i];
                    if(rfq.status != undefined && (rfq.status == t30 || rfq.status == dead || rfq.status == rej)){
                        closedProject = true;
                    }
                    else{
                        closedProject = false;
                        break;
                    }
                }
                if(closedProject){
                    if (project.status != closed){
                        project.status = closed;
                        $http({
                            method: 'POST',
                            url: 'api/projects/update',
                            data: {
                                id: project.id,
                                name: project.name,
                                status: project.status,
                                marketId: project.market !== null ? project.market.no_entity : null,
                                standards: project.standards,
                                description: project.description,
                                remarks: project.remarks,
                                estimatedValue: project.estimated_value,
                                estimatedValueCurrencyId: project.estimated_value_currency !== null ? project.estimated_value_currency.id : null,
                                itemToDesign: project.item_to_design,
                                customerId: project.customer !== null ? project.customer.id : null,
                                endCustomer: project.end_customer,
                                customerPurchaseManagerId: project.purchase_manager !== null ? project.purchase_manager.id : null,
                                managerId: project.manager !== null ? project.manager.id : null,
                                businessLocation: project.business_location !== null ? project.business_location.iso2 : null,
                                lastModifiedBy: ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                                life: project.life,
                                spotOrder: project.spot_order,
                                managerPhone : project.manager !== null ?  project.manager.phone : null,
                                managerMobile : project.manager !== null ?  project.manager.mobile : null,
                                purchaseManager :  project.purchase_manager
                            }
                        }).then(function (successResponse) {
                        }, function (errorResponse) {
                            console.log(errorResponse);
                        });
                    }
                }
                else{
                    if (project.status != opened){
                        project.status = opened;
                        $http({
                            method: 'POST',
                            url: 'api/projects/update',
                            data: {
                                id: project.id,
                                name: project.name,
                                status: project.status,
                                marketId: project.market !== null ? project.market.no_entity : null,
                                standards: project.standards,
                                description: project.description,
                                remarks: project.remarks,
                                estimatedValue: project.estimated_value,
                                estimatedValueCurrencyId: project.estimated_value_currency !== null ? project.estimated_value_currency.id : null,
                                itemToDesign: project.item_to_design,
                                customerId: project.customer !== null ? project.customer.id : null,
                                endCustomer: project.end_customer,
                                customerPurchaseManagerId: project.purchase_manager !== null ? project.purchase_manager.id : null,
                                managerId: project.manager !== null ? project.manager.id : null,
                                businessLocation: project.business_location !== null ? project.business_location.iso2 : null,
                                lastModifiedBy: ($rootScope.getLoggedUser().username !== null && $rootScope.getLoggedUser().username !== "") ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                                life: project.life,
                                spotOrder: project.spot_order,
                                managerPhone : project.manager !== null ?  project.manager.phone : null,
                                managerMobile : project.manager !== null ?  project.manager.mobile : null,
                                purchaseManager :  project.purchase_manager
                            }
                        }).then(function (successResponse) {
                        }, function (errorResponse) {
                            console.log(errorResponse);
                        });
                    }
                }
            }
        });
    }
        
    function getSalesManagers()
    {
        $http({
            method :'GET',
            url : 'api/persons/salesManagers'
        }).then(function (successResponse){
            $scope.projectManagers = successResponse.data;
            $scope.projectManagersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }
    
    function getCountries()
    {
        $http({
            method :'GET',
            url : 'api/countries/all'
        }).then(function (successResponse){
            $scope.countries = successResponse.data;
            $scope.countriesAll = successResponse.data;
            $scope.deliveryLocations = successResponse.data;
            $scope.deliveryLocationsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });

    }
    
    function getCurrencies()
    {
        $http({
            method :'GET',
            url : 'api/currencies/all'
        }).then(function (successResponse){
            $scope.currencies = successResponse.data;
            $scope.currenciesAll = successResponse.data;
            $scope.rfqCurrencies = successResponse.data;
            $scope.rfqCurrenciesAll = successResponse.data;
            $scope.rfqMaterialCurrencies = successResponse.data;
            $scope.rfqMaterialCurrenciesAll = successResponse.data;

            var defaultCurrency = $scope.rfqMaterialCurrenciesAll.filter(function(obj){
                return obj.default === "X";
            })[0];

            $scope.rfqMaterialCurrency = defaultCurrency;
            $scope.oldRfqMaterialCurrency = defaultCurrency;
            $scope.estimatedEXW1OldCurrency = defaultCurrency;
            $scope.estimatedEXW2OldCurrency = defaultCurrency;
            $scope.estimatedEXW3OldCurrency = defaultCurrency;

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    $scope.changeCurrency = function(npiCurrency){

        var allCurrenciesToConvert = {
            "materialCost" : $scope.npi.rfq_material_cost_value,
            "materialCostValue" : $scope.npi.material_cost_value,
            "npiLabour" : $scope.npi.labour,
            "rfqLabourValue" : $scope.npi.rfq_labour_value,
            "adminValue" : $scope.npi.admin_value,
            "freightValue" : $scope.npi.freight_value,
            "profitValueFactory" : $scope.npi.profit_value_factory,
            "rfqSellingPriceFactory" : $scope.npi.rfq_selling_price_factory,
            "rfqProfitValuesSalesOffice" : $scope.npi.rfq_profit_value_sales_office,
            "rfqSellingPriceSalesOffice" : $scope.npi.rfq_selling_price_sales_office
        };



        for(var i = 0; i < $scope.currenciesShortNames.length;i++){
            if($scope.currenciesShortNames[i] ==  npiCurrency){
                $scope.npi.currency_symbol = $scope.currenciesAll[i].symbol;
                newCurrencyId = $scope.currenciesAll[i].id;
                actualCurrency =  npiCurrency;
                break;
            }
        }
        $scope.npiCurrencyChange(newCurrencyId, oldCurrencyId, allCurrenciesToConvert);
        oldCurrencyId = newCurrencyId;
    };

    $scope.changeSampleTeuPn = function(npi){
        $rootScope.loading = true;
        $http({
            method: 'POST',
            url: 'api/npi/changeSampleTeuPn',
            data: {
                npi: npi
            }
        }).then(function (successResponse) {
            $rootScope.loading = false;
            getData();
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.changeNewSaleCurrency = function (){

        $scope.generateSale.currency_symbol =  $scope.generateSale.currency.symbol;
        actualCurrency =  $scope.generateSale.currency;
        newCurrencyId =  $scope.generateSale.currency.id;


        var allCurrenciesToConvert = {
            "lmeCu" : $scope.generateSale.lme_cu,
            "lmeAl" : $scope.generateSale.lme_al,
            "buyPriceEngineering" : $scope.generateSale.buy_price_engineering,
            "buyPriceFactory" :  $scope.generateSale.buy_price_factory,
            "buyPrice" : $scope.generateSale.buy_price,
            "fobCharge" : $scope.generateSale.tamura_entity_fob_charge,
            "fobChargeValue" : $scope.generateSale.tamura_entity_fob_charge_value,
            "freight" : $scope.generateSale.tamura_entity_freight,
            "freightValue" : $scope.generateSale.tamura_entity_freight_value,
            "cifCost" : $scope.generateSale.cif_cost,
            "terminalHandling" : $scope.generateSale.tamura_entity_terminal_handling,
            "terminalHandlingValue" : $scope.generateSale.tamura_entity_terminal_handling_value,
            "deliveryOrder" : $scope.generateSale.tamura_entity_delivery_order,
            "deliveryOrderValue" : $scope.generateSale.tamura_entity_delivery_order_value,
            "customerClearance" : $scope.generateSale.tamura_entity_customs_clearance,
            "customerClearanceValue" : $scope.generateSale.tamura_entity_customs_clearance_value,
            "advanceFeesValue" : $scope.generateSale.advance_fees_value,
            "lfrValue" : $scope.generateSale.lfr_value,
            "haulageWarehouse" : $scope.generateSale.tamura_entity_haulage_to_warehouse,
            "haulageWarehouseValue" : $scope.generateSale.tamura_entity_haulage_to_warehouse_value,
            "totalInlandChargesValue" : $scope.generateSale.total_inland_charges_value,
            "unloadingValue" : $scope.generateSale.unloading_value,
            "orderPickingValue" : $scope.generateSale.order_picking_value,
            "storageValue" : $scope.generateSale.storage_value,
            "adminValue" : $scope.generateSale.admin_value,
            "totalHubCosts" : $scope.generateSale.total_hub_costs,
            "totalOverseaCostsValue" : $scope.generateSale.total_oversea_costs_value,
            "importDutyValue" : $scope.generateSale.import_duty_value,
            "deliveryToCustomer" : $scope.generateSale.delivery_to_customer,
            "deliveryToCustomerValue" : $scope.generateSale.delivery_to_customer_value,
            "costPrice" : $scope.generateSale.cost_price,
            "gandaValue" : $scope.generateSale.gand_a_value,
            "totalCost" : $scope.generateSale.total_cost,
            "profitValue" : $scope.generateSale.profit_value,
            "sellingPrice" : $scope.generateSale.selling_price
        };
        $scope.newSaleCurrencyChange(newCurrencyId, oldCurrencyId, allCurrenciesToConvert);
        oldCurrencyId = newCurrencyId;
    };

    $scope.customerSearchTextChange = function(text)
    {
        $scope.customers = $scope.customersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.customerSelectedItemChange = function(item)
    {
        $scope.projects.customerPath = (item !== undefined) ? item.path : null;
        $scope.customerSelected = (item !== undefined) ? true : false;
        $scope.purchaser = null;
        if(item !== undefined){
            $scope.projectCustomerSelected = true;
            $scope.project.customer = {
                "path" : $scope.projects.customerPath
            };
            getPurchasers();
            getSalesManagers();
        }else{
            $scope.purchasers = [];
            $scope.purchasersAll = [];
            $scope.customerEngineers = [];
            $scope.customerEngineersAll = [];
        }
    };

    $scope.purchaserSearchTextChange = function(text)
    {
        $scope.purchasers = $scope.purchasersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.purchaserSelectedItemChange = function(item)
    {
        if($scope.loadProjects !== undefined && $scope.loadProjects.purchaser !== null){
            $scope.projects.purchaser = (item !== undefined) ? item : null;
        }else{
            $scope.projects.purchaser = (item !== undefined) ? item : null;
        }
        if ($scope.loadProjects !== undefined )
            $scope.loadProjects.purchaser = null;
    };

    $scope.salesManagerTextChange = function(text)
    {
        $scope.projectManagers = $scope.projectManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.salesManagerSelectedItemChange = function(item)
    {
        if($scope.loadProjects !== undefined && $scope.loadProjects.projectManager !== null){
            $scope.projects.projectManager = (item !== undefined) ? item : null;
        }else{
            $scope.projects.projectManager = (item !== undefined) ? item : null;
        }
        if ($scope.loadProjects !== undefined )
            $scope.loadProjects.purchasprojectManagerer = null;
    };

    $scope.changeGenerateSale = function(){
        $scope.generateSaleShow = true;
        $scope.createNewSale($scope.npi,$scope.rfqByNpi);

    };

    $scope.addCustomerEngineer = function (ev) {

        $mdDialog.show({
            controller: 'AddPersonToDirectoryCtrl',
            templateUrl: 'views/components/addPersonToDirectory/addPersonToDirectory.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                entity : null,
                customerId : $scope.rfq.project.customer_id,
                isEngineer : true
            }
        })
            .then(function(res) {
                console.log("res");
            }, function() {
                refreshCustomerEngineers();
            });

    };

    $scope.createAllExports = function (projects) {
        $scope.allProjectsExport = [];
        $scope.allRFQsExport = [];
        $scope.allNPIsExport = [];
        $scope.allSalesExport = [];
        if(projects.length > 0){
            projects.forEach(function (project) {

                pushProjectExportToArray(project,$scope.allProjectsExport);

                if (project.rfqs.length > 0){
                    project.rfqs.forEach(function (rfq) {

                        pushRfqExportToArray(rfq,$scope.allRFQsExport);

                        if(rfq.npis.length > 0){
                            rfq.npis.forEach(function (npi) {

                                pushNPIExportToArray(npi,$scope.allNPIsExport);

                                if(npi.sales.length > 0){
                                    npi.sales.forEach(function (sale) {

                                        pushSaleExportToArray(sale,$scope.allSalesExport);
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    };

    function refreshCustomerEngineers()
    {
        if($scope.rfqViewed){
            $http({
                method :'GET',
                url : 'api/customers/engineers?customerId='+$scope.rfq.project.customer_id
            }).then(function (successResponse){
                $scope.customerEngineers = successResponse.data;
                $scope.customerEngineersAll = successResponse.data;
                if($scope.customerEngineers.length > 0)
                    $scope.rfq.customer_engineer = $scope.customerEngineers[0];
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    }

    function prepareGenerateSaleVars(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) { dd='0'+dd}
        if(mm<10) {mm='0'+mm}
        today = dd+'/'+mm+'/'+yyyy;
        $scope.lclFcl = ["LCL","FCL"];

        $scope.generateSale.palletQty = 16;
        $scope.generateSale.buyPriceEngineering = $scope.npi.rfq_selling_price_sales_office;
        $scope.generateSale.buyPriceFactory = $scope.npi.rfq_selling_price_sales_office;
        $scope.generateSale.buyPrice = $scope.npi.rfq_selling_price_sales_office;
        $scope.generateSale.creationDate = today;
        $scope.generateSale.createdBy = $scope.npiProject.last_modified_by;
    }

    $scope.viewHideDetails = function(){
        if($scope.viewedDetails){

            $scope.viewedDetails = false;
        }else{

            $scope.viewedDetails = true;
        }

    };

    $scope.computeTotalOverseaFreightCosts = function () {
        document.getElementById('totalUpdated').checked = true;
        document.getElementById('totalUpdatedButton').disabled = false;
        var costConst = 1;
        $scope.generateSale.total_oversea_costs_value = parseFloat(($scope.generateSale.cif_cost / (costConst - ($scope.generateSale.total_oversea_costs / PERCENT))).toFixed(3));

        updateTotalSale();
    };

    $scope.computeTotalOverseaFreightCostsValue = function () {
        document.getElementById('totalUpdated').checked = true;
        document.getElementById('totalUpdatedButton').disabled = false;

        $scope.generateSale.total_oversea_costs = parseFloat((($scope.generateSale.total_oversea_costs_value - $scope.generateSale.cif_cost) / $scope.generateSale.total_oversea_costs_value).toFixed(3));

        updateTotalSale();
    };
    
    function updateTotalSale() {
        var profitConst = 1;
        $scope.generateSale.cost_price = parseFloat(($scope.generateSale.total_oversea_costs_value + $scope.generateSale.delivery_to_customer_value + $scope.generateSale.import_duty_value).toFixed(3));
        $scope.generateSale.cost_price_percentage = parseFloat((($scope.generateSale.cost_price - $scope.generateSale.buy_price) / $scope.generateSale.buy_price).toFixed(3));
        $scope.generateSale.gand_a_value = parseFloat(($scope.generateSale.cost_price * ($scope.generateSale.gand_a / PERCENT)).toFixed(3));
        $scope.generateSale.total_cost = parseFloat(($scope.generateSale.cost_price + $scope.generateSale.gand_a_value).toFixed(3));
        $scope.generateSale.total_cost_percentage = parseFloat((($scope.generateSale.total_cost - $scope.generateSale.buy_price) / $scope.generateSale.buy_price).toFixed(3));
        $scope.generateSale.selling_price = parseFloat(($scope.generateSale.total_cost / (profitConst - ($scope.generateSale.profit / PERCENT) )).toFixed(3));
        $scope.generateSale.profit_value = parseFloat(($scope.generateSale.selling_price - $scope.generateSale.total_cost).toFixed(3));
    }

    function updateTotalSaleOversea() {
        $scope.generateSale.total_inland_charges_value = parseFloat(($scope.generateSale.tamura_entity_terminal_handling_value + $scope.generateSale.tamura_entity_delivery_order_value+
            $scope.generateSale.tamura_entity_customs_clearance_value + $scope.generateSale.advance_fees_value + $scope.generateSale.lfr_value +
            $scope.generateSale.tamura_entity_haulage_to_warehouse_value).toFixed(3));
        switch ($scope.generateSale.incoterms_id){
            case "EXW" :
            case "FCA" :
            case "FOB" :
            case "FSA" :
                $scope.generateSale.total_oversea_costs_value = parseFloat($scope.generateSale.cif_cost.toFixed(3));
                break;
            default:
                $scope.generateSale.total_oversea_costs_value = parseFloat(($scope.generateSale.cif_cost + $scope.generateSale.total_inland_charges_value + $scope.generateSale.total_hub_costs).toFixed(3));
                break;
        }
        $scope.generateSale.total_oversea_costs = parseFloat((( $scope.generateSale.total_oversea_costs_value - $scope.generateSale.cif_cost) /  $scope.generateSale.cif_cost).toFixed(3));
    }

    $scope.computeImportDuty = function () {
        if($scope.generateSale.incoterms_id == "DDP"){
            $scope.generateSale.import_duty_value = parseFloat(($scope.generateSale.cif_cost * ($scope.generateSale.import_duty / PERCENT)).toFixed(3));
            console.log($scope.generateSale.import_duty_value);
        }
        else{
            $scope.generateSale.import_duty_value = 0;
        }
        console.log($scope.generateSale.incoterms_id);
        $scope.generateSale.advance_fees_value = parseFloat(($scope.generateSale.import_duty_value * ($scope.generateSale.advance_fees / PERCENT)).toFixed(3));

        if(!$scope.generateSale.total_oversea_cost_flag){
            updateTotalSaleOversea();
        }
        else{
            updateTotalSale();
        }
    };

    $scope.updateDeliveryCustomer = function () {
        switch ($scope.generateSale.incoterms_id){
            case "DAT" :
            case "DAP" :
            case "DDP" :
                $scope.generateSale.delivery_to_customer_value = parseFloat(($scope.generateSale.delivery_to_customer / $scope.generateSale.pallet_qty).toFixed(3));
                break;
            default:
                $scope.generateSale.delivery_to_customer_value = 0;
                break;
        }
        updateTotalSale();
    };

    $scope.computeGandADuty = function () {
        $scope.generateSale.gand_a_value = parseFloat(($scope.generateSale.cost_price*($scope.generateSale.gand_a / PERCENT)).toFixed(3));
        updateTotalSale();
    };

    $scope.computeSaleProfit = function () {
        var profitConst = 1;
        $scope.generateSale.selling_price = parseFloat(($scope.generateSale.total_cost / (profitConst - ($scope.generateSale.profit / PERCENT))).toFixed(3));
        $scope.generateSale.profit_value =  parseFloat(($scope.generateSale.selling_price - $scope.generateSale.total_cost).toFixed(3));
    };

    $scope.computeSaleSallingPrice = function () {
        $scope.generateSale.profit_value = parseFloat(($scope.generateSale.selling_price - $scope.generateSale.total_cost).toFixed(3));
        $scope.generateSale.profit = parseFloat(($scope.generateSale.profit_value /  $scope.generateSale.selling_price).toFixed(3));
    };

    $scope.allowTotalCompute = function(){
        document.getElementById('totalUpdated').checked = true;
        document.getElementById('totalUpdatedButton').disabled = false;
    };

    $scope.totalUpdateCompute = function () {

        document.getElementById('totalUpdated').checked = false;
        document.getElementById('totalUpdatedButton').disabled = true;

        updateTotalSaleOversea();
        updateTotalSale();
    };

    function formatDate(date) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];


        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    var empty = function(e) {
        switch (e) {
            case "":
            case null:
            case typeof this == "undefined":
                return true;
            default:
                return false;
        }
    };

    $scope.createSampleRequestPdf = function ()
    {
        console.log($scope.npi.sample_incoterms_id);
        var actualDate = formatDate (new Date());
        var month = $scope.npi.creation_date.getMonth( ) + 1;
        if ($scope.npi.sample_price == null) $scope.npi.sample_price = 0.0;
        var creationDate =$scope.npi.creation_date.getDate( ) +'/'  + month + '/' +$scope.npi.creation_date.getFullYear( );
        month = $scope.npi.sample_delivery_date.getMonth( ) + 1;
        var deliveryDate =  $scope.npi.sample_delivery_date.getDate( ) +'/'  + month + '/' +$scope.npi.sample_delivery_date.getFullYear( );
        var manufactured = "";
        if ($scope.npi.sample_manufactured_purchased == 1) manufactured = pdfSampleRequestFormText.manufactured;
        if ($scope.npi.sample_manufactured_purchased == 2) manufactured = pdfSampleRequestFormText.purchased;
        var docDefinition = {
            content: [
                {
                    alignment: 'justify',
                    text: 'SAMPLE REQUEST FORM' + "\n",
                    style: ['header']
                },
                // SALES DEPARTMENT
                {
                    alignment: 'justify',
                    text: pdfSampleRequestFormText.salesDepartment + "\n",
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.enquiryNo + "\n", width: '30%' },
                        {  text: $scope.npi.rfq_id + "\n"},
                        {  text: pdfSampleRequestFormText.requestDate + "\n"},
                        {  text: creationDate + "\n" }
                        ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.salesPersonResponsible + "\n", width: '30%' },
                        {  text: $scope.npiProject.manager.name + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.customerName  + "\n" , width: '30%'},
                        {  text: $scope.npiProject.customer.name + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.customerPartNo + "\n" , width: '30%'},
                        {  text: $scope.rfqByNpi.pn_customer + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.description + "\n" , width: '30%'},
                        {  text: $scope.rfqByNpi.pn_desc + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.quantityRequired + "\n\n", width: '30%' },
                        {  text: $scope.npi.sample_quantity_required + "\n\n"},
                        {  text: pdfSampleRequestFormText.deliveryDate + "\n\n"},
                        {  text: deliveryDate + "\n\n" }
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.contactName + "\n" , width: '30%'},
                        {  text: $scope.npiProject.purchase_manager.name + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.phoneNo + "\n", width: '30%' },
                        {  text: $scope.npiProject.purchase_manager.phone + "\n"},
                        {  text: pdfSampleRequestFormText.mobileNo + "\n"},
                        {  text: $scope.npiProject.purchase_manager.mobile + "\n" }
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.email + "\n" , width: '30%'},
                        {  text: $scope.npiProject.purchase_manager.email + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.address + "\n\n" , width: '30%'},
                        {  text: $scope.npi.sample_customer_address+ "\n\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.chargeable + "\n", width: '30%' },
                        {  text: $scope.npi.sample_chargeable + "\n"},
                        {  text: pdfSampleRequestFormText.currencyPrice + "\n"},
                        {  text: $scope.npi.currency_symbol + " " +$scope.npi.sample_price +"\n" }
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.deliveryTerms + "\n" , width: '30%'},
                        {  text: $scope.npi.sample_incoterms_id + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.customerSpec + "\n" , width: '40%'},
                        {  text: "\n", width: '60%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.specification + "\n" , width: '30%'},
                        {  text: "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.manufacturedPurchased + "\n" , width: '30%'},
                        {  text: manufactured + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.supplier + "\n" , width: '30%'},
                        {  text: pdfSampleRequestFormText.tamuraSupplier + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.buyingPrice + "\n" , width: '30%'},
                        {  text: $scope.npi.currency_symbol + " " + $scope.npi.rfq_selling_price_factory + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.leadTime + "\n" , width: '30%'},
                        {  text: $scope.npi.sample_lead_time + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.tamidReference + "\n\n\n" , width: '30%'},
                        {  text: "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                // ENGINEERING DEPARTMENT
                {
                    alignment: 'justify',
                    text: pdfSampleRequestFormText.engineeringDepartment + "\n",
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    text: pdfSampleRequestFormText.customerSpecFrom + "\n",
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.engineerResponsible + "\n" , width: '30%'},
                        {  text: $scope.npiElectEngineer.name + "\n", width: '70%'}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.engPromiseQuality + "\n", width: '30%' },
                        {  text:  "\n"},
                        {  text: pdfSampleRequestFormText.engPromiseDate + "\n"},
                        {  text: "\n" }
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.teuTopLevelNo + "\n", width: '30%' },
                        {  text: $scope.npi.sample_teu_pn + "\n"}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.comments + "\n\n\n", width: '30%' },
                        {  text: $scope.npi.comment + "\n\n\n"}
                    ],
                    style: ['marginTop']
                },
                // LOGISTICS DEPARTMENT
                {
                    alignment: 'justify',
                    text: pdfSampleRequestFormText.logisticsDepartmnet + "\n",
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.teuContactsNo + "\n", width: '30%' },
                        {  text: "\n"}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    columns: [
                        {  text: pdfSampleRequestFormText.teuPurchaseOrderNo + "\n\n", width: '30%' },
                        {  text:  "\n"}
                    ],
                    style: ['marginTop']
                },
                {
                    alignment: 'justify',
                    text: actualDate + "\n",
                    style: ['marginTop']
                }
            ],
            styles: {
                table: {
                    width: '100%'
                },
                marginTop: {
                    marginTop: 5
                },
                header: {
                    fontSize: 18,
                    bold: true,
                    alignment: 'center',
                    marginBottom: 30
                }
            }
        };
        pdfMake.createPdf(docDefinition).open();
    };


    function getCustomerEngineers()
    { 
        if($scope.rfqViewed){
            $http({
                method :'GET',
                url : 'api/customers/engineers?customerId='+$scope.rfq.project.customer_id
            }).then(function (successResponse){
                $scope.customerEngineers = successResponse.data;
                $scope.customerEngineersAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });   
        }
    }
        
    function getDesignOffices()
    { 
        if($scope.rfqViewed){
            $http({
                method :'GET',
                url : 'api/tamuraCodifications/designOffices'
            }).then(function (successResponse){
                $scope.designOffices = successResponse.data;
                $scope.designOfficesAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });   
        }
    }

    function getRfqDeadReasons()
    {
        $http({
            method :'GET',
            url : 'api/rfqs/deadReasons'
        }).then(function (successResponse){
            $scope.rfqDeadReasons = successResponse.data;
            $scope.rfqDeadReasonsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getEngineers()
    {
        $http({
            method :'GET',
            url : 'api/persons/engineers'
        }).then(function (successResponse){
            $scope.electricalEngineers = successResponse.data;
            $scope.electricalEngineersAll = successResponse.data;
            $scope.mechanicalEngineers = successResponse.data;
            $scope.mechanicalEngineersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getLME() {
        $http({
            method :'GET',
            url : 'api/lme/getAll'
        }).then(function (successResponse){
            $scope.lmeAl = successResponse.data[0].al;
            $scope.lmeCu = successResponse.data[0].cu;
            $scope.generateSale.lme_al = $scope.lmeAl;
            $scope.generateSale.lme_cu = $scope.lmeCu;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getCategories()
    {
        $http({
            method :'GET',
            url : 'api/categories/categories'
        }).then(function (successResponse){
            $scope.categories = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getSalesOffices() {
        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/tamuraEntities/salesOffices'
        }).then(function (successResponse){
            $scope.salesOffices = [];
            for(var i = 0; i < successResponse.data.length ; i++){
                var office = {
                    "id" : successResponse.data[i].id,
                    "name" :  successResponse.data[i].country.name +" - " + successResponse.data[i].short_name + " - " +successResponse.data[i].name
                };
                $scope.salesOffices.push(office);
            }
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getCustomers()
    {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/customers/all?deactivated=' + false
        }).then(function (successResponse){
            // ITERATION THROUGH CUSTOMERS WHICH TYPE IS NOT GROUP
            for(var i = 0; i < successResponse.data.length ; i++){
                if(successResponse.data[i].type != CUSTOMER_TYPE_GROUP) {
                    var groupName = "";
                    // FIND GROUP NAME ADDED TO CUSTOMER NAME
                    for(var j = 0; j < successResponse.data.length ; j++){
                        if(successResponse.data[i].id_root == successResponse.data[j].id ){
                            groupName = successResponse.data[j].name;
                            break;
                        }
                    }
                    // CREATE CUSTOMER LIST -> ID, GROUP NAME AND NAME
                    var customer = {
                        "id": successResponse.data[i].id,
                        "name": groupName + " / " + successResponse.data[i].name
                    };
                    $scope.allCustomers.push(customer);
                }
            }
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    function getPersonById(id)
    {
        $http({
            method :'GET',
            url : 'api/persons/getPerson?id='+id
        }).then(function (successResponse){
            $scope.npiElectEngineer = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }



    function getProjectById(projectId)
    {
        $http({
            method :'GET',
            url : 'api/projects/get?id=' + projectId
        }).then(function (successResponse){
            $scope.npiProject = successResponse.data;
            console.log($scope.npiProject);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getSpecifications(rfqId)
    {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/rfqs/specifications?id=' + rfqId
        }).then(function (successResponse){

            $scope.specifications = successResponse.data;

            var activeDocs = 0;
            var totalDocs = $scope.specifications.length;
           // var masterRecorded = "";

            $translate(['data-sheet.not']).then(function(translations) {
                masterRecorded = translations["data-sheet.not"];
            });

            angular.forEach($scope.specifications, function(value, key){
                if(value.status === 1){
                    activeDocs++;
                }
                if(value.master === 1) {
                    masterRecorded = "";
                }
            });

            $translate('data-sheet.specifications-status-text', {'activeDocs': activeDocs, 'totalDocs' : totalDocs, 'masterRecorded' : masterRecorded}).then(function(text) {
                // consume text
                $scope.specificationsStatusText = text;
            });

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getProduction(id) {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/rfqs/productions?id=' + id
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.rfq.rfqProductions = successResponse.data;
        }, function (errorResponse) {

            console.log(errorResponse);
        });
    }

    function refreshRfqMaterials()
    {
        $http({
            method :'GET',
            url : 'api/rfqMaterial/get?rfqId='+$scope.rfq.id
        }).then(function (successResponse){
            $scope.rfqMaterialsGridOptions.data = successResponse.data;

            // re-pre-count total materials value
            $scope.totalMaterialsValue = 0.0;
            angular.forEach(successResponse.data, function(value){
                $scope.totalMaterialsValue += value.total_price;
            });

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getFactories()
    {
        $http({
            method :'GET',
            url : 'api/tamuraEntities/factories'
        }).then(function (successResponse){
            $scope.factories = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getIncoterms(incotermId) {
        $http({
            method :'GET',
            url : 'api/incoterms/getAll'
        }).then(function (successResponse){
            $scope.allIncoterms = successResponse.data;
            if ($scope.incotermsFormated.length <= 0) {
                for (var i = 0; i < $scope.allIncoterms.length; i++) {
                    var incot = $scope.allIncoterms[i].short + " - " + $scope.allIncoterms[i].label;
                    var id = $scope.allIncoterms[i].short;
                    $scope.incotermsFormated.push(incot);
                }
            }
            for(var i = 0; i < $scope.allIncoterms.length;i++) {
                if (incotermId == $scope.allIncoterms[i].short) {
                    var incot = $scope.allIncoterms[i].short + " - " + $scope.allIncoterms[i].label;
                    $scope.npi.sample_incoterms_id = incot;
                    break;
                }
            }
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getSaleIncoterms(incotermId) {
        $http({
            method :'GET',
            url : 'api/incoterms/getAll'
        }).then(function (successResponse){
            $scope.allIncoterms = successResponse.data;
            if ($scope.incotermsFormated.length <= 0) {
                for (var i = 0; i < $scope.allIncoterms.length; i++) {
                    var incot = $scope.allIncoterms[i].short + " - " + $scope.allIncoterms[i].label;
                    var id = $scope.allIncoterms[i].short;
                    $scope.incotermsFormated.push(incot);
                }
            }
            for(var i = 0; i < $scope.allIncoterms.length;i++) {
                if (incotermId == $scope.allIncoterms[i].short) {
                    var incot = $scope.allIncoterms[i].short + " - " + $scope.allIncoterms[i].label;
                    if(incot == DPP){
                        $scope.incotermDisable = true;
                        $scope.incotermDisableDPP = true;
                    }
                    else if(incot == DAP || incot == DAT){
                        $scope.incotermDisable = true;
                        $scope.incotermDisableDPP = false;
                    }
                    else{
                        $scope.incotermDisable = false;
                        $scope.incotermDisableDPP = false;
                    }
                    $scope.generateSale.sample_incoterms_id = incot;
                    break;
                }
            }
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getRfqForNpi(rfqId) {
        $http({
            method :'GET',
            url : 'api/rfqs/getRfqForNpi?rfqId=' + rfqId
        }).then(function (successResponse){
            $scope.rfqByNpi = successResponse.data;
            getProjectById($scope.rfqByNpi.project_id);
            getPersonById($scope.rfqByNpi.tamura_elec_engineer_id);
            $scope.rfqByNpi.totalMaterialsValue = 0;
            angular.forEach($scope.rfqByNpi.materials, function (value) {
                $scope.rfqByNpi.totalMaterialsValue += value.total_price;
            });

            if($scope.rfqByNpi.product_other_materials != null && $scope.rfqByNpi.totalMaterialsValue != 0 && $scope.rfqByNpi.product_handling_scrap != null)
                $scope.materialCost = parseFloat(((($scope.rfqByNpi.product_other_materials / 100) * $scope.rfqByNpi.totalMaterialsValue) +
                (($scope.rfqByNpi.product_handling_scrap / 100) * $scope.rfqByNpi.totalMaterialsValue) + $scope.rfqByNpi.totalMaterialsValue).toFixed(3));
            else
                $scope.materialCost = null;
            console.log($scope.rfqByNpi );
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }
    function getCustomersCompanies()
    {
        $http({
            method :'GET',
            url : 'api/customers/companies'
        }).then(function (successResponse){
            $scope.customers = successResponse.data;
            $scope.customersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getPurchasers()
    {
        if($scope.projectCustomerSelected){
            $http({
                method :'GET',
                url : 'api/customers/purchasers?customerPath='+$scope.project.customer.path
            }).then(function (successResponse){
                $scope.purchasers = successResponse.data;
                $scope.purchasersAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    }

    function getSalesByNpi(npiId)
    {
        $http({
            method :'GET',
            url : 'api/sale/getSalesByNPI?NPIid='+npiId
        }).then(function (successResponse){
            $scope.saleByNpi = successResponse.data[0];
            prepareGenerateSaleVars();
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });

    }

    function getMarketParents()
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/parentMarketEntities'
        }).then(function (successResponse){
            $scope.marketParents = successResponse.data;
            $scope.marketParentsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getMarketChildren()
    {
        if($scope.projectMarketParentSelected){
            $http({
                method :'GET',
                url : 'api/tamuraCodifications/childMarketEntities?parent='+$scope.project.marketParent.entity
            }).then(function (successResponse){
                $scope.marketChildren = successResponse.data;
                $scope.marketChildrenAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });
        }
    }
    function getAllCustomers(){
        $http({
            method : 'GET',
            url : 'api/customers/all?deactivated=' + false
        }).then(function (successResponse){
            $scope.allCustomersNPI = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    // fire
    getData();
    getSalesManagers();
    getCountries();
    getCurrencies();
    getIncoterms();
    getCustomerEngineers();
    getDesignOffices();
    getRfqDeadReasons();
    getEngineers();
    getCategories();
    getCustomersCompanies();
    getMarketParents();
    getProductParents();
    getAllCustomers();

});
