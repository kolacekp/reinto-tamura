tamuraApp.controller("StdItemsMgtCtrl", function ($scope, $http, $rootScope, $mdDialog) {
    
    $rootScope.activeTopMenu = "std-items-mgt";

    $scope.stdItems = [];

    $scope.stdItemsGridOptions = {
        //headerTemplate: 'header-template.html',
        showGridFooter : true,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 30,
        data: [],
        columnDefs : [
            {name : 'id', visible : false},
            {name : 'category', displayName : 'Category'},
            {name : 'design_office', displayName : 'Design Office'},
            {name : 'factory', displayName : 'Factory'},
            {name : 'sales_office', displayName : 'Sales Office'},
            {name : 'tam_id', displayName : 'Tam ID'},
            {name : 'part_number', displayName : 'Part Number'},
            {name : 'description', displayName : 'Description'},
            {name : 'box_quantity', displayName : 'Box Quantity'},
            {name : 'carton_quantity', displayName : 'Carton Quantity'},
            {name : 'pallet_quantity', displayName : 'Pallet Quantity'},
            {name : 'unit_weight', displayName : 'Unit Weight'},
            {name : 'buy_price_fca_hk', displayName : 'Buy Price FCA HK'},
            {name : 'comment', displayName : 'Comment'}

        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.deleteSelected = function()
    {
        angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
            console.log(data);
            $http({
                method :'POST',
                url : 'api/stdItems/delete',
                data : {
                    stdItemId : data.id
                }
            }).then(function (successResponse){
                console.log(successResponse.data);
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });

            $scope.stdItemsGridOptions.data.splice($scope.stdItemsGridOptions.data.lastIndexOf(data), 1);
        });
    };

    $scope.saveRow = function(rowEntity)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/stdItems/update',
            data : {
                stdItem : rowEntity
            }
        }).then(function (successResponse){

            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.addStdItem = function(ev)
    {

        $mdDialog.show({
            controller: 'NewStdItemDialogCtrl',
            templateUrl: 'views/components/newStdItemDialog/newStdItemDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {grid : $scope.stdItemsGridOptions}
        })
        .then(function(res) {
            console.log("res");
        }, function() {

        });

    };

    function getStdItems()
    {
        $http({
            method :'GET',
            url : 'api/stdItems/getStdItems'
        }).then(function (successResponse){
            $scope.stdItemsGridOptions.data = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    //fire
    getStdItems();
});
