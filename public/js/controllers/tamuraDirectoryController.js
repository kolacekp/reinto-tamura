tamuraApp.controller("TamuraDirectoryCtrl", function ($scope, $http, $rootScope, $state, $mdDialog) {
    
    $rootScope.activeTopMenu = "tamura-directory";

    $scope.canEdit = function () { return $scope.pageOptions.isView; };

    $scope.pageOptions = { isView : false};
    $scope.allCurrencies = null;
    $scope.allCurrenciesShortNames = [];
    $scope.entityType = null;
    $scope.lockText = "Unlock";
    var locked = true;
    var actualCurrency = null;
    $scope.currencySymbol = '$';
    $scope.factorySales = null;
    $scope.tamuraEntities = [
        {
            "type" : 1000,
            "data" : []
        },
        {
            "type" : 2000,
            "data" : []
        },
        {
            "type" : 3000,
            "data" : []
        }

    ];

    $scope.types = [
         1000,
         2000,
         3000
    ];

    $scope.entityPersonsGridOptions = {
        showGridFooter : true,
        data: [

        ]
        ,
        columnDefs: [
            {field: 'name', displayName: 'Person Name',cellEditableCondition: $scope.canEdit },
            {field: 'phone', displayName: 'Phone',cellEditableCondition: $scope.canEdit },
            {field: 'mobile', displayName: 'Mobile',cellEditableCondition: $scope.canEdit },
            {field: 'email', displayName: 'eMail',cellEditableCondition: $scope.canEdit },
            {field: 'sales_manager', displayName: 'Sales Mgr',cellEditableCondition: false },
            {field: 'engineer', displayName: 'Engineer',type:"boolean",cellEditableCondition: $scope.canEdit },
            {field: 'deactivated', displayName: 'Deactivated',type:"boolean",cellEditableCondition: $scope.canEdit }
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.saveRow = function(rowEntity)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/tamura-directory/design/person-update',
            data : {
                person : rowEntity
            }
        }).then(function (successResponse){
            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.countries = null;
    $scope.entityViewed = false;
    $scope.tamuraEntity = null;

    function getData()
    {
        $rootScope.loading = true;
        $scope.tamuraEntities[0].data = [];
        $scope.tamuraEntities[1].data = [];
        $scope.tamuraEntities[2].data = [];

        $http({
            method : 'GET',
            url : 'api/tamura-directory/fetch'
        }).then(function (successResponse){
            $rootScope.loading = false;
            var entities = successResponse.data;
            entities.forEach(function (entity) {
                switch (entity.type){
                    case 1000:
                        $scope.tamuraEntities[0].data.push(entity);
                        break;
                    case 2000:
                        $scope.tamuraEntities[1].data.push(entity);
                        break;
                    case 3000:
                        $scope.tamuraEntities[2].data.push(entity);
                        break;
                    default:
                        break;
                }
            });
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }
    function getAllCountries()
    {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/countries/all'
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.countries = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    $scope.viewEntity = function(item)
    {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/tamura-directory/getEntityById?id=' + item.id
        }).then(function (successResponse){

            $scope.tamuraEntity = successResponse.data;
            $scope.tamuraEntity = $scope.tamuraEntity[0];
            $scope.countries.forEach(function (country) {
                if (country.iso2 === $scope.tamuraEntity.location) {
                    $scope.tamuraEntity.locationName =  country.name;
                }
            });
            if($scope.tamuraEntity.linked_sold != null){
                for (var i = 0; i < $scope.tamuraEntities[2].data.length; i++){
                    if ($scope.tamuraEntities[2].data[i].id == $scope.tamuraEntity.linked_sold){
                        $scope.tamuraEntity.linked_sold = $scope.tamuraEntities[2].data[i];
                        break;
                    }
                }
            }
            $scope.entityType = $scope.tamuraEntity.type;
            $scope.tamuraEntity.deactivated = parseInt($scope.tamuraEntity.deactivated);
            $scope.entityViewed = true;
            $rootScope.loading = false;
            console.log($scope.tamuraEntity );
            for(var i = 0; i < $scope.allCurrencies.length ;i++)
            {
                if ($scope.allCurrencies[i].id === $scope.tamuraEntity.currency_id) {
                    $scope.tamuraEntity.currency_id =  $scope.allCurrencies[i].short_name;
                    actualCurrency = $scope.allCurrencies[i].short_name;
                    $scope.currencySymbol = $scope.allCurrencies[i].symbol;
                    break;
                }
            }
            getFactorySales($scope.tamuraEntity.id);

            if($scope.tamuraEntity.tamura_entity_persons != null)
                $scope.entityPersonsGridOptions.data = $scope.tamuraEntity.tamura_entity_persons;
            else
                $scope.entityPersonsGridOptions.data = [];
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };

    $scope.saveEntity= function () {
        $rootScope.loading = true;

        $scope.countries.forEach(function (country) {
            if (country.name === $scope.tamuraEntity.locationName) {
                $scope.tamuraEntity.location =  country.iso2;
            }
        });
        console.log($scope.tamuraEntity);
        $http({
            method :'POST',
            url : 'api/tamura-directory/factories/update',
            data : {
                entity : $scope.tamuraEntity
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            getData();
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.saveFactoryValues = function (){
        $rootScope.loading = true;
        var currency_id = null;
        for(var i = 0; i < $scope.allCurrencies.length ;i++)
        {
            if ($scope.allCurrencies[i].short_name === actualCurrency) {
                currency_id=  $scope.allCurrencies[i].id;
                break;
            }
        }

        if($scope.tamuraEntity.linked_sold != undefined && $scope.tamuraEntity.linked_sold != null)
            $scope.tamuraEntity.linked_sold = $scope.tamuraEntity.linked_sold.id;
        else
            $scope.tamuraEntity.linked_sold = null;
        // console.log( $scope.factorySales);
        $http({
            method :'POST',
            url : 'api/tamura-directory/factories/update',
            data : {
                currency_id : currency_id,
                entity : $scope.tamuraEntity,
                factorySalesEntityId : $scope.tamuraEntity.linked_sold
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log( successResponse.data);
            $scope.viewEntity(successResponse.data);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.saveSalesValues = function(){
        $rootScope.loading = true;
        var currency_id = null;
        for(var i = 0; i < $scope.allCurrencies.length ;i++)
        {
            if ($scope.allCurrencies[i].short_name === actualCurrency) {
                currency_id=  $scope.allCurrencies[i].id;
                break;
            }
        }

        $http({
            method :'POST',
            url : 'api/tamura-directory/sales/update',
            data : {
                currency_id : currency_id,
                entity : $scope.tamuraEntity
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log( successResponse.data);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.changeCurrency = function(currency){
        actualCurrency =  currency;
        $scope.currencySymbol = currency.symbol;
        for(var i = 0; i < $scope.allCurrencies.length ;i++){
            if($scope.allCurrencies[i].short_name == actualCurrency){
                $scope.currencySymbol = $scope.allCurrencies[i].symbol;
                break;
            }
        }
    };

    $scope.addTamuraEntity = function(ev, type)
    {
        $mdDialog.show({
            controller: 'AddTamuraEntityDialogCtrl',
            templateUrl: 'views/components/addTamuraEntityDialog/addTamuraEntityDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                type : type
            }
        })
        .then(function(res) {
            console.log("res");
        }, function() {
            getData();
        });
    };

    $scope.deleteTamuraEntity = function(entityId)
    {
        $http({
            method :'POST',
            url : 'api/tamura-directory/delete',
            data : {
                tamuraEntityId : entityId
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log( successResponse.data);
            $state.go('tamura-directory', {}, {reload: true});
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    function getActualCurrencies()
    {
        $http({
            method :'GET',
            url : 'api/currencies/all'
        }).then(function (successResponse){
            $scope.allCurrencies = successResponse.data;
            for(var i = 0; i < $scope.allCurrencies.length ;i++){
                $scope.allCurrenciesShortNames.push($scope.allCurrencies[i].short_name);
            }
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getFactorySales(id)
    {
        $http({
            method :'GET',
            url : 'api/tamura-directory/getFactorySales?id=' + id
        }).then(function (successResponse){
            $scope.factorySales = successResponse.data;
            $scope.factorySales = $scope.factorySales[0];
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });

    }


    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
        $scope.entityViewed = false;
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };


    $scope.lock =function () {
        if (!locked) {
            $scope.pageOptions.isView = false;
            $scope.lockText = "Unlock";
            locked = true;
        }
        else{
            $scope.pageOptions.isView = true;
            $scope.lockText = "Lock";
            locked = false;
        }
    };

    $scope.addCustomerForDirectory = function (ev) {

        $mdDialog.show({
            controller: 'AddPersonToDirectoryCtrl',
            templateUrl: 'views/components/addPersonToDirectory/addPersonToDirectory.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                entity : $scope.tamuraEntity,
                customerId : null,
                isEngineer : false
            }
        })
            .then(function(res) {
                console.log("res");
            }, function() {
                $scope.viewEntity($scope.tamuraEntity);
            });

    };
    // $scope.unlock =function () {
    //     $scope.pageOptions.isView = true;
    // };


    getAllCountries();
    getData();
    getActualCurrencies();

});
