tamuraApp.controller("TopMenuCtrl", function ($scope, $rootScope, $translate)
{

    $rootScope.$on('doTranslations', function(){
        $translate([
                        "top-menu.search",
                        "top-menu.project-rfq-npi-sales",
                        "top-menu.customer-directory",
                        "top-menu.tamura-directory",
                        "top-menu.time-sheet-invoice",
                        "top-menu.lme-mgt",
                        "top-menu.exch-rate-mgt",
                        "top-menu.std-items-mgt",
                        "top-menu.material-mgt",
                        "top-menu.db-user",
                        "top-menu.admin",
                  ])
        .then(function (translations) {
            $scope.topMenu = [
                {
                    name :  translations["top-menu.search"],
                    state : "search"
                }, {
                    name :  translations["top-menu.project-rfq-npi-sales"],
                    state : "project-rfq-npi-sales"
                }, {
                    name :  translations["top-menu.customer-directory"],
                    state : "customer-directory"
                }, {
                    name :  translations["top-menu.tamura-directory"],
                    state : "tamura-directory"
                }, {
                    name :  translations["top-menu.time-sheet-invoice"],
                    state : "time-sheet-invoice"
                },{
                    name :  translations["top-menu.lme-mgt"],
                    state : "lme-mgt"
                }, {
                    name :  translations["top-menu.exch-rate-mgt"],
                    state : "exch-rate-mgt"
                }, {
                    name :  translations["top-menu.std-items-mgt"],
                    state : "std-items-mgt"
                }, {
                    name :  translations["top-menu.material-mgt"],
                    state : "material-mgt"
                }, {
                    name :  translations["top-menu.db-user"],
                    state : "db-user"
                }, {
                    name :  translations["top-menu.admin"],
                    state : "admin"
                }
            ];
        });
    });

    $rootScope.$emit('doTranslations');


});