tamuraApp.controller("DbUserCtrl", function ($scope, $http, $rootScope, $translate, $mdDialog, $mdMedia, Flash) {
    
    $rootScope.activeTopMenu = "db-user";

    $scope.usersToAdd = {};
    $scope.users = [];

    $translate([
        "db-user.tamura-entity-short-name",
        "db-user.tamura-entity-name",
        "db-user.name",
        "db-user.login",
        "db-user.password",
        "db-user.email",
        "db-user.deactivated",
        "db-user.role",
        "db-user.username"
    ]).then(function (translations){
        $scope.usersGridOptions.columnDefs = [
            {field: 'id', visible:false},
            {field: 'tamura_entity.short_name', displayName: translations['db-user.tamura-entity-short-name'], enableCellEdit:false},
            {field: 'tamura_entity.name', displayName: translations['db-user.tamura-entity-name'], enableCellEdit:false},
            {field: 'name', displayName: translations['db-user.name'], enableCellEdit:false},
            {field: 'db_login', displayName: translations['db-user.login']},
            {field: 'db_password', displayName: translations['db-user.password']},
            {field: 'email', displayName: translations['db-user.email'], enableCellEdit:false},
            {field: 'deactivated', displayName: translations['db-user.deactivated'], type:"boolean"},
            {field: 'user.role.display_name', displayName: translations['db-user.role'], enableCellEdit:false}
            ];

        $scope.allUsersGridOptions.columnDefs = [
            {field: 'id', visible:false},
            {field: 'name', displayName: translations['db-user.name']},
            {field: 'username', displayName: translations['db-user.username']},
            {field: 'email', displayName: translations['db-user.email']},
            {field: 'deactivated', displayName: translations['db-user.deactivated'], type:"boolean"},
            {field: 'role.display_name', displayName: translations['db-user.role'], enableCellEdit:false}
        ];
    });

    $scope.usersGridOptions = {
        //headerTemplate: 'header-template.html',
        showGridFooter : true,
        data: [],
        columnDefs: [],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.allUsersGridOptions = {
        showGridFooter : true,
        data: [],
        columnDefs: [],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.allUsersGridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveUserRow);
        }
    };

    $scope.saveRow = function(rowEntity)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/persons/updateDatabaseUser',
            data : {
                person : rowEntity
            }
        }).then(function (successResponse){

            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.saveUserRow = function(rowEntity)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/users/updateUser',
            data : {
                user : rowEntity
            }
        }).then(function (successResponse){

            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.addDBUser = function(ev)
    {
        $mdDialog.show({
            controller: 'AddDbUserDialogCtrl',
            templateUrl: 'views/components/dbUserDialog/addDbUserDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            locals : {
                roles : $scope.roles
            }
        })
        .then(function(res) {
            console.log(res);
        }, function() {
            getDatabaseUsers();
        });
    };

    $scope.editSelectedDBUser = function(ev)
    {
        var selectedUsers = $scope.gridApi.selection.getSelectedRows();
        if(selectedUsers.length === 1){

            $mdDialog.show({
                controller: 'EditDbUserDialogCtrl',
                templateUrl: 'views/components/dbUserDialog/editDbUserDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    selectedUser : selectedUsers[0],
                    roles : $scope.roles
                }
            })
            .then(function(res) {
                console.log(res);
            }, function() {
                getDatabaseUsers();
            });

        }else{
           // Flash.clear();
            $translate(["db-user.select-exactly-one-user"]).then(function (translations) {
                Flash.create('danger', translations["db-user.select-exactly-one-user"], 0, {}, true);
            });
        }
    };

    $scope.deleteSelectedDBUser = function(ev)
    {
        var selectedUsers = $scope.gridApi.selection.getSelectedRows();
        if(selectedUsers.length === 1){

            $rootScope.loading = true;
            $http({
                method :'POST',
                url : 'api/persons/deleteDatabaseUser',
                data : {
                    id : selectedUsers[0].id
                }
            }).then(function (successResponse){
                getDatabaseUsers();
                $rootScope.loading = false;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });

        }else{
            // Flash.clear();
            $translate(["db-user.select-exactly-one-user"]).then(function (translations) {
                Flash.create('danger', translations["db-user.select-exactly-one-user"], 0, {}, true);
            });
        }
    };

    $scope.editSelectedUser = function(ev)
    {
        var selectedUsers = $scope.allUsersGridApi.selection.getSelectedRows();
        if(selectedUsers.length === 1){

            $mdDialog.show({
                controller: 'EditUserCtrl',
                templateUrl: 'views/components/editUser/editUser.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    user : selectedUsers[0],
                    roles : $scope.roles
                }
            })
                .then(function(res) {
                    console.log(res);
                }, function() {
                    getDatabaseUsers();
                });

        }else{
            // Flash.clear();
            $translate(["db-user.select-exactly-one-user"]).then(function (translations) {
                Flash.create('danger', translations["db-user.select-exactly-one-user"], 0, {}, true);
            });
        }
    };



    function getDatabaseUsers()
    {
        $http({
            method :'GET',
            url : 'api/persons/databaseUsers'
        }).then(function (successResponse){
            $scope.usersGridOptions.data = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }


    function getUsers() {
        $http({
            method :'GET',
            url : 'api/admin/getUsers'
        }).then(function (successResponse){
            console.log(successResponse.data);
            $scope.allUsersGridOptions.data  = successResponse.data;
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    function getRoles() {
        $http({
            method :'GET',
            url : 'api/admin/getRoles'
        }).then(function (successResponse){
            console.log(successResponse.data);
            $scope.roles  = successResponse.data;
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    getUsers();
    getRoles();


    //fire
    getDatabaseUsers();
    
});