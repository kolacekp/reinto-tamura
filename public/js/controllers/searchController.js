tamuraApp.controller("SearchCtrl", function ($scope, $state, $http, $rootScope, localStorageService, $translate, Flash) {
    
    // root scope
    $rootScope.activeTopMenu = "search";
    
    // scope
    $scope.projects = {
        id : $rootScope.search.projects.id,
        name : $rootScope.search.projects.name,
        status : $rootScope.search.projects.status,
        manager : $rootScope.search.projects.manager,
        keyAccountManager : $rootScope.search.projects.keyAccountManager,
        customer : $rootScope.search.projects.customer,
        customerPath : $rootScope.search.projects.customerPath,
        purchaser : $rootScope.search.projects.purchaser,
        includeAllGroups : $rootScope.search.projects.includeAllGroups,
        marketParent : $rootScope.search.projects.marketParent,
        marketChild : $rootScope.search.projects.marketChild,
        marketChildNumber : $rootScope.search.projects.marketChildNumber,
        creationDateFrom : $rootScope.search.projects.creationDateFrom,
        creationDateTo : $rootScope.search.projects.creationDateTo,
        modificationDateFrom : $rootScope.search.projects.modificationDateFrom,
        modificationDateTo : $rootScope.search.projects.modificationDateTo
    };
    
    
    $scope.loadProjects = {
        purchaser : null
    };
    
    
    $scope.rfqs = {
        id : $rootScope.search.rfqs.id,
        customerPN : $rootScope.search.rfqs.customerPN,
        status : $rootScope.search.rfqs.status,
        deadReason : $rootScope.search.rfqs.deadReason,
        engineeringActions : $rootScope.search.rfqs.engineeringActions,
        category : $rootScope.search.rfqs.category,
        PNFormat :  $rootScope.search.rfqs.PNFormat,
        priority :  $rootScope.search.rfqs.priority,
        newDev : $rootScope.search.rfqs.newDev,
        newBiz : $rootScope.search.rfqs.newBiz,
        productParent : $rootScope.search.rfqs.productParent,
        productChildOne : $rootScope.search.rfqs.productChildOne,
        productChildTwo : $rootScope.search.rfqs.productChildTwo,
        productProduct : $rootScope.search.rfqs.productProduct,
        customerEngineer : $rootScope.search.rfqs.customerEngineer,
        electricalEngineer : $rootScope.search.rfqs.electricalEngineer,
        mechanicalEngineer : $rootScope.search.rfqs.mechanicalEngineer,
        creationDateFrom : $rootScope.search.rfqs.creationDateFrom,
        creationDateTo : $rootScope.search.rfqs.creationDateTo,
        modificationDateFrom : $rootScope.search.rfqs.modificationDateFrom,
        modificationDateTo : $rootScope.search.rfqs.modificationDateTo
    };
    
    $scope.sales = {
        customer : $rootScope.search.sales.customer,
        customerPath : $rootScope.search.sales.customerPath,
        purchaseManager : $rootScope.search.sales.purchaseManager,
        manager : $rootScope.search.sales.manager,
        alsoInvalid : $rootScope.search.sales.alsoInvalid
    };
    
    $scope.managers = [];
    $scope.managersAll = [];
    $scope.keyAccountManagers = [];
    $scope.keyAccountManagersAll = [];
    $scope.managerSearchText = "";
    $scope.keyAccountManagerSearchText = "";
    $scope.customers = [];
    $scope.customersAll = [];
    $scope.customerSearchText = "";
    $scope.customerEngineerSearchText = "";
    $scope.customerSelected = ($scope.projects.customer !== null);
    $scope.purchasers = [];
    $scope.purchasersAll = [];
    $scope.marketParents = [];
    $scope.marketParentsAll = [];
    $scope.marketParentSearchText = "";
    $scope.marketParentSelected = ($scope.projects.marketParent !== null);
    $scope.marketChildren = [];
    $scope.marketChildrenAll = [];
    $scope.marketChildSearchText = "";
    $scope.categories = [];   
    
    $scope.PNFormats = [];
    $scope.priorities = [];
    
    $translate(["search.pn-formats.linear", "search.pn-formats.toroid"]).then(function (translations){
        $scope.PNFormats = [
            {id : 1, value : "Linear", text : translations["search.pn-formats.linear"]},
            {id : 2, value : "Toroid", text : translations["search.pn-formats.toroid"]}
        ];
    });
    
    $translate(["search.priorities.high", "search.priorities.medium", "search.priorities.price-checking","data-sheet.choose-product","data-sheet.choose-market"]).then(function (translations){
        $scope.priorities = [
            {id : 1, value : "HIGH", text : translations["search.priorities.high"]},
            {id : 2, value : "MEDIUM", text : translations["search.priorities.medium"]},
            {id : 3, value : "PRICE-CHECKING", text : translations["search.priorities.price-checking"]}
        ];
        $scope.chooseProduct = [
            {id:1 , text: translations["data-sheet.choose-product"]}
        ];
        $scope.chooseMarket = [
            {id : 1, text: translations["data-sheet.choose-market"]}
        ]
    });
    
    $scope.productParents = [];
    $scope.productParentsAll = [];
    $scope.productChildrenOne = [];
    $scope.productChildrenOneAll = [];
    $scope.productChildrenTwo = [];
    $scope.productChildrenTwoAll = [];
    $scope.productProducts = [];
    $scope.productProductsAll = [];
    
    $scope.productParentSearchText = "";
    $scope.productChildOneSearchText = "";
    $scope.productChildTwoSearchText = "";
    $scope.productProductSearchText = "";
    
    $scope.productParentSelected = ($scope.rfqs.productParent !== null);  
    $scope.productChildOneSelected = ($scope.rfqs.productChildOne !== null);
    $scope.productChildTwoSelected = ($scope.rfqs.productChildTwo !== null);
    $scope.productProductSelected = ($scope.rfqs.productProduct !== null);
    
    $scope.productChildOneShown = false;
    $scope.productChildTwoShown = false;
    $scope.productProductShown = false;
    
    $scope.customerEngineers = [];
    $scope.customerEngineersAll = [];
    $scope.electricalEngineers = [];
    $scope.electricalEngineersAll = [];
    $scope.mechanicalEngineers = [];
    $scope.mechanicalEngineersAll = [];
    $scope.mechanicalEngineerSearchText = "";
    $scope.electricalEngineerSearchText = "";
    
    $scope.salesCustomers = [];
    $scope.salesCustomersAll = [];
    $scope.salesCustomerSearchText = "";
    $scope.salesCustomerSelected = ($scope.sales.customer !== null);
    
    $scope.salesManagers = [];
    $scope.salesManagersAll = [];
    $scope.salesManagerSearchText = "";
    $scope.purchaseManagers = [];
    $scope.purchaseManagersAll = [];
    $scope.purchaseManagerSearchText = "";
    
    $scope.personalization = {};
    $scope.personalizations = [];
    $scope.personalizationName = "";
    
    $scope.rfqDeadReasons = [];
    $scope.rfqDeadReasonsAll = [];
    $scope.rfqDeadReasonSearchText = "";

    $scope.dataQuantity = {};
       
    // change search param projects
    $scope.changeSearchParamProjects = function(model)
    {
        $rootScope.search.projects[model] = $scope.projects[model];
        saveSearchProjectParams();
    };
    
    // change search param projects status
    $scope.changeSearchParamProjectsStatus = function(type)
    {
        $rootScope.search.projects.status[type] = $scope.projects.status[type];
        saveSearchProjectParams();
    };
    
    // change search param rfqs
    $scope.changeSearchParamRfqs = function(model)
    {
        $rootScope.search.rfqs[model] = $scope.rfqs[model];
        saveSearchRfqParams();
    };
    
    // change search param rfqs status
    $scope.changeSearchParamRfqsStatus = function(type)
    {
        $rootScope.search.rfqs.status[type] = $scope.rfqs.status[type];
        saveSearchRfqParams();
    };
    
    // change search param rfqs status
    $scope.changeSearchParamRfqsEngineeringActions = function(type)
    {
        $rootScope.search.rfqs.engineeringActions[type] = $scope.rfqs.engineeringActions[type];
        saveSearchRfqParams();
    };
    
    $scope.managerSearchTextChange = function(text)
    {
        $scope.managers = $scope.managersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.keyAccountManagerSearchTextChange = function(text)
    {
        $scope.keyAccountManagers = $scope.keyAccountManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.customerSearchTextChange = function(text)
    {
        $scope.customers = $scope.customersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.customerEngineerSearchTextChange = function(text)
    {
        $scope.customerEngineers = $scope.customerEngineersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.electricalEngineerSearchTextChange = function(text)
    {
        $scope.electricalEngineers = $scope.electricalEngineersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.mechanicalEngineerSearchTextChange = function(text)
    {
        $scope.mechanicalEngineers = $scope.mechanicalEngineersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
     
    $scope.purchaserSearchTextChange = function(text)
    {
        $scope.purchasers = $scope.purchasersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.marketParentSearchTextChange = function(text)
    {
        $scope.marketParents = $scope.marketParentsAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });    
    };
    
    $scope.productParentSearchTextChange = function(text)
    {
        $scope.productParents = $scope.productParentsAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });    
    };
    
    $scope.productChildOneSearchTextChange = function(text)
    {
        $scope.productChildrenOne = $scope.productChildrenOneAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });    
    };
    
    $scope.productChildTwoSearchTextChange = function(text)
    {
        $scope.productChildrenTwo = $scope.productChildrenTwoAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });    
    };
    
    $scope.productProductSearchTextChange = function(text)
    {
        $scope.productProducts = $scope.productProductsAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });    
    };
    
    $scope.marketChildSearchTextChange = function(text)
    {
        $scope.marketChildren = $scope.marketChildrenAll.filter(function(obj){
            return obj.entity.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.salesCustomerSearchTextChange = function(text)
    {
        $scope.salesCustomers = $scope.salesCustomersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.purchaseManagerSearchTextChange = function(text)
    {
        $scope.purchaseManagers = $scope.purchaseManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.salesManagerSearchTextChange = function(text)
    {
        $scope.salesManagers = $scope.salesManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.rfqDeadReasonSearchTextChange = function(text)
    {
        $scope.rfqDeadReasons = $scope.rfqDeadReasonsAll.filter(function(obj){
            return obj.reason.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };
    
    $scope.managerSelectedItemChange = function(item)
    {
        $rootScope.search.projects.manager = (item !== undefined) ? item : null;
        saveSearchProjectParams();
    };
    
    $scope.keyAccountManagerSelectedItemChange = function(item)
    {
        $rootScope.search.projects.keyAccountManager = (item !== undefined) ? item : null;
        saveSearchProjectParams();
    };
       
    $scope.customerSelectedItemChange = function(item)
    {
        $rootScope.search.projects.customer = (item !== undefined) ? item : null;
        $rootScope.search.projects.customerPath = (item !== undefined) ? item.path : null;    
        $scope.projects.customerPath = (item !== undefined) ? item.path : null;
        $scope.customerSelected = (item !== undefined) ? true : false;
        $scope.projects.purchaser = null;
        $scope.rfqs.customer = null;
        
        if(item !== undefined){
            getPurchasers();
            getCustomerEngineers();
        }else{
            $scope.purchasers = [];
            $scope.purchasersAll = [];
            $scope.customerEngineers = [];
            $scope.customerEngineersAll = [];
        }
        
        saveSearchProjectParams();
    };
    
    $scope.purchaserSelectedItemChange = function(item)
    {
        if($scope.loadProjects !== undefined && $scope.loadProjects.purchaser !== null){
            $scope.projects.purchaser = $scope.loadProjects.purchaser;
            $rootScope.search.projects.purchaser = (item !== undefined) ? item : null;
            saveSearchProjectParams();
        }else{
            $rootScope.search.projects.purchaser = (item !== undefined) ? item : null;
            saveSearchProjectParams();
        }
        $scope.loadProjects.purchaser = null;
    };
    
    $scope.marketParentSelectedItemChange = function(item)
    {
        $rootScope.search.projects.marketParent = (item !== undefined) ? item : null;
        $scope.marketParentSelected = (item !== undefined) ? true : false;
        $scope.projects.marketChild = null;
        $scope.projects.marketChildNumber = null;

        if(item !== undefined){
            getMarketChildren();
        }
        
        saveSearchProjectParams();
    };
        
    $scope.marketChildSelectedItemChange = function(item)
    {
        if($scope.loadProjects !== undefined &&  $scope.loadProjects.marketChild !== null){
            $scope.projects.marketChild = $scope.loadProjects.marketChild;
            $scope.projects.marketChildNumber = $scope.loadProjects.marketChildNumber;
            $rootScope.search.projects.marketChild = (item !== undefined) ? item : null;
            saveSearchProjectParams();
        }else{
            $rootScope.search.projects.marketChild = (item !== undefined) ? item : null;
            $rootScope.search.projects.marketChildNumber = (item !== undefined) ? item.no_entity : null;    
            $scope.projects.marketChildNumber = (item !== undefined) ? item.no_entity : null;
        
            saveSearchProjectParams();
        }      
        $scope.loadProjects.marketChild = null;
    };
    
    $scope.productParentSelectedItemChange = function(item)
    {  
        
        $rootScope.search.rfqs.productParent = (item !== undefined) ? item : null;
        $scope.productParentSelected = (item !== undefined) ? true : false;

        $scope.rfqs.productChildOne = null;
        $scope.rfqs.productProduct = null;

        
        if(item !== undefined){
            getProductChildrenOne();
        }
        
        saveSearchRfqParams();
    };
    
    $scope.productChildOneSelectedItemChange = function(item)
    {
                      
        if($scope.loadRfqs !== undefined && $scope.loadRfqs.productChildOne !== null){
            $scope.rfqs.productChildOne = $scope.loadRfqs.productChildOne;
            $rootScope.search.rfqs.productChildOne = (item !== undefined) ? item : null;
            $scope.loadRfqs.productChildOne = null;
        }else{
            $rootScope.search.rfqs.productChildOne = (item !== undefined) ? item : null;
            $scope.productChildOneSelected = (item !== undefined) ? true : false;
         
        }
        
        $scope.rfqs.productChildTwo = null;
        $scope.rfqs.productProduct = null;

        if(item !== undefined){
            getProductChildrenTwo();
        }
        
        saveSearchRfqParams();
    };
    
    $scope.productChildTwoSelectedItemChange = function(item)
    {
        if($scope.loadRfqs !== undefined && $scope.loadRfqs.productChildTwo !== null){
            $scope.rfqs.productChildTwo = $scope.loadRfqs.productChildTwo;
            $rootScope.search.rfqs.productChildTwo = (item !== undefined) ? item : null;
            $scope.loadRfqs.productChildTwo = null;
        }else{
            $rootScope.search.rfqs.productChildTwo = (item !== undefined) ? item : null;
            $scope.productChildTwoSelected = (item !== undefined) ? true : false;      
        }
        
        $scope.rfqs.productProduct = null;

        if(item !== undefined){
            getProductProducts();
        }
        
        saveSearchRfqParams();
    };
    
    $scope.productProductSelectedItemChange = function(item)
    {
        if($scope.loadRfqs !== undefined && $scope.loadRfqs.productProduct !== null){
            $scope.rfqs.productProduct = $scope.loadRfqs.productProduct;
            $rootScope.search.rfqs.productProduct = (item !== undefined) ? item : null;
            $scope.loadRfqs.productProduct = null;
        }else{
            $rootScope.search.rfqs.productProduct = (item !== undefined) ? item : null;  
        }        
        saveSearchRfqParams();
    };
    
    $scope.customerEngineerSelectedItemChange = function(item)
    {
        $rootScope.search.rfqs.customerEngineer = (item !== undefined) ? item : null;
        saveSearchRfqParams();
    };
    
    $scope.electricalEngineerSelectedItemChange = function(item)
    {
        $rootScope.search.rfqs.electricalEngineer = (item !== undefined) ? item : null;
        saveSearchRfqParams();
    };
    
    $scope.mechanicalEngineerSelectedItemChange = function(item)
    {
        $rootScope.search.rfqs.mechanicalEngineer = (item !== undefined) ? item : null;
        saveSearchRfqParams();
    };
    
    $scope.rfqDeadReasonSelectedItemChange = function(item)
    {
        $rootScope.search.rfqs.deadReason = (item !== undefined) ? item : null;
        saveSearchRfqParams();
    };
    
    $scope.resetDevelopmentStatus = function()
    {
        console.log($scope.rfqs);
        $scope.rfqs.newDev = null;
        saveSearchRfqParams();
    };
    
    $scope.resetBusinessStatus = function()
    {
        $scope.rfqs.newBiz = null;
        saveSearchRfqParams();
    };
    
    $scope.salesCustomerSelectedItemChange = function(item)
    {
        $rootScope.search.sales.customer = (item !== undefined) ? item : null;
        $rootScope.search.sales.customerPath = (item !== undefined) ? item.path : null;    
        $scope.sales.customerPath = (item !== undefined) ? item.path : null;
        $scope.salesCustomerSelected = (item !== undefined) ? true : false;
        
        if(item !== undefined){
            getPurchaseManagers();         
        }else{
            $scope.purchaseManagers = [];
            $scope.purchaseManagersAll = [];
        }
        
        saveSearchSaleParams();
    };
    
    $scope.purchaseManagerSelectedItemChange = function(item)
    {
        $rootScope.search.sales.purchaseManager = (item !== undefined) ? item : null;
        saveSearchSaleParams();
    };
    
    $scope.salesManagerSelectedItemChange = function(item)
    {
        $rootScope.search.sales.manager = (item !== undefined) ? item : null;
        saveSearchSaleParams();
    };
    
    $scope.changeSalesAlsoInvalid = function(number)
    {
        $rootScope.search.sales.alsoInvalid = number;
        saveSearchSaleParams();
    };
    
    function saveSearchRfqParams()
    {
        localStorageService.set("searchRfqParams", JSON.stringify($scope.rfqs));
    }
    
    function saveSearchProjectParams()
    {
        localStorageService.set("searchProjectParams", JSON.stringify($scope.projects));
    }
    
    function saveSearchSaleParams()
    {
        localStorageService.set("searchSaleParams", JSON.stringify($scope.sales));
    }
    
    
    function getSalesManagers()
    {
        $http({
            method :'GET',
            url : 'api/persons/salesManagers'
        }).then(function (successResponse){
            $scope.managers = successResponse.data;
            $scope.managersAll = successResponse.data;
            $scope.keyAccountManagers = successResponse.data;
            $scope.keyAccountManagersAll = successResponse.data;
            $scope.salesManagers = successResponse.data;
            $scope.salesManagersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }
    
    function getCustomersCompanies()
    { 
        $http({
            method :'GET',
            url : 'api/customers/companies'
        }).then(function (successResponse){
            $scope.customers = successResponse.data;
            $scope.customersAll = successResponse.data;
            $scope.salesCustomers = successResponse.data;
            $scope.salesCustomersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });      
    }
    
    function getPurchasers()
    {
        if($scope.customerSelected){
            $http({
                method :'GET',
                url : 'api/customers/purchasers?customerPath='+$scope.projects.customerPath
            }).then(function (successResponse){
                $scope.purchasers = successResponse.data;
                $scope.purchasersAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });      
        }
    }
    
    function getMarketParents()
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/parentMarketEntities'
        }).then(function (successResponse){
            $scope.marketParents = successResponse.data;
            $scope.marketParentsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });  
    }
    
    function getMarketChildren()
    {
        if($scope.marketParentSelected){
            $http({
                method :'GET',
                url : 'api/tamuraCodifications/childMarketEntities?parent='+$scope.projects.marketParent.entity
            }).then(function (successResponse){
                $scope.marketChildren = successResponse.data;
                $scope.marketChildrenAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            }); 
        }
    }
    
    function getCategories()
    {
        $http({
            method :'GET',
            url : 'api/categories/categories'
        }).then(function (successResponse){
            $scope.categories = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        }); 
    }
    
    function getProductParents()
    {
        $http({
            method :'GET',
            url : 'api/tamuraCodifications/parentProductEntities'
        }).then(function (successResponse){
            $scope.productParents = successResponse.data;
            $scope.productParentsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });  
    }
    
    function getProductChildrenOne()
    {
        if($scope.productParentSelected){
            $http({
                method :'GET',
                url : 'api/tamuraCodifications/childProductEntities?level=2&parent='+$scope.rfqs.productParent.entity_path+$scope.rfqs.productParent.entity+"/"
            }).then(function (successResponse){                           
                $scope.productChildrenOne = successResponse.data.childProductEntities;
                $scope.productChildrenOneAll = successResponse.data.childProductEntities;                     
                $scope.productChildOneShown = ($scope.productChildrenOne.length > 0);

                $scope.productProducts = successResponse.data.childProducts;
                $scope.productProductsAll = successResponse.data.childProducts;                      
                $scope.productProductShown = ($scope.productProducts.length > 0);              
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            }); 
        }
    }
    
    function getProductChildrenTwo()
    {
        if($scope.productChildOneSelected){
            $http({
                method :'GET',
                url : 'api/tamuraCodifications/childProductEntities?level=3&parent='+$scope.rfqs.productChildOne.entity_path+$scope.rfqs.productChildOne.entity+"/"
            }).then(function (successResponse){               
                $scope.productChildrenTwo = successResponse.data.childProductEntities;
                $scope.productChildrenTwoAll = successResponse.data.childProductEntities;                     
                $scope.productChildTwoShown = ($scope.productChildrenTwo.length > 0);
                                
                $scope.productProducts = successResponse.data.childProducts;
                $scope.productProductsAll = successResponse.data.childProducts;                      
                $scope.productProductShown = ($scope.productProducts.length > 0);              
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            }); 
        }
    }
    
    function getProductProducts()
    {
        if($scope.productChildTwoSelected){
            $http({
                method :'GET',
                url : 'api/tamuraCodifications/childProductEntities?level=4&parent='+$scope.rfqs.productChildTwo.entity_path+$scope.rfqs.productChildTwo.entity+"/"
            }).then(function (successResponse){               
                $scope.productProducts = successResponse.data.childProducts;
                $scope.productProductsAll = successResponse.data.childProducts;                      
                $scope.productProductShown = ($scope.productProducts.length > 0);              
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            }); 
        }
    }
    
    function getCustomerEngineers()
    { 
        if($scope.customerSelected){
            $http({
                method :'GET',
                url : 'api/customers/engineers?customerId='+$scope.projects.customer.id
            }).then(function (successResponse){
                $scope.customerEngineers = successResponse.data;
                $scope.customerEngineersAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });   
        }
    }
        
    function getEngineers()
    { 
        $http({
            method :'GET',
            url : 'api/persons/engineers'
        }).then(function (successResponse){
            $scope.electricalEngineers = successResponse.data;
            $scope.electricalEngineersAll = successResponse.data;    
            $scope.mechanicalEngineers = successResponse.data;
            $scope.mechanicalEngineersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });           
    }
    
    function getPurchaseManagers()
    {
        if($scope.salesCustomerSelected){
            $http({
                method :'GET',
                url : 'api/customers/purchasers?customerPath='+$scope.sales.customerPath
            }).then(function (successResponse){
                $scope.purchaseManagers = successResponse.data;
                $scope.purchaseManagersAll = successResponse.data;
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });   
        }
    }
    
    function getRfqDeadReasons()
    {
        $http({
            method :'GET',
            url : 'api/rfqs/deadReasons'
        }).then(function (successResponse){
            $scope.rfqDeadReasons = successResponse.data;
            $scope.rfqDeadReasonsAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });   
    }
    
    $scope.saveSearchParams = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/searchParams/save',
            data : {
                personLogin : $rootScope.getLoggedUser().email,
                personName : $scope.personalizationName,
                projectId : $scope.projects.id,
                projectStatusInit : $scope.projects.status.init,
                projectStatusOpened : $scope.projects.status.opened,
                projectStatusClosed : $scope.projects.status.closed,
                projectManager : $scope.projects.manager,
                keyAccountManager : $scope.projects.keyAccountManager,
                projectCustomer : $scope.projects.customer,
                projectIncludeAllGroup : $scope.projects.includeAllGroups,
                projectPurchaser : $scope.projects.purchaser,
                projectMarket : $scope.projects.marketChild,
                projectMarketParent : $scope.projects.marketParent,
                projectCreationDateFrom : $scope.projects.creationDateFrom,
                projectCreationDateTo : $scope.projects.creationDateTo,
                projectModificationDateFrom : $scope.projects.modificationDateFrom,
                projectModificationDateTo : $scope.projects.modificationDateTo,
                rfqId : $scope.rfqs.id,
                rfqStatusInit : $scope.rfqs.status.init,
                rfqStatusT10 : $scope.rfqs.status.t10,
                rfqStatusT20 : $scope.rfqs.status.t20,
                rfqStatusT30 : $scope.rfqs.status.t30,
                rfqStatusDead : $scope.rfqs.status.dead,
                rfqStatusRej : $scope.rfqs.status.rejected,
                rfqCustomerPn : $scope.rfqs.customerPN,
                rfqEngActionDesignRequired : $scope.rfqs.engineeringActions.designRequired,
                rfqEngActionCostingComplete : $scope.rfqs.engineeringActions.costingComplete,
                rfqEngActionDesignComplete : $scope.rfqs.engineeringActions.designComplete,
                rfqEngActionMassProdAuth : $scope.rfqs.engineeringActions.massProdAuth,
                rfqDeadReason : $scope.rfqs.deadReason,
                rfqPnCategory : $scope.rfqs.category,
                rfqPnFormat : $scope.rfqs.PNFormat,
                rfqPnNewDev : $scope.rfqs.newDev !== null ? ($scope.rfqs.newDev === 'newDevelopment') : null,
                rfqPnNewBiz : $scope.rfqs.newBiz !== null ? ($scope.rfqs.newBiz === 'newBusiness') : null,
                rfqPriority : $scope.rfqs.priority,
                rfqCustomerEngineer : $scope.rfqs.customerEngineer,
                rfqTamuraElecEngineer : $scope.rfqs.electricalEngineer,
                rfqTamuraMecEngineer : $scope.rfqs.mechanicalEngineer,
                rfqProduct : $scope.rfqs.productProduct,
                rfqProductChildOne : $scope.rfqs.productChildOne,
                rfqProductChildTwo : $scope.rfqs.productChildTwo,
                rfqProductParent : $scope.rfqs.productParent,
                rfqCreationDateFrom : $scope.rfqs.creationDateFrom,
                rfqCreationDateTo : $scope.rfqs.creationDateTo,
                rfqModificationDateFrom : $scope.rfqs.modificationDateFrom,
                rfqModificationDateTo : $scope.rfqs.modificationDateTo,
                salesCustomer : $scope.sales.customer,
                salesPurchaseManager : $scope.sales.purchaseManager,
                salesManager : $scope.sales.manager,
                salesValid : !$scope.sales.alsoInvalid
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            Flash.create('success', successResponse.data, 0, {}, true);
            getPersonalizations();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
            Flash.create('danger', successResponse.error, 0, {}, true);
        });   
    };
    
    $scope.loadSearchParams = function()
    {
        $scope.loading = true;
        
        $scope.customerSelected = true;
        $scope.marketParentSelected = true;
        $scope.productParentSelected = true; 
        $scope.productChildOneSelected = true;
        $scope.productChildTwoSelected = true;
        $scope.productProductSelected = true;
        $scope.salesCustomerSelected = true;
     
            $scope.loadProjects = {
            purchaser : $scope.personalization.project_purchaser,
            marketChildNumber : $scope.personalization.project_market.no_entity,
            marketChild :  $scope.personalization.project_market
        };
     
        $scope.projects = {
            id : $scope.personalization.project_id,
            status : {init : Boolean($scope.personalization.project_status_init), opened : Boolean($scope.personalization.project_status_opened), closed : Boolean($scope.personalization.project_status_closed)},
            manager : $scope.personalization.project_manager,
            keyAccountManager : $scope.personalization.key_account_manager,
            customer : $scope.personalization.project_customer,
            customerPath : $scope.personalization.project_customer.path,
            includeAllGroups : Boolean($scope.personalization.project_include_all_group),
            purchaser : $scope.personalization.project_purchaser,
            marketChildNumber : $scope.personalization.project_market.no_entity,
            marketChild :  $scope.personalization.project_market,
            marketParent : $scope.personalization.project_market_parent,
            creationDateFrom : $scope.personalization.project_creation_date_from,
            creationDateTo : $scope.personalization.project_creation_date_to,
            modificationDateFrom : $scope.personalization.project_modification_date_from,
            modificationDateTo : $scope.personalization.project_modification_date_to
        };
                            
        var PNFormatFilter = $scope.PNFormats.filter(function(obj){return obj.id === parseInt($scope.personalization.rfq_pn_format);});
        var priorityFilter = $scope.priorities.filter(function(obj){return obj.id === parseInt($scope.personalization.rfq_priority);});
        
        $scope.loadRfqs = {
            productChildOne : $scope.personalization.rfq_product_child_one,
            productChildTwo : $scope.personalization.rfq_product_child_two,
            productProduct : $scope.personalization.rfq_product
        };
        
        console.log($scope.personalization);
   
        $scope.rfqs = {
            id : $scope.personalization.rfq_id,
            customerPN : $scope.personalization.rfq_customer_pn,
            status : {
                init : Boolean($scope.personalization.rfq_status_init),
                t10 : Boolean($scope.personalization.rfq_status_t10),
                t20 : Boolean($scope.personalization.rfq_status_t20),
                t30 : Boolean($scope.personalization.rfq_status_t30),
                rejected : Boolean($scope.personalization.rfq_status_rej),
                dead : Boolean($scope.personalization.rfq_status_dead)
            },
            deadReason : $scope.personalization.rfq_dead_reason,
            engineeringActions : {
                designRequired : Boolean($scope.personalization.rfq_eng_action_design_required),
                costingComplete : Boolean($scope.personalization.rfq_eng_action_costing_complete),
                designComplete : Boolean($scope.personalization.rfq_eng_action_design_complete),
                massProdAuth : Boolean($scope.personalization.rfq_eng_action_design_mass_prd_auth)
            },
            category : $scope.personalization.rfq_pn_category,
            PNFormat : PNFormatFilter[0] !== undefined ? PNFormatFilter[0] : null,
            priority : priorityFilter[0] !== undefined ? priorityFilter[0] : null,
            newDev : $scope.personalization.rfq_pn_new_dev !== null ? ($scope.personalization.rfq_pn_new_dev === 1 ? 'newDevelopment' : 'runningOnSecondSource') : null,
            newBiz : $scope.personalization.rfq_pn_new_biz !== null ? ($scope.personalization.rfq_pn_new_biz === 1 ? 'newBusiness' : 'replacingExistingBusiness') : null,
            productParent : $scope.personalization.rfq_product_parent,
            productChildOne : $scope.personalization.rfq_product_child_one,
            productChildTwo : $scope.personalization.rfq_product_child_two,
            productProduct : $scope.personalization.rfq_product,
            customerEngineer : $scope.personalization.rfq_customer_engineer,
            electricalEngineer : $scope.personalization.rfq_electrical_engineer,
            mechanicalEngineer : $scope.personalization.rfq_mechanical_engineer,
            creationDateFrom : $scope.personalization.rfq_creation_date_from,
            creationDateTo : $scope.personalization.rfq_creation_date_to,
            modificationDateFrom : $scope.personalization.rfq_modification_date_from,
            modificationDateTo : $scope.personalization.rfq_modification_date_to
        };
        
        $scope.sales = {
            customer : $scope.personalization.sales_customer,
            customerPath : $scope.personalization.sales_customer.path,
            purchaseManager : $scope.personalization.sales_purchase_manager,
            manager : $scope.personalization.sales_manager,
            alsoInvalid : !Boolean($scope.personalization.sales_valid)
        };

        
        $scope.loading = false;
    };
    
    $scope.clearSearchParams = function()
    {
        
        $scope.loadProjects = {
            purchaser : null,
            marketChildNumber : null,
            marketChild :  null
        };
        
        $scope.loadRfqs = {
            productChildOne : null,
            productChildTwo : null,
            productProduct : null
        };
        
        $scope.projects = {
            id : null,
            status : {init : false, opened : false, closed : false},
            manager : null,
            keyAccountManager : null,
            customer : null,
            customerPath : null,
            purchaser : null,
            includeAllGroups : false,
            marketParent : null,
            marketChild : null,
            marketChildNumber : null,
            creationDateFrom : null,
            creationDateTo : null,
            modificationDateFrom : null,
            modificationDateTo : null
        };
        
        $scope.rfqs = {
            id : null,
            customerPN : null,
            status : {init : false, t10 : false, t20 : false, t30 : false, rejected : false, dead : false},
            deadReason : null,
            engineeringActions : {designRequired : false, costingComplete : false, designComplete : false, massProdAuth : false},
            category : null,
            PNFormat : null,
            priority : null,
            newDev : null,
            newBiz : null,
            productParent : null,
            productChildOne : null,
            productChildTwo : null,
            productProduct : null,
            customerEngineer : null,
            electricalEngineer : null,
            mechanicalEngineer : null,
            creationDateFrom : null,
            creationDateTo : null,
            modificationDateFrom : null,
            modificationDateTo : null
        };
        
        $scope.sales = {
            customer : null,
            customerPath : null,
            purchaseManager : null,
            manager : null,
            alsoInvalid : 3
        };
        
        $scope.customerSelected = false;
        $scope.marketParentSelected = false;
        $scope.productParentSelected = false; 
        $scope.productChildOneSelected = false;
        $scope.productChildTwoSelected = false;
        $scope.productProductSelected = false;
        $scope.salesCustomerSelected = false;
        
        saveSearchProjectParams();
        saveSearchRfqParams();
        saveSearchSaleParams();
        
        console.log($scope.rfqs.priority);
        location.reload();
    };

    var changeDateFormatsProjects = function (project){
        var lastModifiedDateSplit1;
        var lastModifiedDateSplit2;
        var creationDateSplit1;
        var creationDateSplit2;
        if ( project.creationDateFrom != null) {
            lastModifiedDateSplit1 = project.creationDateFrom.split(" ");
            lastModifiedDateSplit2 = lastModifiedDateSplit1[0].split("/");
            project.creationDateFrom = lastModifiedDateSplit2[1] + "/" + lastModifiedDateSplit2[0] + "/" + lastModifiedDateSplit2[2];
        }
        if ( project.creationDateTo != null) {
            creationDateSplit1 = project.creationDateTo.split(" ");
            creationDateSplit2 = creationDateSplit1[0].split("/");
            project.creationDateTo = creationDateSplit2[1] + "/" + creationDateSplit2[0] + "/" + creationDateSplit2[2] ;
        }
        if ( project.modificationDateFrom != null) {
            lastModifiedDateSplit1 = project.modificationDateFrom.split(" ");
            lastModifiedDateSplit2 = lastModifiedDateSplit1[0].split("/");
            project.modificationDateFrom = lastModifiedDateSplit2[1] + "/" + lastModifiedDateSplit2[0] + "/" + lastModifiedDateSplit2[2];
        }
        if ( project.modificationDateTo != null) {
            creationDateSplit1 = project.modificationDateTo.split(" ");
            creationDateSplit2 = creationDateSplit1[0].split("/");
            project.modificationDateTo = creationDateSplit2[1] + "/" + creationDateSplit2[0] + "/" + creationDateSplit2[2];
        }

    };
    var changeDateFormatsRfqs = function (rfq){
        var lastModifiedDateSplit1;
        var lastModifiedDateSplit2;
        var creationDateSplit1;
        var creationDateSplit2;
        if ( rfq.creationDateFrom != null) {
            lastModifiedDateSplit1 = rfq.creationDateFrom.split(" ");
            lastModifiedDateSplit2 = lastModifiedDateSplit1[0].split("/");
            rfq.creationDateFrom = lastModifiedDateSplit2[1] + "/" + lastModifiedDateSplit2[0] + "/" + lastModifiedDateSplit2[2];
        }
        if ( rfq.creationDateTo != null) {
            creationDateSplit1 = rfq.creationDateTo.split(" ");
            creationDateSplit2 = creationDateSplit1[0].split("/");
            rfq.creationDateTo = creationDateSplit2[1] + "/" + creationDateSplit2[0] + "/" + creationDateSplit2[2] ;
        }
        if ( rfq.modificationDateFrom != null) {
            lastModifiedDateSplit1 = rfq.modificationDateFrom.split(" ");
            lastModifiedDateSplit2 = lastModifiedDateSplit1[0].split("/");
            rfq.modificationDateFrom = lastModifiedDateSplit2[1] + "/" + lastModifiedDateSplit2[0] + "/" + lastModifiedDateSplit2[2];
        }
        if ( rfq.modificationDateTo != null) {
            creationDateSplit1 = rfq.modificationDateTo.split(" ");
            creationDateSplit2 = creationDateSplit1[0].split("/");
            rfq.modificationDateTo = creationDateSplit2[1] + "/" + creationDateSplit2[0] + "/" + creationDateSplit2[2];
        }

    };

    $scope.performSearch = function(){
        changeDateFormatsProjects($rootScope.search.projects);
        changeDateFormatsRfqs($rootScope.search.rfqs);
        console.log($rootScope.search);
        var checkProduct = $scope.checkRfqProduct($rootScope.search.rfqs.productParent,$rootScope.search.rfqs.productProduct);
        var checkMarket = $scope.checkProjectMarket($rootScope.search.projects.marketParent,$rootScope.search.projects.marketChild);
        if (checkProduct == 1 || checkMarket == 1){
            return false;
        }

        $state.go('project-rfq-npi-sales', {}, {reload: true});
    };


    $scope.checkRfqProduct = function(parent,market){
        if (parent !== null){
            if (market == null){
                alert($scope.chooseProduct[0].text);
                return 1;
            }
            else
                return 0;
        }
        else
            return 0;
    };
    $scope.checkProjectMarket = function(parent,product){
        if (parent !== null){
            if (product == null){
                alert($scope.chooseMarket[0].text);
                return 1;
            }
            else
                return 0;
        }
        else
            return 0;
    };


    $scope.refreshDataQuantity = function ()
    {
        $rootScope.loading = true;
        $http({
            method :'GET',
            url : 'api/projects/count?projects=' + JSON.stringify($rootScope.search.projects)
            + '&rfqs=' + JSON.stringify($rootScope.search.rfqs)
            + '&sales=' + JSON.stringify($rootScope.search.sales)
        }).then(function (successResponse){
            $scope.dataQuantity = successResponse.data
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };
    
    function getPersonalizations()
    {
        $http({
            method :'GET',
            url : 'api/searchParams/getPersonalizations?personLogin='+$rootScope.getLoggedUser().email
        }).then(function (successResponse){
            $scope.personalizations = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });  
    }
    
    // fire
    
    if($rootScope.getLoggedUser() != null){
        getSalesManagers();
        getCustomersCompanies();
        getPurchasers();
        getMarketParents();
        getMarketChildren();
        getCategories();
        getProductParents();
        getProductChildrenOne();
        getProductChildrenTwo();
        getProductProducts();
        getCustomerEngineers();
        getEngineers();
        getPurchaseManagers();
        getRfqDeadReasons();
        getPersonalizations();
    }

    changeDateFormatsProjects($scope.projects);
    changeDateFormatsRfqs($scope.rfqs);
    
    
    
});