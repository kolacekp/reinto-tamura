tamuraApp.controller("MaterialMgtCtrl", function ($scope,$state , $http, $rootScope, $mdDialog,Flash) {
    
    $rootScope.activeTopMenu = "material-mgt";

    $scope.stdItemToAdd = {};
    $scope.stdItems = [];

    $scope.materialsGridOptions = {
        //headerTemplate: 'header-template.html',
        showGridFooter : true,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 30,
        data: [],
        columnDefs: [
            {field : 'id', visible : false},
            {field : 'type', displayName : 'Type'},
            {field : 'part_no', displayName : 'Part No'},
            {field : 'description', displayName : 'Description'},
            {field : 'unitary_price', displayName : 'Unitary Price'}
        ],
        onRegisterApi : function(gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.saveRow = function(rowEntity)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/material/update',
            data : {
                material : rowEntity
            }
        }).then(function (successResponse){
            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.deleteSelected = function()
    {
        angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
            console.log(data);
            $http({
                method :'POST',
                url : 'api/material/delete',
                data : {
                    materialId : data.id
                }
            }).then(function (successResponse){
                console.log(successResponse.data);
            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
            });

            $scope.materialsGridOptions.data.splice($scope.materialsGridOptions.data.lastIndexOf(data), 1);
        });
    };

    $scope.addMaterial = function(ev)
    {

        $mdDialog.show({
            controller: "newMaterialDialogController",
            templateUrl: 'views/components/newMaterialDialog/newMaterialDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
        })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });

    };

    function getMaterials()
    {
        $http({
            method :'GET',
            url : 'api/material/getMaterials'
        }).then(function (successResponse){
            $scope.materialsGridOptions.data = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    //fire
    getMaterials();
});
