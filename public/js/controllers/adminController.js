tamuraApp.controller("AdminCtrl", function ($scope, $http, $rootScope,$translate,$mdDialog ,Flash) {
    
    $rootScope.activeTopMenu = "admin";

    $scope.updateAppliParamDate = function(name)
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/appliParam/updateDate',
            data : {
                name : name
            }
        }).then(function (successResponse){
            console.log(successResponse.data);
            $rootScope.loading = false;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.editUser = function (ev)
    {
        var selectedUser = $scope.gridApi.selection.getSelectedRows();

        if(selectedUser.length === 1){

            $mdDialog.show({
                controller: 'EditUserCtrl',
                templateUrl: 'views/components/editUser/editUser.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen:true,
                locals : {
                    user : selectedUser[0]
                }
            })
                .then(function(res) {
                    console.log("res");
                }, function() {
                    getUsers();
                });

        }else{
            Flash.clear();
            $translate(["data-sheet.select-exactly-one-material"]).then(function (translations) {
                Flash.create('danger', translations["data-sheet.select-exactly-one-material"], 0, {}, true);
            });
        }
    };

});
