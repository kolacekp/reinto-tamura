tamuraApp.controller("AddPersonToDirectoryCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, entity,customerId,isEngineer) {

    $scope.entity = entity;
    $scope.isCustomerDirectory = false;

    if(isEngineer != undefined && isEngineer != null){
        $scope.isEngineer = isEngineer;
    }
    else{
        $scope.isEngineer = false;
    }

    if($scope.entity != undefined && $scope.entity != null){
        if($scope.entity.type == "company"){
            $scope.isCustomerDirectory = true;
        }
    }
    $scope.customerId = customerId;
    $scope.person = {
        name: null,
        phone : null,
        mobile : null,
        email : null,
        saleManager : false,
        purchaseManager : false,
        engineer : false,
        deactivated : false
    };

    if(isEngineer)
        $scope.person.engineer = true;

    $scope.addPerson = function ()
    {
        $rootScope.loading = true;
        console.log(customerId);
        $http({
            method : 'POST',
            url : 'api/persons/addPerson',
            data : {
                person : $scope.person,
                entity : $scope.entity,
                customerId : $scope.customerId,
                isEngineer : $scope.isEngineer
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $mdDialog.cancel();
            $rootScope.loading = false;
        });
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };



});
