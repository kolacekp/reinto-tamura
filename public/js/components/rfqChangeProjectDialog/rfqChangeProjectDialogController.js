tamuraApp.controller("RfqChangeProjectDialogCtrl", function ($scope, $state, $rootScope, $http, $mdDialog, $mdMedia, rfq) {

    $scope.rfq = rfq;
    $scope.project = null;
    $scope.projects = [];
    $scope.projectsAll = [];

    $scope.projectSearchText = "";

    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };

    $scope.changeRfqProject = function()
    {
        $rootScope.loading = true;
        $http({
            method :'POST',
            url : 'api/rfqs/changeRfqProject',
            data : {
                rfqId : $scope.rfq.id,
                projectId : $scope.project.id
            }
        }).then(function (successResponse){
            console.log(successResponse.data);
            $rootScope.loading = false;
            $mdDialog.cancel();

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });

    };

    $scope.projectSearchTextChange = function(text)
    {
        $scope.projects = $scope.projectsAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    function getProjects()
    {
        $http({
            method :'GET',
            url : 'api/projects/getProjects'
        }).then(function (successResponse){
            $scope.projects = successResponse.data;
            $scope.projectsAll = successResponse.data;
            console.log($scope.projects);

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    getProjects();

});