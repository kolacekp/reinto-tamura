tamuraApp.controller("MailToPersonDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, person){

    $scope.person = person;
    $scope.subject = "";
    $scope.text = "";

    $scope.sendMailToPerson = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/mail/sendMailToPerson',
            data : {
                to : $scope.person.email,
                subject : $scope.subject,
                text : $scope.text
             }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };


    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };


});
