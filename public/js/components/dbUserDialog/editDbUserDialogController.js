tamuraApp.controller("EditDbUserDialogCtrl", function ($scope, $state, $rootScope, $translate, $http, $mdDialog, $mdMedia, selectedUser,roles) {

    var userRole = null;
    if(selectedUser.user !== null  && selectedUser.user.role !== null){
         userRole = selectedUser.user.role;
    }
    $scope.dbUser = {
        name : selectedUser.name,
        tamuraEntity : selectedUser.tamura_entity,
        email : selectedUser.email,
        deactivated : selectedUser.deactivated,
        login: selectedUser.db_login,
        password: selectedUser.db_password,
        id : selectedUser.id,
        role : userRole
    };

    console.log(selectedUser);
    $scope.roles = roles;

    $scope.tamuraEntities = [];
    $scope.tamuraEntitiesAll = [];

    $scope.tamuraEntitySearchText = "";

    $scope.emailExists = false;

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.checkEmailExists = function()
    {
        $http({
            method : 'GET',
            url : 'api/persons/checkEmailExists?email='+$scope.dbUser.email
        }).then(function (successResponse){
            $scope.emailExists = successResponse.data === true;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.editDbUser = function ()
    {
        $rootScope.loading = true;

        $http({
            method : 'POST',
            url : 'api/persons/updateDatabaseUserForm',
            data : {
                id : $scope.dbUser.id,
                tamuraEntity : $scope.dbUser.tamuraEntity.id,
                name : $scope.dbUser.name,
                email : $scope.dbUser.email,
                login : $scope.dbUser.login,
                password : $scope.dbUser.password,
                deactivated : $scope.dbUser.deactivated,
                role : $scope.dbUser.role
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $mdDialog.cancel();
            $rootScope.loading = false;
        });
    };

    $scope.tamuraEntitySearchTextChange = function(text)
    {
        $scope.tamuraEntities = $scope.tamuraEntitiesAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    function getTamuraEntities()
    {
        $http({
            method :'GET',
            url : 'api/tamuraEntities/all'
        }).then(function (successResponse){
            $scope.tamuraEntities = successResponse.data;
            $scope.tamuraEntitiesAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    getTamuraEntities();

});