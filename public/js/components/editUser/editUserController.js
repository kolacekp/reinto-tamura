tamuraApp.controller("EditUserCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, user,roles) {

    $scope.user = user;

    $scope.roles = roles;

    console.log($scope.user);


    $scope.editUser = function ()
    {
        $rootScope.loading = true;

        $http({
            method : 'POST',
            url : 'api/users/updateUser',
            data : {
                user : $scope.user
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $mdDialog.cancel();
            $rootScope.loading = false;
        });
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };



});
