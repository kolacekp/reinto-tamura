tamuraApp.controller("AddCustomerCompanyDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, entity, keyAccountManagers){

    $scope.keyAccountManagers = keyAccountManagers;
    $scope.keyAccountManagersAll = keyAccountManagers;

    $scope.keyAccountManager = null;
    $scope.keyAccountManagerSearchText = "";

    $scope.entity = entity;

    $scope.name = null;
    $scope.paymentTermNr = null;
    $scope.paymentTermType = null;
    $scope.sup = null;
    $scope.root = null;
    $scope.path = null;

    $scope.deactivated = false;

    $scope.keyAccountManagerSearchTextChange = function(text)
    {
        $scope.keyAccountManagers = $scope.keyAccountManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.addCustomerGroup = function()
    {
        $rootScope.loading = true;

        $http({
            method : 'POST',
            url : 'api/customers/addCustomerCompany',
            data : {
                name : $scope.name,
                deactivated : $scope.deactivated,
                keyAccountManagerId : $scope.keyAccountManager !== null ? $scope.keyAccountManager.id : null,
                paymentTermNr : $scope.paymentTermNr,
                paymentTermType : $scope.paymentTermType,
                sup : $scope.entity !== null ? $scope.entity.id : null,
                root : $scope.entity !== null ? $scope.entity.id_root : null,
                path : $scope.entity !== null ? ($scope.entity.path !== null ? ($scope.entity.path.concat($scope.entity.name.concat("\\"))) : ($scope.entity.name.concat("\\"))) : null,
                keyAccountManager : $scope.keyAccountManager
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $state.go('customer-directory', {}, {reload: true});
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    $scope.addCustomerCompany = function()
    {
        $rootScope.loading = true;
        console.log($scope.entity);
        $http({
            method : 'POST',
            url : 'api/customers/addCustomerCompany',
            data : {
                name : $scope.name,
                deactivated : $scope.deactivated,
                keyAccountManagerId : $scope.keyAccountManager !== null ? $scope.keyAccountManager.id : null,
                paymentTermNr : $scope.paymentTermNr,
                paymentTermType : $scope.paymentTermType,
                sup : $scope.entity !== null ? $scope.entity.id : null,
                root : $scope.entity !== null ? $scope.entity.id_root : null,
                path : $scope.entity !== null ? ($scope.entity.path !== null ? ($scope.entity.path.concat($scope.entity.name.concat("\\"))) : ($scope.entity.name.concat("\\"))) : null,
                keyAccountManager : $scope.keyAccountManager
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $state.go('customer-directory', {}, {reload: true});
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };


    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };


});
