tamuraApp.controller("NewRfqMaterialDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, rfq) {

    $scope.rfq = rfq;

    $scope.type = {};
    $scope.material = {};
    $scope.types = [];
    $scope.materials = [];

    $scope.typeSelected = false;

    $scope.saveRfqMaterial = function () {
        $rootScope.loading = true;
        console.log( $scope.material);
        $http({
            method: 'POST',
            url: 'api/rfqMaterial/create',
            data: {
                matId: $scope.material.id,
                quantity : $scope.qty,
                rfqId : $scope.rfq.id
            }
        }).then(function (successResponse) {
            $rootScope.loading = false;
            console.log(successResponse);
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.toggleTypeSelected = function ()
    {
        $scope.typeSelected = true;
        $scope.material = {};
        getMaterials();
    };

    function getTypes()
    {
        $http({
            method : 'GET',
            url : 'api/rfqMaterial/getTypes'
        }).then(function (successResponse){
            $scope.types = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getMaterials()
    {
        $http({
            method : 'GET',
            url : 'api/rfqMaterial/getForType?type='+$scope.type
        }).then(function (successResponse){
            $scope.materials = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    getTypes();



});
