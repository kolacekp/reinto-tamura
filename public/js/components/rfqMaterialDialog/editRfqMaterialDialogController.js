tamuraApp.controller("EditRfqMaterialDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, rfqMaterial) {

    $scope.rfqMaterial = rfqMaterial;

    $scope.type = rfqMaterial.type;

    console.log($scope.rfqMaterial);

    $scope.partNo = rfqMaterial.part_no;
    $scope.material = {};
    $scope.types = [];
    $scope.materials = [];

    $scope.qty = rfqMaterial.qty;

    $scope.typeSelected = true;

    $scope.editRfqMaterial = function () {
        $rootScope.loading = true;
        console.log($scope.material);
        $http({
            method: 'POST',
            url: 'api/rfqMaterial/update',
            data: {
                rfqMatId : $scope.rfqMaterial.id,
                matId: $scope.material.id,
                quantity : $scope.qty
            }
        }).then(function (successResponse) {
            $rootScope.loading = false;
            console.log(successResponse);
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.toggleTypeSelected = function ()
    {
        $scope.typeSelected = true;
        $scope.material = {};
        getMaterials();
    };

    function getTypes()
    {
        $http({
            method : 'GET',
            url : 'api/rfqMaterial/getTypes'
        }).then(function (successResponse){
            $scope.types = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getMaterials()
    {
        $http({
            method : 'GET',
            url : 'api/rfqMaterial/getForType?type='+$scope.type
        }).then(function (successResponse){
            $scope.materials = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getCurrentMaterial()
    {
        $http({
            method : 'GET',
            url : 'api/rfqMaterial/getForTypeAndPartNo?type='+$scope.type+'&partNo='+$scope.partNo
        }).then(function (successResponse){
            $scope.material = successResponse.data;
            console.log($scope.material);
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    getTypes();
    getMaterials();
    getCurrentMaterial();


});
