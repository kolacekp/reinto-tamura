tamuraApp.controller("AddCustomerGroupDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, sup, root, keyAccountManagers){

    $scope.keyAccountManagers = keyAccountManagers;
    $scope.keyAccountManagersAll = keyAccountManagers;

    $scope.keyAccountManager = null;
    $scope.keyAccountManagerSearchText = "";

    $scope.name = null;
    $scope.deactivated = false;

    $scope.sup = sup;
    $scope.root = root;

    $scope.keyAccountManagerSearchTextChange = function(text)
    {
        $scope.keyAccountManagers = $scope.keyAccountManagersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    $scope.addCustomerGroup = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/customers/addCustomerGroup',
            data : {
                name : $scope.name,
                deactivated : $scope.deactivated,
                keyAccountManagerId : $scope.keyAccountManager !== null ? $scope.keyAccountManager.id : null,
                sup : $scope.sup,
                root : $scope.root
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $state.go('customer-directory', {}, {reload: true});
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };


    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };


});
