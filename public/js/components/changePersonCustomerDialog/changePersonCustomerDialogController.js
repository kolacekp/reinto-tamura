tamuraApp.controller("ChangePersonCustomerDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, customers, person){

    $scope.customers = customers;
    $scope.customersAll = customers;

    $scope.customer = null;
    $scope.customerSearchText = "";

    $scope.person = person;

    $scope.customerSearchTextChange = function(text)
    {
        $scope.customers = $scope.customersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };


    $scope.changePersonCustomer = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/persons/changeCustomer',
            data : {
                customerId : $scope.customer.id,
                personId : $scope.person.id
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $state.go('customer-directory', {}, {reload: true});
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };


    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };


});
