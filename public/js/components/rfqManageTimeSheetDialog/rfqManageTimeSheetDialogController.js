tamuraApp.controller("RfqManageTimeSheetDialogCtrl", function ($scope, $state, $rootScope, $translate, $http, $mdDialog, $mdMedia, Flash, rfq) {

    $scope.rfq = rfq;
    $scope.timeSheet = {};
    $scope.totalHours = 0;
    $scope.tamuraEngineerSearchText = "";
    $scope.tamuraEngineers = [];
    $scope.tamuraEngineersAll = [];
    $scope.actions = [];

    $scope.timeSheetsGridOptions = {
        showGridFooter: true,
        data: [],
        columnDefs: [
            {name: 'id', visible: false},
            {field: 'date', date: 'Date', cellEditableCondition: false},
            {field: 'engineer.name', displayName: 'Person Name', cellEditableCondition: false},
            {field: 'action', displayName: 'Action', cellEditableCondition: false},
            {field: 'hours', displayName: 'Hours', cellEditableCondition: false},
            {field: 'invoice_number', displayName: 'Time Sheet Invoice Nr', cellEditableCondition: false}
        ],
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
        }
    };

    $translate([
        "rfq-manage-time-sheet-dialog.actions.rfq-review",
        "rfq-manage-time-sheet-dialog.actions.electrical-design",
        "rfq-manage-time-sheet-dialog.actions.mechanical-design",
        "rfq-manage-time-sheet-dialog.actions.travel",
        "rfq-manage-time-sheet-dialog.actions.npi",
        "rfq-manage-time-sheet-dialog.actions.others",
    ]).then(function (translations){
        $scope.actions = [
            {id : 1, value : "RFQ Review", text : translations["rfq-manage-time-sheet-dialog.actions.rfq-review"]},
            {id : 2, value : "Electrical Design", text : translations["rfq-manage-time-sheet-dialog.actions.electrical-design"]},
            {id : 3, value : "Mechanical Design", text : translations["rfq-manage-time-sheet-dialog.actions.mechanical-design"]},
            {id : 4, value : "Travel", text : translations["rfq-manage-time-sheet-dialog.actions.travel"]},
            {id : 5, value : "NPI", text : translations["rfq-manage-time-sheet-dialog.actions.npi"]},
            {id : 6, value : "Others", text : translations["rfq-manage-time-sheet-dialog.actions.others"]}
        ];
    });

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.createTimeSheet = function ()
    {
        $rootScope.loading = true;

        $http({
            method : 'POST',
            url : 'api/timeSheets/create',
            data : {
                rfqId : $scope.rfq.id,
                createdBy : $rootScope.getLoggedUser().username !== null ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name,
                tamuraEngineerId : ($scope.timeSheet.tamuraEngineer !== undefined && $scope.timeSheet.tamuraEngineer !== null)? $scope.timeSheet.tamuraEngineer.id : null,
                date : $scope.timeSheet.date,
                action: $scope.timeSheet.action,
                hours : $scope.timeSheet.hours
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            getTimeSheets();
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };

    $scope.tamuraEngineerSearchTextChange = function(text)
    {
        $scope.tamuraEngineers = $scope.tamuraEngineersAll.filter(function(obj){
            return obj.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
        });
    };

    function summarizeHours(arr)
    {
        var sum = 0;
        for(var i = 0; i < arr.length; i++){
            sum += parseInt(arr[i].hours);
        }
        return sum;
    }

    function getTimeSheets()
    {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/timeSheets/getByRfq?rfqId=' + $scope.rfq.id
        }).then(function (successResponse){

            $scope.timeSheetsGridOptions.data = successResponse.data;

            $scope.totalHours = summarizeHours($scope.timeSheetsGridOptions.data);

            $rootScope.loading = false;

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    function getEngineers()
    {
        $http({
            method :'GET',
            url : 'api/persons/engineers'
        }).then(function (successResponse){
            $scope.tamuraEngineers = successResponse.data;
            $scope.tamuraEngineersAll = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    getTimeSheets();
    getEngineers();

});