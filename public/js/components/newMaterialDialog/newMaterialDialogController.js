tamuraApp.controller("newMaterialDialogController", function ($scope,$state, $http, $rootScope, $mdDialog, Flash){

    $scope.material = {
        type : null,
        partNo : null,
        description: null,
        unitaryPrice : null
    };

    $scope.saveMaterial = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/material/create',
            data : {
                type : $scope.material.type,
                partNo : $scope.material.partNo,
                description: $scope.material.description,
                unitaryPrice : $scope.material.unitaryPrice
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            Flash.clear();
            Flash.create('success', "Saved", 0, {}, true);
            $state.go('material-mgt', {}, {reload: true});
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
            Flash.clear();
            Flash.create('danger', "Error in saving.", 0, {}, true);
        });
    };

    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };


});
