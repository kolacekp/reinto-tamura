tamuraApp.controller("AddTamuraEntityDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash, type){

    $scope.type = type;
    $scope.name = null;
    $scope.shortName = null;
    $scope.country = null;
    $scope.shortNameAlreadyExists = false;
    $scope.tamuraEntities = [] ;

    $scope.addTamuraEntity = function()
    {
        $rootScope.loading = true;
        $http({
            method : 'POST',
            url : 'api/tamura-directory/create',
            data : {
                name : $scope.name,
                shortName : $scope.shortName,
                type : $scope.type,
                country : $scope.country
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };

    function getData()
    {
        $rootScope.loading = true;
        $scope.tamuraEntities = [];

        $http({
            method : 'GET',
            url : 'api/tamura-directory/fetch'
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.tamuraEntities = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    $scope.checkShortName = function(shortName){
        $scope.shortNameAlreadyExists = false;
        $scope.tamuraEntities.forEach(function (entity) {
            if (entity.short_name == shortName){
                $scope.shortNameAlreadyExists = true;
                return false;
            }
        });
    };

    function getAllCountries()
    {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/countries/all'
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.countries = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }
    getAllCountries();
    getData();
});
