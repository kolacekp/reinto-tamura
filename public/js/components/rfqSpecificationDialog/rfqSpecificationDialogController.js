tamuraApp.controller("RfqSpecificationDialogCtrl", function ($scope, $state, $rootScope, $translate, $http, $mdDialog, $mdMedia, Flash, rfq) {

    $scope.rfq = rfq;

    $scope.specificationsGridOptions = {
        showGridFooter: true,
        data: [],
        columnDefs: [
            {name: 'id', visible: false},
            {field: 'label', displayName: 'Reference', cellEditableCondition: true},
            {field: 'version', displayName: 'Version', cellEditableCondition: true},
            {field: 'link', displayName: 'Link to document', cellEditableCondition: false, minWidth: 350},
            {field: 'status', displayName: 'Active Doc', cellEditableCondition: true, type: 'boolean'},
            {field: 'master', displayName: 'Master Doc', cellEditableCondition: true, type: 'boolean'}
        ],
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        }
    };

    $scope.saveRow = function (rowEntity) {
        $rootScope.loading = true;
        $http({
            method: 'POST',
            url: 'api/rfqs/updateSpecification',
            data: {
                specification: rowEntity
            }
        }).then(function (successResponse) {
            console.log(successResponse.data);
            $rootScope.loading = false;
            if (successResponse.data === 1) {
                getSpecifications();
            }
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    };


    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.addSpecification = function (ev)
    {
        $mdDialog.show({
            controller: 'NewRfqSpecificationDialogController',
            templateUrl: 'views/components/newRfqSpecificationDialog/newRfqSpecificationDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen:true,
            skipHide : true,
            locals : {
                rfq : $scope.rfq
            }
        })
        .then(function(res) {
            console.log("res");
        }, function() {
            getSpecifications();
        });
    };

    $scope.deleteSelectedSpecification = function ()
    {
        var selectedSpecifications = $scope.gridApi.selection.getSelectedRows();

        if(selectedSpecifications.length == 1){

            $http({
                method : 'POST',
                url : 'api/rfqs/deleteSpecification?specification_id=' + selectedSpecifications[0].id
            }).then(function (successResponse){
                $rootScope.loading = false;

                $scope.specificationsGridOptions.data = $scope.specificationsGridOptions.data.filter(function( obj ) {
                    return obj.id !== selectedSpecifications[0].id;
                });

            }, function (errorResponse) {
                // error callback
                console.log(errorResponse);
                $rootScope.loading = false;
            });

        }else{
            Flash.clear();
            $translate(["rfq-specification-dialog.select-exactly-one-specification"]).then(function (translations) {
                Flash.create('danger', translations["rfq-specification-dialog.select-exactly-one-specification"], 0, {}, true);
            });
        }
    };

    function getSpecifications()
    {
        $rootScope.loading = true;

        $http({
            method : 'GET',
            url : 'api/rfqs/specifications?id=' + $scope.rfq.id
        }).then(function (successResponse){

            $scope.specificationsGridOptions.data = successResponse.data;
            $rootScope.loading = false;

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
            $rootScope.loading = false;
        });
    }

    getSpecifications();

});