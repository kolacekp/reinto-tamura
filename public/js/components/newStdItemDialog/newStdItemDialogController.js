tamuraApp.controller("NewStdItemDialogCtrl", function ($scope, $state, $rootScope, $http, $mdDialog, $mdMedia) {

    $scope.newStdItem = {};

    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };

    $scope.saveNewStdItem = function()
    {
        $http({
            method :'POST',
            url : 'api/stdItems/create',
            data : {
                stdItem : $scope.newStdItem
            }
        }).then(function (successResponse){
            console.log(successResponse);

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });


        $mdDialog.cancel();

        $state.go('std-items-mgt', {}, {reload:true});

    };

});