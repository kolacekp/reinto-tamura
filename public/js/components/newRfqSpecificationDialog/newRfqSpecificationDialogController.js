tamuraApp.controller("NewRfqSpecificationDialogController", function ($scope, $state, $rootScope, $timeout, $translate, $http, $mdDialog, $mdMedia, Flash, Upload, rfq) {

    $scope.newRfqSpecification = {
        rfq_id : rfq.id,
        label : null,
        version : null,
        status : false,
        master : false
    };
    $scope.file = null;

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.saveRfqSpecification = function(specFile)
    {
        if($scope.newRfqSpecification !== null) {

            console.log(specFile);

            specFile.upload = Upload.upload({
                url: 'api/rfqs/addSpecification',
                data : {
                    specification : $scope.newRfqSpecification,
                    file : specFile
                }
            });

            specFile.upload.then(function (response) {
                $timeout(function () {
                    console.log(response.data);
                    $mdDialog.cancel();
                });
            }, function (response) {
                console.log(response.data)
            }, function (evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });

        }
    };

});