tamuraApp.controller("editRfqProductionCtrl", function ($scope,$state, $http, $rootScope, $mdDialog,$translate, Flash,productionId) {

    console.log("editRfqProductionCtrl");
    $scope.editRfqProduction = function()
    {
        $rootScope.loading = true;

        if (  $scope.production != null &&  $scope.production.lastProductionDate != null ) {
            var pordDate =  $scope.production.lastProductionDate.split("/");
            $scope.production.lastProductionDate = pordDate[1] + "/" + pordDate[0] + "/" + pordDate[2];
        }

        $http({
            method : 'POST',
            url : 'api/rfqs/editProduction',
            data : {
                production : $scope.production
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            console.log(successResponse);
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    function getFactories()
    {
        $http({
            method :'GET',
            url : 'api/tamuraEntities/factories'
        }).then(function (successResponse){
            $scope.factories = successResponse.data;
        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    function getCurrencies()
    {
        $http({
            method :'GET',
            url : 'api/currencies/all'
        }).then(function (successResponse){
            $scope.currencies = successResponse.data;
            $scope.currenciesAll = successResponse.data;
            $scope.rfqCurrencies = successResponse.data;
            $scope.rfqCurrenciesAll = successResponse.data;
            $scope.rfqMaterialCurrencies = successResponse.data;
            $scope.rfqMaterialCurrenciesAll = successResponse.data;

            var defaultCurrency = $scope.rfqMaterialCurrenciesAll.filter(function(obj){
                return obj.default === "X";
            })[0];

            $scope.rfqMaterialCurrency = defaultCurrency;
            $scope.oldRfqMaterialCurrency = defaultCurrency;
            $scope.estimatedEXW1OldCurrency = defaultCurrency;
            $scope.estimatedEXW2OldCurrency = defaultCurrency;
            $scope.estimatedEXW3OldCurrency = defaultCurrency;

        }, function (errorResponse) {
            // error callback
            console.log(errorResponse);
        });
    }

    function getProduction() {
        $rootScope.loading = true;
        $http({
            method : 'GET',
            url : 'api/rfqs/productionById?productionId=' + productionId
        }).then(function (successResponse){
            $rootScope.loading = false;
            $scope.production = successResponse.data;
            if (  $scope.production != null &&  $scope.production.lastProductionDate != null ) {
                var pordDate =  $scope.production.lastProductionDate.split("/");
                $scope.production.lastProductionDate = pordDate[1] + "/" + pordDate[0] + "/" + pordDate[2];
            }
        }, function (errorResponse) {

            console.log(errorResponse);
        });
    }

    getProduction();
    getFactories();
    getCurrencies();
});