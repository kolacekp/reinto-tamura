tamuraApp.controller("AddCurrencyDialogCtrl", function ($scope,$state, $http, $rootScope, $mdDialog, Flash){

    $scope.currency = {
        long_name : null,
        short_name : null,
        symbol: null,
        exch_rate : null,
        last_modified_by : $rootScope.getLoggedUser().username !== null ? $rootScope.getLoggedUser().username : $rootScope.getLoggedUser().name
    };

    $scope.addCurrency = function()
    {
        $rootScope.loading = true;

        $http({
            method : 'POST',
            url : 'api/currencies/create',
            data : {
                long_name : $scope.currency.long_name,
                short_name : $scope.currency.short_name,
                symbol: $scope.currency.symbol,
                exch_rate : $scope.currency.exch_rate,
                last_modified_by : $scope.currency.last_modified_by
            }
        }).then(function (successResponse){
            $rootScope.loading = false;
            $mdDialog.cancel();
        }, function (errorResponse) {
            // error callback
            $rootScope.loading = false;
        });
    };

    $scope.cancel = function()
    {
        $mdDialog.cancel();
    };

});
