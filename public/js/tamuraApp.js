/**
 * Tamura
 * Author Petr Kolacek
 * Developer at Reinto s.r.o.
 * kolacek@reinto.cz
 *
 */

'use strict';

var tamuraApp = angular.module("tamuraApp", [
    "ngResource",
    "ui.router",
    "ui.bootstrap",
    "pascalprecht.translate",
    "ngFileUpload",
    "ngAnimate",
    "ngAria",
    "ngMaterial",
    "LocalStorageModule",
    "ngSanitize",
    "ngFileUpload",
    "ngFlash",
    "ui.tree",
    "ui.grid",
    "ui.grid.edit",
    "ui.grid.rowEdit",
    "ui.grid.cellNav",
    "ui.grid.selection",
    "ngCsv"
]);


tamuraApp.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider, $provide, localStorageServiceProvider, $qProvider, FlashProvider)
{

    // Redirect home if any other states
    // are requested other than users
    $urlRouterProvider.otherwise("/search");

    $stateProvider
        .state("search", {
            url: "/search",
            templateUrl: "views/search.html",
            controller: "SearchCtrl"
        })
        .state("project-rfq-npi-sales", {
            url: "/project-rfq-npi-sales",
            templateUrl: "views/project-rfq-npi-sales.html",
            controller: "ProjectRfqNpiSalesCtrl"
        })
        .state("customer-directory", {
            url: "/customer-directory",
            templateUrl: "views/customer-directory.html",
            controller: "CustomerDirectoryCtrl"
        })
        .state("tamura-directory", {
            url: "/tamura-directory",
            templateUrl: "views/tamura-directory.html",
            controller: "TamuraDirectoryCtrl"
        })
        .state("time-sheet-invoice", {
            url: "/time-sheet-invoice",
            templateUrl: "views/time-sheet-invoice.html",
            controller: "TimeSheetInvoiceCtrl"
        })
        .state("lme-mgt", {
            url: "/lme-mgt",
            templateUrl: "views/lme-mgt.html",
            controller: "LmeMgtCtrl"
        })
        .state("exch-rate-mgt", {
            url: "/exch-rate-mgt",
            templateUrl: "views/exch-rate-mgt.html",
            controller: "ExchRateMgtCtrl"
        })
        .state("std-items-mgt", {
            url: "/std-items-mgt",
            templateUrl: "views/std-items-mgt.html",
            controller: "StdItemsMgtCtrl"
        })
        .state("material-mgt", {
            url: "/material-mgt",
            templateUrl: "views/material-mgt.html",
            controller: "MaterialMgtCtrl"
        })
        .state("db-user", {
            url: "/db-user",
            templateUrl: "views/db-user.html",
            controller: "DbUserCtrl"
        })
        .state("admin", {
            url: "/admin",
            templateUrl: "views/admin.html",
            controller: "AdminCtrl"
        });

    localStorageServiceProvider
        .setPrefix('tamuraProperty');

    // Translations settings
    $translateProvider.useStaticFilesLoader({
        prefix: './translations/',
        suffix: '.json'
    }).preferredLanguage('en').useMissingTranslationHandlerLog();

    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.preferredLanguage('en');
    
    $qProvider.errorOnUnhandledRejections(false);
    
    FlashProvider.setTimeout(3000);

}).run(function($rootScope, $state, $http, $translate, localStorageService, Flash) {
    console.log("run");

    // no loader at the beginning
    $rootScope.loading = false;

    var roles = {
      superUser  : 30,
      basicUser  : 20
    };

    // check selected language (default en)
    var lang = localStorageService.get("lang");
    if(lang !== null){
        $translate.use(lang).then(function() { $rootScope.$emit('doTranslations'); });
        $rootScope.selectedLanguage = lang;
    }else{
        $translate.use("en");
        $rootScope.selectedLanguage = "en";
        localStorageService.set("lang", "en");
    }
    
    // check if is logged
    $rootScope.isLogged = function()
    {
        return localStorageService.get("isLogged");
    };
    
    // logout function
    $rootScope.logout = function()
    {
        localStorageService.set("isLogged", false);
        localStorageService.set("loggedUser", null);
    };
    
    $rootScope.getLoggedUser = function()
    {
        return localStorageService.get("loggedUser");
    };

    $rootScope.isSuperUser = function()
    {
        var loggedUser = localStorageService.get("loggedUser");
        if (loggedUser != null && loggedUser.role != null){
            return loggedUser.role.id == roles.superUser;
        }
        return false;
    };

    $rootScope.isBasicUser = function()
    {
        var loggedUser = localStorageService.get("loggedUser");
        if (loggedUser != null && loggedUser.role != null){
            return loggedUser.role.id == roles.basicUser;
        }
        return false;
    };
    
    // init
    $rootScope.search = {};
    $rootScope.search.projects = {};
    
    // search page initializations
    $rootScope.search.projects = {
        id : null,
        name : null,
        status : {init : false, opened : false, closed : false},
        manager : null,
        keyAccountManager : null,
        customer : null,
        customerRootId : null,
        customerPath : null,
        purchaser : null,
        includeAllGroups : false,
        marketParent : null,
        marketChild : null,
        marketChildNumber : null,
        creationDateFrom : null,
        creationDateTo : null,
        modificationDateFrom : null,
        modificationDateTo : null
    };
    
    var searchProjectParams = localStorageService.get("searchProjectParams");
    if(searchProjectParams !== null){
        var obj = JSON.parse(searchProjectParams);        
        $rootScope.search.projects.id = (obj.hasOwnProperty('id') && obj.id !== null) ? obj.id : null;
        $rootScope.search.projects.name = (obj.hasOwnProperty('name') && obj.name !== null) ? obj.name : null;
        $rootScope.search.projects.status = (obj.hasOwnProperty('status') && obj.status !== null) ? obj.status : {init : false, opened : false, closed : false};
        $rootScope.search.projects.manager = (obj.hasOwnProperty('manager') && obj.manager !== null) ? obj.manager : null;
        $rootScope.search.projects.keyAccountManager = (obj.hasOwnProperty('keyAccountManager') && obj.keyAccountManager !== null) ? obj.keyAccountManager : null;
        $rootScope.search.projects.customer = (obj.hasOwnProperty('customer') && obj.customer !== null) ? obj.customer : null;
        $rootScope.search.projects.customerRootId = (obj.hasOwnProperty('customerRootId') && obj.customerRootId !== null) ? obj.customerRootId : null;       
        $rootScope.search.projects.customerPath = (obj.hasOwnProperty('customerPath') && obj.customerPath !== null) ? obj.customerPath : null;
        $rootScope.search.projects.purchaser = (obj.hasOwnProperty('purchaser') && obj.purchaser !== null) ? obj.purchaser : null;
        $rootScope.search.projects.includeAllGroups = (obj.hasOwnProperty('includeAllGroups') && obj.includeAllGroups !== null) ? obj.includeAllGroups : false;
        $rootScope.search.projects.marketParent = (obj.hasOwnProperty('marketParent') && obj.marketParent !== null) ? obj.marketParent : null;
        $rootScope.search.projects.marketChild = (obj.hasOwnProperty('marketChild') && obj.marketChild !== null) ? obj.marketChild : null;
        $rootScope.search.projects.marketChildNumber = (obj.hasOwnProperty('marketChildNumber') && obj.marketChildNumber !== null) ? obj.marketChildNumber : null;
        $rootScope.search.projects.creationDateFrom = (obj.hasOwnProperty('creationDateFrom') && obj.creationDateFrom !== null) ? obj.creationDateFrom : null;
        $rootScope.search.projects.creationDateTo = (obj.hasOwnProperty('creationDateTo') && obj.creationDateTo !== null) ? obj.creationDateTo : null;
        $rootScope.search.projects.modificationDateFrom = (obj.hasOwnProperty('modificationDateFrom') && obj.modificationDateFrom !== null) ? obj.modificationDateFrom : null;
        $rootScope.search.projects.modificationDateTo = (obj.hasOwnProperty('modificationDateTo') && obj.modificationDateTo !== null) ? obj.modificationDateTo : null;                
    }
    
    $rootScope.search.rfqs = {
        id : null,
        customerPN : null,
        status : {init : false, t10 : false, t20 : false, t30 : false, rejected : false, dead : false},
        deadReason : null,
        engineeringActions : {designRequired : false, costingComplete : false, designComplete : false, massProdAuth : false},
        category : null,
        PNFormat : null,
        priority : null,
        newDev : null,
        newBiz : null,
        productParent : null,
        productChildOne : null,
        productChildTwo : null,
        productProduct : null,
        customerEngineer : null,
        electricalEngineer : null,
        mechanicalEngineer : null,
        creationDateFrom : null,
        creationDateTo : null,
        modificationDateFrom : null,
        modificationDateTo : null
    };
    
    var searchRfqParams = localStorageService.get("searchRfqParams");
    if(searchRfqParams !== null){
        var obj = JSON.parse(searchRfqParams);        
        $rootScope.search.rfqs.id = (obj.hasOwnProperty('id') && obj.id !== null) ? obj.id : null;
        $rootScope.search.rfqs.customerPN = (obj.hasOwnProperty('customerPN') && obj.customerPN !== null) ? obj.customerPN : null;
        $rootScope.search.rfqs.status = (obj.hasOwnProperty('status') && obj.status !== null) ? obj.status : {init : false, t10 : false, t20 : false, t30 : false, rejected : false, dead : false};
        $rootScope.search.rfqs.deadReason = (obj.hasOwnProperty('deadReason') && obj.deadReason !== null) ? obj.deadReason : null;
        $rootScope.search.rfqs.engineeringActions = (obj.hasOwnProperty('engineeringActions') && obj.engineeringActions !== null) ? obj.engineeringActions : {designRequired : false, costingComplete : false, designComplete : false, massProdAuth : false};
        $rootScope.search.rfqs.category = (obj.hasOwnProperty('category') && obj.category !== null) ? obj.category : null;
        $rootScope.search.rfqs.PNFormat = (obj.hasOwnProperty('PNFormat') && obj.PNFormat !== null) ? obj.PNFormat : null;
        $rootScope.search.rfqs.priority = (obj.hasOwnProperty('priority') && obj.priority !== null) ? obj.priority : null;     
        $rootScope.search.rfqs.newDev = (obj.hasOwnProperty('newDev') && obj.newDev !== null) ? obj.newDev : null;
        $rootScope.search.rfqs.newBiz = (obj.hasOwnProperty('newBiz') && obj.newBiz !== null) ? obj.newBiz : null;       
        $rootScope.search.rfqs.productParent = (obj.hasOwnProperty('productParent') && obj.productParent !== null) ? obj.productParent : null;
        $rootScope.search.rfqs.productChildOne = (obj.hasOwnProperty('productChildOne') && obj.productChildOne !== null) ? obj.productChildOne : null;
        $rootScope.search.rfqs.productChildTwo = (obj.hasOwnProperty('productChildTwo') && obj.productChildTwo !== null) ? obj.productChildTwo : null;
        $rootScope.search.rfqs.productProduct = (obj.hasOwnProperty('productProduct') && obj.productProduct !== null) ? obj.productProduct : null;
        $rootScope.search.rfqs.customerEngineer = (obj.hasOwnProperty('customerEngineer') && obj.customerEngineer !== null) ? obj.customerEngineer : null;
        $rootScope.search.rfqs.electricalEngineer = (obj.hasOwnProperty('electricalEngineer') && obj.electricalEngineer !== null) ? obj.electricalEngineer : null;
        $rootScope.search.rfqs.mechanicalEngineer = (obj.hasOwnProperty('mechanicalEngineer') && obj.mechanicalEngineer !== null) ? obj.mechanicalEngineer : null;                                                
        $rootScope.search.rfqs.creationDateFrom = (obj.hasOwnProperty('creationDateFrom') && obj.creationDateFrom !== null) ? obj.creationDateFrom : null;
        $rootScope.search.rfqs.creationDateTo = (obj.hasOwnProperty('creationDateTo') && obj.creationDateTo !== null) ? obj.creationDateTo : null;
        $rootScope.search.rfqs.modificationDateFrom = (obj.hasOwnProperty('modificationDateFrom') && obj.modificationDateFrom !== null) ? obj.modificationDateFrom : null;
        $rootScope.search.rfqs.modificationDateTo = (obj.hasOwnProperty('modificationDateTo') && obj.modificationDateTo !== null) ? obj.modificationDateTo : null;                
    }
    
    $rootScope.search.sales = {
        customer : null,
        customerPath : null,
        purchaseManager : null,
        manager : null,
        alsoInvalid : 3
    };
    
    var searchSaleParams = localStorageService.get("searchSaleParams");
    if(searchSaleParams !== null){
        var obj = JSON.parse(searchSaleParams);    
        $rootScope.search.sales.customer = (obj.hasOwnProperty('customer') && obj.customer !== null) ? obj.customer : null;
        $rootScope.search.sales.customerPath = (obj.hasOwnProperty('customerPath') && obj.customerPath !== null) ? obj.customerPath : null;
        $rootScope.search.sales.purchaseManager = (obj.hasOwnProperty('purchaseManager') && obj.purchaseManager !== null) ? obj.purchaseManager : null;     
        $rootScope.search.sales.manager = (obj.hasOwnProperty('manager') && obj.manager !== null) ? obj.manager : null;
        $rootScope.search.sales.alsoInvalid = (obj.hasOwnProperty('alsoInvalid') && obj.alsoInvalid !== null) ? obj.alsoInvalid : 3;
    }

    $rootScope.getDateTime = function() {
        var now     = new Date();
        var year    = now.getFullYear();
        var month   = now.getMonth()+1;
        var day     = now.getDate();
        var hour    = now.getHours();
        var minute  = now.getMinutes();
        var second  = now.getSeconds();
        if(month.toString().length == 1) {
            var month = '0'+month;
        }
        if(day.toString().length == 1) {
            var day = '0'+day;
        }
        if(hour.toString().length == 1) {
            var hour = '0'+hour;
        }
        if(minute.toString().length == 1) {
            var minute = '0'+minute;
        }
        if(second.toString().length == 1) {
            var second = '0'+second;
        }
        var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
        return dateTime;
    };

    $rootScope.getDateTimeFromDate = function(date) {
        var year    = date.getFullYear();
        var month   = date.getMonth()+1;
        var day     = date.getDate();
        var hour    = date.getHours();
        var minute  = date.getMinutes();
        var second  = date.getSeconds();
        if(month.toString().length == 1) {
            var month = '0'+month;
        }
        if(day.toString().length == 1) {
            var day = '0'+day;
        }
        if(hour.toString().length == 1) {
            var hour = '0'+hour;
        }
        if(minute.toString().length == 1) {
            var minute = '0'+minute;
        }
        if(second.toString().length == 1) {
            var second = '0'+second;
        }
        var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
        return dateTime;
    }

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
        Flash.clear();
    });

});

// confirm click directive
tamuraApp.directive('ngConfirmClick', [
    function() {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);