<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TamuraEntity extends Model
{
    protected $table = "tamura_entities";
    
    protected $filable = [
        'id'
        ,'name'
        ,'short_name'
        ,'location'
        ,'type'
        ,'deactivated'
        ,'labour_rate'
        ,'freight'
        ,'admin'
        ,'profit'
        ,'linked_sold'
        ,'currency_id'
        ,'mail'
        ,'fob_charge'
        ,'terminal_handling'
        ,'delivery_order'
        ,'customs_clearance'
        ,'haulage_to_warehouse'
        ,'manufactured_purchased'
        ,'fcl_lcl'
        ,'teu'
    ];

    protected $relations = [
        'country'
    ];

    public function getRelations(){
        return $this->relations;
    }

    public function country()
    {
        return $this->hasOne('App\Country', 'iso2', 'location');
    }

    public function persons()
    {
        return $this->hasOne('App\Person', 'tamura_entity_id');
    }

    public function tamuraEntityPersons()
    {
        return $this->hasMany('App\Person', 'tamura_entity_id');
    }
}
