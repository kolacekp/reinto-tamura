<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcelExtractLog extends Model
{
    protected $table = 'excel_extract_logs';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'person_login'
        ,'date'
        ,'project_id'
        ,'project_status_init'
        ,'project_status_opened'
        ,'project_status_closed'
        ,'project_manager_id'
        ,'key_account_manager_id'
        ,'project_customer_id'
        ,'project_customer_root_id'
        ,'project_include_all_group'
        ,'project_purchaser_id'
        ,'project_market_id'
        ,'project_creation_date_from'
        ,'project_creation_date_to'
        ,'project_modification_date_from'
        ,'project_modification_date_to'
        ,'rfq_id'
        ,'rfq_status_init'
        ,'rfq_status_t10'
        ,'rfq_status_t20'
        ,'rfq_status_t30'
        ,'rfq_status_dead'
        ,'rfq_status_rej'
        ,'rfq_customer_pn'
        ,'rfq_eng_action_design_required'
        ,'rfq_eng_action_costing_complete'
        ,'rfq_eng_action_design_complete'
        ,'rfq_eng_action_mass_prod_auth'
        ,'rfq_dead_reason_id'
        ,'rfq_pn_category'
        ,'rfq_pn_format'
        ,'rfq_pn_new_dev'
        ,'rfq_pn_new_biz'
        ,'rfq_priority'
        ,'rfq_customer_engineer_id'
        ,'rfq_tamura_elec_engineer_id'
        ,'rfq_tamura_mec_engineer_id'
        ,'rfq_product_id'
        ,'rfq_creation_date_from'
        ,'rfq_creation_date_to'
        ,'rfq_modification_date_from'
        ,'rfq_modification_date_to'
        ,'sales_valid'   
    ];
}
