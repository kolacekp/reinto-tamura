<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQEstimatedFactoryCost extends Model
{
    protected $table = 'rfq_estimated_factory_costs';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'tamura_entity_id_factory1'
        ,'tamura_entity_currency_factory1'
        ,'rfq_material_factory1'
        ,'tamura_entity_profit_factory1'
        ,'tamura_entity_profit_value_factory1'
        ,'tamura_entity_labour_rate_choice_factory1'
        ,'tamura_entity_labour_time_value_factory1'
        ,'tamura_entity_labour_rate_factory1'
        ,'tamura_entity_labour_rate_value_factory1'
        ,'tamura_entity_freight_factory1'
        ,'tamura_entity_freight_value_factory1'
        ,'tamura_entity_admin_factory1'
        ,'tamura_entity_admin_value_factory1'
        ,'tamura_entity_selling_price_factory1'
        ,'tamura_entity_id_sales_office1'
        ,'tamura_entity_profit_sales_office1'
        ,'tamura_entity_profit_value_sales_office1'
        ,'tamura_entity_selling_price_sales_office1'
        ,'tamura_entity_id_factory2'
        ,'tamura_entity_currency_factory2'
        ,'rfq_material_factory2'
        ,'tamura_entity_profit_factory2'
        ,'tamura_entity_profit_value_factory2'
        ,'tamura_entity_labour_rate_choice_factory2'
        ,'tamura_entity_labour_time_value_factory2'
        ,'tamura_entity_labour_rate_factory2'
        ,'tamura_entity_labour_rate_value_factory2'
        ,'tamura_entity_freight_factory2'
        ,'tamura_entity_freight_value_factory2'
        ,'tamura_entity_admin_factory2'
        ,'tamura_entity_admin_value_factory2'
        ,'tamura_entity_selling_price_factory2'
        ,'tamura_entity_id_sales_office2'
        ,'tamura_entity_profit_sales_office2'
        ,'tamura_entity_profit_value_sales_office2'
        ,'tamura_entity_selling_price_sales_office2'
        ,'tamura_entity_id_factory3'
        ,'tamura_entity_currency_factory3'
        ,'rfq_material_factory3'
        ,'tamura_entity_profit_factory3'
        ,'tamura_entity_profit_value_factory3'
        ,'tamura_entity_labour_rate_choice_factory3'
        ,'tamura_entity_labour_time_value_factory3'
        ,'tamura_entity_labour_rate_factory3'
        ,'tamura_entity_labour_rate_value_factory3'
        ,'tamura_entity_freight_factory3'
        ,'tamura_entity_freight_value_factory3'
        ,'tamura_entity_admin_factory3'
        ,'tamura_entity_admin_value_factory3'
        ,'tamura_entity_selling_price_factory3'
        ,'tamura_entity_id_sales_office3'
        ,'tamura_entity_profit_sales_office3'
        ,'tamura_entity_profit_value_sales_office3'
        ,'tamura_entity_selling_price_sales_office3'
    ];

    protected $relations = [
        'factory1',
        'currencyFactory1',
        'factory2',
        'currencyFactory2',
        'factory3',
        'currencyFactory3'
    ];

    public function getRelations(){
        return $this->relations;
    }

    public function factory1()
    {
        return $this->hasOne('App\TamuraEntity', 'id', 'tamura_entity_id_factory1');
    }

    public function currencyFactory1()
    {
        return $this->hasOne('App\Currency', 'id', 'tamura_entity_currency_factory1');
    }

    public function factory2()
    {
        return $this->hasOne('App\TamuraEntity', 'id', 'tamura_entity_id_factory2');
    }

    public function currencyFactory2()
    {
        return $this->hasOne('App\Currency', 'id', 'tamura_entity_currency_factory2');
    }

    public function factory3()
    {
        return $this->hasOne('App\TamuraEntity', 'id', 'tamura_entity_id_factory3');
    }

    public function currencyFactory3()
    {
        return $this->hasOne('App\Currency', 'id', 'tamura_entity_currency_factory3');
    }


}
