<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'persons';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'customer_id'
        ,'tamura_entity_id'
        ,'name'
        ,'email'
        ,'phone'
        ,'mobile'
        ,'deactivated'
        ,'sales_manager'
        ,'engineer'
        ,'purchaser'
        ,'db_login'
        ,'db_password'
    ];


    protected $casts = [
        'deactivated' => 'boolean',
        'sales_manager' => 'boolean',
        'engineer' => 'boolean',
        'purchaser' => 'boolean'
    ];

    protected $relations = [
        'customer',
        'tamuraEntity'
    ];
    
    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function tamuraEntity()
    {
        return $this->belongsTo('App\TamuraEntity', 'tamura_entity_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'email', 'email');
    }
}
