<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQProduction extends Model
{
    protected $table = 'rfq_production';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'factory_id'
        ,'currency_id'
        ,'rfq_material_factory1'
        ,'last_production_date'
        ,'std_cost_roll_up_material'
        ,'std_cost_roll_up_labour_time'
        ,'std_cost_roll_up_labour_euro'
        ,'std_cost_roll_up_total_euro'
        ,'so_price_factory_material_cost'
        ,'so_price_factory_labour_cost'
        ,'so_price_factory_cost'
        ,'roll_up_material_estimated_material_cost'
        ,'roll_up_labour_estimated_labour_time'
        ,'roll_up_labour_estimated_labour_cost'
        ,'created_at'
        ,'updated_at'
    ];

    protected $relations = [
        'factory',
        'currency',
        'rfq'
    ];

    public function getRelations(){
        return $this->relations;
    }

    public function factory()
    {
        return $this->hasOne('App\TamuraEntity', 'id', 'factory_id');
    }

    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'currency_id');
    }

    public function rfq()
    {
        return $this->hasOne('App\RFQ', 'id', 'rfq_id');
    }

}
