<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NPI extends Model
{
    protected $table = 'npis';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'tamura_entity_id_factory'
        ,'tamura_entity_id_sales_office'
        ,'currency_id'
        ,'rfq_material_cost_value'
        ,'rfq_profit_factory'
        ,'rfq_profit_value_factory'
        ,'rfq_labour'
        ,'rfq_labour_time'
        ,'rfq_labour_value'
        ,'rfq_freight'
        ,'rfq_freight_value'
        ,'rfq_admin'
        ,'rfq_admin_value'
        ,'rfq_selling_price_factory'
        ,'rfq_profit_sales_office'
        ,'rfq_profit_value_sales_office'
        ,'rfq_selling_price_sales_office'
        ,'material_cost_value'
        ,'profit_factory'
        ,'profit_value_factory'
        ,'labour'
        ,'labour_time'
        ,'labour_value'
        ,'freight'
        ,'freight_value'
        ,'admin'
        ,'admin_value'
        ,'selling_price_factory'
        ,'profit_sales_office'
        ,'profit_value_sales_office'
        ,'selling_price_sales_office'
        ,'sample_quantity_required'
        ,'sample_delivery_date'
        ,'sample_chargeable'
        ,'sample_price'
        ,'sample_incoterms_id'
        ,'sample_manufactured_purchased'
        ,'sample_lead_time'
        ,'sample_teu_pn'
        ,'sample_customer_address'
        ,'minimum_order_quantity'
        ,'minimum_package_quantity'
        ,'comment'
        ,'creation_date'
        ,'created_by'
        ,'last_modified_on'
        ,'last_modified_by'
    ];

    protected $relations = [
        'sales'
    ];

    public function getRelations(){
        return $this->relations;
    }

    public function sales()
    {
        return $this->hasMany('App\Sale', 'npi_id');
    }

}
