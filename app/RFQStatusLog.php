<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQStatusLog extends Model
{
    protected $table = 'rfq_status_logs';
    const INITIAL_STATUS = "INIT";
    const T10_STATUS = "T10";
    const T20_STATUS = "T20";
    const T30_STATUS = "T30";
    const DESIGNCOM_STATUS = "Design Com";
    const DESIGNREQ_STATUS = "Design Req";
    const COSTINGCO_STATUS = "Costing Co";
    const MASSPROD_STATUS = "Mass Prod.";
    const DEAD_STATUS = "Dead";
    const REJECTED_STATUS = "Rejected";
    const ACTION_CHECKED = "CHECKED";
    const ACTION_UNCHECKED = "UNCHECKED";
    
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'status'
        ,'action'
        ,'who'
        ,'when'
        ,'dead_reason'
    ];

    public function getWhenAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s A', strtotime($value)) : null;
    }

}
