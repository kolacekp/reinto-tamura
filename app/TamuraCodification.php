<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TamuraCodification extends Model
{
    protected $table = 'tamura_codifications';
    
    protected $fillable = [
        'id'
        ,'type_entity'
        ,'no_entity'
        ,'entity'
        ,'no_entity_s'
        ,'code_list'
        ,'entity_path'
    ];
    
    public function getEntityPathAttribute($value)
    {
        return trim($value);
    }


    protected $relations = [
        'parent'
    ];

    public function parent()
    {
        return $this->hasOne('App\TamuraCodification', 'no_entity', 'no_entity_s');
    }

    public function allParents()
    {
        return $this->parent()->with('allParents');
    }
}
