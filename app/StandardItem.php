<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StandardItem extends Model
{
    protected $table = 'standard_items';
    
    protected $fillable = [
         'id'
        ,'category'
        ,'design_office'
        ,'factory'
        ,'sales_office'
        ,'tam_id'
        ,'part_number'
        ,'description'
        ,'box_quantity'
        ,'carton_quantity'
        ,'pallet_quantity'
        ,'unit_weight'
        ,'currency'
        ,'buy_price_fca_hk'
        ,'comment'
    ];
}
