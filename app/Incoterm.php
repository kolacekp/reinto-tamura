<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incoterm extends Model
{
    protected $table = 'incoterms';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'short'
        ,'label'      
    ];
}
