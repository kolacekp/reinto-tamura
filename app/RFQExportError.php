<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQExportError extends Model
{
    protected $table = 'rfq_export_errors';
    
    protected $fillable = [
        'id'
        ,'error'
        ,'field'
        ,'row'
    ];
}
