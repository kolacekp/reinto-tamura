<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQ extends Model
{
    protected $table = 'rfqs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        , 'father'
        , 'is_std_item'
        , 'project_id'
        , 'status'
        , 'folder'
        , 'priority'
        , 'design_office_id'
        , 'customer_engineer_id'
        , 'tamura_elec_engineer_id'
        , 'tamura_mec_engineer_id'
        , 'product_id'
        , 'pn_customer'
        , 'pn_desc'
        , 'pn_category_id'
        , 'pn_format'
        , 'pn_new_dev'
        , 'pn_new_biz'
        , 'com_currency_id'
        , 'com_estimated_price'
        , 'com_target_price'
        , 'com_annual_qty'
        , 'com_annual_value'
        , 'com_confidence_level'
        , 'com_market_share'
        , 'com_incoterms'
        , 'com_delivery_location'
        , 'teu_group'
        , 'qualif_quotation_required_date'
        , 'qualif_sample_date'
        , 'qualif_mass_prod_date'
        , 'comments'
        , 'duplicate'
        , 'duplicate_from'
        , 'creation_date'
        , 'last_modified_on'
        , 'last_modified_by'
        , 'status_10'
        , 'status_10_who'
        , 'status_10_when'
        , 'status_20'
        , 'status_20_who'
        , 'status_20_when'
        , 'status_30'
        , 'status_30_who'
        , 'status_30_when'
        , 'status_dead'
        , 'status_dead_who'
        , 'status_dead_when'
        , 'status_dead_reason_id'
        , 'status_rejected'
        , 'status_rejected_who'
        , 'status_rejected_when'
        , 'eng_action_costing_planned_date'
        , 'eng_action_costing_complete'
        , 'eng_action_costing_complete_who'
        , 'eng_action_costing_complete_when'
        , 'eng_action_design_required'
        , 'eng_action_design_required_who'
        , 'eng_action_design_required_when'
        , 'eng_action_design_complete_planned_date'
        , 'eng_action_design_complete'
        , 'eng_action_design_complete_who'
        , 'eng_action_design_complete_when'
        , 'eng_action_mass_prod_auth'
        , 'eng_action_mass_prod_auth_who'
        , 'eng_action_mass_prod_auth_when'
        , 'product_type'
        , 'product_length'
        , 'product_width'
        , 'product_height'
        , 'product_weight'
        , 'product_field1'
        , 'product_field2'
        , 'product_field3'
        , 'product_field4'
        , 'product_field5'
        , 'product_field6'
        , 'product_other_materials'
        , 'product_other_materials_value'
        , 'product_handling_scrap'
        , 'product_handling_scrap_value'
        , 'mat_currency_id'
        , 'mat_total_value'
        , 'pallet_length'
        , 'pallet_width'
        , 'pallet_space'
        , 'pallet_layers'
        , 'pallet_quantity'
        , 'labour1'
        , 'labour2'
        , 'labour3'
        , 'labour_time1'
        , 'labour_time2'
        , 'labour_time3'
        , 'labour_time4'
    ];

    protected $casts = [
        'status_10' => 'boolean',
        'status_20' => 'boolean',
        'status_30' => 'boolean',
        'status_dead' => 'boolean',
        'status_rejected' => 'boolean',
        'eng_action_costing_complete' => 'boolean',
        'eng_action_design_required' => 'boolean',
        'eng_action_design_complete' => 'boolean',
        'eng_action_mass_prod_auth' => 'boolean',
    ];

    public function getQualifQuotationRequiredDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y', strtotime($value)) : null;
    }

    public function getQualifSampleDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y', strtotime($value)) : null;
    }

    public function getQualifMassProdDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y', strtotime($value)) : null;
    }

    public function getCreationDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s', strtotime($value)) : null;
    }

    public function getLastModifiedOnAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s', strtotime($value)) : null;
    }

    public function getEngActionCostingPlannedDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y', strtotime($value)) : null;
    }

    public function getEngActionDesignCompletePlannedDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y', strtotime($value)) : null;
    }

    public function getEngActionCostingCompleteWhenAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s', strtotime($value)) : null;
    }

    public function getEngActionDesignRequiredWhenAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s', strtotime($value)) : null;
    }

    public function getEngActionDesignCompleteWhenAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s', strtotime($value)) : null;
    }

    public function getEngActionMassProdAuthWhenAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s', strtotime($value)) : null;
    }

    public function getProductOtherMaterialsAttribute($value)
    {
        return !empty($value) ? $value * 100 : null;
    }

    public function getProductHandlingScrapAttribute($value)
    {
        return !empty($value) ? $value * 100 : null;
    }

    public function getComConfidenceLevelAttribute($value)
    {
        return !empty($value) ? $value * 100 : null;
    }

    public function getComMarketShareAttribute($value)
    {
        return !empty($value) ? $value * 100 : null;
    }

    public function setComConfidenceLevelAttribute($value)
    {
        if(!empty($value) && strpos($value, "%") !== false){
            $this->attributes["com_confidence_level"] = ((double)(str_replace("%","",$value))) / 100;
        }
    }

    public function setComMarketShareAttribute($value)
    {
        if(!empty($value) && strpos($value, "%") !== false){
            $this->attributes["com_market_share"] = ((double)(str_replace("%","",$value))) / 100;
        }
    }

    public function setQualifQuotationRequiredDateAttribute($value)
    {
        if(!empty($value)){
            $this->attributes["qualif_quotation_required_date"] = date('Y-m-d', strtotime($value));
        }
    }

    public function setQualifSampleDateAttribute($value)
    {
        if(!empty($value)){
            $this->attributes["qualif_sample_date"] = date('Y-m-d', strtotime($value));
        }
    }

    public function setQualifMassProdDateAttribute($value)
    {
        if(!empty($value)){
            $this->attributes["qualif_mass_prod_date"] = date('Y-m-d', strtotime($value));
        }
    }

    public function setCreationDateAttribute($value)
    {
        if(!empty($value)){
            $this->attributes["creation_date"] = date('Y-m-d H:i:s', strtotime($value));
        }
    }

    public function setLastModifiedOnAttribute($value)
    {
        if(!empty($value)){
            $this->attributes["last_modified_on"] = date('Y-m-d H:i:s', strtotime($value));
        }
    }



    protected $relations = [
        'npis',
        'category',
        'product',
        'comCurrency',
        'customerEngineer',
        'project',
        'designOffice',
        'comDeliveryLocation',
        'electricalEngineer',
        'mechanicalEngineer',
        'product',
        'materials',
        'specifications',
        'estimatedFactoryCost',
        'statusDeadReason',
        'statusLogs'
    ];
    
    public function getRelations(){
        return $this->relations;
    }
    
    public function npis()
    {
        return $this->hasMany('App\NPI', 'rfq_id');
    }
    
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'pn_category_id');
    }
    
    public function product()
    {
        return $this->hasOne('App\TamuraCodification', 'no_entity', 'product_id');
    }

    public function comCurrency()
    {
        return $this->hasOne('App\Currency', 'id', 'com_currency_id');
    }
    
    public function customerEngineer()
    {
        return $this->hasOne('App\Person', 'id', 'customer_engineer_id');
    }
    
    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id', 'id');
    }

    public function designOffice()
    {
        return $this->hasOne('App\TamuraEntity', 'id', 'design_office_id');
    }

    public function comDeliveryLocation()
    {
        return $this->hasOne('App\Country', 'iso2', 'com_delivery_location');
    }

    public function electricalEngineer()
    {
        return $this->hasOne('App\Person', 'id', 'tamura_elec_engineer_id');
    }

    public function mechanicalEngineer()
    {
        return $this->hasOne('App\Person', 'id', 'tamura_mec_engineer_id');
    }

    public function materials()
    {
        return $this->hasMany('App\RFQMat', 'rfq_id');
    }

    public function specifications()
    {
        return $this->hasMany('App\RFQSpecification', 'rfq_id');
    }

    public function estimatedFactoryCost()
    {
        return $this->hasOne('App\RFQEstimatedFactoryCost', 'rfq_id');
    }

    public function matCurrency()
    {
        return $this->hasOne('App\Currency', 'id', 'mat_currency_id');
    }

    public function statusDeadReason()
    {
        return $this->hasOne('App\RFQDeadReason', 'id', 'status_dead_reason_id');
    }

    public function statusLogs()
    {
        return $this->hasMany('App\RFQStatusLog', 'rfq_id');
    }



}
