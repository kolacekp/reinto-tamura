<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSheet extends Model
{
    protected $table = 'time_sheets';
    
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'created_by'
        ,'creation_date'
        ,'tamura_engineer_id'
        ,'date'
        ,'hours'
        ,'action'
        ,'invoice_number'
    ];

    public function getDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y', strtotime($value)) : null;
    }

    protected $relations = [
        'engineer'
    ];

    public function getRelations(){
        return $this->relations;
    }

    public function engineer()
    {
        return $this->hasOne('App\Person', 'id', 'tamura_engineer_id');
    }


}
