<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQMat extends Model
{
    protected $table = 'rfq_mats';
    
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'currency_id'
        ,'type'
        ,'part_no'
        ,'description'
        ,'qty'
        ,'unitary_price'
        ,'total_price'
    ];
}
