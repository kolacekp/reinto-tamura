<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'long_name'
        ,'short_name'
        ,'symbol'
        ,'exch_rate'
        ,'default'
        ,'upsize_ts'
    ];
}
