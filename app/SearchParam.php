<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchParam extends Model
{
    protected $table = 'search_params';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'person_login'
        ,'person_name'
        ,'date'
        ,'project_id'
        ,'project_status_init'
        ,'project_status_opened'
        ,'project_status_closed'
        ,'project_manager_id'
        ,'key_account_manager_id'
        ,'project_customer_id'
        ,'project_customer_root_id'
        ,'project_include_all_group'
        ,'project_purchaser_id'
        ,'project_market_id'
        ,'project_market_parent_id'
        ,'project_creation_date_from'
        ,'project_creation_date_to'
        ,'project_modification_date_from'
        ,'project_modification_date_to'
        ,'rfq_id'
        ,'rfq_status_init'
        ,'rfq_status_t10'
        ,'rfq_status_t20'
        ,'rfq_status_t30'
        ,'rfq_status_dead'
        ,'rfq_status_rej'
        ,'rfq_customer_pn'
        ,'rfq_eng_action_design_required'
        ,'rfq_eng_action_costing_complete'
        ,'rfq_eng_action_design_complete'
        ,'rfq_eng_action_mass_prod_auth'
        ,'rfq_dead_reason_id'
        ,'rfq_pn_category'
        ,'rfq_pn_format'
        ,'rfq_pn_new_dev'
        ,'rfq_pn_new_biz'
        ,'rfq_priority'
        ,'rfq_customer_engineer_id'
        ,'rfq_tamura_elec_engineer_id'
        ,'rfq_tamura_mec_engineer_id'
        ,'rfq_product_id'
        ,'rfq_product_child_one_id'
        ,'rfq_product_child_two_id'
        ,'rfq_product_parent_id'
        ,'rfq_creation_date_from'
        ,'rfq_creation_date_to'
        ,'rfq_modification_date_from'
        ,'rfq_modification_date_to'
        ,'sales_valid'   
    ];
    
    protected $relations = [
        'keyAccountManager',
        'projectManager',
        'projectCustomer',
        'projectPurchaser',
        'projectMarket',
        'projectMarketParent',
        'rfqDeadReason',
        'rfqPnCategory',
        'rfqCustomerEngineer',
        'rfqElectricalEngineer',
        'rfqMechanicalEngineer',
        'rfqProduct',
        'rfqProductChildOne',
        'rfqProductChildTwo',
        'rfqProductParent',
        'salesCustomer',
        'salesPurchaseManager',
        'salesManager'
    ];
    
    public function getRelations(){
        return $this->relations;
    }
    
    public function getProjectCreationDateFromAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getProjectCreationDateToAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getProjectModificationDateFromAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getProjectModificationDateToAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getRfqCreationDateFromAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getRfqCreationDateToAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getRfqModificationDateFromAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function getRfqModificationDateToAttribute($value){
        return !is_null($value) ? date('m/d/Y', strtotime($value)) : null;
    }
    
    public function projectManager()
    {
        return $this->belongsTo('App\Person', 'project_manager_id');
    }
    
    public function keyAccountManager()
    {
        return $this->belongsTo('App\Person', 'key_account_manager_id');
    }
    
    public function projectCustomer()
    {
        return $this->belongsTo('App\Customer', 'project_customer_id');
    }
    
    public function projectPurchaser()
    {
        return $this->belongsTo('App\Person', 'project_purchaser_id');
    }
    
    public function projectMarket()
    {
        return $this->belongsTo('App\TamuraCodification', 'project_market_id');
    }
    
    public function projectMarketParent()
    {
        return $this->belongsTo('App\TamuraCodification', 'project_market_parent_id');
    }
    
    public function rfqDeadReason()
    {
        return $this->belongsTo('App\RFQDeadReason', 'rfq_dead_reason_id');
    }
    
    public function rfqPnCategory()
    {
        return $this->belongsTo('App\Category', 'rfq_pn_category');
    }
    
    public function rfqCustomerEngineer()
    {
        return $this->belongsTo('App\Person', 'rfq_customer_engineer_id');
    }
    
    public function rfqElectricalEngineer()
    {
        return $this->belongsTo('App\Person', 'rfq_tamura_elec_engineer_id');
    }
    
    public function rfqMechanicalEngineer()
    {
        return $this->belongsTo('App\Person', 'rfq_tamura_mec_engineer_id');
    }
    
    public function rfqProduct()
    {
        return $this->belongsTo('App\TamuraCodification', 'rfq_product_id');
    }
    
    public function rfqProductChildOne()
    {
        return $this->belongsTo('App\TamuraCodification', 'rfq_product_child_one_id');
    }
    
    public function rfqProductChildTwo()
    {
        return $this->belongsTo('App\TamuraCodification', 'rfq_product_child_two_id');
    }
    
    public function rfqProductParent()
    {
        return $this->belongsTo('App\TamuraCodification', 'rfq_product_parent_id');
    }
    
    public function salesCustomer()
    {
        return $this->belongsTo('App\Customer', 'sales_customer_id');
    }
    
    public function salesPurchaseManager()
    {
        return $this->belongsTo('App\Person', 'sales_purchase_manager_id');
    }
    
    public function salesManager()
    {
        return $this->belongsTo('App\Person', 'sales_manager_id');
    }
}
