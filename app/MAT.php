<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MAT extends Model
{
    protected $table = 'mats';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'type'
        ,'part_no'
        ,'description'
        ,'unitary_price'
    ];
}
