<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sales';
    
    protected $fillable = [
        'id'
        ,'npi_id'
        ,'valid'
        ,'valid_last_update_time'
        ,'currency_id'
        ,'sales_manager_id'
        ,'tamura_entity_id_sales_office'
        ,'buy_price_engineering'
        ,'buy_price_factory'
        ,'buy_price'
        ,'tamura_entity_fob_charge'
        ,'tamura_entity_fob_charge_value'
        ,'tamura_entity_freight'
        ,'tamura_entity_freight_value'
        ,'cif_cost'
        ,'tamura_entity_terminal_handling'
        ,'tamura_entity_terminal_handling_value'
        ,'tamura_entity_delivery_order'
        ,'tamura_entity_delivery_order_value'
        ,'tamura_entity_customs_clearance'
        ,'tamura_entity_customs_clearance_value'
        ,'advance_fees'
        ,'advance_fees_value'
        ,'lfr'
        ,'lfr_value'
        ,'tamura_entity_haulage_to_warehouse'
        ,'tamura_entity_haulage_to_warehouse_value'
        ,'total_inland_charges_value'
        ,'unloading'
        ,'unloading_value'
        ,'order_picking'
        ,'order_picking_value'
        ,'storage'
        ,'storage_value'
        ,'admin'
        ,'admin_value'
        ,'total_hub_costs'
        ,'total_oversea_costs'
        ,'total_oversea_costs_value'
        ,'total_oversea_cost_flag'
        ,'import_duty'
        ,'import_duty_value'
        ,'delivery_to_customer'
        ,'delivery_to_customer_value'
        ,'cost_price'
        ,'cost_price_percentage'
        ,'gand_a'
        ,'gand_a_value'
        ,'total_cost'
        ,'total_cost_percentage'
        ,'profit'
        ,'profit_value'
        ,'selling_price'
        ,'comment'
        ,'special_req'
        ,'incoterms_id'
        ,'creation_date'
        ,'last_update_date'
        ,'created_by'
        ,'pallet_qty'
        ,'customer_id'
        ,'customer_payment_term_nr'
        ,'customer_payment_term_type'
        ,'sales_purchaser_id'
        ,'sales_fcl_lcl'
        ,'lme_cu'
        ,'lme_cu_at_creation'
        ,'lme_al'
        ,'lme_al_at_cretation'
    ];

    public function saleOffice()
    {
        return $this->hasOne('App\TamuraEntity', 'id', 'tamura_entity_id_sales_office');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function currency()
    {
        return $this->hasOne('App\Currency', 'id', 'currency_id');
    }

    public function saleManager()
    {
        return $this->hasOne('App\Person', 'id', 'sales_manager_id');
    }

    public function purchaseManager()
    {
        return $this->hasOne('App\Person', 'id', 'purchaser_id');
    }
}
