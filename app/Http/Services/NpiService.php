<?php
namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\NPI;
use App\RFQ;

use App\TamuraEntity;
use DB;

class NpiService
{


    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function getNpiByTamuraEntity($id){
        try{

            $npi = NPI::
                select
                (DB::raw('npis.id as npiId,npis.tamura_entity_id_sales_office as salesOfficeId,
               tamura_entities.name as salesName'))
                ->join('tamura_entities', 'npis.tamura_entity_id_sales_office', '=', 'tamura_entities.id')
                ->where('npis.tamura_entity_id_factory','=',$id)
                ->get();
            return $this->resultService->Success($npi);

        }catch(Exception $e){
            $this->logService->log("ERROR NpiService->getNpiByTamuraEntity", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function get($id){
        try{
            if(empty($id)){
                return $this->resultService->Error("NPI Id cannot be empty");
            }

            $npi = NPI::where("id", $id)
                ->first();

            return $this->resultService->Success($npi);

        }catch(Exception $e){
            $this->logService->log("ERROR NpiService->get", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateComment($id,$comment){
        try{
            if(is_null($id)){
                return $this->resultService->Error("NPI id cannot be empty");
            }

            $res = NPI::where("id", $id)
                ->update([
                    "comment" => isset($comment)? $comment:"",
                    "updated_at" => new \DateTime()
                ]);

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR NPIService->updateComment", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateRfqEstimatedFactoryCost($rfqId,$efc)
    {

        if (!is_null($efc) && !empty($efc)) {
            $factory1 = isset($efc["factory1"]) ? $efc["factory1"] : null;
            $factory2 = isset($efc["factory2"]) ? $efc["factory2"] : null;
            $factory3 = isset($efc["factory3"]) ? $efc["factory3"] : null;

            $currency1 = isset($efc["currency_factory1"]) ? $efc["currency_factory1"] : null;
            $currency2 = isset($efc["currency_factory2"]) ? $efc["currency_factory2"] : null;
            $currency3 = isset($efc["currency_factory3"]) ? $efc["currency_factory3"] : null;

            $insertUpdateArray = [
                "rfq_id" => $rfqId,
                "tamura_entity_id_factory1" => isset($factory1["id"]) ? $factory1["id"] : null,
                "tamura_entity_currency_factory1" => isset($currency1["id"]) ? $currency1["id"] : null,
                "rfq_material_factory1" => isset($efc["rfq_material_factory1"]) ? $efc["rfq_material_factory1"] : null,
                "tamura_entity_profit_factory1" => isset($efc["tamura_entity_profit_factory1"]) ? $efc["tamura_entity_profit_factory1"] : null,
                "tamura_entity_profit_value_factory1" => isset($efc["tamura_entity_profit_value_factory1"]) ? $efc["tamura_entity_profit_value_factory1"] : null,
                "tamura_entity_labour_rate_choice_factory1" => isset($efc["tamura_entity_labour_rate_choice_factory1"]) ? $efc["tamura_entity_labour_rate_choice_factory1"] : null,
                "tamura_entity_labour_time_value_factory1" => isset($efc["tamura_entity_labour_time_value_factory1"]) ? $efc["tamura_entity_labour_time_value_factory1"] : null,
                "tamura_entity_labour_rate_factory1" => isset($efc["tamura_entity_labour_rate_factory1"]) ? $efc["tamura_entity_labour_rate_factory1"] : null,
                "tamura_entity_labour_rate_value_factory1" => isset($efc["tamura_entity_labour_rate_value_factory1"]) ? $efc["tamura_entity_labour_rate_value_factory1"] : null,
                "tamura_entity_freight_factory1" => isset($efc["tamura_entity_freight_factory1"]) ? $efc["tamura_entity_freight_factory1"] : null,
                "tamura_entity_freight_value_factory1" => isset($efc["tamura_entity_freight_value_factory1"]) ? $efc["tamura_entity_freight_value_factory1"] : null,
                "tamura_entity_admin_factory1" => isset($efc["tamura_entity_admin_factory1"]) ? $efc["tamura_entity_admin_factory1"] : null,
                "tamura_entity_admin_value_factory1" => isset($efc["tamura_entity_admin_value_factory1"]) ? $efc["tamura_entity_admin_value_factory1"] : null,
                "tamura_entity_selling_price_factory1" => isset($efc["tamura_entity_selling_price_factory1"]) ? $efc["tamura_entity_selling_price_factory1"] : null,
                "tamura_entity_id_sales_office1" => isset($efc["tamura_entity_id_sales_office1"]) ? $efc["tamura_entity_id_sales_office1"] : null,
                "tamura_entity_profit_sales_office1" => isset($efc["tamura_entity_profit_sales_office1"]) ? $efc["tamura_entity_profit_sales_office1"] : null,
                "tamura_entity_profit_value_sales_office1" => isset($efc["tamura_entity_profit_value_sales_office1"]) ? $efc["tamura_entity_profit_value_sales_office1"] : null,
                "tamura_entity_selling_price_sales_office1" => isset($efc["tamura_entity_selling_price_sales_office1"]) ? $efc["tamura_entity_selling_price_sales_office1"] : null,

                "tamura_entity_id_factory2" => isset($factory2["id"]) ? $factory2["id"] : null,
                "tamura_entity_currency_factory2" => isset($currency2["id"]) ? $currency2["id"] : null,
                "rfq_material_factory2" => isset($efc["rfq_material_factory2"]) ? $efc["rfq_material_factory2"] : null,
                "tamura_entity_profit_factory2" => isset($efc["tamura_entity_profit_factory2"]) ? $efc["tamura_entity_profit_factory2"] : null,
                "tamura_entity_profit_value_factory2" => isset($efc["tamura_entity_profit_value_factory2"]) ? $efc["tamura_entity_profit_value_factory2"] : null,
                "tamura_entity_labour_rate_choice_factory2" => isset($efc["tamura_entity_labour_rate_choice_factory2"]) ? $efc["tamura_entity_labour_rate_choice_factory2"] : null,
                "tamura_entity_labour_time_value_factory2" => isset($efc["tamura_entity_labour_time_value_factory2"]) ? $efc["tamura_entity_labour_time_value_factory2"] : null,
                "tamura_entity_labour_rate_factory2" => isset($efc["tamura_entity_labour_rate_factory2"]) ? $efc["tamura_entity_labour_rate_factory2"] : null,
                "tamura_entity_labour_rate_value_factory2" => isset($efc["tamura_entity_labour_rate_value_factory2"]) ? $efc["tamura_entity_labour_rate_value_factory2"] : null,
                "tamura_entity_freight_factory2" => isset($efc["tamura_entity_freight_factory2"]) ? $efc["tamura_entity_freight_factory2"] : null,
                "tamura_entity_freight_value_factory2" => isset($efc["tamura_entity_freight_value_factory2"]) ? $efc["tamura_entity_freight_value_factory2"] : null,
                "tamura_entity_admin_factory2" => isset($efc["tamura_entity_admin_factory2"]) ? $efc["tamura_entity_admin_factory2"] : null,
                "tamura_entity_admin_value_factory2" => isset($efc["tamura_entity_admin_value_factory2"]) ? $efc["tamura_entity_admin_value_factory2"] : null,
                "tamura_entity_selling_price_factory2" => isset($efc["tamura_entity_selling_price_factory2"]) ? $efc["tamura_entity_selling_price_factory2"] : null,
                "tamura_entity_id_sales_office2" => isset($efc["tamura_entity_id_sales_office2"]) ? $efc["tamura_entity_id_sales_office2"] : null,
                "tamura_entity_profit_sales_office2" => isset($efc["tamura_entity_profit_sales_office2"]) ? $efc["tamura_entity_profit_sales_office2"] : null,
                "tamura_entity_profit_value_sales_office2" => isset($efc["tamura_entity_profit_value_sales_office2"]) ? $efc["tamura_entity_profit_value_sales_office2"] : null,
                "tamura_entity_selling_price_sales_office2" => isset($efc["tamura_entity_selling_price_sales_office2"]) ? $efc["tamura_entity_selling_price_sales_office2"] : null,

                "tamura_entity_id_factory3" => isset($factory3["id"]) ? $factory3["id"] : null,
                "tamura_entity_currency_factory3" => isset($currency3["id"]) ? $currency3["id"] : null,
                "rfq_material_factory3" => isset($efc["rfq_material_factory3"]) ? $efc["rfq_material_factory3"] : null,
                "tamura_entity_profit_factory3" => isset($efc["tamura_entity_profit_factory3"]) ? $efc["tamura_entity_profit_factory3"] : null,
                "tamura_entity_profit_value_factory3" => isset($efc["tamura_entity_profit_value_factory3"]) ? $efc["tamura_entity_profit_value_factory3"] : null,
                "tamura_entity_labour_rate_choice_factory3" => isset($efc["tamura_entity_labour_rate_choice_factory3"]) ? $efc["tamura_entity_labour_rate_choice_factory3"] : null,
                "tamura_entity_labour_time_value_factory3" => isset($efc["tamura_entity_labour_time_value_factory3"]) ? $efc["tamura_entity_labour_time_value_factory3"] : null,
                "tamura_entity_labour_rate_factory3" => isset($efc["tamura_entity_labour_rate_factory3"]) ? $efc["tamura_entity_labour_rate_factory3"] : null,
                "tamura_entity_labour_rate_value_factory3" => isset($efc["tamura_entity_labour_rate_value_factory3"]) ? $efc["tamura_entity_labour_rate_value_factory3"] : null,
                "tamura_entity_freight_factory3" => isset($efc["tamura_entity_freight_factory3"]) ? $efc["tamura_entity_freight_factory3"] : null,
                "tamura_entity_freight_value_factory3" => isset($efc["tamura_entity_freight_value_factory3"]) ? $efc["tamura_entity_freight_value_factory3"] : null,
                "tamura_entity_admin_factory3" => isset($efc["tamura_entity_admin_factory3"]) ? $efc["tamura_entity_admin_factory3"] : null,
                "tamura_entity_admin_value_factory3" => isset($efc["tamura_entity_admin_value_factory3"]) ? $efc["tamura_entity_admin_value_factory3"] : null,
                "tamura_entity_selling_price_factory3" => isset($efc["tamura_entity_selling_price_factory3"]) ? $efc["tamura_entity_selling_price_factory3"] : null,
                "tamura_entity_id_sales_office3" => isset($efc["tamura_entity_id_sales_office3"]) ? $efc["tamura_entity_id_sales_office3"] : null,
                "tamura_entity_profit_sales_office3" => isset($efc["tamura_entity_profit_sales_office3"]) ? $efc["tamura_entity_profit_sales_office3"] : null,
                "tamura_entity_profit_value_sales_office3" => isset($efc["tamura_entity_profit_value_sales_office3"]) ? $efc["tamura_entity_profit_value_sales_office3"] : null,
                "tamura_entity_selling_price_sales_office3" => isset($efc["tamura_entity_selling_price_sales_office3"]) ? $efc["tamura_entity_selling_price_sales_office3"] : null,

                "created_at" => new \DateTime(),
                "updated_at" => new \DateTime()
            ];

            $rfqEfc = DB::table("rfq_estimated_factory_costs")->where("rfq_id", $rfqId)->first();

            if (empty($rfqEfc)) {
                DB::table("rfq_estimated_factory_costs")->insert($insertUpdateArray);
            } else {
                DB::table("rfq_estimated_factory_costs")->where("rfq_id", $rfqId)->update($insertUpdateArray);
            }
        }
    }

    public function generateNPI($rfq,$NPINumber,$userName,$customerAddress){
        $ret = null;
        try{
            $rfqId = isset ($rfq["id"]) ?  $rfq["id"] : null;
            $rfqEst = $rfq["estimated_factory_cost"];
            $tamuraEntityFactoryId = isset($rfqEst["factory".$NPINumber]["id"])? $rfqEst["factory".$NPINumber]["id"] : null;
            $npi = NPI::where("rfq_id", $rfqId)
                ->where("tamura_entity_id_factory",$tamuraEntityFactoryId)
                ->first();
            $tamuraEntity = TamuraEntity::where("id",$tamuraEntityFactoryId)
                ->first();

            if ($npi != null){// NPI already exists
                $updateArray = array(
                    "tamura_entity_id_factory" => $tamuraEntityFactoryId,
                    "tamura_entity_id_sales_office" => isset($rfqEst["tamura_entity_id_sales_office".$NPINumber])? $rfqEst["tamura_entity_id_sales_office".$NPINumber]: null,
                    "currency_id" => isset($rfqEst["currency_factory".$NPINumber]["id"]) ? $rfqEst["currency_factory".$NPINumber]["id"] : null,
                    "rfq_material_cost_value" => isset($rfqEst["rfq_material_factory".$NPINumber])? $rfqEst["rfq_material_factory".$NPINumber] : null,
                    "rfq_profit_factory" => isset($rfqEst["tamura_entity_profit_factory".$NPINumber])? $rfqEst["tamura_entity_profit_factory".$NPINumber] : null,
                    "rfq_profit_value_factory" => isset($rfqEst["tamura_entity_profit_value_factory".$NPINumber])? $rfqEst["tamura_entity_profit_value_factory".$NPINumber] : null,
                    "rfq_labour" => isset($rfqEst["tamura_entity_labour_rate_factory".$NPINumber])? $rfqEst["tamura_entity_labour_rate_factory".$NPINumber] : null,
                    "rfq_labour_time" => isset($rfqEst["tamura_entity_labour_time_value_factory".$NPINumber])? $rfqEst["tamura_entity_labour_time_value_factory".$NPINumber] : null,
                    "rfq_labour_value" => isset($rfqEst["tamura_entity_labour_rate_value_factory".$NPINumber])? $rfqEst["tamura_entity_labour_rate_value_factory".$NPINumber] : null,
                    "rfq_freight" => isset($rfqEst["tamura_entity_freight_factory".$NPINumber])? $rfqEst["tamura_entity_freight_factory".$NPINumber] : null,
                    "rfq_freight_value" => isset($rfqEst["tamura_entity_freight_value_factory".$NPINumber])? $rfqEst["tamura_entity_freight_value_factory".$NPINumber] : null,
                    "rfq_admin" => isset($rfqEst["tamura_entity_admin_factory".$NPINumber])? $rfqEst["tamura_entity_admin_factory".$NPINumber] : null,
                    "rfq_admin_value" => isset($rfqEst["tamura_entity_admin_value_factory".$NPINumber])? $rfqEst["tamura_entity_admin_value_factory".$NPINumber] : null,
                    "rfq_selling_price_factory" => isset($rfqEst["tamura_entity_selling_price_factory".$NPINumber])? $rfqEst["tamura_entity_selling_price_factory".$NPINumber] : null,
                    "rfq_profit_sales_office" => isset($rfqEst["tamura_entity_profit_sales_office".$NPINumber])? $rfqEst["tamura_entity_profit_sales_office".$NPINumber] : null,
                    "rfq_profit_value_sales_office" => isset($rfqEst["tamura_entity_profit_value_sales_office".$NPINumber])? $rfqEst["tamura_entity_profit_value_sales_office".$NPINumber] : null,
                    "rfq_selling_price_sales_office" => isset($rfqEst["tamura_entity_selling_price_sales_office".$NPINumber])? $rfqEst["tamura_entity_selling_price_sales_office".$NPINumber] : null,
                    "sample_customer_address" => $customerAddress,
                    "last_modified_on" => date("Y-m-d H:i:s", strtotime("NOW")),
                    "last_modified_by" => isset ($userName) ?  $userName : null,
                    "updated_at" => date("Y-m-d H:i:s", strtotime("NOW"))
                );
                $npi->update($updateArray);
            }
            else {// NPI not exist yet, we have to create it
                $npi = new NPI();
                $npi->rfq_id = $rfqId;
                $npi->tamura_entity_id_factory = $tamuraEntityFactoryId;
                $npi->tamura_entity_id_sales_office = isset($rfqEst["tamura_entity_id_sales_office".$NPINumber])? $rfqEst["tamura_entity_id_sales_office".$NPINumber]: null;
                $npi->currency_id = isset($rfqEst["currency_factory".$NPINumber]["id"]) ? $rfqEst["currency_factory".$NPINumber]["id"] : null;
                $npi->rfq_material_cost_value = isset($rfqEst["rfq_material_factory".$NPINumber])? $rfqEst["rfq_material_factory".$NPINumber] : null;
                $npi->rfq_profit_factory = isset($rfqEst["tamura_entity_profit_factory".$NPINumber])? $rfqEst["tamura_entity_profit_factory".$NPINumber] : null;
                $npi->rfq_profit_value_factory = isset($rfqEst["tamura_entity_profit_value_factory".$NPINumber])? $rfqEst["tamura_entity_profit_value_factory".$NPINumber] : null;
                $npi->rfq_labour = isset($rfqEst["tamura_entity_labour_rate_factory".$NPINumber])? $rfqEst["tamura_entity_labour_rate_factory".$NPINumber] : null;
                $npi->rfq_labour_time = isset($rfqEst["tamura_entity_labour_time_value_factory".$NPINumber])? $rfqEst["tamura_entity_labour_time_value_factory".$NPINumber] : null;
                $npi->rfq_labour_value = isset($rfqEst["tamura_entity_labour_rate_value_factory".$NPINumber])? $rfqEst["tamura_entity_labour_rate_value_factory".$NPINumber] : null;
                $npi->rfq_freight = isset($rfqEst["tamura_entity_freight_factory".$NPINumber])? $rfqEst["tamura_entity_freight_factory".$NPINumber] : null;
                $npi->rfq_freight_value = isset($rfqEst["tamura_entity_freight_value_factory".$NPINumber])? $rfqEst["tamura_entity_freight_value_factory".$NPINumber] : null;
                $npi->rfq_admin = isset($rfqEst["tamura_entity_admin_factory".$NPINumber])? $rfqEst["tamura_entity_admin_factory".$NPINumber] : null;
                $npi->rfq_admin_value = isset($rfqEst["tamura_entity_admin_value_factory".$NPINumber])? $rfqEst["tamura_entity_admin_value_factory".$NPINumber] : null;
                $npi->rfq_selling_price_factory = isset($rfqEst["tamura_entity_selling_price_factory".$NPINumber])? $rfqEst["tamura_entity_selling_price_factory".$NPINumber] : null;
                $npi->rfq_profit_sales_office = isset($rfqEst["tamura_entity_profit_sales_office".$NPINumber])? $rfqEst["tamura_entity_profit_sales_office".$NPINumber] : null;
                $npi->rfq_profit_value_sales_office = isset($rfqEst["tamura_entity_profit_value_sales_office".$NPINumber])? $rfqEst["tamura_entity_profit_value_sales_office".$NPINumber] : null;
                $npi->rfq_selling_price_sales_office = isset($rfqEst["tamura_entity_selling_price_sales_office".$NPINumber])? $rfqEst["tamura_entity_selling_price_sales_office".$NPINumber] : null;

                $npi->sample_customer_address = $customerAddress;
                $npi->sample_manufactured_purchased = isset($tamuraEntity["manufactured_purchased"])? $tamuraEntity["manufactured_purchased"] : 1;
                $npi->creation_date =  date("Y-m-d H:i:s", strtotime("NOW"));
                $npi->created_by = isset ($userName) ?  $userName : null;
                $npi->save();
            }

            $this->updateRfqEstimatedFactoryCost($rfqId,$rfqEst);

            return $this->resultService->Success($npi);

        }catch(Exception $e){
            $this->logService->log("ERROR NPIService->generateNPI", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function changeSampleTeuPn($npi){
        try{
            $id = $npi["id"];
            if(is_null($id)){
                return $this->resultService->Error("NPI id cannot be empty");
            }

            $res = NPI::where("id", $id)
                ->update([
                    "sample_teu_pn" => isset($npi["sample_teu_pn"])? $npi["sample_teu_pn"]:"",
                    "updated_at" => new \DateTime()
                ]);

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR NPIService->updateComment", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}