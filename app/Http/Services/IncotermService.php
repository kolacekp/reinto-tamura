<?php
namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Incoterm;

use DB;

class IncotermService
{


    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function getAll(){
        try{

            $incoterms = Incoterm::get();

            return $this->resultService->Success($incoterms);

        }catch(Exception $e){
            $this->logService->log("ERROR IncotermService->getAll", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}