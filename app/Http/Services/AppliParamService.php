<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\AppliParam;

use DB;

class AppliParamService
{

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function updateDate($name)
    {
        try{
            if(empty($name)){
                return $this->resultService->Error("Name cannot be empty");
            }

            AppliParam::where("name", $name)
                        ->update([
                            "value" => date("m/d/Y", strtotime("NOW")),
                            "updated_at" => new \DateTime()
                        ]);

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR AppliParamService->updateDate", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getAllAppliParams()
    {
        try{

            $appliParams = AppliParam::get();

            return $appliParams;

        }catch(Exception $e){
            $this->logService->log("ERROR AppliParamService->getAllAppliParams", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}
