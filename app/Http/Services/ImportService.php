<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\AppliParam;
use App\Category;
use App\Country;
use App\Currency;
use App\Customer;
use App\ExcelExtractLog;
use App\Incoterm;
use App\LME;
use App\MAT;
use App\NPI;
use App\Person;
use App\Project;
use App\QuotationFormTemplateList;
use App\RFQ;
use App\RFQDeadReason;
use App\RFQEstimatedFactoryCost;
use App\RFQExportError;
use App\RFQMat;
use App\RFQSpecification;
use App\RFQStatusLog;
use App\Sale;
use App\SearchParam;
use App\StandardItem;
use App\TamuraCodification;
use App\TamuraEntity;
use App\TimeSheet;

use Excel;
use DB;
use SimpleXMLElement;

use stdClass;

class ImportService
{

    private $resultService;
    private $logService;
            
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }
    
    public function importAppliParam($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('appli_params')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->AppliParam as $row){
               
                $appliParam = new AppliParam();
                $appliParam->id =          (int)$row->ParamId->__toString();
                $appliParam->name =        $row->ParamName->__toString();
                $appliParam->value =       $row->ParamValue->__toString();
                $appliParam->description = $row->ParamDescription->__toString();
                $appliParam->save();
            }

     
            DB::commit();
            
            return $this->resultService->Success("Imported AppliParam.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importAppliParam", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importCategory($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('categories')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->Category as $row){             
                $category = new Category();
                $category->id =   (int)$row->ProductCategoryId->__toString();
                $category->name = $row->ProductCategory->__toString();
                $category->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Category.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importCategory", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importCountry($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('countries')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->Countries as $row){             
                $country = new Country();
                $country->iso2 =             $row->ISO2->__toString();
                $country->name =             $row->CountryName->__toString();
                $country->default_currency = $row->DefaultCurrency->__toString();
                $country->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Country.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importCountry", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importCurrency($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('currencies')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
          
            foreach($xml->Currency as $row){      
                $currency = new Currency();
                $currency->id =                 (int)$row->CurrencyId->__toString();
                $currency->long_name =          $row->CurrencyLongName->__toString();
                $currency->short_name = $row->CurrencyShortName->__toString();
                $currency->symbol = $row->CurrencySymbol->__toString();
                $currency->exch_rate = (float)$row->CurrencyExchRate->__toString() == 0 ? null : (float)$row->CurrencyExchRate->__toString();
                $currency->default = $row->Default->__toString();
                $currency->upsize_ts = $row->upsize_ts->__toString();
                $currency->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Currency.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importCountry", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importCustomer($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('customers')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
       
            foreach($xml->Customer as $row){      
                $customer = new Customer();
                    $customer->id =                 (int)$row->CustomerId->__toString();
                    $customer->name =               $row->CustomerName->__toString();
                    $customer->address =            $row->CustomerAddress->__toString();
                    $customer->type =               $row->CustomerType->__toString();
                    $customer->id_sup =             (int)$row->CustomerIdSup->__toString() == 0 ? null : (int)$row->CustomerIdSup->__toString();
                    $customer->id_root =            (int)$row->CustomerIdRoot->__toString() == 0 ? null : (int)$row->CustomerIdRoot->__toString();
                    $customer->path =               $row->CustomerPath->__toString();
                    $customer->deactivated =        $row->CustomerDeactivated->__toString() == "1" ? true : false;
                    $customer->account_manager_id = (int)$row->CustomerAccountManagerId->__toString() == 0 ? null : (int)$row->CustomerAccountManagerId->__toString();
                    $customer->payment_term_nr =    $row->CustomerPaymentTermNr->__toString();
                    $customer->payment_term_type =  $row->CustomerPaymentTermType->__toString();
                    $customer->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Customer.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importCustomer", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importExcelExtractLog($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('excel_extract_logs')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->ExcelExtractLog as $row){      
                $eel = new ExcelExtractLog();
                $eel->id = (int)$row->xlExtractId->__toString();
                $eel->person_login = $row->xlExtractPersonLogin->__toString();
                $eel->date = empty($row->xlExtractDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractDate)));
                $eel->project_id = empty ((int)$row->xlExtractProjectId->__toString()) ? null : (int)$row->xlExtractProjectId->__toString();
                $eel->project_status_init = $row->xlExtractProjectStatusINIT->__toString() == "1" ? true : false;
                $eel->project_status_opened = $row->xlExtractProjectStatusOPENED->__toString() == "1" ? true : false;
                $eel->project_status_closed = $row->xlExtractProjectStatusCLOSED->__toString() == "1" ? true : false;
                $eel->project_manager_id = empty ((int)$row->xlExtractProjectManagerId->__toString()) ? null : (int)$row->xlExtractProjectManagerId->__toString();
                $eel->key_account_manager_id = empty ((int)$row->xlExtractKeyAccountManagerId->__toString()) ? null : (int)$row->xlExtractKeyAccountManagerId->__toString();
                $eel->project_customer_id = empty ((int)$row->xlExtractProjectCustomerId->__toString()) ? null : (int)$row->xlExtractProjectCustomerId->__toString();
                $eel->project_include_all_group = $row->xlExtractProjectIncludeAllGroup->__toString() == "1" ? true : false;
                $eel->project_purchaser_id = empty ((int)$row->xlExtractProjectPurchaserId->__toString()) ? null : (int)$row->xlExtractProjectPurchaserId->__toString();
                $eel->project_market_id = $row->xlExtractProjectMarketId->__toString();
                $eel->project_creation_date_from = empty($row->xlExtractProjectCreationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractProjectCreationDateFrom)));
                $eel->project_creation_date_to = empty($row->xlExtractProjectCreationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractProjectCreationDateTo)));
                $eel->project_modification_date_from = empty($row->xlExtractProjectModificationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractProjectModificationDateFrom)));
                $eel->project_modification_date_to = empty($row->xlExtractProjectModificationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractProjectModificationDateTo)));
                $eel->rfq_id = empty ((int)$row->xlExtractRFQId->__toString()) ? null : (int)$row->xlExtractRFQId->__toString();
                $eel->rfq_status_init = $row->xlExtractRFQStatusINIT->__toString() == "1" ? true : false;
                $eel->rfq_status_t10 = $row->xlExtractRFQStatusT10->__toString() == "1" ? true : false;
                $eel->rfq_status_t20 = $row->xlExtractRFQStatusT20->__toString() == "1" ? true : false;
                $eel->rfq_status_t30 = $row->xlExtractRFQStatusT30->__toString() == "1" ? true : false;
                $eel->rfq_status_dead = $row->xlExtractRFQStatusDEAD->__toString() == "1" ? true : false;
                $eel->rfq_status_rej = $row->xlExtractRFQStatusREJ->__toString() == "1" ? true : false;
                $eel->rfq_customer_pn = $row->xlExtractRFQCustomerPN->__toString();
                $eel->rfq_eng_action_design_required = $row->xlExtractRFQEngActionDesignRequired->__toString() == "1" ? true : false;
                $eel->rfq_eng_action_costing_complete = $row->xlExtractRFQEngActionCostingComplete->__toString() == "1" ? true : false;
                $eel->rfq_eng_action_design_complete = $row->xlExtractRFQEngActionDesignComplete->__toString() == "1" ? true : false;
                $eel->rfq_eng_action_mass_prod_auth = $row->xlExtractRFQEngActionMassProdAuth->__toString() == "1" ? true : false;
                $eel->rfq_dead_reason_id = empty ((int)$row->xlExtractRFQDeadReasonId->__toString()) ? null : (int)$row->xlExtractRFQDeadReasonId->__toString();
                $eel->rfq_pn_category = $row->xlExtractRFQPNCategory->__toString();
                $eel->rfq_pn_format = $row->xlExtractRFQPNFormat->__toString();
                $eel->rfq_pn_new_dev = empty ((int)$row->xlExtractRFQPnNewDev->__toString()) ? null : (int)$row->xlExtractRFQPNNewDev->__toString();
                $eel->rfq_pn_new_biz = empty ((int)$row->xlExtractRFQPnNewBiz->__toString()) ? null : (int)$row->xlExtractRFQPNNewBiz->__toString();
                $eel->rfq_priority = $row->xlExtractRFQPriority->__toString();
                $eel->rfq_customer_engineer_id = empty ((int)$row->xlExtractRFQCustomerEngineerId->__toString()) ? null : (int)$row->xlExtractRFQCustomerEngineerId->__toString();
                $eel->rfq_tamura_elec_engineer_id = empty ((int)$row->xlExtractRFQTamuraElecEngineerId->__toString()) ? null : (int)$row->xlExtractRFQTamuraElecEngineerId->__toString();
                $eel->rfq_tamura_mec_engineer_id = empty ((int)$row->xlExtractRFQTamuraMecEngineerId->__toString()) ? null : (int)$row->xlExtractRFQTamuraMecEngineerId->__toString();
                $eel->rfq_product_id = $row->xlExtractRFQProductId->__toString();             
                $eel->rfq_creation_date_from = empty($row->xlExtractRFQCreationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractRFQCreationDateFrom)));
                $eel->rfq_creation_date_to = empty($row->xlExtractRFQCreationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractRFQCreationDateTo)));
                $eel->rfq_modification_date_from = empty($row->xlExtractRFQModificationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractRFQModificationDateFrom)));
                $eel->rfq_modification_date_to = empty($row->xlExtractRFQModificationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->xlExtractRFQModificationDateTo)));               
                $eel->sales_valid = $row->xlExtractSalesValid->__toString() == "1" ? true : false;
                $eel->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Excel Extract Log.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importExcelExtractLog", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importIncoterm($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('incoterms')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
        
            foreach($xml->Incoterms as $row){      
                $incoterm = new Incoterm();
                $incoterm->short = $row->IncotermsShort->__toString();
                $incoterm->label = $row->IncotermsLabel->__toString();
                $incoterm->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Incoterm.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importIncoterm", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importLme($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('lmes')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
       
            foreach($xml->LME as $row){      
                $lme = new LME();
                $lme->id = (int)$row->LMEId->__toString();
                $lme->cu = empty ((int)$row->CU->__toString()) ? null : (int)$row->CU->__toString();
                $lme->al = empty ((int)$row->AL->__toString()) ? null : (int)$row->AL->__toString();
                $lme->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported LME.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importLme", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importMat($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('mats')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
          
            foreach($xml->MAT as $row){      
                $mat = new MAT();
                $mat->id = (int)$row->MATId->__toString();
                $mat->type = $row->Type->__toString();
                $mat->part_no = $row->PartNo->__toString();
                $mat->description = $row->Description->__toString();
                $mat->unitary_price = empty((double)$row->UnitaryPrice->__toString()) ? null : (double)$row->UnitaryPrice->__toString();
                $mat->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported MAT.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importMat", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importNpi($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('npis')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
           
            foreach($xml->NPI as $row){      
                $npi = new NPI();
                $npi->id = (int)$row->NPIId->__toString();
                $npi->rfq_id = empty((int)$row->RFQId->__toString()) ? null : (int)$row->RFQId->__toString();
                $npi->tamura_entity_id_factory = empty((int)$row->NPI_TamuraEntityId_Factory->__toString()) ? null : (int)$row->NPI_TamuraEntityId_Factory->__toString();
                $npi->tamura_entity_id_sales_office = empty((int)$row->NPI_TamuraEntityId_SalesOffice->__toString()) ? null : (int)$row->NPI_TamuraEntityId_SalesOffice->__toString();
                $npi->currency_id = empty((int)$row->NPI_CurrencyId->__toString()) ? null : (int)$row->NPI_CurrencyId->__toString();
                $npi->rfq_material_cost_value = empty((double)$row->NPI_RFQ_MaterialCostValue->__toString()) ? null : (double)$row->NPI_RFQ_MaterialCostValue->__toString();
                $npi->rfq_profit_factory = empty((double)$row->NPI_RFQ_Profit_Factory->__toString()) ? null : (double)$row->NPI_RFQ_Profit_Factory->__toString();
                $npi->rfq_profit_value_factory = empty((double)$row->NPI_RFQ_ProfitValue_Factory->__toString()) ? null : (double)$row->NPI_RFQ_ProfitValue_Factory->__toString();
                $npi->rfq_labour = empty((double)$row->NPI_RFQ_Labour->__toString()) ? null : (double)$row->NPI_RFQ_Labour->__toString();
                $npi->rfq_labour_time = empty((double)$row->NPI_RFQ_LabourTime->__toString()) ? null : (double)$row->NPI_RFQ_LabourTime->__toString();
                $npi->rfq_labour_value = empty((double)$row->NPI_RFQ_LabourValue->__toString()) ? null : (double)$row->NPI_RFQ_LabourValue->__toString();
                $npi->rfq_freight = empty((double)$row->NPI_RFQ_Freight->__toString()) ? null : (double)$row->NPI_RFQ_Freight->__toString();
                $npi->rfq_freight_value = empty((double)$row->NPI_RFQ_FreightValue->__toString()) ? null : (double)$row->NPI_RFQ_FreightValue->__toString();
                $npi->rfq_admin = empty((double)$row->NPI_RFQ_Admin->__toString()) ? null : (double)$row->NPI_RFQ_Admin->__toString();
                $npi->rfq_admin_value = empty((double)$row->NPI_RFQ_AdminValue->__toString()) ? null : (double)$row->NPI_RFQ_AdminValue->__toString();
                $npi->rfq_selling_price_factory = empty((double)$row->NPI_RFQ_SellingPrice_Factory->__toString()) ? null : (double)$row->NPI_RFQ_SellingPrice_Factory->__toString();
                $npi->rfq_profit_sales_office = empty((double)$row->NPI_RFQ_Profit_SalesOffice->__toString()) ? null : (double)$row->NPI_RFQ_Profit_SalesOfiice->__toString();
                $npi->rfq_profit_value_sales_office = empty((double)$row->NPI_RFQ_ProfitValue_SalesOffice->__toString()) ? null : (double)$row->NPI_RFQ_ProfitValue_SalesOffice->__toString();
                $npi->rfq_selling_price_sales_office = empty((double)$row->NPI_RFQ_SellingPrice_SalesOffice->__toString()) ? null : (double)$row->NPI_RFQ_SellingPrice_SalesOffice->__toString();
                $npi->material_cost_value = empty((double)$row->NPI_MaterialCostValue->__toString()) ? null : (double)$row->NPI_MaterialCostValue->__toString();
                $npi->profit_factory = empty((double)$row->NPI_Profit_Factory->__toString()) ? null : (double)$row->NPI_Profit_Factory->__toString();
                $npi->profit_value_factory = empty((double)$row->NPI_ProfitValue_Factory->__toString()) ? null : (double)$row->NPI_ProfitValue_Factory->__toString();
                $npi->labour = empty((double)$row->NPI_Labour->__toString()) ? null : (double)$row->NPI_Labour->__toString();
                $npi->labour_time = empty((double)$row->NPI_LabourTime->__toString()) ? null : (double)$row->NPI_LabourTime->__toString();
                $npi->labour_value = empty((double)$row->NPI_LabourValue->__toString()) ? null : (double)$row->NPI_LabourValue->__toString();
                $npi->freight = empty((double)$row->NPI_Freight->__toString()) ? null : (double)$row->NPI_Freight->__toString();
                $npi->freight_value = empty((double)$row->NPI_FreightValue->__toString()) ? null : (double)$row->NPI_FreightValue->__toString();
                $npi->admin = empty((double)$row->NPI_Admin->__toString()) ? null : (double)$row->NPI_Admin->__toString();
                $npi->admin_value = empty((double)$row->NPI_AdminValue->__toString()) ? null : (double)$row->NPI_AdminValue->__toString();
                $npi->selling_price_factory = empty((double)$row->NPI_SellingPrice_Factory->__toString()) ? null : (double)$row->NPI_SellingPrice_Factory->__toString();
                $npi->profit_sales_office = empty((double)$row->NPI_Profit_SalesOffice->__toString()) ? null : (double)$row->NPI_Profit_SalesOfiice->__toString();
                $npi->profit_value_sales_office = empty((double)$row->NPI_ProfitValue_SalesOffice->__toString()) ? null : (double)$row->NPI_ProfitValue_SalesOffice->__toString();
                $npi->selling_price_sales_office = empty((double)$row->NPI_SellingPrice_SalesOffice->__toString()) ? null : (double)$row->NPI_SellingPrice_SalesOffice->__toString();
                $npi->sample_quantity_required= empty((int)$row->NPI_Sample_QuantityRequired->__toString()) ? null : (int)$row->NPI_Sample_QuantityRequired->__toString();
                $npi->sample_delivery_date = empty($row->NPI_Sample_DeliveryDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->NPI_Sample_DeliveryDate)));
                $npi->sample_chargeable = $row->NPI_Sample_Chargeable->__toString() == "1" ? true : false;
                $npi->sample_price = empty((double)$row->NPI_Sample_Price->__toString()) ? null : (double)$row->NPI_Sample_Price->__toString();
                $npi->sample_incoterms_id = $row->NPI_Sample_IncotermsId->__toString();
                $npi->sample_manufactured_purchased = empty((int)$row->NPI_Sample_ManufacturedPurchased->__toString()) ? null : (int)$row->NPI_Sample_ManufacturedPurchased->__toString();
                $npi->sample_lead_time = empty((int)$row->NPI_Sample_LeadTime->__toString()) ? null : (int)$row->NPI_Sample_LeadTime->__toString();
                $npi->sample_teu_pn = $row->NPI_Sample_TEU_PN->__toString();
                $npi->sample_customer_address = $row->NPI_Sample_CustomerAddress->__toString();
                $npi->minimum_order_quantity = empty((int)$row->NPI_MinimumOrderQuantity->__toString()) ? null : (int)$row->NPI_MinimumOrderQuantity->__toString();
                $npi->minimum_package_quantity = empty((int)$row->NPI_MinimumPackageQuantity->__toString()) ? null : (int)$row->NPI_MinimumPackageQuantity->__toString();
                $npi->comment = $row->NPI_Comment->__toString();
                $npi->creation_date = empty($row->NPI_CreationDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->NPI_CreationDate)));
                $npi->created_by = $row->NPI_CreatedBy->__toString();
                $npi->last_modified_on = empty($row->NPI_LastModifiedOn) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->NPI_LastModifiedOn)));
                $npi->last_modified_by = $row->NPI_LastModifiedBy->__toString();
                $npi->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported NPI.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importNpi", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importPerson($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('persons')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);

            foreach($xml->Person as $row){      
                $person = new Person();
                $person->id =  (int)$row->PersonId->__toString();
                $person->customer_id = empty((int)$row->CustomerId->__toString()) ? null : (int)$row->CustomerId->__toString();
                $person->tamura_entity_id = empty((int)$row->TamuraEntityId->__toString()) ? null : (int)$row->TamuraEntityId->__toString();
                $person->name = $row->PersonName->__toString();
                $person->email = $row->PersonEMail->__toString();
                $person->phone = $row->PersonPhone->__toString();
                $person->mobile = $row->PersonMobile->__toString();
                $person->deactivated = $row->PersonDeactivated->__toString() == "1" ? true : false;
                $person->sales_manager = $row->PersonSalesManager->__toString() == "1" ? true : false;
                $person->engineer = $row->PersonEngineer->__toString() == "1" ? true : false;
                $person->purchaser = $row->PersonPurchaser->__toString() == "1" ? true : false;
                $person->db_login = $row->DBLogin->__toString();
                $person->db_password = $row->DBPassword->__toString();               
                $person->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Person.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importProject($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('projects')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);

            foreach($xml->Project as $row){      
                $project = new Project();
                $project->id = (int)$row->ProjectId->__toString();
                $project->name = $row->Project_Name->__toString();
                $project->status = $row->Project_Status->__toString();
                $project->market_id = $row->Project_MarketId->__toString();
                $project->standards = $row->Project_Standards->__toString();
                $project->description = $row->Project_description->__toString();
                $project->remarks = $row->Project_Remarks->__toString();
                $project->estimated_value = empty((int)$row->Project_EstimatedValue->__toString()) ? null : (int)$row->Project_EstimatedValue->__toString();
                $project->estimated_value_currency_id = empty((int)$row->Project_EstimatedValueCurrencyId->__toString()) ? null : (int)$row->Project_EstimatedValueCurrencyId->__toString();
                $project->item_to_design = empty((int)$row->Project_ItemToDesign->__toString()) ? null : (int)$row->Project_ItemToDesign->__toString();
                $project->customer_id = empty((int)$row->Project_CustomerId->__toString()) ? null : (int)$row->Project_CustomerId->__toString();
                $project->end_customer = $row->Project_EndCustomer->__toString();
                $project->customer_purchase_manager_id = empty((int)$row->Project_CustomerPurchaseManagerId->__toString()) ? null : (int)$row->Project_CustomerPurchaseManagerId->__toString();
                $project->manager_id = empty((int)$row->Project_ManagerId->__toString()) ? null : (int)$row->Project_ManagerId->__toString();
                $project->business_location = $row->Project_BusinessLocation->__toString();
                $project->creation_date = empty($row->Project_CreationDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->Project_CreationDate)));
                $project->last_modified_on = empty($row->Project_LastModifiedOn) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->Project_LastModifiedOn)));
                $project->last_modified_by = $row->Project_LastModifiedBy->__toString();
                $project->life = empty((int)$row->Project_Life->__toString()) ? null : (int)$row->Project_Life->__toString();
                $project->spot_order = empty((int)$row->Project_SpotOrder->__toString()) ? null : (int)$row->Project_SpotOrder->__toString();          
                $project->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Project.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importProject", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importQuotationFormTemplateList($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('quotation_form_template_lists')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);

            foreach($xml->QuotationFormTemplateList as $row){      
                $qftl = new QuotationFormTemplateList();
                $qftl->id = (int)$row->QuotationFormTemplateId->__toString();
                $qftl->name = $row->QuotationFormTemplateName->__toString();
                $qftl->comment = $row->QuotationFormTemplateComment->__toString();
                $qftl->file_name = $row->QuotationFormFileName->__toString();
                $qftl->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported QuotationFormTemplateList.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importQuotationFormTemplateList", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfq($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfqs')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            // TODO
            foreach($xml->RFQ as $row){      
                $rfq = new RFQ();
                $rfq->id = (int)$row->RFQId->__toString();
                $rfq->father = empty((int)$row->RFQFather->__toString()) ? null : (int)$row->RFQFather->__toString();
                $rfq->is_std_item = $row->RFQisStdItem->__toString() == "1" ? true : false;
                $rfq->project_id = empty((int)$row->ProjectId->__toString()) ? null : (int)$row->ProjectId->__toString();
                $rfq->status = $row->RFQ_Status->__toString();
                $rfq->folder = $row->RFQ_Folder->__toString();
                $rfq->priority = $row->RFQ_Priority->__toString();
                $rfq->design_office_id = empty((int)$row->RFQ_DesignOfficeId->__toString()) ? null : (int)$row->RFQ_DesignOfficeId->__toString();
                $rfq->customer_engineer_id = empty((int)$row->RFQ_CustomerEngineerId->__toString()) ? null : (int)$row->RFQ_CustomerEngineerId->__toString();
                $rfq->tamura_elec_engineer_id = empty((int)$row->RFQ_TamuraElecEngineerId->__toString()) ? null : (int)$row->RFQ_TamuraElecEngineerId->__toString();
                $rfq->tamura_mec_engineer_id = empty((int)$row->RFQ_TamuraMecEngineerId->__toString()) ? null : (int)$row->RFQ_TamuraMecEngineerId->__toString();
                $rfq->product_id = $row->RFQ_ProductId->__toString();
                $rfq->pn_customer = $row->RFQ_PN_Customer->__toString();
                $rfq->pn_desc = $row->RFQ_PN_Desc->__toString();
                $rfq->pn_category_id = empty((int)$row->RFQ_PN_CategoryId->__toString()) ? null : (int)$row->RFQ_PN_CategoryId->__toString();
                $rfq->pn_format = $row->RFQ_PN_Format->__toString();
                $rfq->pn_new_dev = empty((int)$row->RFQ_PN_NewDev->__toString()) ? null : (int)$row->RFQ_PN_NewDev->__toString();
                $rfq->pn_new_biz = empty((int)$row->RFQ_PN_NewBiz->__toString()) ? null : (int)$row->RFQ_PN_NewBiz->__toString();              
                $rfq->com_currency_id = empty((int)$row->RFQ_Com_CurrencyId->__toString()) ? null : (int)$row->RFQ_Com_CurrencyId->__toString();
                $rfq->com_estimated_price = empty((double)$row->RFQ_Com_EstimatedPrice->__toString()) ? null : (double)$row->RFQ_Com_EstimatedPrice->__toString();
                $rfq->com_target_price = empty((double)$row->RFQ_Com_TargetPrice->__toString()) ? null : (double)$row->RFQ_Com_TargetPrice->__toString();
                $rfq->com_annual_qty = empty((int)$row->RFQ_Com_AnnualQty->__toString()) ? null : (int)$row->RFQ_Com_AnnualQty->__toString();
                $rfq->com_annual_value = empty((double)$row->RFQ_Com_AnnualValue->__toString()) ? null : (double)$row->RFQ_Com_AnnualValue->__toString();
                $rfq->com_confidence_level = empty((double)$row->RFQ_Com_ConfidenceLevel->__toString()) ? null : (double)$row->RFQ_Com_ConfidenceLevel->__toString();
                $rfq->com_incoterms = $row->RFQ_Com_Incoterms->__toString();
                $rfq->com_delivery_location = $row->RFQ_Com_DeliveryLocation->__toString();
                $rfq->com_teu_group = $row->RFQ_Com_TEUGroup->__toString() == "1" ? true : false;
                $rfq->qualif_quotation_required_date = empty($row->RFQ_Qualif_QuotationRequireddate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Qualif_QuotationRequireddate)));
                $rfq->qualif_sample_date = empty($row->RFQ_Qualif_SampleDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Qualif_SampleDate)));
                $rfq->qualif_mass_prod_date = empty($row->RFQ_Qualif_MassProdDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Qualif_MassProdDate)));
                $rfq->comments = $row->RFQ_Comments->__toString();
                $rfq->duplicate = empty((int)$row->RFQ_Duplicate->__toString()) ? null : (int)$row->RFQ_Duplicate->__toString();              
                $rfq->duplicate_from = empty((int)$row->RFQ_DuplicateFrom->__toString()) ? null : (int)$row->RFQ_DuplicateFrom->__toString();              
                $rfq->creation_date = empty($row->RFQ_CreationDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_CreationDate)));
                $rfq->last_modified_on = empty($row->RFQ_LastModifiedOn) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_LastModifiedOn)));
                $rfq->last_modified_by = $row->RFQ_LastModifiedBy->__toString();
                $rfq->status_10 = $row->RFQ_Status_10->__toString() == "1" ? true : false;
                $rfq->status_10_who = $row->RFQ_Status_10_Who->__toString();
                $rfq->status_10_when = empty($row->RFQ_Status_10_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Status_10_When)));
                $rfq->status_20 = $row->RFQ_Status_20->__toString() == "1" ? true : false;
                $rfq->status_20_who = $row->RFQ_Status_20_Who->__toString();
                $rfq->status_20_when = empty($row->RFQ_Status_20_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Status_20_When)));
                $rfq->status_30 = $row->RFQ_Status_30->__toString() == "1" ? true : false;
                $rfq->status_30_who = $row->RFQ_Status_30_Who->__toString();
                $rfq->status_30_when = empty($row->RFQ_Status_30_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Status_30_When)));
                $rfq->status_dead = $row->RFQ_Status_Dead->__toString() == "1" ? true : false;
                $rfq->status_dead_who = $row->RFQ_Status_Dead_Who->__toString();
                $rfq->status_dead_when = empty($row->RFQ_Status_Dead_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Status_Dead_When)));
                $rfq->status_dead_reason_id = empty((int)$row->RFQ_Dead_ReasonId->__toString()) ? null : (int)$row->RFQ_Dead_ReasonId->__toString();              
                $rfq->status_rejected = $row->RFQ_Status_Rejected->__toString() == "1" ? true : false;
                $rfq->status_rejected_who = $row->RFQ_Status_Rejected_Who->__toString();
                $rfq->status_rejected_when = empty($row->RFQ_Status_Rejected_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_Status_Rejected_When)));
                $rfq->eng_action_costing_planned_date = empty($row->RFQ_EngAction_CostingPlannedDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_EngAction_CostingPlannedDate)));
                $rfq->eng_action_costing_complete = $row->RFQ_EngAction_CostingComplete->__toString() == "1" ? true : false;
                $rfq->eng_action_costing_complete_who = $row->RFQ_EngAction_CostingComplete_Who->__toString();
                $rfq->eng_action_costing_complete_when = empty($row->RFQ_EngAction_CostingComplete_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_EngAction_CostingComplete_When)));
                $rfq->eng_action_design_required = $row->RFQ_EngAction_DesignRequired->__toString() == "1" ? true : false;
                $rfq->eng_action_design_required_who = $row->RFQ_EngAction_DesignRequired_Who->__toString();
                $rfq->eng_action_design_required_when = empty($row->RFQ_EngAction_DesignRequired_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_EngAction_DesignRequired_When)));
                $rfq->eng_action_design_complete_planned_date = empty($row->RFQ_EngAction_DesignCompletePlannedDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_EngAction_DesignCompletePlannedDate)));
                $rfq->eng_action_design_complete = $row->RFQ_EngAction_DesignComplete->__toString() == "1" ? true : false;
                $rfq->eng_action_design_complete_who = $row->RFQ_EngAction_DesignComplete_Who->__toString();
                $rfq->eng_action_design_complete_when = empty($row->RFQ_EngAction_DesignComplete_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_EngAction_DesignComplete_When)));
                $rfq->eng_action_mass_prod_auth = $row->RFQ_EngAction_MassProdAuth->__toString() == "1" ? true : false;
                $rfq->eng_action_mass_prod_auth_who = $row->RFQ_EngAction_MassProdAuth_Who->__toString();
                $rfq->eng_action_mass_prod_auth_when = empty($row->RFQ_EngAction_MassProdAuth_When) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQ_EngAction_MassProdAuth_When)));
                $rfq->product_type = $row->RFQ_Product_Type->__toString();
                $rfq->product_length = empty((double)$row->RFQ_Product_Length->__toString()) ? null : (double)$row->RFQ_Product_Length->__toString();
                $rfq->product_width = empty((double)$row->RFQ_Product_Width->__toString()) ? null : (double)$row->RFQ_Product_Width->__toString();
                $rfq->product_height = empty((double)$row->RFQ_Product_Height->__toString()) ? null : (double)$row->RFQ_Product_Height->__toString();
                $rfq->product_weight = empty((double)$row->RFQ_Product_Weight->__toString()) ? null : (double)$row->RFQ_Product_Weight->__toString();
                $rfq->product_field1 = empty((double)$row->RFQ_Product_Field1->__toString()) ? null : (double)$row->RFQ_Product_Field1->__toString();
                $rfq->product_field2 = empty((double)$row->RFQ_Product_Field2->__toString()) ? null : (double)$row->RFQ_Product_Field2->__toString();
                $rfq->product_field3 = empty((double)$row->RFQ_Product_Field3->__toString()) ? null : (double)$row->RFQ_Product_Field3->__toString();
                $rfq->product_field4 = empty((double)$row->RFQ_Product_Field4->__toString()) ? null : (double)$row->RFQ_Product_Field4->__toString();
                $rfq->product_field5 = empty((double)$row->RFQ_Product_Field5->__toString()) ? null : (double)$row->RFQ_Product_Field5->__toString();
                $rfq->product_field6 = empty((double)$row->RFQ_Product_Field6->__toString()) ? null : (double)$row->RFQ_Product_Field6->__toString();
                $rfq->product_other_materials = empty((double)$row->RFQ_Product_OtherMaterials->__toString()) ? null : (double)$row->RFQ_Product_OtherMaterials->__toString();
                $rfq->product_other_materials_value = empty((double)$row->RFQ_Product_OtherMaterialsValue->__toString()) ? null : (double)$row->RFQ_Product_OtherMaterialsValue->__toString();
                $rfq->product_handling_scrap = empty((double)$row->RFQ_Product_HandlingScrap->__toString()) ? null : (double)$row->RFQ_Product_HandlingScrap->__toString();
                $rfq->product_handling_scrap_value = empty((double)$row->RFQ_Product_HandlingScrapValue->__toString()) ? null : (double)$row->RFQ_Product_HandlingScrapValue->__toString();
                $rfq->mat_currency_id = empty((int)$row->RFQ_Mat_CurrencyId->__toString()) ? null : (int)$row->RFQ_Mat_CurrencyId->__toString();              
                $rfq->mat_total_value = empty((double)$row->RFQ_Mat_TotalValue->__toString()) ? null : (double)$row->RFQ_Mat_TotalValue->__toString();
                $rfq->pallet_length = empty((double)$row->RFQ_Pallet_Length->__toString()) ? null : (double)$row->RFQ_Pallet_Length->__toString();
                $rfq->pallet_width = empty((double)$row->RFQ_Pallet_Width->__toString()) ? null : (double)$row->RFQ_Pallet_Width->__toString();
                $rfq->pallet_space = empty((double)$row->RFQ_Pallet_Space->__toString()) ? null : (double)$row->RFQ_Pallet_Space->__toString();
                $rfq->pallet_layers = empty((double)$row->RFQ_Pallet_Layers->__toString()) ? null : (double)$row->RFQ_Pallet_Layers->__toString();
                $rfq->pallet_quantity = empty((int)$row->RFQ_Pallet_Quantity->__toString()) ? null : (int)$row->RFQ_Pallet_Quantity->__toString();
                $rfq->labour1 = empty((int)$row->RFQ_Labour1->__toString()) ? null : (int)$row->RFQ_Labour1->__toString();
                $rfq->labour2 = empty((int)$row->RFQ_Labour2->__toString()) ? null : (int)$row->RFQ_Labour2->__toString();
                $rfq->labour3 = empty((int)$row->RFQ_Labour3->__toString()) ? null : (int)$row->RFQ_Labour3->__toString();
                $rfq->labour_time1 = empty((double)$row->RFQ_LabourTime1->__toString()) ? null : (double)$row->RFQ_LabourTime1->__toString();
                $rfq->labour_time2 = empty((double)$row->RFQ_LabourTime2->__toString()) ? null : (double)$row->RFQ_LabourTime2->__toString();
                $rfq->labour_time3 = empty((double)$row->RFQ_LabourTime3->__toString()) ? null : (double)$row->RFQ_LabourTime3->__toString();
                $rfq->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQ.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfqEstimatedFactoryCost($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfq_estimated_factory_costs')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->RFQ_EstimatedFactoryCost as $row){      
                $rfqefc = new RFQEstimatedFactoryCost();
                $rfqefc->id = (int)$row->RFQ_EstimatedFactoryCostId->__toString();
                $rfqefc->rfq_id = empty((int)$row->RFQId->__toString()) ? null : (int)$row->RFQId->__toString();
                $rfqefc->tamura_entity_id_factory1 = empty((int)$row->TamuraEntityId_Factory1->__toString()) ? null : (int)$row->TamuraEntityId_Factory1->__toString();
                $rfqefc->tamura_entity_currency_factory1 = empty((int)$row->TamuraEntityCurrency_Factory1->__toString()) ? null : (int)$row->TamuraEntityCurrency_Factory1->__toString();
                $rfqefc->rfq_material_factory1 = empty((double)$row->RFQ_Material_Factory1->__toString()) ? null : (double)$row->RFQ_Material_Factory1->__toString();
                $rfqefc->tamura_entity_profit_factory1 = empty((double)$row->TamuraEntityProfit_Factory1->__toString()) ? null : (double)$row->TamuraEntityProfit_Factory1->__toString();
                $rfqefc->tamura_entity_profit_value_factory1 = empty((double)$row->TamuraEntityProfitValue_Factory1->__toString()) ? null : (double)$row->TamuraEntityProfitValue_Factory1->__toString();
                $rfqefc->tamura_entity_labour_rate_choice_factory1 = empty((double)$row->TamuraEntityLabourRateChoice_Factory1->__toString()) ? null : (double)$row->TamuraEntityLabourRateChoice_Factory1->__toString();
                $rfqefc->tamura_entity_labour_time_value_factory1 = empty((double)$row->TamuraEntityLabourTimeValue_Factory1->__toString()) ? null : (double)$row->TamuraEntityLabourTimeValue_Factory1->__toString();
                $rfqefc->tamura_entity_labour_rate_factory1 = empty((double)$row->TamuraEntityLabourRate_Factory1->__toString()) ? null : (double)$row->TamuraEntityLabourRate_Factory1->__toString();
                $rfqefc->tamura_entity_labour_rate_value_factory1 = empty((double)$row->TamuraEntityLabourRateValue_Factory1->__toString()) ? null : (double)$row->TamuraEntityLabourRateValue_Factory1->__toString();
                $rfqefc->tamura_entity_freight_factory1 = empty((double)$row->TamuraEntityFreight_Factory1->__toString()) ? null : (double)$row->TamuraEntityFreight_Factory1->__toString();
                $rfqefc->tamura_entity_freight_value_factory1 = empty((double)$row->TamuraEntityFreightValue_Factory1->__toString()) ? null : (double)$row->TamuraEntityFreightValue_Factory1->__toString();
                $rfqefc->tamura_entity_admin_factory1 = empty((double)$row->TamuraEntityAdmin_Factory1->__toString()) ? null : (double)$row->TamuraEntityAdmin_Factory1->__toString();
                $rfqefc->tamura_entity_admin_value_factory1 = empty((double)$row->TamuraEntityAdminValue_Factory1->__toString()) ? null : (double)$row->TamuraEntityAdminValue_Factory1->__toString();
                $rfqefc->tamura_entity_selling_price_factory1 = empty((double)$row->TamuraEntitySellingPrice_Factory1->__toString()) ? null : (double)$row->TamuraEntitySellingPrice_Factory1->__toString();
                $rfqefc->tamura_entity_id_sales_office1 = empty((int)$row->TamuraEntityId_SalesOffice1->__toString()) ? null : (int)$row->TamuraEntityId_SalesOffice1->__toString();
                $rfqefc->tamura_entity_profit_sales_office1 = empty((double)$row->TamuraEntityProfit_SalesOffice1->__toString()) ? null : (double)$row->TamuraEntityProfit_SalesOffice1->__toString();
                $rfqefc->tamura_entity_profit_value_sales_office1 = empty((double)$row->TamuraEntityProfitValue_SalesOffice1->__toString()) ? null : (double)$row->TamuraEntityProfitValue_SalesOffice1->__toString();
                $rfqefc->tamura_entity_selling_price_sales_office1 = empty((double)$row->TamuraEntitySellingPrice_SalesOffice1->__toString()) ? null : (double)$row->TamuraEntitySellingPrice_SalesOffice1->__toString();
                $rfqefc->tamura_entity_id_factory2 = empty((int)$row->TamuraEntityId_Factory2->__toString()) ? null : (int)$row->TamuraEntityId_Factory2->__toString();
                $rfqefc->tamura_entity_currency_factory2 = empty((int)$row->TamuraEntityCurrency_Factory2->__toString()) ? null : (int)$row->TamuraEntityCurrency_Factory2->__toString();
                $rfqefc->rfq_material_factory2 = empty((double)$row->RFQ_Material_Factory2->__toString()) ? null : (double)$row->RFQ_Material_Factory2->__toString();
                $rfqefc->tamura_entity_profit_factory2 = empty((double)$row->TamuraEntityProfit_Factory2->__toString()) ? null : (double)$row->TamuraEntityProfit_Factory2->__toString();
                $rfqefc->tamura_entity_profit_value_factory2 = empty((double)$row->TamuraEntityProfitValue_Factory2->__toString()) ? null : (double)$row->TamuraEntityProfitValue_Factory2->__toString();
                $rfqefc->tamura_entity_labour_rate_choice_factory2 = empty((double)$row->TamuraEntityLabourRateChoice_Factory2->__toString()) ? null : (double)$row->TamuraEntityLabourRateChoice_Factory2->__toString();
                $rfqefc->tamura_entity_labour_time_value_factory2 = empty((double)$row->TamuraEntityLabourTimeValue_Factory2->__toString()) ? null : (double)$row->TamuraEntityLabourTimeValue_Factory2->__toString();
                $rfqefc->tamura_entity_labour_rate_factory2 = empty((double)$row->TamuraEntityLabourRate_Factory2->__toString()) ? null : (double)$row->TamuraEntityLabourRate_Factory2->__toString();
                $rfqefc->tamura_entity_labour_rate_value_factory2 = empty((double)$row->TamuraEntityLabourRateValue_Factory2->__toString()) ? null : (double)$row->TamuraEntityLabourRateValue_Factory2->__toString();
                $rfqefc->tamura_entity_freight_factory2 = empty((double)$row->TamuraEntityFreight_Factory2->__toString()) ? null : (double)$row->TamuraEntityFreight_Factory2->__toString();
                $rfqefc->tamura_entity_freight_value_factory2 = empty((double)$row->TamuraEntityFreightValue_Factory2->__toString()) ? null : (double)$row->TamuraEntityFreightValue_Factory2->__toString();
                $rfqefc->tamura_entity_admin_factory2 = empty((double)$row->TamuraEntityAdmin_Factory2->__toString()) ? null : (double)$row->TamuraEntityAdmin_Factory2->__toString();
                $rfqefc->tamura_entity_admin_value_factory2 = empty((double)$row->TamuraEntityAdminValue_Factory2->__toString()) ? null : (double)$row->TamuraEntityAdminValue_Factory2->__toString();
                $rfqefc->tamura_entity_selling_price_factory2 = empty((double)$row->TamuraEntitySellingPrice_Factory2->__toString()) ? null : (double)$row->TamuraEntitySellingPrice_Factory2->__toString();
                $rfqefc->tamura_entity_id_sales_office2 = empty((int)$row->TamuraEntityId_SalesOffice2->__toString()) ? null : (int)$row->TamuraEntityId_SalesOffice2->__toString();
                $rfqefc->tamura_entity_profit_sales_office2 = empty((double)$row->TamuraEntityProfit_SalesOffice2->__toString()) ? null : (double)$row->TamuraEntityProfit_SalesOffice2->__toString();
                $rfqefc->tamura_entity_profit_value_sales_office2 = empty((double)$row->TamuraEntityProfitValue_SalesOffice2->__toString()) ? null : (double)$row->TamuraEntityProfitValue_SalesOffice2->__toString();
                $rfqefc->tamura_entity_selling_price_sales_office2 = empty((double)$row->TamuraEntitySellingPrice_SalesOffice2->__toString()) ? null : (double)$row->TamuraEntitySellingPrice_SalesOffice2->__toString();
                $rfqefc->tamura_entity_id_factory3 = empty((int)$row->TamuraEntityId_Factory3->__toString()) ? null : (int)$row->TamuraEntityId_Factory3->__toString();
                $rfqefc->tamura_entity_currency_factory3 = empty((int)$row->TamuraEntityCurrency_Factory3->__toString()) ? null : (int)$row->TamuraEntityCurrency_Factory3->__toString();
                $rfqefc->rfq_material_factory3 = empty((double)$row->RFQ_Material_Factory3->__toString()) ? null : (double)$row->RFQ_Material_Factory3->__toString();
                $rfqefc->tamura_entity_profit_factory3 = empty((double)$row->TamuraEntityProfit_Factory3->__toString()) ? null : (double)$row->TamuraEntityProfit_Factory3->__toString();
                $rfqefc->tamura_entity_profit_value_factory3 = empty((double)$row->TamuraEntityProfitValue_Factory3->__toString()) ? null : (double)$row->TamuraEntityProfitValue_Factory3->__toString();
                $rfqefc->tamura_entity_labour_rate_choice_factory3 = empty((double)$row->TamuraEntityLabourRateChoice_Factory3->__toString()) ? null : (double)$row->TamuraEntityLabourRateChoice_Factory3->__toString();
                $rfqefc->tamura_entity_labour_time_value_factory3 = empty((double)$row->TamuraEntityLabourTimeValue_Factory3->__toString()) ? null : (double)$row->TamuraEntityLabourTimeValue_Factory3->__toString();
                $rfqefc->tamura_entity_labour_rate_factory3 = empty((double)$row->TamuraEntityLabourRate_Factory3->__toString()) ? null : (double)$row->TamuraEntityLabourRate_Factory3->__toString();
                $rfqefc->tamura_entity_labour_rate_value_factory3 = empty((double)$row->TamuraEntityLabourRateValue_Factory3->__toString()) ? null : (double)$row->TamuraEntityLabourRateValue_Factory3->__toString();
                $rfqefc->tamura_entity_freight_factory3 = empty((double)$row->TamuraEntityFreight_Factory3->__toString()) ? null : (double)$row->TamuraEntityFreight_Factory3->__toString();
                $rfqefc->tamura_entity_freight_value_factory3 = empty((double)$row->TamuraEntityFreightValue_Factory3->__toString()) ? null : (double)$row->TamuraEntityFreightValue_Factory3->__toString();
                $rfqefc->tamura_entity_admin_factory3 = empty((double)$row->TamuraEntityAdmin_Factory3->__toString()) ? null : (double)$row->TamuraEntityAdmin_Factory3->__toString();
                $rfqefc->tamura_entity_admin_value_factory3 = empty((double)$row->TamuraEntityAdminValue_Factory3->__toString()) ? null : (double)$row->TamuraEntityAdminValue_Factory3->__toString();
                $rfqefc->tamura_entity_selling_price_factory3 = empty((double)$row->TamuraEntitySellingPrice_Factory3->__toString()) ? null : (double)$row->TamuraEntitySellingPrice_Factory3->__toString();
                $rfqefc->tamura_entity_id_sales_office3 = empty((int)$row->TamuraEntityId_SalesOffice3->__toString()) ? null : (int)$row->TamuraEntityId_SalesOffice3->__toString();
                $rfqefc->tamura_entity_profit_sales_office3 = empty((double)$row->TamuraEntityProfit_SalesOffice3->__toString()) ? null : (double)$row->TamuraEntityProfit_SalesOffice3->__toString();
                $rfqefc->tamura_entity_profit_value_sales_office3 = empty((double)$row->TamuraEntityProfitValue_SalesOffice3->__toString()) ? null : (double)$row->TamuraEntityProfitValue_SalesOffice3->__toString();
                $rfqefc->tamura_entity_selling_price_sales_office3 = empty((double)$row->TamuraEntitySellingPrice_SalesOffice3->__toString()) ? null : (double)$row->TamuraEntitySellingPrice_SalesOffice3->__toString();             
                $rfqefc->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQEstimatedFactoryCost.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfqEstimatedFactoryCost", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfqExportError($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfq_export_errors')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->RFQ_ExportErrors as $row){      
                $rfqe = new RFQExportError();
                $rfqe->error = $row->Error->__toString();
                $rfqe->field = $row->Field->__toString();
                $rfqe->row = empty((int)$row->Field->__toString()) ? null : (int)$row->Field->__toString();
                $rfqe->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQExportError.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfqExportError", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfqMat($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfq_mats')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->RFQ_MAT as $row){      
                $rfqm = new RFQMat();
                $rfqm->id = (int)$row->RFQ_MATId->__toString();
                $rfqm->rfq_id = empty((int)$row->RFQId->__toString()) ? null : (int)$row->RFQId->__toString();
                $rfqm->currency_id = empty((int)$row->RFQ_MAT_CurrencyId->__toString()) ? null : (int)$row->RFQ_MAT_CurrencyId->__toString();
                $rfqm->type = $row->RFQ_MAT_Type->__toString();
                $rfqm->part_no = $row->RFQ_MAT_PartNo->__toString();
                $rfqm->description = $row->RFQ_MAT_Description->__toString();
                $rfqm->qty = empty((int)$row->RFQ_MAT_Qty->__toString()) ? null : (int)$row->RFQ_MAT_Qty->__toString();
                $rfqm->unitary_price = empty((double)$row->RFQ_MAT_UnitaryPrice->__toString()) ? null : (double)$row->RFQ_MAT_UnitaryPrice->__toString();
                $rfqm->total_price = empty((double)$row->RFQ_MAT_TotalPrice->__toString()) ? null : (double)$row->RFQ_MAT_TotalPrice->__toString();
                $rfqm->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQMat.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfqMat", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfqDeadReason($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfq_dead_reasons')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->RFQDeadReason as $row){      
                $rfqdr = new RFQDeadReason();
                $rfqdr->id = (int)$row->RFQ_DeadReasonId->__toString();
                $rfqdr->reason = $row->Reason->__toString();
                $rfqdr->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQDeadReasion.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfqDeadReason", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfqSpecification($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfq_specifications')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->RFQSpecification as $row){      
                $rfqs = new RFQSpecification();
                $rfqs->id = (int)$row->RFQSpecificationId->__toString();
                $rfqs->rfq_id = empty((int)$row->RFQId->__toString()) ? null : (int)$row->RFQId->__toString();
                $rfqs->label = $row->RFQSpecificationLabel->__toString();
                $rfqs->version = $row->RFQSpecificationVersion->__toString();
                $rfqs->link = $row->RFQSpecificationLink->__toString();
                $rfqs->status = $row->RFQSpecificationStatus->__toString() == "1" ? true : false;
                $rfqs->master = $row->RFQSpecificationMaster->__toString() == "1" ? true : false;
                $rfqs->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQSpecification.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfqSpecification", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importRfqStatusLog($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('rfq_status_logs')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->RFQStatusLog as $row){      
                $rfqs = new RFQStatusLog();
                $rfqs->id = (int)$row->RFQStatusLogId->__toString();
                $rfqs->rfq_id = empty((int)$row->RFQId->__toString()) ? null : (int)$row->RFQId->__toString();
                $rfqs->status = $row->RFQStatus->__toString();
                $rfqs->action = $row->RFQStatusAction->__toString();
                $rfqs->who = $row->RFQStatusWho->__toString();
                $rfqs->when = empty($row->RFQStatusWhen) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->RFQStatusWhen)));
                $rfqs->dead_reason = $row->RFQStatusDeadReason->__toString();
                $rfqs->save();            
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported RFQSpecification.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importRfqSpecification", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importSale($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('sales')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->Sales as $row){      
                $sale = new Sale();
                $sale->id = (int)$row->SalesId->__toString();
                $sale->npi_id = empty((int)$row->NPIId->__toString()) ? null : (int)$row->NPIId->__toString();
                $sale->valid = $row->Sales_Valid->__toString() == "1" ? true : false;
                $sale->valid_last_update_date = empty($row->Sales_Valid_LastUpdateDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->Sales_Valid_LastUpdateDate)));
                $sale->currency_id = empty((int)$row->Sales_CurrencyId->__toString()) ? null : (int)$row->Sales_CurrencyId->__toString();
                $sale->sales_manager_id = empty((int)$row->Sales_SalesManagerId->__toString()) ? null : (int)$row->Sales_SalesManagerId->__toString();
                $sale->tamura_entity_id_sales_office = empty((int)$row->Sales_TamuraEntityId_SalesOffice->__toString()) ? null : (int)$row->Sales_TamuraEntityId_SalesOffice->__toString();
                $sale->buy_price_engineering = empty((double)$row->Sales_BuyPrice_Engineering->__toString()) ? null : (double)$row->Sales_BuyPrice_Engineering->__toString();             
                $sale->buy_price_factory = empty((double)$row->Sales_BuyPrice_Factory->__toString()) ? null : (double)$row->Sales_BuyPrice_Factory->__toString();             
                $sale->buy_price = empty((double)$row->Sales_BuyPrice->__toString()) ? null : (double)$row->Sales_BuyPrice->__toString();             
                $sale->tamura_entity_fob_charge = empty((double)$row->Sales_TamuraEntity_FOBCharge->__toString()) ? null : (double)$row->Sales_TamuraEntity_FOBCharge->__toString();             
                $sale->tamura_entity_fob_charge_value = empty((double)$row->Sales_TamuraEntity_FOBChargeValue->__toString()) ? null : (double)$row->Sales_TamuraEntity_FOBChargeValue->__toString();             
                $sale->tamura_entity_freight = empty((double)$row->Sales_TamuraEntity_Freight->__toString()) ? null : (double)$row->Sales_TamuraEntity_Freight->__toString();             
                $sale->tamura_entity_freight_value = empty((double)$row->Sales_TamuraEntity_FreightValue->__toString()) ? null : (double)$row->Sales_TamuraEntity_FreightValue->__toString();             
                $sale->cif_cost = empty((double)$row->Sales_CIF_Cost->__toString()) ? null : (double)$row->Sales_CIF_Cost->__toString();             
                $sale->tamura_entity_terminal_handling = empty((double)$row->Sales_TamuraEntity_TerminalHandling->__toString()) ? null : (double)$row->Sales_TamuraEntity_TerminalHandling->__toString();             
                $sale->tamura_entity_terminal_handling_value = empty((double)$row->Sales_TamuraEntity_TerminalHandlingValue->__toString()) ? null : (double)$row->Sales_TamuraEntity_TerminalHandlingValue->__toString();             
                $sale->tamura_entity_delivery_order = empty((double)$row->Sales_TamuraEntity_DeliveryOrder->__toString()) ? null : (double)$row->Sales_TamuraEntity_DeliveryOrder->__toString();             
                $sale->tamura_entity_delivery_order_value = empty((double)$row->Sales_TamuraEntity_DeliveryOrderValue->__toString()) ? null : (double)$row->Sales_TamuraEntity_DeliveryOrderValue->__toString();             
                $sale->tamura_entity_customs_clearance = empty((double)$row->Sales_TamuraEntity_CustomsClearance->__toString()) ? null : (double)$row->Sales_TamuraEntity_TerminalHandling->__toString();             
                $sale->tamura_entity_customs_clearance_value = empty((double)$row->Sales_TamuraEntity_CustomsClearanceValue->__toString()) ? null : (double)$row->Sales_TamuraEntity_TerminalHandlingValue->__toString();             
                $sale->advance_fees = empty((double)$row->Sales_AdvanceFees->__toString()) ? null : (double)$row->Sales_AdvanceFees->__toString();             
                $sale->advance_fees_value = empty((double)$row->Sales_AdvanceFeesValue->__toString()) ? null : (double)$row->Sales_AdvanceFeesValue->__toString();             
                $sale->lfr = empty((double)$row->Sales_TamuraEntity_LFR->__toString()) ? null : (double)$row->Sales_LFR->__toString();             
                $sale->lfr_value = empty((double)$row->Sales_TamuraEntity_LFRValue->__toString()) ? null : (double)$row->Sales_LFRValue->__toString();             
                $sale->tamura_entity_haulage_to_warehouse = empty((double)$row->Sales_TamuraEntity_HaulageToWarehouse->__toString()) ? null : (double)$row->Sales_TamuraEntity_HaulageToWarehouse->__toString();             
                $sale->tamura_entity_haulage_to_warehouse_value = empty((double)$row->Sales_TamuraEntity_HaulageToWarehouseValue->__toString()) ? null : (double)$row->Sales_TamuraEntity_HaulageToWarehouseValue->__toString();             
                $sale->total_inland_charges_value = empty((double)$row->Sales_TotalInlandChargesValue->__toString()) ? null : (double)$row->Sales_TotalInlandChargesValue->__toString();             
                $sale->unloading = empty((double)$row->Sales_Unloading->__toString()) ? null : (double)$row->Sales_Unloading->__toString();             
                $sale->unloading_value = empty((double)$row->Sales_UnloadingValue->__toString()) ? null : (double)$row->Sales_UnloadingValue->__toString();             
                $sale->order_picking = empty((double)$row->Sales_OrderPicking->__toString()) ? null : (double)$row->Sales_OrderPicking->__toString();             
                $sale->order_picking_value = empty((double)$row->Sales_OrderPickingValue->__toString()) ? null : (double)$row->Sales_OrderPickingValue->__toString();             
                $sale->storage = empty((double)$row->Sales_Storage->__toString()) ? null : (double)$row->Sales_Storage->__toString();             
                $sale->storage_value = empty((double)$row->Sales_StorageValue->__toString()) ? null : (double)$row->Sales_StorageValue->__toString();             
                $sale->admin = empty((double)$row->Sales_Admin->__toString()) ? null : (double)$row->Sales_Admin->__toString();             
                $sale->admin_value = empty((double)$row->Sales_AdminValue->__toString()) ? null : (double)$row->Sales_AdminValue->__toString();             
                $sale->total_hub_costs = empty((double)$row->Sales_TotalHubCosts->__toString()) ? null : (double)$row->Sales_TotalHubCosts->__toString();             
                $sale->total_oversea_costs = empty((double)$row->Sales_TotalOverseaCosts->__toString()) ? null : (double)$row->Sales_TotalOverseaCosts->__toString();             
                $sale->total_oversea_costs_value = empty((double)$row->Sales_TotalOverseaCostsValue->__toString()) ? null : (double)$row->Sales_TotalOverseaCostsValue->__toString();             
                $sale->total_oversea_cost_flag = $row->Sales_TotalOverseaCostFlag->__toString() == "1" ? true : false;
                $sale->import_duty = empty((double)$row->Sales_ImportDuty->__toString()) ? null : (double)$row->Sales_ImportDuty->__toString();             
                $sale->import_duty_value = empty((double)$row->Sales_ImportDutyValue->__toString()) ? null : (double)$row->Sales_ImportDutyValue->__toString();             
                $sale->delivery_to_customer = empty((double)$row->Sales_DeliveryToCustomer->__toString()) ? null : (double)$row->Sales_DeliveryToCustomer->__toString();             
                $sale->delivery_to_customer_value = empty((double)$row->Sales_DeliveryToCustomerValue->__toString()) ? null : (double)$row->Sales_DeliveryToCustomerValue->__toString();             
                $sale->cost_price = empty((double)$row->Sales_CostPrice->__toString()) ? null : (double)$row->Sales_CostPrice->__toString();             
                $sale->cost_price_percentage = empty((double)$row->Sales_CostPricePercentage->__toString()) ? null : (double)$row->Sales_CostPricePercentage->__toString();             
                $sale->gand_a = empty((double)$row->Sales_GandA->__toString()) ? null : (double)$row->Sales_GandA->__toString();             
                $sale->gand_a_value = empty((double)$row->Sales_GandAValue->__toString()) ? null : (double)$row->Sales_GandAValue->__toString();             
                $sale->total_cost = empty((double)$row->Sales_TotalCost->__toString()) ? null : (double)$row->Sales_TotalCost->__toString();             
                $sale->total_cost_percentage = empty((double)$row->Sales_TotalCostPercentage->__toString()) ? null : (double)$row->Sales_TotalCostPercentage->__toString();             
                $sale->profit = empty((double)$row->Sales_Profit->__toString()) ? null : (double)$row->Sales_Profit->__toString();             
                $sale->profit_value = empty((double)$row->Sales_ProfitValue->__toString()) ? null : (double)$row->Sales_ProfitValue->__toString();                        
                $sale->selling_price = empty((double)$row->Sales_SellingPrice->__toString()) ? null : (double)$row->Sales_SellingPrice->__toString();             
                $sale->comment = $row->Sales_Comment->__toString();
                $sale->special_req = $row->Sales_SpecialReq->__toString();
                $sale->incoterms_id = $row->Sales_IncotermsId->__toString();
                $sale->creation_date = empty($row->Sales_CreationDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->Sales_CreationDate)));
                $sale->last_update_date = empty($row->Sales_LastUpdateDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->Sales_LastUpdateDate)));
                $sale->created_by = $row->Sales_CreatedBy->__toString();
                $sale->pallet_qty = empty((int)$row->Sales_Pallet_Qty->__toString()) ? null : (int)$row->Sales_Pallet_Qty->__toString();
                $sale->customer_id = empty((int)$row->Sales_CustomerId->__toString()) ? null : (int)$row->Sales_CustomerId->__toString();
                $sale->customer_payment_term_nr = $row->Sales_CustomerPaymentTermNr->__toString();
                $sale->customer_payment_term_type = $row->Sales_CustomerPaymentTermType->__toString();
                $sale->purchaser_id = empty((int)$row->Sales_PurchaserId->__toString()) ? null : (int)$row->Sales_PurchaserId->__toString();
                $sale->fcl_lcl = $row->Sales_FCL_LCL->__toString();
                $sale->lme_cu = empty((double)$row->Sales_LME_CU->__toString()) ? null : (double)$row->Sales_LME_CU->__toString();             
                $sale->lme_cu_at_creation = empty((double)$row->Sales_LME_CU_AtCreation->__toString()) ? null : (double)$row->Sales_LME_CU_AtCreation->__toString();      
                $sale->lme_al = empty((double)$row->Sales_LME_CU->__toString()) ? null : (double)$row->Sales_LME_AL->__toString();             
                $sale->lme_al_at_creation = empty((double)$row->Sales_LME_CU_AtCreation->__toString()) ? null : (double)$row->Sales_LME_AL_AtCreation->__toString();      
                $sale->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported Sale.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importSale", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importSearchParam($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('search_params')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->SearchParam as $row){      
                $sp = new SearchParam();
                $sp->id = (int)$row->SearchParamId->__toString();
                $sp->person_login = $row->searchPersonLogin->__toString();
                $sp->person_name = $row->searchPersoName->__toString();
                $sp->project_id = empty ((int)$row->searchProjectId->__toString()) ? null : (int)$row->searchProjectId->__toString();
                $sp->project_status_init = $row->searchProjectStatusINIT->__toString() == "1" ? true : false;
                $sp->project_status_opened = $row->searchProjectStatusOPENED->__toString() == "1" ? true : false;
                $sp->project_status_closed = $row->searchProjectStatusCLOSED->__toString() == "1" ? true : false;
                $sp->project_manager_id = empty ((int)$row->searchProjectManagerId->__toString()) ? null : (int)$row->searchProjectManagerId->__toString();
                $sp->key_account_manager_id = empty ((int)$row->searchKeyAccountManagerId->__toString()) ? null : (int)$row->searchKeyAccountManagerId->__toString();
                $sp->project_customer_id = empty ((int)$row->searchProjectCustomerId->__toString()) ? null : (int)$row->searchProjectCustomerId->__toString();
                $sp->project_include_all_group = $row->searchProjectIncludeAllGroup->__toString() == "1" ? true : false;
                $sp->project_purchaser_id = empty ((int)$row->searchProjectPurchaserId->__toString()) ? null : (int)$row->searchProjectPurchaserId->__toString();
                $sp->project_market_id = $row->searchProjectMarketId->__toString();
                $sp->project_creation_date_from = empty($row->searchProjectCreationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchProjectCreationDateFrom)));
                $sp->project_creation_date_to = empty($row->searchProjectCreationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchProjectCreationDateTo)));
                $sp->project_modification_date_from = empty($row->searchProjectModificationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchProjectModificationDateFrom)));
                $sp->project_modification_date_to = empty($row->searchProjectModificationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchProjectModificationDateTo)));
                $sp->rfq_id = empty ((int)$row->searchRFQId->__toString()) ? null : (int)$row->searchRFQId->__toString();
                $sp->rfq_status_init = $row->searchRFQStatusINIT->__toString() == "1" ? true : false;
                $sp->rfq_status_t10 = $row->searchRFQStatusT10->__toString() == "1" ? true : false;
                $sp->rfq_status_t20 = $row->searchRFQStatusT20->__toString() == "1" ? true : false;
                $sp->rfq_status_t30 = $row->searchRFQStatusT30->__toString() == "1" ? true : false;
                $sp->rfq_status_dead = $row->searchRFQStatusDEAD->__toString() == "1" ? true : false;
                $sp->rfq_status_rej = $row->searchRFQStatusREJ->__toString() == "1" ? true : false;
                $sp->rfq_customer_pn = $row->searchRFQCustomerPN->__toString();
                $sp->rfq_eng_action_design_required = $row->searchRFQEngActionDesignRequired->__toString() == "1" ? true : false;
                $sp->rfq_eng_action_costing_complete = $row->searchRFQEngActionCostingComplete->__toString() == "1" ? true : false;
                $sp->rfq_eng_action_design_complete = $row->searchRFQEngActionDesignComplete->__toString() == "1" ? true : false;
                $sp->rfq_eng_action_mass_prod_auth = $row->searchRFQEngActionMassProdAuth->__toString() == "1" ? true : false;
                $sp->rfq_dead_reason_id = empty ((int)$row->searchRFQDeadReasonId->__toString()) ? null : (int)$row->searchRFQDeadReasonId->__toString();
                $sp->rfq_pn_category = $row->searchRFQPNCategory->__toString();
                $sp->rfq_pn_format = $row->searchRFQPNFormat->__toString();
                $sp->rfq_pn_new_dev = empty ((int)$row->searchRFQPnNewDev->__toString()) ? null : (int)$row->searchRFQPNNewDev->__toString();
                $sp->rfq_pn_new_biz = empty ((int)$row->searchRFQPnNewBiz->__toString()) ? null : (int)$row->searchRFQPNNewBiz->__toString();
                $sp->rfq_priority = $row->searchRFQPriority->__toString();
                $sp->rfq_customer_engineer_id = empty ((int)$row->searchRFQCustomerEngineerId->__toString()) ? null : (int)$row->searchRFQCustomerEngineerId->__toString();
                $sp->rfq_tamura_elec_engineer_id = empty ((int)$row->searchRFQTamuraElecEngineerId->__toString()) ? null : (int)$row->searchRFQTamuraElecEngineerId->__toString();
                $sp->rfq_tamura_mec_engineer_id = empty ((int)$row->searchRFQTamuraMecEngineerId->__toString()) ? null : (int)$row->searchRFQTamuraMecEngineerId->__toString();
                $sp->rfq_product_id = $row->searchRFQProductId->__toString();             
                $sp->rfq_creation_date_from = empty($row->searchRFQCreationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchRFQCreationDateFrom)));
                $sp->rfq_creation_date_to = empty($row->searchRFQCreationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchRFQCreationDateTo)));
                $sp->rfq_modification_date_from = empty($row->searchRFQModificationDateFrom) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchRFQModificationDateFrom)));
                $sp->rfq_modification_date_to = empty($row->searchRFQModificationDateTo) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->searchRFQModificationDateTo)));               
                $sp->sales_valid = $row->searchSalesValid->__toString() == "1" ? true : false;
                $sp->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported SearchParam.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importSearchParam", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importStandardItem($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('standard_items')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->StandardItems as $row){      
                $si = new StandardItem();
                $si->id = (int)$row->StandardItemID->__toString();
                $si->category = $row->Category->__toString();
                $si->design_office = $row->DesignOffice->__toString();
                $si->factory = $row->Factory->__toString();
                $si->sales_office = $row->SalesOffice->__toString();
                $si->tam_id = empty ((double)$row->TAMID->__toString()) ? null : (double)$row->TAMID->__toString();
                $si->part_number = $row->PartNumber->__toString();
                $si->description = $row->Description->__toString();
                $si->box_quantity = empty ((double)$row->BoxQuantity->__toString()) ? null : (double)$row->BoxQuantity->__toString();
                $si->carton_quantity = empty ((double)$row->CartonQuantity->__toString()) ? null : (double)$row->CartonQuantity->__toString();
                $si->pallet_quantity = empty ((double)$row->PalletQuantity->__toString()) ? null : (double)$row->PalletQuantity->__toString();
                $si->unit_weight = empty ((double)$row->UnitWeight->__toString()) ? null : (double)$row->UnitWeight->__toString();
                $si->currency = $row->Currency->__toString();
                $si->buy_price_fca_hk = empty ((double)$row->BuyPriceFCA_HK->__toString()) ? null : (double)$row->BuyPriceFCA_HK->__toString();              
                $si->comment = $row->Comment->__toString();
                $si->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported StandardItem.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importStandardItem", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importTamuraCodification($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('tamura_codifications')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->TamuraCodification as $row){      
                $tc = new TamuraCodification();
                $tc->id = (int)$row->CodeID->__toString();
                $tc->type_entity = empty ((double)$row->TypeEntity->__toString()) ? null : (double)$row->TypeEntity->__toString();           
                $tc->no_entity = $row->NoEntity->__toString();
                $tc->entity = $row->ENTITY->__toString();
                $tc->no_entity_s = $row->NoEntityS->__toString();
                $tc->code_list = $row->CodeList->__toString();
                $tc->entity_path = $row->EntityPath->__toString();               
                $tc->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported TamuraCodification.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importTamuraCodification", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importTamuraEntity($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('tamura_entities')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->TamuraEntity as $row){      
                $te = new TamuraEntity();
                $te->id = (int)$row->TamuraEntityId->__toString();
                $te->name = $row->TamuraEntityName->__toString();      
                $te->short_name = $row->TamuraEntityShortName->__toString();
                $te->location = $row->TamuraEntityLocation->__toString();
                $te->type = empty ((int)$row->TamuraEntityType->__toString()) ? null : (int)$row->TamuraEntityType->__toString();
                $te->deactivated = $row->TamuraEntityDeactivated->__toString() == "1" ? true : false;
                $te->labour_rate = empty ((double)$row->TamuraEntityLabourRate->__toString()) ? null : (double)$row->TamuraEntityLabourRate->__toString();
                $te->freight = empty ((double)$row->TamuraEntityFreight->__toString()) ? null : (double)$row->TamuraEntityFreight->__toString();
                $te->admin = empty ((double)$row->TamuraEntityAdmin->__toString()) ? null : (double)$row->TamuraEntityAdmin->__toString();
                $te->profit = empty ((double)$row->TamuraEntityProfit->__toString()) ? null : (double)$row->TamuraEntityProfit->__toString();
                $te->linked_sold = empty ((int)$row->TamuraEntityLinkedSOld->__toString()) ? null : (int)$row->TamuraEntityLinkedSOld->__toString();
                $te->currency_id = empty ((int)$row->TamuraEntityCurrencyId->__toString()) ? null : (int)$row->TamuraEntityCurrencyId->__toString();
                $te->mail = $row->TamuraEntityMail->__toString();
                $te->fob_charge = empty ((double)$row->TamuraEntityFOBCharge->__toString()) ? null : (double)$row->TamuraEntityFOBCharge->__toString();
                $te->terminal_handling = empty ((double)$row->TamuraEntityTerminalHandling->__toString()) ? null : (double)$row->TamuraEntityTerminalHandling->__toString();
                $te->delivery_order = empty ((double)$row->TamuraEntityDeliveryOrder->__toString()) ? null : (double)$row->TamuraEntityDeliveryOrder->__toString();
                $te->customs_clearance = empty ((double)$row->TamuraEntityCustomsClearance->__toString()) ? null : (double)$row->TamuraEntityCustomsClearance->__toString();
                $te->haulage_to_warehouse = empty ((double)$row->TamuraEntityHaulageToWarehouse->__toString()) ? null : (double)$row->TamuraEntityHaulageToWarehouse->__toString();
                $te->manufactured_purchased = empty ((int)$row->TamuraEntityManufacturedPurchased->__toString()) ? null : (double)$row->TamuraEntityManufacturedPurchased->__toString();
                $te->fcl_lcl = $row->TamuraEntity_FCL_LCL->__toString();
                $te->teu = $row->TamuraEntity_TEU->__toString() == "1" ? true : false;
                $te->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported TamuraEntity.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importTamuraEntity", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function importTimeSheet($sheet)
    {
        try{
            if(empty($sheet)){
                return $this->resultService->Error("Empty import file.");
            }
            
            DB::beginTransaction();
            
            DB::table('time_sheets')->truncate();
            
            $xml = new SimpleXMLElement($sheet, null, true);
            foreach($xml->TimeSheet as $row){      
                $ts = new TimeSheet();
                $ts->id = (int)$row->TimeSheetId->__toString();
                $ts->rfq_id = empty ((int)$row->RFQId->__toString()) ? null : (int)$row->RFQId->__toString();
                $ts->created_by = $row->TimeSheet_CreatedBy->__toString();
                $ts->creation_date = empty($row->TimeSheet_CreationDate) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->TimeSheet_CreationDate)));
                $ts->tamura_engineer_id = empty ((int)$row->TimeSheet_TamuraEngineerId->__toString()) ? null : (int)$row->TimeSheet_TamuraEngineerId->__toString();
                $ts->date = empty($row->TimeSheet_Date) ? null : date('Y-m-d H:i:s', strtotime(str_replace("T", " ", $row->TimeSheet_Date)));
                $ts->hours = empty ((int)$row->TimeSheet_Hours->__toString()) ? null : (int)$row->TimeSheet_Hours->__toString();
                $ts->action = $row->TimeSheet_Action->__toString();
                $ts->invoice_number = $row->TimeSheet_InvoiceNumber->__toString();
                $ts->save();
            }
            
            DB::commit();
            
            return $this->resultService->Success("Imported TimeSheet.");
            
        }catch(Exception $e){
            DB::rollBack();
            $this->logService->log("ERROR ImportService->importTimeSheet", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
}
