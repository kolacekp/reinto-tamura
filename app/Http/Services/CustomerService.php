<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Customer;
use App\Person;
use App\Project;

use DB;

class CustomerService
{

    const CUSTOMER_TYPE_GROUP = 'group';
    const CUSTOMER_TYPE_COMPANY = 'company';
    const CUSTOMER_DEACTIVATED_NO = 0;
    const CUSTOMER_DEACTIVATED_YES = 1;

    const PERSON_PURCHASER_NO = 0;
    const PERSON_PURCHASER_YES = 1;
    const PERSON_SALES_MANAGER_NO = 0;
    const PERSON_SALES_MANAGER_YES = 1;
    const PERSON_ENGINEER_NO = 0;
    const PERSON_ENGINEER_YES = 1;
    const PERSON_DEACTIVATED_NO = 0;
    const PERSON_DEACTIVATED_YES = 1;

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function getCompanies()
    {
        try {

            $companies = Customer::where('type', self::CUSTOMER_TYPE_COMPANY)
                ->where('deactivated', self::CUSTOMER_DEACTIVATED_NO)
                ->orderBy('name')
                ->get();

            return $this->resultService->Success($companies);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getCompanies", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getPurchasers($customerPath)
    {
        try {
            if (empty($customerPath)) {
                return $this->resultService->Error("Customer path cannot be empty");
            }

            $customers = Customer::select('id')->where('path', $customerPath)
                ->where('type', self::CUSTOMER_TYPE_COMPANY)
                ->get();


            $customerIds = array();
            foreach ($customers as $c) {
                $customerIds[] = $c->id;
            }

            $purchasers = Person::whereIn('customer_id', $customerIds)
                ->where('purchaser', self::PERSON_PURCHASER_YES)
                ->where('deactivated', self::PERSON_DEACTIVATED_NO)
                ->orderBy('name')
                ->get();

            return $this->resultService->Success($purchasers);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getCompanies", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getEngineers($customerId)
    {
        try {
            if (empty($customerId)) {
                return $this->resultService->Error("Customer id cannot be empty");
            }

            $engineers = Person::where('customer_id', $customerId)
                ->where('engineer', self::PERSON_ENGINEER_YES)
                ->where('deactivated', self::PERSON_DEACTIVATED_NO)
                ->orderBy('name')
                ->get();

            // dd($engineers);

            return $this->resultService->Success($engineers);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getEngineers", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getSalesManagers($customerPath)
    {
        try {
            if (empty($customerPath)) {
                return $this->resultService->Error("Customer path cannot be empty");
            }

            $customers = Customer::select('id')->where('path', $customerPath)
                ->where('type', self::CUSTOMER_TYPE_COMPANY)
                ->get();


            $customerIds = array();
            foreach ($customers as $c) {
                $customerIds[] = $c->id;
            }

            $salesManager = Person::whereIn('customer_id', $customerIds)
                ->where('sales_manager', self::PERSON_SALES_MANAGER_YES)
                ->where('deactivated', self::PERSON_DEACTIVATED_NO)
                ->orderBy('name')
                ->get();

            return $this->resultService->Success($salesManager);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getSalesManagers", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getCustomersDirectory($deactivated)
    {
        try {

            $customers = Customer::select("id", "name", "type", "id_sup")->orderBy("name");

            if (!$deactivated) {
                $customers = $customers->where('deactivated', self::CUSTOMER_DEACTIVATED_NO);
            }

            $customers = $customers->get()->toArray();

            $tree = $this->buildCustomersTree($customers);

            return $this->resultService->Success($tree);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getCustomersDirectory", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getAll($deactivated){
        try {
            $customers = Customer::orderBy("name");
            if (!$deactivated) {
                $customers = $customers->where('deactivated', self::CUSTOMER_DEACTIVATED_NO);
            }
            $customers = $customers->get();
            return $this->resultService->Success($customers);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getAll", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getCustomerDetail($customerId, $personsDeactivated)
    {
        try{
            if (empty($customerId)) {
                return $this->resultService->Error("Customer id cannot be empty");
            }

            $customer = Customer::with(['persons' => function($query) use($personsDeactivated){
                if(!$personsDeactivated){
                    $query->where('deactivated', self::PERSON_DEACTIVATED_NO);
                }
            }, 'keyAccountManager'])->where("id", $customerId)->first();

            return $this->resultService->Success($customer);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getCustomerDetail", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function update($customer)
    {

        try{
            if(empty($customer)){
                return $this->resultService->Error("Customer cannot be empty");
            }

            $id = isset($customer["id"]) ? $customer["id"] : null;
            $name = isset($customer["name"]) ? $customer["name"] : null;
            $id_root = isset($customer["id_root"]) ? $customer["id_root"] : null;
            $id_sup = isset($customer["id_sup"]) ? $customer["id_sup"] : null;
            $deactivated = isset($customer["deactivated"]) ? $customer["deactivated"] : null;
            $keyAccountManager = isset($customer["key_account_manager"]) ? $customer["key_account_manager"]["id"] : null;
            $paymentTermNr = isset($customer["payment_term_nr"]) ? $customer["payment_term_nr"] : "";
            $paymentTermType = isset($customer["payment_term_type"]) ? $customer["payment_term_type"] : "";

            $res = Customer::where("id", $id)
                ->update([
                    "name" => $name,
                    "id_root" => $id_root,
                    "id_sup" => $id_sup,
                    "deactivated" => $deactivated,
                    "account_manager_id" => $keyAccountManager,
                    "payment_term_nr" => $paymentTermNr,
                    "payment_term_type" => $paymentTermType,
                    "updated_at" => new \DateTime()
                ]);

            // update paths in children

            if($customer["type"] == self::CUSTOMER_TYPE_GROUP){

                $parent = Customer::where("id", $customer["id"])->first();
                $parentPath = empty($parent->path) ? "" : $parent->path;

                $children = Customer::where("id_sup", $customer["id"])->get();
                foreach ($children as $ch)
                {
                    Customer::where("id", $ch->id)->update(["path" => $parentPath . $parent->name . '\\' ]);

                    $child = Customer::where("id", $ch->id)->first();
                    $childPath = empty($child->path) ? "" : $child->path;

                    $childrenTwo = Customer::where("id_sup", $ch->id)->get();
                    foreach ($childrenTwo as $ch2)
                    {
                        Customer::where("id", $ch2->id)->update(["path" => $childPath . $child->name . '\\' ]);

                        $childTwo = Customer::where("id", $ch2->id)->first();
                        $childTwoPath = empty($childTwo->path) ? "" : $childTwo->path;

                        $childrenThree = Customer::where("id_sup", $ch2->id)->get();
                        foreach ($childrenThree as $ch3)
                        {
                            Customer::where("id", $ch3->id)->update(["path" => $childTwoPath . $childTwo->name . '\\' ]);
                        }
                    }
                }
            }

            // update Key manager phones
            if (!empty($customer["key_account_manager"])) {
                Person::where('id',$customer["key_account_manager"]["id"])
                    ->update([
                        "phone" => $customer["key_account_manager"]['phone'],
                        "mobile" => $customer["key_account_manager"]['mobile'],
                        "updated_at" => new \DateTime()
                    ]);
            }

            return $this->resultService->Success($res);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->update", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getGroupCustomers($customerId)
    {
        try{
            if(empty($customerId)){
                return $this->resultService->Error("Customer id cannot be empty");
            }

            $customers = array();

            $customer = Customer::where("id", $customerId)
                        ->first();

            if(!empty($customer)){
                $customers = Customer::where("id_sup", $customer->id_sup)
                                        ->where("id", "!=", $customerId)
                                        ->orderBy("name")
                                        ->get()
                                        ->toArray();
            }

            return $this->resultService->Success($customers);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->getGroupCustomers", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function addCustomerGroup($name, $keyAccountManagerId, $deactivated, $sup, $root,$paymentTerm,$paymentType)
    {
        try{
            if(empty($name)) {
                return $this->resultService->Error("Name cannot be empty");
            }

            $customer = new Customer();
            $customer->name = $name;
            $customer->address = null;
            $customer->type = self::CUSTOMER_TYPE_GROUP;
            $customer->id_sup = $sup;
            $customer->id_root = $root;
            $customer->path = null;
            $customer->deactivated = $deactivated;
            $customer->account_manager_id = $keyAccountManagerId;
            $customer->payment_term_nr = !empty($paymentTerm)? $paymentTerm : null;
            $customer->payment_term_type = !empty($paymentType)? $paymentType : null;
            $customer->created_at = new \DateTime();

            $customer->save();

            //set id_root after creating
            if($root == null) {
                $customer::where("id", $customer->id)->update(["id_root" => $customer->id]);
            }

            $customerRet = Customer::where("id", $customer->id)->first();

            return $this->resultService->Success($customerRet);

        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->addCustomerGroup", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function addCustomerCompany($name, $keyAccountManagerId, $deactivated, $sup, $root, $path, $paymentTermNr, $paymentTermType,$keyAccountManager)
    {
        try{
            if(empty($name)) {
                return $this->resultService->Error("Name cannot be empty");
            }

            $customer = new Customer();
            $customer->name = $name;
            $customer->address = null;
            $customer->type = self::CUSTOMER_TYPE_COMPANY;
            $customer->id_sup = $sup;
            $customer->id_root = $root;
            $customer->path = $path;
            $customer->deactivated = $deactivated;
            $customer->account_manager_id = $keyAccountManagerId;
            $customer->payment_term_nr = $paymentTermNr;
            $customer->payment_term_type = $paymentTermType;
            $customer->created_at = new \DateTime();

            $customer->save();

            //set id_root after creating
            if($root == null) {
                $customer::where("id", $customer->id)->update(["id_root" => $customer->id]);
            }

            // update key account manager phones
            $person = null;
            if(isset($keyAccountManager)){
                $person = Person::where('id',$keyAccountManager["id"])
                    ->update([
                        "phone" => $keyAccountManager["phone"],
                        "mobile" => $keyAccountManager["mobile"],
                        "updated_at" => new \DateTime()
                    ]);
            }

            return $this->resultService->Success($person);


        } catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->addCustomerCompany", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function deleteCustomer($customerId)
    {
        try{
            if(empty($customerId)) {
                return $this->resultService->Error("Customer id cannot be empty");
            }

            Customer::where("id_root", $customerId)->delete();
            Customer::where("id_sup", $customerId)->delete();
            Customer::where("id", $customerId)->delete();

            Person::where("customer_id", $customerId)->update(["customer_id" => null]);
            Project::where("customer_id", $customerId)->update(["customer_id" => null]);

        }catch (Exception $e) {
            $this->logService->log("ERROR CustomerService->deleteCustomer", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    /* ********************** */
    /* CUSTOM FUNCTIONS BELOW */
    /* ********************** */

    public function buildCustomersTree(array &$elements, $parentId = null)
    {

        $branch = array();

        foreach ($elements as &$element) {

            if ($element['id_sup'] == $parentId) {
                $children = $this->buildCustomersTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
                unset($element);
            }
        }
        return $branch;
    }

}
