<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\SearchParam;

use DB;

class SearchParamService
{

    private $resultService;
    private $logService;
    private $searchParam;
            
    public function __construct(ResultService $resultService, LogService $logService, SearchParam $searchParam)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
        $this->searchParam = $searchParam;
    }
    
    public function saveSearchParams($params)
    {
        try{       
            if(empty($params)){
                return $this->resultService->Error("Empty parameters.");
            }
            if(empty($params->personLogin)){
                return $this->resultService->Error("Person login cannot be empty.");
            }
            if(empty($params->personName)){
                return $this->resultService->Error("Personalization name cannot be empty.");
            }
             
            $dataArray = [
                "person_login" => $params->personLogin,
                "person_name" => $params->personName,
                "project_id" => empty($params->projectId) ? null : $params->projectId,
                "project_status_init" => isset($params->projectStatusInit) ? $params->projectStatusInit : false,
                "project_status_opened" => isset($params->projectStatusOpened) ? $params->projectStatusOpened : false,
                "project_status_closed" => isset($params->projectStatusClosed) ? $params->projectStatusClosed : false,
                "project_manager_id" => empty($params->projectManager) ? null : $params->projectManager["id"],
                "key_account_manager_id" => empty($params->keyAccountManager) ? null : $params->keyAccountManager["id"],
                "project_customer_id" => empty($params->projectCustomer) ? null : $params->projectCustomer["id"],
                "project_customer_root_id" => empty($params->projectCustomer) ? null : $params->projectCustomer["id_root"],
                "project_include_all_group" => isset($params->projectIncludeAllGroup) ? $params->projectIncludeAllGroup : false,
                "project_purchaser_id" => empty($params->projectPurchaser) ? null : $params->projectPurchaser["id"],
                "project_market_id" => empty($params->projectMarket) ? null : $params->projectMarket["id"],
                "project_market_parent_id" => empty($params->projectMarketParent) ? null : $params->projectMarketParent["id"],
                "project_creation_date_from" => empty($params->projectCreationDateFrom) ? null : date('Y-m-d H:i:s', strtotime($params->projectCreationDateFrom)),
                "project_creation_date_to" => empty($params->projectCreationDateTo) ? null : date('Y-m-d H:i:s', strtotime($params->projectCreationDateTo)),
                "project_modification_date_from" => empty($params->projectModificationDateFrom) ? null : date('Y-m-d H:i:s', strtotime($params->projectModificationDateFrom)),
                "project_modification_date_to" => empty($params->projectModificationDateTo) ? null : date('Y-m-d H:i:s', strtotime($params->projectModificationDateTo)),
                "rfq_id" => empty($params->rfqId) ? null : $params->rfqId,
                "rfq_status_init" => isset($params->rfqStatusInit) ? $params->rfqStatusInit : false,
                "rfq_status_t10" =>  isset($params->rfqStatusT10) ? $params->rfqStatusT10 : false,
                "rfq_status_t20" =>  isset($params->rfqStatusT20) ? $params->rfqStatusT20 : false,
                "rfq_status_t30" =>  isset($params->rfqStatusT30) ? $params->rfqStatusT30 : false,
                "rfq_status_dead" =>  isset($params->rfqStatusDead) ? $params->rfqStatusDead : false,
                "rfq_status_rej" =>  isset($params->rfqStatusRej) ? $params->rfqStatusRej : false,
                "rfq_customer_pn" =>  $params->rfqCustomerPn,
                "rfq_eng_action_design_required" => isset($params->rfqEngActionDesignRequired) ? $params->rfqEngActionDesignRequired : false,
                "rfq_eng_action_costing_complete" => isset($params->rfqEngActionCostingComplete) ? $params->rfqEngActionCostingComplete : false,
                "rfq_eng_action_design_complete" => isset($params->rfqEngActionDesignComplete) ? $params->rfqEngActionDesignComplete : false,
                "rfq_eng_action_mass_prod_auth" => isset($params->rfqEngActionMassProdAuth) ? $params->rfqEngActionMassProdAuth : false,
                "rfq_dead_reason_id" => empty($params->rfqDeadReason) ? null : $params->rfqDeadReason["id"],
                "rfq_pn_category" => empty($params->rfqPnCategory) ? null : $params->rfqPnCategory["id"],
                "rfq_pn_format" => empty($params->rfqPnFormat) ? null : $params->rfqPnFormat["id"],
                "rfq_pn_new_dev" => !is_null($params->rfqPnNewDev) ? ($params->rfqPnNewDev == 'newDevelopment') : null,
                "rfq_pn_new_biz" => !is_null($params->rfqPnNewBiz) ? ($params->rfqPnNewBiz == 'newBusiness') : null,
                "rfq_priority" => empty($params->rfqPriority) ? null : $params->rfqPriority["id"],
                "rfq_customer_engineer_id" => empty($params->rfqCustomerEngineer) ? null : $params->rfqCustomerEngineer["id"],
                "rfq_tamura_elec_engineer_id" => empty($params->rfqTamuraElecEngineer) ? null : $params->rfqTamuraElecEngineer["id"],
                "rfq_tamura_mec_engineer_id" => empty($params->rfqTamuraMecEngineer) ? null : $params->rfqTamuraMecEngineer["id"],
                "rfq_product_id" => empty($params->rfqProduct) ? null : $params->rfqProduct["id"],
                "rfq_product_child_one_id" => empty($params->rfqProductChildOne) ? null : $params->rfqProductChildOne["id"],
                "rfq_product_child_two_id" => empty($params->rfqProductChildTwo) ? null : $params->rfqProductChildTwo["id"],
                "rfq_product_parent_id" => empty($params->rfqProductParent) ? null : $params->rfqProductParent["id"],
                "rfq_creation_date_from" => empty($params->rfqCreationDateFrom) ? null : date('Y-m-d H:i:s', strtotime($params->rfqCreationDateFrom)),
                "rfq_creation_date_to" => empty($params->rfqCreationDateTo) ? null : date('Y-m-d H:i:s', strtotime($params->rfqCreationDateTo)),
                "rfq_modification_date_from" => empty($params->rfqModificationDateFrom) ? null : date('Y-m-d H:i:s', strtotime($params->rfqModificationDateFrom)),
                "rfq_modification_date_to" => empty($params->rfqModificationDateTo) ? null : date('Y-m-d H:i:s', strtotime($params->rfqModificationDateTo)),
                "sales_customer_id" => empty($params->salesCustomer) ? null : $params->salesCustomer["id"],
                "sales_purchase_manager_id" => empty($params->salesPurchaseManager) ? null : $params->salesPurchaseManager["id"],
                "sales_manager_id" => empty($params->salesManager) ? null : $params->salesManager["id"],
                "sales_valid" => isset($params->salesValid) ? $params->salesValid : false,
            ];
            
            $searchParam = SearchParam::where("person_login", $params->personLogin)
                                      ->where("person_name", $params->personName)
                                      ->first();
            
            if(empty($searchParam)){              
                $searchParamNew = new SearchParam();
                foreach($dataArray as $key => $value){
                    
                    $searchParamNew->{$key} = $value;
                }
                $searchParamNew->save();                           
            }else{
                SearchParam::where("person_login", $params->personLogin)
                                      ->where("person_name", $params->personName)
                                      ->update($dataArray);
            }
            
            return $this->resultService->Success("Search parameters successfuly saved.");
            
        }catch(Exception $e){
            $this->logService->log("ERROR SearchParamService->saveSearchParams", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        } 
    }
    
    public function getPersonalizations($personLogin)
    {
        try{         
            if(empty($personLogin)){
                return $this->resultService->Error("Person login cannot be empty.");
            }
            
            $personalizations = SearchParam::with($this->searchParam->getRelations())->where("person_login", $personLogin)
                                    ->orderBy("person_name")
                                    ->get();
            
            return $this->resultService->Success($personalizations);
            
        }catch(Exception $e){
            $this->logService->log("ERROR SearchParamService->getPersonalizations", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        } 
    }
    

}
