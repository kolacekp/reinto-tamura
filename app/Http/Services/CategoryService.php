<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Category;

use DB;

class CategoryService
{
    
    private $resultService;
    private $logService;
            
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }
    
    public function getCategories()
    {
        try{
            $categories = Category::orderBy("name")
                            ->get();
            return $this->resultService->Success($categories);           
        }catch(Exception $e){
            $this->logService->log("ERROR CategoryService->getCategories", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
}
