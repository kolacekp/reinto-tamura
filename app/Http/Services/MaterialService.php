<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\MAT;

use DB;

class MaterialService
{

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function getMaterials()
    {
        try{
            $materials = MAT::select(
                  "id"
                , "type"
                , "part_no"
                , "description"
                , "unitary_price")
                ->orderBy("type")
                ->get();

            return $this->resultService->Success($materials);

        }catch(Exception $e){
            $this->logService->log("ERROR MaterialService->getMaterials", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function create($type,$partNo,$description,$unitaryPrice)
    {
        try{
            $material = new MAT();
            $material->type = $type;
            $material->part_no = $partNo;
            $material->description = $description;
            $material->unitary_price = $unitaryPrice;
            $material->save();

            return $this->resultService->Success($material);

        }catch(Exception $e){
            $this->logService->log("ERROR MaterialService->create", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function update($material)
    {
        try{

            if(!empty($material)){

                $id = isset($material["id"]) ? $material["id"] : null;
                $type = isset($material["type"]) ? $material["type"] : null;
                $partNo = isset($material["part_no"]) ? $material["part_no"] : null;
                $description = isset($material["description"]) ? $material["description"] : null;
                $unitaryPrice = isset($material["unitary_price"]) ? (double)$material["unitary_price"] : null;

                $res = MAT::where("id", $id)
                    ->update([
                        "type" => $type,
                        "part_no" => $partNo,
                        "description" => $description,
                        "unitary_price" => $unitaryPrice,
                        "updated_at" => new \DateTime()
                    ]);

                return $this->resultService->Success($res);

            }

        }catch(Exception $e){
            $this->logService->log("ERROR MaterialService->update", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function delete($materialId)
    {
        try{

            if(!empty($materialId)){
                MAT::find($materialId)->delete();
            }

        }catch(Exception $e){
            $this->logService->log("ERROR MaterialService->delete", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }


}
