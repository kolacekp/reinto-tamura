<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Person;

use App\Project;
use App\User;
use DB;

class PersonService
{

    const PERSON_SALES_MANAGER_NO = 0;
    const PERSON_SALES_MANAGER_YES = 1;
    
    const CUSTOMER_ID_NULL = null;
    
    const PERSON_ENGINEER_NO = 0;
    const PERSON_ENGINEER_YES = 1;

    const BASIC_USER_ROLE_ID = 10;
    
    
    private $resultService;
    private $logService;
            
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }
    
    public function getSalesManagers()
    {
        try{
    
            $persons = Person::where('sales_manager', self::PERSON_SALES_MANAGER_YES)
                        ->orderBy("name")
                        ->get();
            
            return $this->resultService->Success($persons);
            
        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->getSalesManagers", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function getEngineers()
    {
        try{
    
            $persons = Person::where('customer_id', self::CUSTOMER_ID_NULL)
                        ->where('engineer', self::PERSON_ENGINEER_YES)
                        ->orderBy("name")
                        ->get();
            
            return $this->resultService->Success($persons);
            
        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->getSalesManagers", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }

    public function getDatabaseUsers()
    {
        try{

            $persons = Person::whereHas('tamuraEntity')
                            ->with(['tamuraEntity',
                                    'user.role'])
                            ->get();


            return $this->resultService->Success($persons);

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->getDatabaseUsers", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function createDatabaseUser($tamuraEntity, $name, $email, $login, $password, $deactivated,$role)
    {
        try{

            $person = new Person();
            $person->tamura_entity_id = $tamuraEntity;
            $person->name = $name;
            $person->email = $email;
            $person->deactivated = empty($deactivated) ? false : true;
            $person->db_login = $login;
            $person->db_password = $password;
            $person->created_at = new \DateTime();
            $person->save();

            $user = User::where("email", $email)->first();
            if(!empty($user)){
                User::where("email", $email)
                    ->update([
                        "username" => $login,
                        "password" => bcrypt($password),
                        "deactivated" => $deactivated,
                        "role_id" => !empty($role)? $role["id"] : self::BASIC_USER_ROLE_ID,
                        "updated_at" => new \DateTime()
                    ]);
            }else{
                $user = new User();
                $user->name = $name;
                $user->username = $login;
                $user->email = $email;
                $user->password = bcrypt($password);
                $user->deactivated = empty($deactivated) ? false : true;
                $user->role_id = !empty($role)? $role["id"] : self::BASIC_USER_ROLE_ID;
                $user->created_at = new \DateTime();
                $user->save();
            }

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->createDatabaseUser", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateDatabaseUserForm($id, $tamuraEntity, $name, $email, $login, $password, $deactivated,$role)
    {
        try{
            if(empty($id)){
                return $this->resultService->Error("Person Id cannot be empty");
            }

            Person::where("id", $id)
                    ->update([
                        "tamura_entity_id" => $tamuraEntity,
                        "name" => $name,
                        "email" => $email,
                        "deactivated" => empty($deactivated) ? false : true,
                        "db_login" => $login,
                        "db_password" => $password,
                        "updated_at" => new \DateTime()
                    ]);

            $user = User::where("email", $email)->first();
            if(!empty($user)){
                User::where("email", $email)
                    ->update([
                        "username" => $login,
                        "password" => bcrypt($password),
                        "deactivated" => $deactivated,
                        "role_id" => !empty($role)? $role["id"] : self::BASIC_USER_ROLE_ID,
                        "updated_at" => new \DateTime()
                    ]);
            }else{
                $user = new User();
                $user->username = $login;
                $user->name = $name;
                $user->email = $email;
                $user->password = bcrypt($password);
                $user->deactivated = empty($deactivated) ? false : true;
                $user->role_id = !empty($role)? $role["id"] : self::BASIC_USER_ROLE_ID;
                $user->created_at = new \DateTime();
                $user->save();
            }

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->updateDatabaseUserForm", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateDatabaseUser($person)
    {
        try{

            if(!empty($person)){

                $id = isset($person["id"]) ? $person["id"] : null;
                $email = isset($person["email"]) ? $person["email"] : null;
                $dbLogin = isset($person["db_login"]) ? $person["db_login"] : null;
                $dbPassword = isset($person["db_password"]) ? $person["db_password"] : null;
                $deactivated = isset($person["deactivated"]) ? $person["deactivated"] : false;

                $res = Person::where("id", $id)
                    ->update([
                        "db_login" => $dbLogin,
                        "db_password" => $dbPassword,
                        "deactivated" => $deactivated,
                        "updated_at" => new \DateTime()
                    ]);

                $res2 = User::where("email", $email)
                    ->update([
                        "username" => $dbLogin,
                        "password" => bcrypt($dbPassword),
                        "deactivated" => $deactivated,
                        "updated_at" => new \DateTime()
                    ]);

                return $this->resultService->Success($res && $res2);
            }

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->update", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateCustomerPerson($person)
    {
        try{

            if(!empty($person)){
                $id = isset($person["id"]) ? $person["id"] : null;
                $name = isset($person["name"]) ? $person["name"] : null;
                $phone = isset($person["phone"]) ? $person["phone"] : null;
                $mobile = isset($person["mobile"]) ? $person["mobile"] : null;
                $email = isset($person["email"]) ? $person["email"] : null;
                $purchaser = isset($person["purchaser"]) ? $person["purchaser"] : null;
                $engineer = isset($person["engineer"]) ? $person["engineer"] : null;
                $deactivated = isset($person["deactivated"]) ? $person["deactivated"] : null;

                $res = Person::where("id", $id)
                    ->update([
                        "name" => $name,
                        "phone" => $phone,
                        "mobile" => $mobile,
                        "email" => $email,
                        "purchaser" => $purchaser,
                        "engineer" => $engineer,
                        "deactivated" => $deactivated
                    ]);

                return $this->resultService->Success($res);
            }

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->updateCustomerPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function deleteDatabaseUser($id)
    {
        try{
            if(empty($id)){
                return $this->resultService->Error("Person Id cannot be empty");
            }

            $person = Person::where("id", $id)
                ->first();

            if(!empty($person)){
                User::where("email", $person->email)->delete();
                Person::where("id", $id)->delete();
            }

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->updateCustomerPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function changeCustomer($customerId, $personId)
    {
        try{
            if(empty($customerId)){
                return $this->resultService->Error("Customer id cannot be empty");
            }
            if(empty($personId)){
                return $this->resultService->Error("Person id cannot be empty");
            }

            $res = Person::find($personId)->update(["customer_id" => $customerId]);

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->changeCustomer", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
    public function updateDirectoryCustomerPerson($person){
        try{

            if(!empty($person)){
                $id = isset($person["personId"]) ? $person["personId"] : null;
                $name = isset($person["personName"]) ? $person["personName"] : null;
                $phone = isset($person["personPhone"]) ? $person["personPhone"] : null;
                $mobile = isset($person["personMobile"]) ? $person["personMobile"] : null;
                $email = isset($person["personEmail"]) ? $person["personEmail"] : null;
                $purchaser = isset($person["personSalesMgr"]) ? $person["personSalesMgr"] : null;
                $engineer = isset($person["personEngineer"]) ? $person["personEngineer"] : null;
                $deactivated = isset($person["personDeactivated"]) ? $person["personDeactivated"] : null;

                $res = Person::where("id", $id)
                    ->update([
                        "name" => $name,
                        "phone" => $phone,
                        "mobile" => $mobile,
                        "email" => $email,
                        "sales_manager" => $purchaser,
                        "engineer" => $engineer,
                        "deactivated" => $deactivated
                    ]);

                return $this->resultService->Success($res);
            }

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->updateDirectoryCustomerPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getPerson($id){
        try{
            if(empty($id)){
                return $this->resultService->Error("Person Id cannot be empty");
            }

            $person = Person::where("id", $id)
                ->first();

            return $this->resultService->Success($person);

        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->getPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function checkEmailExists($email)
    {
        try{
            $person = Person::where("email", $email)
                ->first();

            return $this->resultService->Success(empty($person) ? false : true);
        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->checkEmailExists", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function addPerson($person,$entity,$customerId,$isEngineer)
    {
        try{

            $newPerson = new Person();
            $newPerson->customer_id = isset($customerId)? $customerId : null;
            $newPerson->tamura_entity_id = (!empty($entity) && !isset($customerId))? $entity["id"] : null ;
            $newPerson->name = !empty($person)? $person["name"] : null;
            $newPerson->email = !empty($person)? $person["email"] : null;
            $newPerson->phone = !empty($person)? $person["phone"] : null;
            $newPerson->mobile = !empty($person)? $person["mobile"] : null;
            $newPerson->deactivated = !empty($person) ? $person["deactivated"] : false;
            $newPerson->sales_manager = !empty($person) ? $person["saleManager"] : false;
            $newPerson->purchaser = !empty($person) ? $person["purchaseManager"] : false;
            if($isEngineer)
                $newPerson->engineer = true;
            else
                $newPerson->engineer = !empty($person) ? $person["engineer"] : false;
            $newPerson->created_at = new \DateTime();
            $newPerson->save();

            return $this->resultService->Success(empty($newPerson) ? false : true);
        }catch(Exception $e){
            $this->logService->log("ERROR PersonService->addPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
    
}
