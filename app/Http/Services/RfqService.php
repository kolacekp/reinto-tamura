<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\MAT;
use App\Person;
use App\RFQ;
use App\RFQDeadReason;
use App\RFQMat;
use App\RFQProduction;
use App\RFQSpecification;
use App\RFQStatusLog;
use App\Project;

use DB;
use Faker\Provider\cs_CZ\DateTime;
use League\Flysystem\Exception;

class RfqService
{

    const TAMURA_CODIFICATION_CODE_LIST_MARKET = 'MARKET';
    const TAMURA_CODIFICATION_CODE_LIST_GTC = 'GTC';
    
    const TAMURA_CODIFICATION_TYPE_PARENT = '1';
    const TAMURA_CODIFICATION_TYPE_CHILD = '4';

    const RFQ_SPECIFICATION_MASTER_NO = 0;
    const RFQ_SPECIFICATION_MASTER_YES = 1;

    const RFQ_SPECIFICATION_STATUS_INACTIVE = 0;
    const RFQ_SPECIFICATION_STATUS_ACTIVE = 1;

    const RFQ_STATUS_INIT = "INIT";
    
    private $resultService;
    private $logService;
            
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function get($rfqId)
    {
        try{
            if(empty($rfqId)){
                return $this->resultService->Error("RFQ Id cannot be empty");
            }
            
            $rfq = RFQ::with([
                'npis',
                'category',
                'product.parent.parent.parent' => function($query){
                    $query->where("type_entity", self::TAMURA_CODIFICATION_TYPE_CHILD)
                          ->where("code_list", self::TAMURA_CODIFICATION_CODE_LIST_GTC);
                },
                'comCurrency',
                'customerEngineer',
                'project',
                'designOffice',
                'comDeliveryLocation',
                'electricalEngineer',
                'mechanicalEngineer',
                'materials',
                'specifications',
                'estimatedFactoryCost',
                'estimatedFactoryCost.factory1.country',
                'estimatedFactoryCost.currencyFactory1',
                'estimatedFactoryCost.factory2.country',
                'estimatedFactoryCost.currencyFactory2',
                'estimatedFactoryCost.factory3.country',
                'estimatedFactoryCost.currencyFactory3',
                'matCurrency',
                'statusDeadReason',
                'statusLogs'
            ])
            ->where("id", $rfqId)
            ->first();
            
            return $this->resultService->Success($rfq);
            
        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->get", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getDeadReasons()
    {
        try{
            $deadReasons = RFQDeadReason::orderBy('id')
                ->get();

            return $this->resultService->Success($deadReasons);
        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getDeadReasons", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getStatusLogs($rfqId)
    {
        try{

            if(empty($rfqId)){
                return $this->resultService->Error("RFQ Id cannot be empty");
            }

            $statusLogs = RFQStatusLog::where("rfq_id", $rfqId)
                                        ->orderBy("when", "DESC")
                                        ->get();

            return $this->resultService->Success($statusLogs);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getStatusLogs", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getSpecifications($rfqId)
    {
        try{
            if(empty($rfqId)){
                return $this->resultService->Error("RFQ Id cannot be empty");
            }

            $specifications = RFQSpecification::where("rfq_id", $rfqId)
                                                ->orderBy("created_at")
                                                ->get();

            return $this->resultService->Success($specifications);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getSpecifications", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateSpecification($specification)
    {
        try{

            if(!empty($specification)){

                // unset other rfq specifications master sign
                if($specification["master"] == true){
                    RFQSpecification::where("rfq_id", $specification["rfq_id"])
                                        ->update([
                                            "master" => self::RFQ_SPECIFICATION_MASTER_NO
                                        ]);
                }

                $id = isset($specification["id"]) ? $specification["id"] : null;
                $label = isset($specification["label"]) ? $specification["label"] : "";
                $version = isset($specification["version"]) ? $specification["version"] : "";
                $status = isset($specification["status"]) ? (int)$specification["status"] : self::RFQ_SPECIFICATION_STATUS_INACTIVE;
                $master = isset($specification["master"]) ? (int)$specification["master"] : self::RFQ_SPECIFICATION_MASTER_NO;

                $res = RFQSpecification::where("id", $id)
                        ->update([
                            "label" => $label,
                            "version" => $version,
                            "status" => $status,
                            "master" => $master,
                            "updated_at" => new \DateTime()
                        ]);

                return $this->resultService->Success($res);

            }

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->updateSpecification", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function deleteSpecification($specificationId)
    {
        try{
            if(empty($specificationId)){
                return $this->resultService->Error("Specification id cannot be empty");
            }

            $rfqSpec = RFQSpecification::where("id", $specificationId)
                                ->first();
            // try to delete rfq specification
            @unlink(public_path()."/content/specifications/".$rfqSpec->link);

            RFQSpecification::where("id", $specificationId)
                                ->delete();

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->deleteSpecification", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function addSpecification($specification, $file)
    {
        try{

            $ret = null;

            if(!empty($specification)){
                $filename = "";
                if(!empty($file)){
                    $filename = $file->getClientOriginalName();
                    $file->move(public_path()."/content/specifications/", $filename);
                }

                $rfqId = isset($specification["rfq_id"]) ? $specification["rfq_id"] : null;
                $label = isset($specification["label"]) ? $specification["label"] : "";
                $version = isset($specification["version"]) ? $specification["version"] : "";

                $status = isset($specification["status"]) ? ($specification["status"] == "true" ? self::RFQ_SPECIFICATION_STATUS_ACTIVE : self::RFQ_SPECIFICATION_STATUS_INACTIVE) : self::RFQ_SPECIFICATION_STATUS_INACTIVE;
                $master = isset($specification["master"]) ? ($specification["master"] == "true" ? self::RFQ_SPECIFICATION_MASTER_YES : self::RFQ_SPECIFICATION_MASTER_NO) : self::RFQ_SPECIFICATION_MASTER_NO;

                $specification = new RFQSpecification();
                $specification->rfq_id = $rfqId;
                $specification->label = $label;
                $specification->version = $version;
                $specification->link = $filename;
                $specification->status = $status;
                $specification->master = $master;
                $specification->created_at = new \DateTime();
                $specification->save();

                $ret = $specification;

            }

            return $this->resultService->Success($ret);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->addSpecification", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMaterialTypes()
    {
        try{

            $materialTypes = MAT::distinct()->select('type')->get()->toArray();
            return $this->resultService->Success($materialTypes);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getMaterialTypes", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMaterialsForType($type)
    {
        try{

            $materials = MAT::where("type", $type)->orderBy("part_no")->get();
            return $this->resultService->Success($materials);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getMaterialsForType", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMaterialForTypeAndPartNo($type,$partNo)
    {
        try{
            $materials = MAT::where("type", $type)->where("part_no", $partNo)->first();
            return $this->resultService->Success($materials);
        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getMaterialForTypeAndPartNo", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function createRfqMaterial($matId, $quantity, $rfqId)
    {
        try{
            if(is_null($matId)){
                return $this->resultService->Error("Material id cannot be empty");
            }
            if(is_null($quantity)){
                return $this->resultService->Error("Quantity cannot be empty");
            }
            if(is_null($rfqId)){
                return $this->resultService->Error("RFQ Id cannot be empty");
            }

            $material = MAT::where("id", $matId)->first();
            if(empty($material)){
                return $this->resultService->Error("Non existing material");
            }

            $rfqMaterial = new RFQMat();
            $rfqMaterial->rfq_id = $rfqId;
            $rfqMaterial->currency_id = null;
            $rfqMaterial->type = $material->type;
            $rfqMaterial->part_no = $material->part_no;
            $rfqMaterial->description = $material->description;
            $rfqMaterial->qty = $quantity;
            $rfqMaterial->unitary_price = $material->unitary_price;
            $rfqMaterial->total_price = $quantity * $material->unitary_price;
            $rfqMaterial->created_at = new \DateTime();
            $rfqMaterial->save();

            return $this->resultService->Success($rfqMaterial);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->createRfqMaterial", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMaterialsForRfq($rfqId)
    {
        try{
            if(is_null($rfqId)){
                return $this->resultService->Error("RFQ Id cannot be empty");
            }

            $rfqMaterials = RFQMat::where("rfq_id", $rfqId)->get()->toArray();
            return $this->resultService->Success($rfqMaterials);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getMaterialsForRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function deleteRfqMaterial($id)
    {
        try{
            if(is_null($id)){
                return $this->resultService->Error("RFQ Material id cannot be empty");
            }

            $res = RFQMat::where("id", $id)->delete();
            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->deleteRfqMaterial", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateRfqMaterial($rfqMatId, $matId, $quantity)
    {
        try{
            if(is_null($rfqMatId)){
                return $this->resultService->Error("RFQ Material id cannot be empty");
            }
            if(is_null($matId)){
                return $this->resultService->Error("Material id cannot be empty");
            }
            if(is_null($quantity)){
                return $this->resultService->Error("Quantity cannot be empty");
            }

            $material = MAT::where("id", $matId)->first();
            if(empty($material)){
                return $this->resultService->Error("Non existing material");
            }

            $res = RFQMat::where("id", $rfqMatId)
                    ->update([
                        "type" => $material->type,
                        "part_no" => $material->part_no,
                        "description" => $material->description,
                        "qty" => $quantity,
                        "unitary_price" => $material->unitary_price,
                        "total_price" => $quantity * $material->unitary_price,
                        "updated_at" => new \DateTime()
                    ]);

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->updateRfqMaterial", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMaterialCost($rfqId){
        try{
            if(is_null($rfqId)){
                return $this->resultService->Error("RFQ Material id cannot be empty");
            }

            $res = RFQ::where("id", $rfqId)
                ->select("mat_total_value")
                ->first();

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getMaterialCost", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getRfqForNpi($rfqId){
        try{
            if(is_null($rfqId)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }

            $res = RFQ::where("id", $rfqId)
                ->with(['materials'])
                ->first();

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->getRfqForNpi", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function createRFQStatusLog($rfqId, $statusNumber,$status,$lastModifiedBy,$lastModifiedWhen,$deadReason=null){
        try {
            $rfqStatusLog = new RFQStatusLog();
            $rfqStatusLog->rfq_id = $rfqId;
            $rfqStatusLog->status = $status;
            if ($statusNumber == 1)
                $rfqStatusLog->action = RFQStatusLog::ACTION_CHECKED;
            else if ($statusNumber == 0)
                $rfqStatusLog->action = RFQStatusLog::ACTION_UNCHECKED;
            else
                $rfqStatusLog->action = null;
            $rfqStatusLog->who = !empty($lastModifiedBy) ? $lastModifiedBy : "";
            $rfqStatusLog->when = $lastModifiedWhen;
            $rfqStatusLog->dead_reason = $deadReason;
            $rfqStatusLog->save();
            return true;
        }
        catch (Exception $e){
            return false;
        }
    }

    public function createRfq($projectId, $lastModifiedBy)
    {
        try{
            if(empty($projectId)) {
                return $this->resultService->Error("Project Id cannot be empty");
            }

            $rfq = new RFQ();
            $rfq->project_id = $projectId;
            $rfq->status = self::RFQ_STATUS_INIT;
            $rfq->creation_date = date('Y-m-d H:i:s', strtotime("NOW"));
            $rfq->created_at = date('Y-m-d H:i:s', strtotime("NOW"));
            $rfq->last_modified_by = !empty($lastModifiedBy) ? $lastModifiedBy : "";
            $rfq->save();

            // Creation of initial Status Log of RFQ
            if (!empty($rfq->id)) {
                $rfqStatusLog = new RFQStatusLog();
                $rfqStatusLog->rfq_id = $rfq->id;
                $rfqStatusLog->status = RFQStatusLog::INITIAL_STATUS;
                $rfqStatusLog->action = RFQStatusLog::ACTION_CHECKED;
                $rfqStatusLog->who = !empty($lastModifiedBy) ? $lastModifiedBy : "";
                $rfqStatusLog->when = new \DateTime();
                $rfqStatusLog->save();
            }

            return $this->resultService->Success($rfq);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->createRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateRfq($rfq,$lastModifiedBy)
    {
        try{
            $ret = null;
            if(!empty($rfq)){

                $id = isset($rfq["id"]) ? $rfq["id"] : null;

                $oldRfq =  RFQ::where("id", $id)->first();

                $updateArray = array(
                    "father" => isset($rfq["father"]) ? $rfq["father"] : null,
                    "is_std_item" => isset($rfq["is_std_item"]) ? $rfq["is_std_item"] : 0,
                    "status" => isset($rfq["status"]) ? $rfq["status"] : "INIT",
                    "folder" => isset($rfq["folder"]) ? $rfq["folder"] : null,
                    "priority" => isset($rfq["priority"]) ? $rfq["priority"] : null,
                    "design_office_id" => (isset($rfq["design_office"]) && !is_null($rfq["design_office"])) ? $rfq["design_office"]["id"] : null,
                    "customer_engineer_id" => (isset($rfq["customer_engineer"]) && !is_null($rfq["customer_engineer"])) ? $rfq["customer_engineer"]["id"] : null,
                    "tamura_elec_engineer_id" => (isset($rfq["electrical_engineer"]) && !is_null($rfq["electrical_engineer"])) ? $rfq["electrical_engineer"]["id"] : null,
                    "tamura_mec_engineer_id" => (isset($rfq["mechanical_engineer"]) && !is_null($rfq["mechanical_engineer"])) ? $rfq["mechanical_engineer"]["id"] : null,
                    "product_id" => (isset($rfq["product"]) && !is_null($rfq["product"])) ? $rfq["product"]["no_entity"] : null,
                    "pn_customer" => isset($rfq["pn_customer"]) ? $rfq["pn_customer"] : null,
                    "pn_desc" => isset($rfq["pn_desc"]) ? $rfq["pn_desc"] : null,
                    "pn_category_id" => (isset($rfq["category"]) && !is_null($rfq["category"])) ? $rfq["category"]["id"] : null,
                    "pn_format" => isset($rfq["pn_format"]) ? $rfq["pn_format"] : null,
                    "pn_new_dev" => isset($rfq["pn_new_dev"]) ? $rfq["pn_new_dev"] : null,
                    "pn_new_biz" => isset($rfq["pn_new_biz"]) ? $rfq["pn_new_biz"] : null,
                    "com_currency_id" => (isset($rfq["com_currency"]) && !is_null($rfq["com_currency"])) ? $rfq["com_currency"]["id"] : null,
                    "com_estimated_price" => isset($rfq["com_estimated_price"]) ? $rfq["com_estimated_price"] : null,
                    "com_target_price" => isset($rfq["com_target_price"]) ? $rfq["com_target_price"] : null,
                    "com_annual_qty" => isset($rfq["com_annual_qty"]) ? $rfq["com_annual_qty"] : null,
                    "com_annual_value" => isset($rfq["com_annual_value"]) ? $rfq["com_annual_value"] : null,
                    "com_confidence_level" => isset($rfq["com_confidence_level"]) ? (double)$rfq["com_confidence_level"] / 100 : null,
                    "com_market_share" => isset($rfq["com_market_share"]) ? (double)$rfq["com_market_share"] / 100 : null,
                    "com_incoterms" => isset($rfq["com_incoterms"]) ? $rfq["com_incoterms"] : null,
                    "com_delivery_location" => (isset($rfq["com_delivery_location"]) && !is_null($rfq["com_delivery_location"])) ? $rfq["com_delivery_location"]["iso2"] : null,
                    "com_teu_group" => isset($rfq["com_teu_group"]) ? $rfq["com_teu_group"] : null,
                    "qualif_quotation_required_date" => isset($rfq["qualif_quotation_required_date"]) ? date('Y-m-d H:i:s', strtotime($rfq["qualif_quotation_required_date"])) : null,
                    "qualif_sample_date" => isset($rfq["qualif_sample_date"]) ? date('Y-m-d H:i:s', strtotime($rfq["qualif_sample_date"])) : null,
                    "qualif_mass_prod_date" => isset($rfq["qualif_mass_prod_date"]) ? date('Y-m-d H:i:s', strtotime($rfq["qualif_mass_prod_date"])) : null,
                    "comments" => isset($rfq["comments"]) ? $rfq["comments"] : null,
                    "duplicate" => isset($rfq["duplicate"]) ? $rfq["duplicate"] : null,
                    "duplicate_from" => isset($rfq["duplicate_from"]) ? $rfq["duplicate_from"] : null,
                    "last_modified_on" => date('Y-m-d H:i:s', strtotime("NOW")),
                    "last_modified_by" => empty($lastModifiedBy)? (isset($rfq["last_modified_by"]) ? $rfq["last_modified_by"] : null) : $lastModifiedBy,
                    "status_10" => isset($rfq["status_10"]) ? $rfq["status_10"] : 0,
                    "status_10_who" => isset($rfq["status_10_who"]) ? $rfq["status_10_who"] : null,
                    "status_10_when" => isset($rfq["status_10_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["status_10_when"])) : null,
                    "status_20" => isset($rfq["status_20"]) ? $rfq["status_20"] : 0,
                    "status_20_who" => isset($rfq["status_20_who"]) ? $rfq["status_20_who"] : null,
                    "status_20_when" => isset($rfq["status_20_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["status_20_when"])) : null,
                    "status_30" => isset($rfq["status_30"]) ? $rfq["status_30"] : 0,
                    "status_30_who" => isset($rfq["status_30_who"]) ? $rfq["status_30_who"] : null,
                    "status_30_when" => isset($rfq["status_30_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["status_30_when"])) : null,
                    "status_dead" => isset($rfq["status_dead"]) ? $rfq["status_dead"] : 0,
                    "status_dead_who" => isset($rfq["status_dead_who"]) ? $rfq["status_dead_who"] : null,
                    "status_dead_when" => isset($rfq["status_dead_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["status_dead_when"])) : null,
                    "status_dead_reason_id" => (isset($rfq["status_dead_reason"]) && !is_null($rfq["status_dead_reason"])) ? $rfq["status_dead_reason"]["id"] : null,
                    "status_rejected" => isset($rfq["status_rejected"]) ? $rfq["status_rejected"] : 0,
                    "status_rejected_who" => isset($rfq["status_rejected_who"]) ? $rfq["status_rejected_who"] : null,
                    "status_rejected_when" => isset($rfq["status_rejected_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["status_rejected_when"])) : null,
                    "eng_action_costing_planned_date" => isset($rfq["eng_action_costing_planned_date"]) ? date('Y-m-d H:i:s', strtotime($rfq["eng_action_costing_planned_date"])) : null,
                    "eng_action_costing_complete" => isset($rfq["eng_action_costing_complete"]) ? $rfq["eng_action_costing_complete"] : 0,
                    "eng_action_costing_complete_who" => isset($rfq["eng_action_costing_complete_who"]) ? $rfq["eng_action_costing_complete_who"] : null,
                    "eng_action_costing_complete_when" => isset($rfq["eng_action_costing_complete_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["eng_action_costing_complete_when"])) : null,
                    "eng_action_design_required" => isset($rfq["eng_action_design_required"]) ? $rfq["eng_action_design_required"] : 0,
                    "eng_action_design_required_who" => isset($rfq["eng_action_design_required_who"]) ? $rfq["eng_action_design_required_who"] : null,
                    "eng_action_design_required_when" => isset($rfq["eng_action_design_required_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["eng_action_design_required_when"])) : null,
                    "eng_action_design_complete_planned_date" => isset($rfq["eng_action_design_complete_planned_date"]) ? date('Y-m-d H:i:s', strtotime($rfq["eng_action_design_complete_planned_date"])) : null,
                    "eng_action_design_complete" => isset($rfq["eng_action_design_complete"]) ? $rfq["eng_action_design_complete"] : 0,
                    "eng_action_design_complete_who" => isset($rfq["eng_action_design_complete_who"]) ? $rfq["eng_action_design_complete_who"] : null,
                    "eng_action_design_complete_when" => isset($rfq["eng_action_design_complete_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["eng_action_design_complete_when"])) : null,
                    "eng_action_mass_prod_auth" => isset($rfq["eng_action_mass_prod_auth"]) ? $rfq["eng_action_mass_prod_auth"] : 0,
                    "eng_action_mass_prod_auth_who" => isset($rfq["eng_action_mass_prod_auth_who"]) ? $rfq["eng_action_mass_prod_auth_who"] : null,
                    "eng_action_mass_prod_auth_when" => isset($rfq["eng_action_mass_prod_auth_when"]) ? date('Y-m-d H:i:s', strtotime($rfq["eng_action_mass_prod_auth_when"])) : null,
                    "product_type" => isset($rfq["product_type"]) ? $rfq["product_type"] : null,
                    "product_length" => isset($rfq["product_length"]) ? $rfq["product_length"] : null,
                    "product_width" => isset($rfq["product_width"]) ? $rfq["product_width"] : null,
                    "product_height" => isset($rfq["product_height"]) ? $rfq["product_height"] : null,
                    "product_weight" => isset($rfq["product_weight"]) ? $rfq["product_weight"] : null,
                    "product_field1" => isset($rfq["product_field1"]) ? $rfq["product_field1"] : null,
                    "product_field2" => isset($rfq["product_field2"]) ? $rfq["product_field2"] : null,
                    "product_field3" => isset($rfq["product_field3"]) ? $rfq["product_field3"] : null,
                    "product_field4" => isset($rfq["product_field4"]) ? $rfq["product_field4"] : null,
                    "product_field5" => isset($rfq["product_field5"]) ? $rfq["product_field5"] : null,
                    "product_field6" => isset($rfq["product_field6"]) ? $rfq["product_field6"] : null,
                    "product_other_materials" => isset($rfq["product_other_materials"]) ? (double)$rfq["product_other_materials"] / 100 : null,
                    "product_other_materials_value" => isset($rfq["product_other_materials_value"]) ? $rfq["product_other_materials_value"] : null,
                    "product_handling_scrap" => isset($rfq["product_handling_scrap"]) ? (double)$rfq["product_handling_scrap"] / 100 : null,
                    "product_handling_scrap_value" => isset($rfq["product_handling_scrap_value"]) ? $rfq["product_handling_scrap_value"] : null,
                    "mat_currency_id" => (isset($rfq["mat_currency"]) && !is_null($rfq["mat_currency"])) ? $rfq["mat_currency"]["id"] : null,
                    "mat_total_value" => isset($rfq["mat_total_value"]) ? $rfq["mat_total_value"] : null,
                    "pallet_length" => isset($rfq["pallet_length"]) ? $rfq["pallet_length"] : null,
                    "pallet_width" => isset($rfq["pallet_width"]) ? $rfq["pallet_width"] : null,
                    "pallet_space" => isset($rfq["pallet_space"]) ? $rfq["pallet_space"] : null,
                    "pallet_layers" => isset($rfq["pallet_layers"]) ? $rfq["pallet_layers"] : null,
                    "pallet_quantity" => isset($rfq["pallet_quantity"]) ? $rfq["pallet_quantity"] : null,
                    "labour1" => isset($rfq["labour1"]) ? $rfq["labour1"] : null,
                    "labour2" => isset($rfq["labour2"]) ? $rfq["labour2"] : null,
                    "labour3" => isset($rfq["labour3"]) ? $rfq["labour3"] : null,
                    "labour_time1" => isset($rfq["labour_time1"]) ? $rfq["labour_time1"] : null,
                    "labour_time2" => isset($rfq["labour_time2"]) ? $rfq["labour_time2"] : null,
                    "labour_time3" => isset($rfq["labour_time3"]) ? $rfq["labour_time3"] : null,
                    "labour_time4" => isset($rfq["labour_time4"]) ? $rfq["labour_time4"] : null,
                    "updated_at" => date("Y-m-d H:i:s", strtotime("NOW"))
                );

                RFQ::where("id", $id)
                    ->update($updateArray);

                if(!is_null($rfq["estimated_factory_cost"]) && !empty($rfq["estimated_factory_cost"]))
                {
                    $efc = $rfq["estimated_factory_cost"];
                    $factory1 = isset($efc["factory1"]) ? $efc["factory1"] : null;
                    $factory2 = isset($efc["factory2"]) ? $efc["factory2"] : null;
                    $factory3 = isset($efc["factory3"]) ? $efc["factory3"] : null;

                    $currency1 = isset($efc["currency_factory1"]) ? $efc["currency_factory1"] : null;
                    $currency2 = isset($efc["currency_factory2"]) ? $efc["currency_factory2"] : null;
                    $currency3 = isset($efc["currency_factory3"]) ? $efc["currency_factory3"] : null;

                    $insertUpdateArray = [
                        "rfq_id" => $id,
                        "tamura_entity_id_factory1" => isset($factory1["id"]) ? $factory1["id"] : null,
                        "tamura_entity_currency_factory1" => isset($currency1["id"]) ? $currency1["id"] : null,
                        "rfq_material_factory1" => isset($efc["rfq_material_factory1"]) ? $efc["rfq_material_factory1"] : null,
                        "tamura_entity_profit_factory1" => isset($efc["tamura_entity_profit_factory1"]) ? $efc["tamura_entity_profit_factory1"] : null,
                        "tamura_entity_profit_value_factory1" => isset($efc["tamura_entity_profit_value_factory1"]) ? $efc["tamura_entity_profit_value_factory1"] : null,
                        "tamura_entity_labour_rate_choice_factory1" => isset($efc["tamura_entity_labour_rate_choice_factory1"]) ? $efc["tamura_entity_labour_rate_choice_factory1"] : null,
                        "tamura_entity_labour_time_value_factory1" => isset($efc["tamura_entity_labour_time_value_factory1"]) ? $efc["tamura_entity_labour_time_value_factory1"] : null,
                        "tamura_entity_labour_rate_factory1" => isset($efc["tamura_entity_labour_rate_factory1"]) ? $efc["tamura_entity_labour_rate_factory1"] : null,
                        "tamura_entity_labour_rate_value_factory1" => isset($efc["tamura_entity_labour_rate_value_factory1"]) ? $efc["tamura_entity_labour_rate_value_factory1"] : null,
                        "tamura_entity_freight_factory1" => isset($efc["tamura_entity_freight_factory1"]) ? $efc["tamura_entity_freight_factory1"] : null,
                        "tamura_entity_freight_value_factory1" => isset($efc["tamura_entity_freight_value_factory1"]) ? $efc["tamura_entity_freight_value_factory1"] : null,
                        "tamura_entity_admin_factory1" => isset($efc["tamura_entity_admin_factory1"]) ? $efc["tamura_entity_admin_factory1"] : null,
                        "tamura_entity_admin_value_factory1" => isset($efc["tamura_entity_admin_value_factory1"]) ? $efc["tamura_entity_admin_value_factory1"] : null,
                        "tamura_entity_selling_price_factory1" => isset($efc["tamura_entity_selling_price_factory1"]) ? $efc["tamura_entity_selling_price_factory1"] : null,
                        "tamura_entity_id_sales_office1" => isset($efc["tamura_entity_id_sales_office1"]) ? $efc["tamura_entity_id_sales_office1"] : null,
                        "tamura_entity_profit_sales_office1" => isset($efc["tamura_entity_profit_sales_office1"]) ? $efc["tamura_entity_profit_sales_office1"] : null,
                        "tamura_entity_profit_value_sales_office1" => isset($efc["tamura_entity_profit_value_sales_office1"]) ? $efc["tamura_entity_profit_value_sales_office1"] : null,
                        "tamura_entity_selling_price_sales_office1" => isset($efc["tamura_entity_selling_price_sales_office1"]) ? $efc["tamura_entity_selling_price_sales_office1"] : null,

                        "tamura_entity_id_factory2" => isset($factory2["id"]) ? $factory2["id"] : null,
                        "tamura_entity_currency_factory2" => isset($currency2["id"]) ? $currency2["id"] : null,
                        "rfq_material_factory2" => isset($efc["rfq_material_factory2"]) ? $efc["rfq_material_factory2"] : null,
                        "tamura_entity_profit_factory2" => isset($efc["tamura_entity_profit_factory2"]) ? $efc["tamura_entity_profit_factory2"] : null,
                        "tamura_entity_profit_value_factory2" => isset($efc["tamura_entity_profit_value_factory2"]) ? $efc["tamura_entity_profit_value_factory2"] : null,
                        "tamura_entity_labour_rate_choice_factory2" => isset($efc["tamura_entity_labour_rate_choice_factory2"]) ? $efc["tamura_entity_labour_rate_choice_factory2"] : null,
                        "tamura_entity_labour_time_value_factory2" => isset($efc["tamura_entity_labour_time_value_factory2"]) ? $efc["tamura_entity_labour_time_value_factory2"] : null,
                        "tamura_entity_labour_rate_factory2" => isset($efc["tamura_entity_labour_rate_factory2"]) ? $efc["tamura_entity_labour_rate_factory2"] : null,
                        "tamura_entity_labour_rate_value_factory2" => isset($efc["tamura_entity_labour_rate_value_factory2"]) ? $efc["tamura_entity_labour_rate_value_factory2"] : null,
                        "tamura_entity_freight_factory2" => isset($efc["tamura_entity_freight_factory2"]) ? $efc["tamura_entity_freight_factory2"] : null,
                        "tamura_entity_freight_value_factory2" => isset($efc["tamura_entity_freight_value_factory2"]) ? $efc["tamura_entity_freight_value_factory2"] : null,
                        "tamura_entity_admin_factory2" => isset($efc["tamura_entity_admin_factory2"]) ? $efc["tamura_entity_admin_factory2"] : null,
                        "tamura_entity_admin_value_factory2" => isset($efc["tamura_entity_admin_value_factory2"]) ? $efc["tamura_entity_admin_value_factory2"] : null,
                        "tamura_entity_selling_price_factory2" => isset($efc["tamura_entity_selling_price_factory2"]) ? $efc["tamura_entity_selling_price_factory2"] : null,
                        "tamura_entity_id_sales_office2" => isset($efc["tamura_entity_id_sales_office2"]) ? $efc["tamura_entity_id_sales_office2"] : null,
                        "tamura_entity_profit_sales_office2" => isset($efc["tamura_entity_profit_sales_office2"]) ? $efc["tamura_entity_profit_sales_office2"] : null,
                        "tamura_entity_profit_value_sales_office2" => isset($efc["tamura_entity_profit_value_sales_office2"]) ? $efc["tamura_entity_profit_value_sales_office2"] : null,
                        "tamura_entity_selling_price_sales_office2" => isset($efc["tamura_entity_selling_price_sales_office2"]) ? $efc["tamura_entity_selling_price_sales_office2"] : null,
                        
                        "tamura_entity_id_factory3" => isset($factory3["id"]) ? $factory3["id"] : null,
                        "tamura_entity_currency_factory3" => isset($currency3["id"]) ? $currency3["id"] : null,
                        "rfq_material_factory3" => isset($efc["rfq_material_factory3"]) ? $efc["rfq_material_factory3"] : null,
                        "tamura_entity_profit_factory3" => isset($efc["tamura_entity_profit_factory3"]) ? $efc["tamura_entity_profit_factory3"] : null,
                        "tamura_entity_profit_value_factory3" => isset($efc["tamura_entity_profit_value_factory3"]) ? $efc["tamura_entity_profit_value_factory3"] : null,
                        "tamura_entity_labour_rate_choice_factory3" => isset($efc["tamura_entity_labour_rate_choice_factory3"]) ? $efc["tamura_entity_labour_rate_choice_factory3"] : null,
                        "tamura_entity_labour_time_value_factory3" => isset($efc["tamura_entity_labour_time_value_factory3"]) ? $efc["tamura_entity_labour_time_value_factory3"] : null,
                        "tamura_entity_labour_rate_factory3" => isset($efc["tamura_entity_labour_rate_factory3"]) ? $efc["tamura_entity_labour_rate_factory3"] : null,
                        "tamura_entity_labour_rate_value_factory3" => isset($efc["tamura_entity_labour_rate_value_factory3"]) ? $efc["tamura_entity_labour_rate_value_factory3"] : null,
                        "tamura_entity_freight_factory3" => isset($efc["tamura_entity_freight_factory3"]) ? $efc["tamura_entity_freight_factory3"] : null,
                        "tamura_entity_freight_value_factory3" => isset($efc["tamura_entity_freight_value_factory3"]) ? $efc["tamura_entity_freight_value_factory3"] : null,
                        "tamura_entity_admin_factory3" => isset($efc["tamura_entity_admin_factory3"]) ? $efc["tamura_entity_admin_factory3"] : null,
                        "tamura_entity_admin_value_factory3" => isset($efc["tamura_entity_admin_value_factory3"]) ? $efc["tamura_entity_admin_value_factory3"] : null,
                        "tamura_entity_selling_price_factory3" => isset($efc["tamura_entity_selling_price_factory3"]) ? $efc["tamura_entity_selling_price_factory3"] : null,
                        "tamura_entity_id_sales_office3" => isset($efc["tamura_entity_id_sales_office3"]) ? $efc["tamura_entity_id_sales_office3"] : null,
                        "tamura_entity_profit_sales_office3" => isset($efc["tamura_entity_profit_sales_office3"]) ? $efc["tamura_entity_profit_sales_office3"] : null,
                        "tamura_entity_profit_value_sales_office3" => isset($efc["tamura_entity_profit_value_sales_office3"]) ? $efc["tamura_entity_profit_value_sales_office3"] : null,
                        "tamura_entity_selling_price_sales_office3" => isset($efc["tamura_entity_selling_price_sales_office3"]) ? $efc["tamura_entity_selling_price_sales_office3"] : null,

                        "created_at" => new \DateTime(),
                        "updated_at" => new \DateTime()
                    ];

                    $rfqEfc = DB::table("rfq_estimated_factory_costs")->where("rfq_id", $id)->first();
                    if(empty($rfqEfc)){
                        DB::table("rfq_estimated_factory_costs")->insert($insertUpdateArray);
                    }else{
                        DB::table("rfq_estimated_factory_costs")->where("rfq_id", $id)->update($insertUpdateArray);
                    }

                }


                if(isset($rfq["electrical_engineer"]) && !is_null($rfq["electrical_engineer"])){
                    Person::where('id',$rfq["electrical_engineer"]["id"] )->update([
                        'phone' => $rfq["electrical_engineer"]["phone"],
                        'mobile' =>  $rfq["electrical_engineer"]["mobile"]
                    ]);
                }

                if(isset($rfq["mechanical_engineer"]) && !is_null($rfq["mechanical_engineer"])){
                    Person::where('id',$rfq["mechanical_engineer"]["id"] )->update([
                        'phone' => $rfq["mechanical_engineer"]["phone"],
                        'mobile' => $rfq["mechanical_engineer"]["mobile"]
                    ]);
                }

                // TODO : TESTING
                // Create new RFQ Status Log records
                if ($oldRfq["status_10"] != $updateArray["status_10"]){
                    $this->createRFQStatusLog($id, $updateArray["status_10"],RFQStatusLog::T10_STATUS,
                        isset($updateArray["status_10_who"])?$updateArray["status_10_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }

                if ( ($oldRfq["eng_action_costing_complete"] != $updateArray["eng_action_costing_complete"])){
                    $this->createRFQStatusLog($id, $updateArray["eng_action_costing_complete"],RFQStatusLog::COSTINGCO_STATUS,
                        isset($updateArray["eng_action_costing_complete_who"])?$updateArray["eng_action_costing_complete_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }
                if ( ($oldRfq["eng_action_design_required"] != $updateArray["eng_action_design_required"])){
                    $this->createRFQStatusLog($id, $updateArray["eng_action_design_required"],RFQStatusLog::DESIGNREQ_STATUS,
                        isset($updateArray["eng_action_design_required_who"])?$updateArray["eng_action_design_required_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }
                if ( ($oldRfq["eng_action_design_complete"] != $updateArray["eng_action_design_complete"])){
                    $this->createRFQStatusLog($id, $updateArray["eng_action_design_complete"],RFQStatusLog::DESIGNCOM_STATUS,
                        isset($updateArray["eng_action_design_complete_who"])?$updateArray["eng_action_design_complete_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }
                if (  ($oldRfq["eng_action_mass_prod_auth"] != $updateArray["eng_action_mass_prod_auth"])){
                    $this->createRFQStatusLog($id, $updateArray["eng_action_mass_prod_auth"],RFQStatusLog::MASSPROD_STATUS,
                        isset($updateArray["eng_action_mass_prod_auth_who"])?$updateArray["eng_action_mass_prod_auth_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }

                if ($oldRfq["status_20"] != $updateArray["status_20"]){
                    $this->createRFQStatusLog($id, $updateArray["status_20"],RFQStatusLog::T20_STATUS,
                        isset($updateArray["status_20_who"])?$updateArray["status_20_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }
                if ($oldRfq["status_30"] != $updateArray["status_30"]){
                    $this->createRFQStatusLog($id, $updateArray["status_30"],RFQStatusLog::T30_STATUS,
                        isset($updateArray["status_30_who"])?$updateArray["status_30_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }
                if ($oldRfq["status_dead"] != $updateArray["status_dead"]){
                    $this->createRFQStatusLog($id, $updateArray["status_dead"],RFQStatusLog::DEAD_STATUS,
                        isset($updateArray["status_dead_who"])?$updateArray["status_dead_who"]:$updateArray["last_modified_by"],
                        new \DateTime(),isset($rfq["status_dead_reason"]["reason"])?$rfq["status_dead_reason"]["reason"]:null);
                }
                if ($oldRfq["status_rejected"] != $updateArray["status_rejected"]){
                    $this->createRFQStatusLog($id, $updateArray["status_rejected"],RFQStatusLog::REJECTED_STATUS,
                        isset($updateArray["status_rejected_who"])?$updateArray["status_rejected_who"]:$updateArray["last_modified_by"],
                        new \DateTime());
                }


                return $this->get($id);

            }

            return $this->resultService->Success($ret);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->updateRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }


    public function changeRfqProject($rfqId, $projectId)
    {
        try{
            if(empty($rfqId)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }
            if(empty($projectId)){
                return $this->resultService->Error("Project id cannot be empty");
            }

            $updateArray = [
                "project_id" => $projectId
            ];

            $newProject = Project::with(['customer'])->where("id", $projectId)->first()->toArray();

            $rfq = RFQ::where("id", $rfqId)->first();
            if(!empty($rfq)){
                $oldProject = Project::with(['customer'])->where("id", $rfq->project_id)->first()->toArray();

                if(!empty($newProject) && !empty($oldProject) && $newProject["customer"]["id_root"] != $oldProject["customer"]["id_root"]){
                    $updateArray["customer_engineer_id"] = null;
                }

            }else{
                return $this->resultService->Error("Cannot find RFQ by provided id");
            }

            $ret = RFQ::where("id", $rfqId)
                ->update($updateArray);

            return $this->resultService->Success($ret);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->changeRfqProject", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function copyRfq($rfqId)
    {
        try{
            if(empty($rfqId)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }

            $rfq = RFQ::with([
                'npis',
                'category',
                'product.parent.parent.parent' => function($query){
                    $query->where("type_entity", self::TAMURA_CODIFICATION_TYPE_CHILD)
                        ->where("code_list", self::TAMURA_CODIFICATION_CODE_LIST_GTC);
                },
                'comCurrency',
                'customerEngineer',
                'project',
                'designOffice',
                'comDeliveryLocation',
                'electricalEngineer',
                'mechanicalEngineer',
                'materials',
                'specifications',
                'estimatedFactoryCost',
                'estimatedFactoryCost.factory1.country',
                'estimatedFactoryCost.currencyFactory1',
                'estimatedFactoryCost.factory2.country',
                'estimatedFactoryCost.currencyFactory2',
                'estimatedFactoryCost.factory3.country',
                'estimatedFactoryCost.currencyFactory3',
                'matCurrency',
                'statusDeadReason',
                'statusLogs'
            ])
            ->where("id", $rfqId)
            ->first();

            if(!empty($rfq)){
                $newRfq = $rfq->replicate();
                $newRfq->save();

                // npis
                foreach($rfq->npis as $npi){
                    $newNpi = $npi->replicate();
                    $newNpi->rfq_id = $newRfq->id;
                    $newNpi->save();
                }

                // materials
                foreach($rfq->materials as $material){
                    $newMaterial = $material->replicate();
                    $newMaterial->rfq_id = $newRfq->id;
                    $newMaterial->save();
                }

                // specifications
                foreach($rfq->specifications as $specification){
                    $newSpecification = $specification->replicate();
                    $newSpecification->rfq_id = $newRfq->id;
                    $newSpecification->save();
                }

                // status logs
                foreach($rfq->statusLogs as $statusLog){
                    $newStatusLog = $statusLog->replicate();
                    $newStatusLog->rfq_id = $newRfq->id;
                    $newStatusLog->save();
                }

                // estimated factory cost
                if(!empty($rfq->estimatedFactoryCost)){
                    $newEstimatedFactoryCost = $rfq->estimatedFactoryCost->replicate();
                    $newEstimatedFactoryCost->rfq_id = $newRfq->id;
                    $newEstimatedFactoryCost->save();
                }

            }

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->copyRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function cloneRfq($rfqId, $projectId)
    {
        try{

            if(empty($rfqId)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }
            if(empty($projectId)){
                return $this->resultService->Error("Project id cannot be empty");
            }

            $rfq = RFQ::with([
                'npis',
                'category',
                'product.parent.parent.parent' => function($query){
                    $query->where("type_entity", self::TAMURA_CODIFICATION_TYPE_CHILD)
                        ->where("code_list", self::TAMURA_CODIFICATION_CODE_LIST_GTC);
                },
                'comCurrency',
                'customerEngineer',
                'project',
                'designOffice',
                'comDeliveryLocation',
                'electricalEngineer',
                'mechanicalEngineer',
                'materials',
                'specifications',
                'estimatedFactoryCost',
                'estimatedFactoryCost.factory1.country',
                'estimatedFactoryCost.currencyFactory1',
                'estimatedFactoryCost.factory2.country',
                'estimatedFactoryCost.currencyFactory2',
                'estimatedFactoryCost.factory3.country',
                'estimatedFactoryCost.currencyFactory3',
                'matCurrency',
                'statusDeadReason',
                'statusLogs'
            ])
                ->where("id", $rfqId)
                ->first();

            if(!empty($rfq)){
                $newRfq = $rfq->replicate();
                $newRfq->project_id = $projectId;
                $newRfq->duplicate = 2;
                $newRfq->duplicate_from = $rfqId;
                $newRfq->save();

                // npis
                foreach($rfq->npis as $npi){
                    $newNpi = $npi->replicate();
                    $newNpi->rfq_id = $newRfq->id;
                    $newNpi->save();
                }

                // materials
                foreach($rfq->materials as $material){
                    $newMaterial = $material->replicate();
                    $newMaterial->rfq_id = $newRfq->id;
                    $newMaterial->save();
                }

                // specifications
                foreach($rfq->specifications as $specification){
                    $newSpecification = $specification->replicate();
                    $newSpecification->rfq_id = $newRfq->id;
                    $newSpecification->save();
                }

                // status logs
                foreach($rfq->statusLogs as $statusLog){
                    $newStatusLog = $statusLog->replicate();
                    $newStatusLog->rfq_id = $newRfq->id;
                    $newStatusLog->save();
                }

                // estimated factory cost
                if(!empty($rfq->estimatedFactoryCost)){
                    $newEstimatedFactoryCost = $rfq->estimatedFactoryCost->replicate();
                    $newEstimatedFactoryCost->rfq_id = $newRfq->id;
                    $newEstimatedFactoryCost->save();
                }

            }

            return $this->resultService->Success(true);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->cloneRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function checkCustomerPn($customerPN, $id)
    {
        try{

            if(is_null($customerPN)){
                return $this->resultService->Error("Customer PN cannot be empty");
            }

            $res = RFQ::where("pn_customer", $customerPN)
                ->first();

            if(!empty($id)){
                $result = (!empty($res) && $res->id != $id) ? true : false;
            }else{
                $result = !empty($res) ? true : false;
            }

            return $this->resultService->Success($result);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->checkCustomerPn", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getProduction($id)
    {
        try{

            if(is_null($id)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }

            $res = RFQProduction::where("rfq_id", $id)
                ->with(['factory','currency'])
                ->get();

            $productions = [];
            foreach ($res as $r) {
                $production = [];
                $production["id"] = isset($r["id"])? $r["id"] : null;
                $production["lastProductionDate"] = isset($r["last_production_date"])? date("d/m/Y", strtotime($r["last_production_date"])) : null;
                $production["currency"] = isset($r["currency"])? $r["currency"] : null;
                $production["factory"] = isset($r["factory"])? $r["factory"] : null;
                $production["rollUpLabourEstimatedLabourCostEuro"] = isset($r["roll_up_labour_estimated_labour_cost"])? $r["roll_up_labour_estimated_labour_cost"] : null ;
                $production["rollUpLabourEstimatedLabourTimeEuro"] = isset($r["roll_up_labour_estimated_labour_time"])? $r["roll_up_labour_estimated_labour_time"] : null ;
                $production["rollUpMaterialEstimatedMaterialCostEuro"] = isset($r["roll_up_material_estimated_material_cost"])? $r["roll_up_material_estimated_material_cost"] : null ;
                $production["soPriceFactoryCost"] = isset($r["so_price_factory_cost"])? $r["so_price_factory_cost"] : null ;
                $production["soPriceFactoryLabourCost"] = isset($r["so_price_factory_labour_cost"])? $r["so_price_factory_labour_cost"] : null ;
                $production["soPriceFactoryMaterialCost"] = isset($r["so_price_factory_material_cost"])? $r["so_price_factory_material_cost"] : null ;
                $production["stdCostRollUpLabourEuro"] = isset($r["std_cost_roll_up_labour_euro"])? $r["std_cost_roll_up_labour_euro"] : null ;
                $production["stdCostRollUpLabourTime"] = isset($r["std_cost_roll_up_labour_time"])? $r["std_cost_roll_up_labour_time"] : null ;
                $production["stdCostRollUpMaterial"] = isset($r["std_cost_roll_up_material"])? $r["std_cost_roll_up_material"] : null ;
                $production["stdCostRollUpTotalEuro"] = isset($r["std_cost_roll_up_total_euro"])? $r["std_cost_roll_up_total_euro"] : null ;
                $productions[] = $production;
            }


            return $this->resultService->Success($productions);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->checkCustomerPn", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getProductionById($id)
    {
        try{

            if(is_null($id)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }

            $res = RFQProduction::where("id", $id)
                ->with(['factory','currency'])
                ->first();

            $production = [];
            $production["id"] = isset($res["id"])? $res["id"] : null;
            $production["lastProductionDate"] = isset($res["last_production_date"])? date("m/d/Y", strtotime($res["last_production_date"])) : null;
            $production["currency"] = isset($res["currency"])? $res["currency"] : null;
            $production["factory"] = isset($res["factory"])? $res["factory"] : null;
            $production["rollUpLabourEstimatedLabourCostEuro"] = isset($res["roll_up_labour_estimated_labour_cost"])? $res["roll_up_labour_estimated_labour_cost"] : null ;
            $production["rollUpLabourEstimatedLabourTimeEuro"] = isset($res["roll_up_labour_estimated_labour_time"])? $res["roll_up_labour_estimated_labour_time"] : null ;
            $production["rollUpMaterialEstimatedMaterialCostEuro"] = isset($res["roll_up_material_estimated_material_cost"])? $res["roll_up_material_estimated_material_cost"] : null ;
            $production["soPriceFactoryCost"] = isset($res["so_price_factory_cost"])? $res["so_price_factory_cost"] : null ;
            $production["soPriceFactoryLabourCost"] = isset($res["so_price_factory_labour_cost"])? $res["so_price_factory_labour_cost"] : null ;
            $production["soPriceFactoryMaterialCost"] = isset($res["so_price_factory_material_cost"])? $res["so_price_factory_material_cost"] : null ;
            $production["stdCostRollUpLabourEuro"] = isset($res["std_cost_roll_up_labour_euro"])? $res["std_cost_roll_up_labour_euro"] : null ;
            $production["stdCostRollUpLabourTime"] = isset($res["std_cost_roll_up_labour_time"])? $res["std_cost_roll_up_labour_time"] : null ;
            $production["stdCostRollUpMaterial"] = isset($res["std_cost_roll_up_material"])? $res["std_cost_roll_up_material"] : null ;
            $production["stdCostRollUpTotalEuro"] = isset($res["std_cost_roll_up_total_euro"])? $res["std_cost_roll_up_total_euro"] : null ;

            return $this->resultService->Success($production);

        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->checkCustomerPn", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function addProduction($rfqId,$production)
    {
        try{
            if(!is_null($production) && !empty($production))
            {
                $factory = isset($production["factory"]) ? $production["factory"] : null;
                $currency = isset($production["currency"]) ? $production["currency"] : null;

                $insertArray = [
                    "rfq_id" => $rfqId,
                    "factory_id" => isset($factory["id"]) ? $factory["id"] : null,
                    "currency_id" => isset($currency["id"]) ? $currency["id"] : null,
                    "last_production_date" => isset($production["lastProductionDate"]) ? date("Y-m-d", strtotime($production["lastProductionDate"])) : null,
                    "std_cost_roll_up_material" => isset($production["stdCostRollUpMaterial"]) ? $production["stdCostRollUpMaterial"] : null,
                    "std_cost_roll_up_labour_time" => isset($production["stdCostRollUpLabourTime"]) ? $production["stdCostRollUpLabourTime"] : null,
                    "std_cost_roll_up_labour_euro" => isset($production["stdCostRollUpLabourEuro"]) ? $production["stdCostRollUpLabourEuro"] : null,
                    "std_cost_roll_up_total_euro" => isset($production["stdCostRollUpTotalEuro"]) ? $production["stdCostRollUpTotalEuro"] : null,
                    "so_price_factory_material_cost" => isset($production["soPriceFactoryMaterialCost"]) ? $production["soPriceFactoryMaterialCost"] : null,
                    "so_price_factory_labour_cost" => isset($production["soPriceFactoryLabourCost"]) ? $production["soPriceFactoryLabourCost"] : null,
                    "so_price_factory_cost" => isset($production["soPriceFactoryCost"]) ? $production["soPriceFactoryCost"] : null,
                    "roll_up_material_estimated_material_cost" => isset($production["rollUpMaterialEstimatedMaterialCostEuro"]) ? $production["rollUpMaterialEstimatedMaterialCostEuro"] : null,
                    "roll_up_labour_estimated_labour_time" => isset($production["rollUpLabourEstimatedLabourTimeEuro"]) ? $production["rollUpLabourEstimatedLabourTimeEuro"] : null,
                    "roll_up_labour_estimated_labour_cost" => isset($production["rollUpLabourEstimatedLabourCostEuro"]) ? $production["rollUpLabourEstimatedLabourCostEuro"] : null,
                    "created_at" => new \DateTime(),
                    "updated_at" => new \DateTime()
                ];

                DB::table("rfq_production")->insert($insertArray);
            }
            return $this->resultService->Success(["success" => true]);
        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->addProduction", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function editProduction($production)
    {
        try{
            if(!is_null($production) && !empty($production))
            {
                $factory = isset($production["factory"]) ? $production["factory"] : null;
                $currency = isset($production["currency"]) ? $production["currency"] : null;

                $updateArray = [
                    "factory_id" => isset($factory["id"]) ? $factory["id"] : null,
                    "currency_id" => isset($currency["id"]) ? $currency["id"] : null,
                    "last_production_date" => isset($production["lastProductionDate"]) ? date("Y-m-d", strtotime($production["lastProductionDate"])) : null,
                    "std_cost_roll_up_material" => isset($production["stdCostRollUpMaterial"]) ? $production["stdCostRollUpMaterial"] : null,
                    "std_cost_roll_up_labour_time" => isset($production["stdCostRollUpLabourTime"]) ? $production["stdCostRollUpLabourTime"] : null,
                    "std_cost_roll_up_labour_euro" => isset($production["stdCostRollUpLabourEuro"]) ? $production["stdCostRollUpLabourEuro"] : null,
                    "std_cost_roll_up_total_euro" => isset($production["stdCostRollUpTotalEuro"]) ? $production["stdCostRollUpTotalEuro"] : null,
                    "so_price_factory_material_cost" => isset($production["soPriceFactoryMaterialCost"]) ? $production["soPriceFactoryMaterialCost"] : null,
                    "so_price_factory_labour_cost" => isset($production["soPriceFactoryLabourCost"]) ? $production["soPriceFactoryLabourCost"] : null,
                    "so_price_factory_cost" => isset($production["soPriceFactoryCost"]) ? $production["soPriceFactoryCost"] : null,
                    "roll_up_material_estimated_material_cost" => isset($production["rollUpMaterialEstimatedMaterialCostEuro"]) ? $production["rollUpMaterialEstimatedMaterialCostEuro"] : null,
                    "roll_up_labour_estimated_labour_time" => isset($production["rollUpLabourEstimatedLabourTimeEuro"]) ? $production["rollUpLabourEstimatedLabourTimeEuro"] : null,
                    "roll_up_labour_estimated_labour_cost" => isset($production["rollUpLabourEstimatedLabourCostEuro"]) ? $production["rollUpLabourEstimatedLabourCostEuro"] : null,
                    "updated_at" => new \DateTime()
                ];

                DB::table("rfq_production")->where('id',$production["id"])
                    ->update($updateArray);
            }
            return $this->resultService->Success(["success" => true]);
        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->editProduction", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function deleteProduction($id)
    {
        try{
            if( !empty($id)) {
                DB::table("rfq_production")
                    ->where('id', $id)->delete();
            }
            return $this->resultService->Success(["success" => true]);
        }catch(Exception $e){
            $this->logService->log("ERROR RfqService->addProduction", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
  
}
