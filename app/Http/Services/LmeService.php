<?php

namespace App\Http\Services;

/**
 * Description of LmeService
 *
 * @author Juraj Marcin
 */

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\LME;
use DB;

class LmeService
{


    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function all()
    {
        try {

            $lme = LME::get()->toArray();
            return $this->resultService->Success($lme);

        } catch (Exception $e) {
            $this->logService->log("ERROR LmeService->all", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function update($id,$cu,$al)
    {
        try {
            $lme = LME::findOrFail($id);
            if($lme) {
                $lme->cu = $cu;
                $lme->al = $al;
                $lme->save();
            }
            return $this->resultService->Success(
                (object)[
                    "Ok" => "OK",
                ]);

        } catch (Exception $e) {
            $this->logService->log("ERROR LmeService->update", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}
