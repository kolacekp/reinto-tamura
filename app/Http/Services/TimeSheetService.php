<?php

namespace App\Http\Services;

use App\Currency;
use App\Customer;
use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\TimeSheet;
use App\RFQ;
use App\Project;
use App\Person;
use App\TamuraEntity;

use DB;


class TimeSheetService
{

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function getSisterCompanies(){
        try {
            $rfqIds = TimeSheet::select('rfq_id')->get();
            $rfqs = [];
            $projects = [];
            $persons = [];
            $tamuraEntity = [];

            foreach ($rfqIds as $rfq){
                $rfgItem = RFQ::select('project_id')->where('id','=',$rfq['rfq_id'])->get();
                array_push($rfqs,$rfgItem);
            }

            foreach ($rfqs as $rfq){
                $pushManager = true;
                if (count($rfq) > 0){
                    $project = Project::select('manager_id')->where('id', '=',$rfq[0]['project_id'])->get();
                    foreach($projects as $prj){
                        if ($prj[0]['manager_id'] == $project[0]['manager_id']){
                            $pushManager = false;
                            break;
                        }
                    }
                    if ($pushManager)
                        array_push($projects,$project);
                }
            }

            foreach ($projects as $project){
                if (count($project) > 0){
                    $person = Person::select('tamura_entity_id')->where('id', '=',$project[0]['manager_id'])->get();
                    array_push($persons,$person);
                }
            }

            foreach ($persons as $person){
                if (count($person) > 0){
                    $entity = TamuraEntity::where('id', '=',$person[0]['tamura_entity_id'])->get();
                    array_push($tamuraEntity,$entity);
                }
            }
            return $this->resultService->Success($tamuraEntity);

        } catch (Exception $e) {
            $this->logService->log("ERROR TimeSheetService->getSisterCompanies", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getCustomersByEntityId($EntityId){
        try {
            $personId = Person::select('customer_id')->where('tamura_entity_id', '=',$EntityId)
                ->whereNotNull('customer_id')->get();
            $customers = [];
            foreach ($personId as $person){
                if (count($person) > 0){
                    $customer = Customer::select('id','name')->where('id', '=',$person['customer_id'])->get();
                    array_push($customers,$customer);
                }
            }
            return $this->resultService->Success($customers);

        } catch (Exception $e) {
            $this->logService->log("ERROR TimeSheetService->getCustomersByEntityId", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getProjectRfqByCustomerId($customerId){
        try {
            $projects = Project::where('customer_id','=',$customerId)
                ->join('rfqs', 'rfqs.project_id', '=', 'projects.id')
                ->select('projects.id as projectId','projects.name','rfqs.id as rfqsId','rfqs.pn_customer')
                ->get();
            return $this->resultService->Success($projects);

        } catch (Exception $e) {
            $this->logService->log("ERROR TimeSheetService->getProjectRfqByCustomerId", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function geTimeSheetByRfqId($rfqId){
        try {
            $timeSheets = RFQ::where('rfqs.id','=',$rfqId)
                ->join('time_sheets', 'rfqs.id', '=', 'time_sheets.rfq_id')
                ->selectRAW('rfqs.id as rfqId,rfqs.pn_customer,time_sheets.hours,time_sheets.action,
                    DATE_FORMAT(time_sheets.date, "%d/%l/%Y") as date')
                ->get();
            return $this->resultService->Success($timeSheets);

        } catch (Exception $e) {
            $this->logService->log("ERROR TimeSheetService->geTimeSheetByRfqId", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getByRfq($rfqId)
    {
        try{
            if(empty($rfqId)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }

            $timeSheets = TimeSheet::with(['engineer'])
                            ->where("rfq_id", $rfqId)
                            ->get();

            return $this->resultService->Success($timeSheets);

        }catch (Exception $e) {
            $this->logService->log("ERROR TimeSheetService->getByRfq", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function create($rfqId, $createdBy, $tamuraEngineerId, $date, $action, $hours)
    {
        try{
            if(empty($rfqId)){
                return $this->resultService->Error("RFQ id cannot be empty");
            }

            $timeSheet = new TimeSheet();
            $timeSheet->rfq_id = $rfqId;
            $timeSheet->created_by = $createdBy;
            $timeSheet->creation_date = new \DateTime();
            $timeSheet->tamura_engineer_id = $tamuraEngineerId;
            $timeSheet->date = new \DateTime($date);
            $timeSheet->hours = $hours;
            $timeSheet->action = $action;
            $timeSheet->created_at = new \DateTime();

            $res = $timeSheet->save();

            return $this->resultService->Success($res);

        }catch (Exception $e) {
            $this->logService->log("ERROR TimeSheetService->create", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }


}
