<?php
namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Http\Services\AppliParamService;
use App\Http\Services\CurrencyService;
use App\Sale;
use App\TamuraEntity;

use DB;

class SaleService
{
    private $resultService;
    private $logService;
    private $applyParamService;
    private $currencyService;

    const PALLET_CONTAINER_VALUE = 3;
    const SALES_ADVANCE_FEES = "Sales_AdvanceFees";
    const SALES_LFR = "Sales_LFR";
    const SALES_UNLOADING = "Sales_Unloading";
    const SALES_ORDER_PICKING = "Sales_OrderPicking";
    const SALES_STORAGE = "Sales_Storage";
    const SALES_ADMIN = "Sales_Admin";
    const PERCENT = 100;

    public function __construct(ResultService $resultService, LogService $logService, AppliParamService $applyParamService, CurrencyService $currencyService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
        $this->applyParamService = $applyParamService;
        $this->currencyService = $currencyService;
    }

    public function getSalesBtNPI($NPIid){
        try{
            if (empty($NPIid)) {
                return $this->resultService->Error("NPI id cannot be empty");
            }

            $sales = Sale::where('npi_id', '=',$NPIid )->get();
            return $this->resultService->Success($sales);

        }catch(Exception $e){
            $this->logService->log("ERROR SaleService->getSalesBtNPI", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function findOrCreateSale($rfq,$npi,$createdBy){
        try{

            if(isset($rfq["pallet_quantity"]) && $rfq["pallet_quantity"] > 0)
                $palletQty = $rfq["pallet_quantity"] ;
            else
                $palletQty = 1;

            $appliParams = $this->applyParamService->getAllAppliParams();
            $salesAdvanceFees = 0;
            $salesLFR = 0;
            $salesUnloading = 0;
            $salesOrderPicking = 0;
            $salesAdmin = 0;
            $salesStorage = 0;

            foreach ($appliParams as $param){
                if($param->name == self::SALES_ADVANCE_FEES){
                    $salesAdvanceFees = doubleval($param->value);
                }
                if($param->name  == self::SALES_LFR){
                    $salesLFR = doubleval($param->value);
                }
                if($param->name  == self::SALES_UNLOADING){
                    $salesUnloading = doubleval($param->value);
                }
                if($param->name == self::SALES_ORDER_PICKING){
                    $salesOrderPicking = doubleval($param->value);
                }
                if($param->name  == self::SALES_STORAGE){
                    $salesAdmin = doubleval($param->value);
                }
                if($param->name  == self::SALES_ADMIN){
                    $salesStorage = doubleval($param->value);
                }
            }

            $palletPerContainer = self::PALLET_CONTAINER_VALUE ; //Because default value is LCL

            $sale = new Sale;
            $sale->npi_id = isset($npi["id"]) ? $npi["id"] : null;
            $sale->buy_price_engineering = isset($npi["rfq_selling_price_sales_office"]) ? $npi["rfq_selling_price_sales_office"] : null;
            $sale->buy_price_factory = isset($npi["selling_price_sales_office"]) ? $npi["selling_price_sales_office"] : null;
            $sale->buy_price = ($sale->buy_price_engineering > $sale->buy_price_factory)? round($sale->buy_price_engineering,3) : round($sale->buy_price_factory,3);
            $sale->cif_cost =  $sale->buy_price  ;
            $sale->tamura_entity_fob_charge = 0;
            $sale->tamura_entity_fob_charge_value = 0;
            $sale->advance_fees = !empty($salesAdvanceFees)? round($salesAdvanceFees,3) : 0;
            $sale->advance_fees_value = 0;
            $sale->lfr = round($salesLFR,3);
            $sale->lfr_value = !empty($salesLFR)? round($salesLFR / ($palletQty * $palletPerContainer),3) : 0;
            $sale->valid = 1;
            $sale->pallet_qty = $palletQty;
            $sale->import_duty = 0;
            $sale->import_duty_value = 0;
            $sale->delivery_to_customer = 0;
            $sale->delivery_to_customer_value = 0;
            $sale->unloading = !empty($salesUnloading)? round(($salesUnloading  / self::PERCENT ), 3) : 0;
            $sale->order_picking = !empty($salesOrderPicking)? $salesOrderPicking  / self::PERCENT : 0;
            $sale->storage = !empty($salesStorage)? round(($salesStorage  / self::PERCENT),3) : 0;
            $sale->admin = !empty($salesAdmin)? round(($salesAdmin  / self::PERCENT),3) : 0;
            $sale->unloading_value = !empty($salesUnloading)? round(($salesUnloading  / $palletQty),3) : 0;
            $sale->order_picking_value = !empty($salesOrderPicking)? round(($salesOrderPicking  / $palletQty),3) : 0;
            $sale->storage_value = !empty($salesStorage)? round(($salesStorage  / $palletQty),3) : 0;
            $sale->admin_value = !empty($salesAdmin)? round(($salesAdmin  / $palletQty),3) : 0;
            $sale->total_hub_costs = round(($sale->unloading_value + $sale->order_picking_value + $sale->storage_value + $sale->admin_value),3);
            $sale->delivery_to_customer_value = 0;
            $sale->total_inland_charges_value = round(($sale->advance_fees_value + $sale->lfr_value),3);
            $sale->total_oversea_costs_value = round(($sale->cif_cost + $sale->total_hub_costs + $sale->total_inland_charges_value),3) ;
            $sale->total_oversea_cost_flag = 0;
            $sale->tamura_entity_freight = 0;
            $sale->tamura_entity_freight_value = 0;
            $sale->tamura_entity_terminal_handling = 0;
            $sale->tamura_entity_terminal_handling_value = 0;
            $sale->tamura_entity_delivery_order = 0;
            $sale->tamura_entity_delivery_order_value = 0;
            $sale->tamura_entity_customs_clearance = 0;
            $sale->tamura_entity_customs_clearance_value = 0;
            $sale->tamura_entity_haulage_to_warehouse = 0;
            $sale->tamura_entity_haulage_to_warehouse_value = 0;

            $sale->created_by = $createdBy;
            $sale->creation_date = date('Y-m-d H:i:s', strtotime("now"));

            if (!empty($npi["tamura_entity_id_sales_office"])) {
                $saleOffice = TamuraEntity::where('id', '=',$npi["tamura_entity_id_sales_office"] )->first();
                $sale->currency_id = isset($saleOffice["currency_id"]) ? $saleOffice["currency_id"] : null;
                $sale->tamura_entity_id_sales_office = isset($saleOffice["id"]) ? $saleOffice["id"] : null;
                $sale->tamura_entity_fob_charge = isset($saleOffice["fob_charge"]) ? $saleOffice["fob_change"] : 0;
                $sale->tamura_entity_fob_charge_value = isset($saleOffice["fob_charge"]) ? $saleOffice["fob_change"] : 0;
                $sale->tamura_entity_freight = isset($saleOffice["freight"]) ? $saleOffice["freight"] : 0;
                $sale->tamura_entity_freight_value = isset($saleOffice["freight"]) ? $saleOffice["freight"] : 0;
                $sale->tamura_entity_terminal_handling = isset($saleOffice["terminal_handling"]) ? $saleOffice["terminal_handling"] : 0;
                $sale->tamura_entity_terminal_handling_value = isset($saleOffice["terminal_handling"]) ? round(($saleOffice["terminal_handling"] / $palletQty),3) : 0;
                $sale->tamura_entity_delivery_order = isset($saleOffice["delivery_order"]) ? $saleOffice["delivery_order"] : 0;
                $sale->tamura_entity_delivery_order_value = isset($saleOffice["delivery_order"]) ? round(($saleOffice["delivery_order"] / $palletQty),3) : 0;
                $sale->tamura_entity_customs_clearance = isset($saleOffice["customs_clearance"]) ? $saleOffice["customs_clearance"] : 0;
                $sale->tamura_entity_customs_clearance_value = isset($saleOffice["customs_clearance"]) ? round(($saleOffice["customs_clearance"] / ($palletQty * $palletPerContainer)),3) : 0;
                $sale->tamura_entity_haulage_to_warehouse = isset($saleOffice["haulage_to_warehouse"]) ? $saleOffice["haulage_to_warehouse"] : 0;
                $sale->tamura_entity_haulage_to_warehouse_value = isset($saleOffice["haulage_to_warehouse"]) ? round(($saleOffice["haulage_to_warehouse"] / ($palletQty * $palletPerContainer)),3) : 0;
                $sale->cif_cost =  $sale->buy_price + $sale->tamura_entity_fob_charge_value + $sale->tamura_entity_freight_value ;
                $sale->total_inland_charges_value = $sale->advance_fees_value + $sale->lfr_value + $sale->tamura_entity_terminal_handling_value
                    + $sale->tamura_entity_delivery_order_value + $sale->tamura_entity_customs_clearance_value + $sale->tamura_entity_haulage_to_warehouse_value;
                $sale->total_oversea_costs_value = $sale->cif_cost + $sale->total_hub_costs + $sale->total_inland_charges_value;

                $gbp = $this->currencyService->getByShortName('GBP');
                $actCurrency =  $this->currencyService->getById($sale->currency_id);

                $sale->unloading_value =   ($sale->unloading / $actCurrency['exch_rate'] * $gbp['exch_rate']) / $sale->pallet_qty;
                $sale->order_picking_value = ($sale->order_picking / $actCurrency['exch_rate'] * $gbp['exch_rate']) / $sale->pallet_qty;
                $sale->storage_value =($sale->storage / $actCurrency['exch_rate'] * $gbp['exch_rate']) / $sale->pallet_qty;
                $sale->admin_value = ($sale->storage / $actCurrency['exch_rate'] * $gbp['exch_rate']) / $sale->pallet_qty;
            }

            $sale->total_oversea_costs = round((($sale->total_oversea_costs_value - $sale->cif_cost ) / $sale->buy_price),3) ;
            $sale->cost_price = $sale->total_oversea_costs_value + $sale->import_duty + $sale->delivery_to_customer;
            $sale->cost_price_percentage = round((($sale->cost_price - $sale->cif_cost) / $sale->buy_price),3);
            $sale->gand_a = 0;
            $sale->gand_a_value = 0;
            $sale->total_cost = $sale->cost_price + $sale->grand_a_value ;
            $sale->total_cost_percentage = round((($sale->total_cost - $sale->cif_cost) / $sale->buy_price),3);
            $sale->profit = 0;
            $sale->selling_price = round(($sale->total_cost /(1 - $sale->profit )),3);
            $sale->profit_value = $sale->selling_price - $sale->total_cost;
            $sale->fcl_lcl = "LCL";
            $sale->lme_cu = 0;
            $sale->lme_cu_at_creation = 0;
            $sale->lme_al = 0;
            $sale->lme_al_at_creation = 0;


            $sale->save();

            return $this->resultService->Success($sale);

        }catch(Exception $e){
            $this->logService->log("ERROR SaleService->createSale", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getSaleById($id){
        try{
            if (empty($id)) {
                return $this->resultService->Error("NPI id cannot be empty");
            }

            $sale = Sale::with(["saleOffice"])->where('id', $id )->first();
            return $this->resultService->Success($sale);

        }catch(Exception $e){
            $this->logService->log("ERROR SaleService->getSalesBtNPI", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateSale($sale,$npi){
        try{
            if (empty($sale["id"])) {
                $sale = new Sale();
                $sale->save();
                $sale["id"] = $sale->id;
            }

            $sale = Sale::where('id', $sale["id"] )
                ->update([
                'valid' => !empty($sale["valid"]) ? $sale["valid"] : -1,
                'valid_last_update_date' => date('Y-m-d H:i:s', strtotime("now")),
                'currency_id' => isset($sale["currency"]["id"]) ? $sale["currency"]["id"] : null,
                'sales_manager_id' => isset($sale["sale_manager"]["id"]) ? $sale["sale_manager"]["id"] : null,
                'tamura_entity_id_sales_office' => isset($sale["sale_office"]["id"]) ? $sale["sale_office"]["id"] : null,
                'buy_price_engineering' => isset($sale["buy_price_engineering"]) ? $sale["buy_price_engineering"] : null,
                'buy_price_factory' => isset($sale["buy_price_factory"]) ? $sale["buy_price_factory"] : null,
                'buy_price' => isset($sale["buy_price"]) ? $sale["buy_price"] : null,
                'tamura_entity_fob_charge' => isset($sale["tamura_entity_fob_charge"]) ? $sale["tamura_entity_fob_charge"] : null,
                'tamura_entity_fob_charge_value' => isset($sale["tamura_entity_fob_charge_value"]) ? $sale["tamura_entity_fob_charge_value"] : null,
                'tamura_entity_freight' => isset($sale["tamura_entity_freight"]) ? $sale["tamura_entity_freight"] : null,
                'tamura_entity_freight_value' => isset($sale["tamura_entity_freight_value"]) ? $sale["tamura_entity_freight_value"] : null,
                'cif_cost' => isset($sale["cif_cost"]) ? $sale["cif_cost"] : null,
                'tamura_entity_terminal_handling' => isset($sale["tamura_entity_terminal_handling"]) ? $sale["tamura_entity_terminal_handling"] : null,
                'tamura_entity_terminal_handling_value' => isset($sale["tamura_entity_terminal_handling_value"]) ? $sale["tamura_entity_terminal_handling_value"] : null,
                'tamura_entity_delivery_order' => isset($sale["tamura_entity_delivery_order"]) ? $sale["tamura_entity_delivery_order"] : null,
                'tamura_entity_delivery_order_value' => isset($sale["tamura_entity_delivery_order_value"]) ? $sale["tamura_entity_delivery_order_value"] : null,
                'tamura_entity_customs_clearance' => isset($sale["tamura_entity_customs_clearance"]) ? $sale["tamura_entity_customs_clearance"] : null,
                'tamura_entity_customs_clearance_value' => isset($sale["tamura_entity_customs_clearance_value"]) ? $sale["tamura_entity_customs_clearance_value"] : null,
                'advance_fees' => isset($sale["advance_fees"]) ? $sale["advance_fees"] : null,
                'advance_fees_value' => isset($sale["advance_fees_value"]) ? $sale["advance_fees_value"] : null,
                'lfr' => isset($sale["lfr"]) ? $sale["lfr"] : null,
                'lfr_value' => isset($sale["lfr_value"]) ? $sale["lfr_value"] : null,
                'tamura_entity_haulage_to_warehouse' => isset($sale["tamura_entity_haulage_to_warehouse"]) ? $sale["tamura_entity_haulage_to_warehouse"] : null,
                'tamura_entity_haulage_to_warehouse_value' => isset($sale["tamura_entity_haulage_to_warehouse_value"]) ? $sale["tamura_entity_haulage_to_warehouse_value"] : null,
                'total_inland_charges_value' => isset($sale["total_inland_charges_value"]) ? $sale["total_inland_charges_value"] : null,
                'unloading' => isset($sale["unloading"]) ? $sale["unloading"] : null,
                'unloading_value' => isset($sale["unloading_value"]) ? $sale["unloading_value"] : null,
                'order_picking' => isset($sale["order_picking"]) ? $sale["order_picking"] : null,
                'order_picking_value' => isset($sale["order_picking_value"]) ? $sale["order_picking_value"] : null,
                'storage' => isset($sale["storage"]) ? $sale["storage"] : null,
                'storage_value' => isset($sale["storage_value"]) ? $sale["storage_value"] : null,
                'admin' => isset($sale["admin"]) ? $sale["admin"] : null,
                'admin_value' => isset($sale["admin_value"]) ? $sale["admin_value"] : null,
                'total_hub_costs' => isset($sale["total_hub_costs"]) ? $sale["total_hub_costs"] : null,
                'total_oversea_costs' => isset($sale["total_oversea_costs"]) ? $sale["total_oversea_costs"] : null,
                'total_oversea_costs_value' => isset($sale["total_oversea_costs_value"]) ? $sale["total_oversea_costs_value"] : null,
                'total_oversea_cost_flag' => isset($sale["total_oversea_cost_flag"]) ? $sale["total_oversea_cost_flag"] : 0,
                'import_duty' => isset($sale["import_duty"]) ? $sale["import_duty"] : 0,
                'import_duty_value' => isset($sale["import_duty_value"]) ? $sale["import_duty_value"] : 0,
                'delivery_to_customer' => isset($sale["delivery_to_customer"]) ? $sale["delivery_to_customer"] : 0,
                'delivery_to_customer_value' => isset($sale["delivery_to_customer_value"]) ? $sale["delivery_to_customer_value"] : 0,
                'cost_price' => isset($sale["cost_price"]) ? $sale["cost_price"] : null,
                'cost_price_percentage' => isset($sale["cost_price_percentage"]) ? $sale["cost_price_percentage"] : null,
                'gand_a' => isset($sale["gand_a"]) ? $sale["gand_a"] : null,
                'gand_a_value' => isset($sale["gand_a_value"]) ? $sale["gand_a_value"] : null,
                'total_cost' => isset($sale["total_cost"]) ? $sale["total_cost"] : null,
                'total_cost_percentage' => isset($sale["total_cost_percentage"]) ? $sale["total_cost_percentage"] : null,
                'profit' => isset($sale["profit"]) ? $sale["profit"] : null,
                'profit_value' => isset($sale["profit_value"]) ? $sale["profit_value"] : null,
                'selling_price' => isset($sale["selling_price"]) ? $sale["selling_price"] : null,
                'comment' => isset($sale["comment"]) ? $sale["comment"] : "",
                'special_req' => isset($sale["special_req"]) ? $sale["special_req"] : "",
                'incoterms_id' => isset($sale["incoterms_id"]) ? $sale["incoterms_id"] : null,
                'last_update_date' => date('Y-m-d H:i:s', strtotime("now")),
                'pallet_qty' => isset($sale["pallet_qty"]) ? $sale["pallet_qty"] : null,
                'customer_id' =>isset($sale["customer"]["id"]) ? $sale["customer"]["id"] : null,
                'customer_payment_term_nr' => isset($sale["customer_payment_term_nr"]) ? $sale["customer_payment_term_nr"] : null,
                'customer_payment_term_type' => isset($sale["customer_payment_term_type"]) ? $sale["customer_payment_term_type"] : null,
                'purchaser_id' => isset($sale["purchase_manager"]["id"]) ? $sale["purchase_manager"]["id"] : null,
                'fcl_lcl' => isset($sale["fcl_lcl"]) ? $sale["fcl_lcl"] : null,
                'lme_cu' => isset($sale["lme_cu"]) ? $sale["lme_cu"] : null,
                'lme_cu_at_creation' => isset($sale["lme_cu_at_creation"]) ? $sale["lme_cu_at_creation"] : null,
                'lme_al' => isset($sale["lme_al"]) ? $sale["lme_al"] : null,
                'lme_al_at_creation' => isset($sale["lme_al_at_creation"]) ? $sale["lme_al_at_creation"] : null,
                'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
            ]);


            return $this->resultService->Success($sale);

        }catch(Exception $e){
            $this->logService->log("ERROR SaleService->updateSale", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function changeSaleOffice($sale_office,$sale){
        try{
            if (empty($sale["id"])) {
                return $this->resultService->Error("Sale id cannot be empty");
            }

            if (empty($sale_office["id"])) {
                return $this->resultService->Error("Sale office id cannot be empty");
            }
            $palletPerContainer = self::PALLET_CONTAINER_VALUE ;

            $saleOffice = TamuraEntity::where('id',$sale_office["id"])->first();

            $sale = Sale::where('id', $sale["id"] )->first();
            $palletQty = $sale["pallet_qty"];
            $sale->update([
                'currency_id' => isset($saleOffice["currency_id"]) ? $saleOffice["currency_id"] : 0,
                'tamura_entity_id_sales_office' => isset($saleOffice["id"]) ? $saleOffice["id"] : 0,
                'tamura_entity_fob_charge' => isset($saleOffice["tamura_entity_fob_charge"]) ? $saleOffice["tamura_entity_fob_charge"] : 0,
                'tamura_entity_fob_charge_value' => isset($saleOffice["tamura_entity_fob_charge"]) ? $saleOffice["tamura_entity_fob_charge"] : 0,
                'tamura_entity_freight' => isset($saleOffice["tamura_entity_freight"]) ? $saleOffice["tamura_entity_freight"] : 0,
                'tamura_entity_freight_value' => isset($saleOffice["tamura_entity_freight"]) ? $saleOffice["tamura_entity_freight"] : 0,
                'tamura_entity_terminal_handling' => isset($saleOffice["tamura_entity_terminal_handling"]) ? $saleOffice["tamura_entity_terminal_handling"]: 0,
                'tamura_entity_terminal_handling_value' => isset($saleOffice["tamura_entity_terminal_handling"]) ? $saleOffice["tamura_entity_terminal_handling"]  / $palletQty  : 0,
                'tamura_entity_delivery_order' => isset($saleOffice["tamura_entity_delivery_order"]) ? $saleOffice["tamura_entity_delivery_order"] : 0,
                'tamura_entity_delivery_order_value' => isset($saleOffice["tamura_entity_delivery_order"]) ? $saleOffice["tamura_entity_delivery_order"]  / $palletQty : 0,
                'tamura_entity_customs_clearance' => isset($saleOffice["tamura_entity_customs_clearance"]) ? $saleOffice["tamura_entity_customs_clearance"] : 0,
                'tamura_entity_customs_clearance_value' => isset($saleOffice["tamura_entity_customs_clearance_value"]) ? $saleOffice["tamura_entity_customs_clearance_value"] / ($palletQty * $palletPerContainer) : 0,
                'tamura_entity_haulage_to_warehouse' => isset($saleOffice["tamura_entity_haulage_to_warehouse"]) ? $saleOffice["tamura_entity_haulage_to_warehouse"] : 0,
                'tamura_entity_haulage_to_warehouse_value' => isset($saleOffice["tamura_entity_haulage_to_warehouse_value"]) ? $saleOffice["tamura_entity_haulage_to_warehouse_value"] / ($palletQty * $palletPerContainer): 0,
                'cif_cost' => isset($sale["cif_cost"]) ? $sale["cif_cost"] : 0,
                'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
            ]);
            return $sale;

        }catch(Exception $e){
            $this->logService->log("ERROR SaleService->changeSaleOffice", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}