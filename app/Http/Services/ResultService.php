<?php

namespace App\Http\Services;

use Response;

class ResultService
{
   
    public function Success($data)
    {
        return Response::json($data, 200);
    }
    
    public function Error($message)
    {
        return Response::json(array('error' =>  $message), 400);
    }
      
}
