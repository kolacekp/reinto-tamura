<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\StandardItem;

use DB;

class StdItemService
{

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function getStdItems()
    {
        try{

            $stdItems = StandardItem::select(
                  "id"
                , "category"
                , "design_office"
                , "factory"
                , "sales_office"
                , "tam_id"
                , "part_number"
                , "description"
                , "box_quantity"
                , "carton_quantity"
                , "pallet_quantity"
                , "unit_weight"
                , "currency"
                , "buy_price_fca_hk"
                , "comment")
                        ->orderBy("category")
                        ->get();

            return $this->resultService->Success($stdItems);

        }catch(Exception $e){
            $this->logService->log("ERROR StdItemService->getStdItems", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function create($stdItem)
    {
        try{

            if(!empty($stdItem)){

                $category = isset($stdItem["category"]) ? $stdItem["category"] : null;
                $designOffice = isset($stdItem["designOffice"]) ? $stdItem["designOffice"] : null;
                $factory = isset($stdItem["factory"]) ? $stdItem["factory"] : null;
                $salesOffice = isset($stdItem["salesOffice"]) ? $stdItem["salesOffice"] : null;
                $tamId = isset($stdItem["tamId"]) ? $stdItem["tamId"] : null;
                $partNumber = isset($stdItem["partNumber"]) ? $stdItem["partNumber"] : null;
                $description = isset($stdItem["description"]) ? $stdItem["description"] : null;
                $boxQuantity = isset($stdItem["boxQuantity"]) ? $stdItem["boxQuantity"] : null;
                $cartonQuantity = isset($stdItem["cartonQuantity"]) ? $stdItem["cartonQuantity"] : null;
                $palletQuantity = isset($stdItem["palletQuantity"]) ? $stdItem["palletQuantity"] : null;
                $unitWeight = isset($stdItem["unitWeight"]) ? $stdItem["unitWeight"] : null;
                $currency = isset($stdItem["currency"]) ? $stdItem["currency"] : null;
                $buyPriceFcaHk = isset($stdItem["buyPriceFcaHk"]) ? $stdItem["buyPriceFcaHk"] : null;
                $comment = isset($stdItem["comment"]) ? $stdItem["comment"] : null;

                $stdItem = new StandardItem();
                $stdItem->category = $category;
                $stdItem->design_office = $designOffice;
                $stdItem->factory = $factory;
                $stdItem->sales_office = $salesOffice;
                $stdItem->tam_id = $tamId;
                $stdItem->part_number = $partNumber;
                $stdItem->description = $description;
                $stdItem->box_quantity = $boxQuantity;
                $stdItem->carton_quantity = $cartonQuantity;
                $stdItem->pallet_quantity = $palletQuantity;
                $stdItem->unit_weight = $unitWeight;
                $stdItem->currency = $currency;
                $stdItem->buy_price_fca_hk = $buyPriceFcaHk;
                $stdItem->comment = $comment;

                $stdItem->save();

                return $this->resultService->Success($stdItem);


            }

        }catch(Exception $e){
            $this->logService->log("ERROR StdItemService->create", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }

    }

    public function update($stdItem)
    {
        try{

            if(!empty($stdItem)){

                $id = isset($stdItem["id"]) ? $stdItem["id"] : null;
                $category = isset($stdItem["category"]) ? $stdItem["category"] : null;
                $designOffice = isset($stdItem["design_office"]) ? $stdItem["design_office"] : null;
                $factory = isset($stdItem["factory"]) ? $stdItem["factory"] : null;
                $salesOffice = isset($stdItem["sales_office"]) ? $stdItem["sales_office"] : null;
                $tamId = isset($stdItem["tamId"]) ? (double)$stdItem["tamId"] : null;
                $partNumber = isset($stdItem["part_number"]) ? $stdItem["part_number"] : null;
                $description = isset($stdItem["description"]) ? $stdItem["description"] : null;
                $boxQuantity = isset($stdItem["box_quantity"]) ? (double)$stdItem["box_quantity"] : null;
                $cartonQuantity = isset($stdItem["carton_quantity"]) ? (double)$stdItem["carton_quantity"] : null;
                $palletQuantity = isset($stdItem["pallet_quantity"]) ? (double)$stdItem["pallet_quantity"] : null;
                $unitWeight = isset($stdItem["unit_weight"]) ? (double)$stdItem["unit_weight"] : null;
                $currency = isset($stdItem["currency"]) ? $stdItem["currency"] : null;
                $buyPriceFcaHk = isset($stdItem["buy_price_fca_hk"]) ? (double)$stdItem["buy_price_fca_hk"] : null;
                $comment = isset($stdItem["comment"]) ? $stdItem["comment"] : null;

                $res = StandardItem::where("id", $id)
                    ->update([
                        "category" => $category,
                        "design_office" => $designOffice,
                        "factory" => $factory,
                        "sales_office" => $salesOffice,
                        "tam_id" => $tamId,
                        "part_number" => $partNumber,
                        "description" => $description,
                        "box_quantity" => $boxQuantity,
                        "carton_quantity" => $cartonQuantity,
                        "pallet_quantity" => $palletQuantity,
                        "unit_weight" => $unitWeight,
                        "currency" => $currency,
                        "buy_price_fca_hk" => $buyPriceFcaHk,
                        "comment" => $comment,
                        "updated_at" => new \DateTime()
                    ]);

                return $this->resultService->Success($res);


            }

        }catch(Exception $e){
            $this->logService->log("ERROR StdItemService->update", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function delete($stdItemId)
    {
        try{

            if(!empty($stdItemId)){
                StandardItem::find($stdItemId)->delete();
            }

        }catch(Exception $e){
            $this->logService->log("ERROR StdItemService->delete", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }



}
