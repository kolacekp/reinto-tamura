<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Mail\MailToPerson;
use Illuminate\Support\Facades\Mail;

use DB;

class MailService
{

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function sendMailToPerson($to, $subject, $text)
    {
        try{
            if(empty($to)){
                return $this->resultService->Error("Recipient (to) cannot be empty");
            }
            if(empty($subject)){
                return $this->resultService->Error("Subject cannot be empty");
            }
            if(empty($text)){
                return $this->resultService->Error("Text cannot be empty");
            }

            Mail::to($to)->send(new MailToPerson($subject, $text));

        }catch(Exception $e){
            $this->logService->log("ERROR MailService->sendMailToPerson", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}
