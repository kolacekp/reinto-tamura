<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\User;
use App\Role;

use Illuminate\Support\Facades\Auth;
use stdClass;

use DB;
use App\Person;

class UserService
{

    private $resultService;
    private $logService;
            
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }
    
    public function login($emailOrUsername, $password)
    {
        try{
            
            if(empty($emailOrUsername)){
                return $this->resultService->Error("Email or username cannot be empty.");
            }
            if(empty($password)){
                return $this->resultService->Error("Password cannot be empty.");
            }
            if (Auth::attempt(['email' => $emailOrUsername, 'password' => $password, 'deactivated' => 0])) {
                $user = User::select('name', 'email', 'username','role_id')
                        ->with('role')
                        ->where("email", $emailOrUsername)
                        ->first();
                
                return $this->resultService->Success($user);                
            }elseif (Auth::attempt(['username' => $emailOrUsername, 'password' => $password, 'deactivated' => 0])) {
                $user = User::select('name', 'email', 'username','role_id')
                    ->with('role')
                    ->where("username", $emailOrUsername)
                    ->first();

                return $this->resultService->Success($user);
            }else{
                return $this->resultService->Error("This user doesn't exist.");
            }
            
        }catch(Exception $e){
            $this->logService->log("ERROR UserService->login", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function makeUsersFromPersons()
    {
        try{
            // at first - empty users table
            DB::table("users")->truncate();

            // get persons
            $persons = Person::join("tamura_entities as te", "te.id", "=", "persons.tamura_entity_id")
                ->select("persons.*")
                ->with('tamuraEntity')
                ->orderBy("te.short_name")
                ->get();

            foreach($persons as $p) {
                $user = new User();
                $user->name = $p->name;
                $user->email = $p->email;
                $user->username = $p->db_login;
                $user->password = !empty($p->db_password) ? bcrypt($p->db_password) : null;
                $user->created_at = new \DateTime();
                $user->save();
            }

            return $this->resultService->Success(true);


        }catch(Exception $e){
            $this->logService->log("ERROR UserService->makeUsersFromPersons", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateUser($user)
    {
        try{

            if(empty($user["id"])){
                return $this->resultService->Error("User Id cannot be empty");
            }

            if(!empty($user["role"])){
                $userRoleId = $user["role"]["id"];
            }
            else{
                $userRoleId = null;
            }

            $res = User::where("id", $user["id"])
                ->update([
                    'name' => !empty($user["name"]) ? $user["name"] : "",
                    'username' => !empty($user["username"]) ? $user["username"] : "",
                    'email' => !empty($user["email"]) ? $user["email"] : "",
                    'deactivated' =>!empty($user["deactivated"]) ? $user["deactivated"] : 0,
                    'role_id' => isset($userRoleId)? $userRoleId : 10,
                    'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
                ]);

            return $this->resultService->Success($res);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->createProject", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getUsers(){

        try{
            $user = User::with('role')
                    ->get();

            return $this->resultService->Success($user);

        }catch(Exception $e){
            $this->logService->log("ERROR UserService->login", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getRoles(){
        try{
            $roles = Role::get();
            return $this->resultService->Success($roles);

        }catch(Exception $e){
            $this->logService->log("ERROR UserService->login", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
    
}
