<?php

namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\TamuraCodification;
use App\TamuraEntity;

use DB;

class TamuraCodificationService
{

    const TAMURA_CODIFICATION_CODE_LIST_MARKET = 'MARKET';
    const TAMURA_CODIFICATION_CODE_LIST_GTC = 'GTC';
    
    const TAMURA_CODIFICATION_TYPE_PARENT = '1';
    const TAMURA_CODIFICATION_TYPE_PARENT2 = '2';
    const TAMURA_CODIFICATION_TYPE_PARENT3 = '3';
    const TAMURA_CODIFICATION_TYPE_CHILD = '4';

    const TAMURA_ENTITY_TYPE_DESIGN_OFFICE = 1000;
    const TAMURA_ENTITY_DEACTIVATED_NO = 0;
    const TAMURA_ENTITY_DEACTIVATED_YES = 1;
    
    private $resultService;
    private $logService;
            
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }
    
    public function getParentMarketEntities()
    {
        try{
    
            $parentMarketEntities = TamuraCodification::where('type_entity', self::TAMURA_CODIFICATION_TYPE_PARENT)
                                                        ->where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                                                        ->orderBy("entity")
                                                        ->get();
            
            return $this->resultService->Success($parentMarketEntities);
            
        }catch(Exception $e){
            $this->logService->log("ERROR TamuraCodificationService->getParentMarketEntities", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function getChildMarketEntities($parent)
    {
        try{
            if(empty($parent)){
                return $this->resultService->Error("Market parent entity cannot be empty.");
            }
            
            $childMarketEntities = TamuraCodification::where('type_entity', self::TAMURA_CODIFICATION_TYPE_CHILD)
                                                        ->where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                                                        ->where('entity_path', 'like', "%$parent%")
                                                        ->orderBy("entity_path")                                    
                                                        ->orderBy("entity")
                                                        ->get();
            
            return $this->resultService->Success($childMarketEntities);
            
            
        }catch(Exception $e){
            $this->logService->log("ERROR TamuraCodificationService->getChildMarketEntities", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
    
    public function getParentProductEntities()
    {
        try{
    
            $parentProductEntities = TamuraCodification::where('type_entity', self::TAMURA_CODIFICATION_TYPE_PARENT)
                                                        ->where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_GTC)
                                                        ->orderBy("entity")
                                                        ->get();
            
            return $this->resultService->Success($parentProductEntities);
            
        }catch(Exception $e){
            $this->logService->log("ERROR TamuraCodificationService->getParentProductEntities", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }      
    }
    
    public function getChildProductEntities($level, $parent)
    {
        try{
            if(is_null($level)){
                return $this->resultService->Error("Level cannot be empty.");
            }
            
            if(empty($parent)){
                return $this->resultService->Error("Project parent entity cannot be empty.");
            }

            
            $childProductEntities = TamuraCodification::where('type_entity', $level)
                                                        ->where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_GTC)
                                                        ->where('entity_path', 'like', "%$parent%")
                                                        ->orderBy("entity_path")                                    
                                                        ->orderBy("entity")
                                                        ->get();
            
            $childProducts = TamuraCodification::where('type_entity', self::TAMURA_CODIFICATION_TYPE_CHILD)
                                                        ->where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_GTC)
                                                        ->where('entity_path', $parent)
                                                        ->orderBy("entity_path")                                    
                                                        ->orderBy("entity")
                                                        ->get();
            
            $results = array(
                "childProductEntities" => $childProductEntities,
                "childProducts" => $childProducts
            );
            
            return $this->resultService->Success((object)$results);
            
            
        }catch(Exception $e){
            $this->logService->log("ERROR TamuraCodificationService->getChildProjectEntities", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
    
    public function getDesignOffices()
    {
        try {

            $designOffices = TamuraEntity::with('country')
                ->where("type", self::TAMURA_ENTITY_TYPE_DESIGN_OFFICE)
                ->where("deactivated", self::TAMURA_ENTITY_DEACTIVATED_NO)
                ->orderBy("location")
                ->get();

            return $this->resultService->Success($designOffices);


        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraCodificationService->getDesignOffices", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMarketParent($market)
    {
        try{
            //TODO :: Pre kazdu uroven ziskat Parenta a vratit ho
            $ParentEntities = null;
            if($market["type_entity"] == self::TAMURA_CODIFICATION_TYPE_CHILD  && $market["parent"] == null){
                if($market["no_entity_s"] != null) {
                    $ParentEntities = TamuraCodification::where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                        ->where('no_entity', 'like', $market["no_entity_s"])
                        ->first();

                }
                if ($ParentEntities != null && ($ParentEntities["type_entity"] == self::TAMURA_CODIFICATION_TYPE_PARENT2 || $ParentEntities["type_entity"] ==  self::TAMURA_CODIFICATION_TYPE_PARENT3)){
                    if($ParentEntities["no_entity_s"] != null) {
                        $market = $ParentEntities;
                        $ParentEntities = TamuraCodification::where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                            ->where('no_entity', 'like', $market["no_entity_s"])
                            ->first();

                    }
                }
                if ($ParentEntities != null && ($ParentEntities["type_entity"] == self::TAMURA_CODIFICATION_TYPE_PARENT2 || $ParentEntities["type_entity"] ==  self::TAMURA_CODIFICATION_TYPE_PARENT3)){
                    if($ParentEntities["no_entity_s"] != null) {
                        $market = $ParentEntities;
                        $ParentEntities = TamuraCodification::where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                            ->where('no_entity', 'like', $market["no_entity_s"])
                            ->first();

                    }
                }
            }

            return $this->resultService->Success($ParentEntities);

        }catch(Exception $e){
            $this->logService->log("ERROR TamuraCodificationService->getChildProjectEntities", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
}
