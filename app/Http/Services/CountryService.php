<?php

namespace App\Http\Services;

/**
 * Description of CountryService
 *
 * @author Petr Koláček
 */

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Country;
use DB;

class CountryService {
    
    private $resultService;
    private $logService;
                
    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }
    
    public function all()
    {
        try{
            
            $countries = Country::orderBy('name')->get()->toArray();
            return $this->resultService->Success($countries);
            
        }catch(Exception $e){
            $this->logService->log("ERROR CountryService->all", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }  
    }
    
}
