<?php

namespace App\Http\Services;

/**
 * Description of CurrencyService
 *
 * @author Petr Koláček
 */

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Currency;
use DB;

class CurrencyService
{

    const CURRENCY_DEFAULT = "X";

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }

    public function all()
    {
        try {
            $currencies = Currency::orderBy('short_name')->get()->toArray();
            return $this->resultService->Success($currencies);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->all", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getDefault()
    {
        try {
            $currency = Currency::where('default', self::CURRENCY_DEFAULT)->first();
            return $this->resultService->Success($currency);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->default", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getByShortName($shortName)
    {
        try {
            $currency = Currency::where('short_name',$shortName)->first();
            return $currency;

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->getByShortName", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getById($id)
    {
        try {
            $currency = Currency::where('id',$id)->first();
            return $currency;

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->getByShortName", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }


    public function changeDefault($currencyId)
    {
        try{
            if(empty($currencyId)){
                return $this->resultService->Error("Currency Id cannot be empty");
            }

            DB::table('currencies')->update(['default' => '']);

            Currency::where("id", $currencyId)->update([
                "default" => self::CURRENCY_DEFAULT
            ]);

        }catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->changeDefault", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function convert($currencyId, $oldCurrencyId, $estimatedValue)
    {
        try {
            if (empty($currencyId)) {
                return $this->resultService->Error("Currency id cannot be empty");
            }

            if (empty($oldCurrencyId)) {
                return $this->resultService->Error("Old currency id cannot be empty");
            }

            if (is_null($estimatedValue)) {
                return $this->resultService->Error("Estimated value cannot be empty");
            }

            $defaultCurrency = Currency::where("default", self::CURRENCY_DEFAULT)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("No currency is set as default");
            }

            $newCurrency = Currency::where("id", $currencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert to non-existing currency");
            }

            $oldCurrency = Currency::where("id", $oldCurrencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert from non-existing currency");
            }

            if ($oldCurrencyId == $defaultCurrency->id) {
                $convertedEstimatedValue = $estimatedValue * $newCurrency->exch_rate;
            } else {
                $convertedEstimatedValueDefault = $estimatedValue / $oldCurrency->exch_rate;
                $convertedEstimatedValue = $convertedEstimatedValueDefault * $newCurrency->exch_rate;
            }

            return $this->resultService->Success($convertedEstimatedValue);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->convert", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function npiConvert($currencyId, $oldCurrencyId, $estimatedValue){

        try {
            if (empty($currencyId)) {
                return $this->resultService->Error("Currency id cannot be empty");
            }

            if (empty($oldCurrencyId)) {
                return $this->resultService->Error("Old currency id cannot be empty");
            }

            if (is_null($estimatedValue)) {
                return $this->resultService->Error("Estimated value cannot be empty");
            }

            $defaultCurrency = Currency::where("default", self::CURRENCY_DEFAULT)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("No currency is set as default");
            }

            $newCurrency = Currency::where("id", $currencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert to non-existing currency");
            }

            $oldCurrency = Currency::where("id", $oldCurrencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert from non-existing currency");
            }
            $key = null;

            $convertedEstimatedValue = (object)[
                "materialCost" => null,
                "materialCostValue" => null,
                "npiLabour" => null,
                "rfqLabourValue" => null,
                "adminValue" => null,
                "freightValue" => null,
                "profitValueFactory" => null,
                "rfqSellingPriceFactory" => null,
                "rfqProfitValuesSalesOffice" => null,
                "rfqSellingPriceSalesOffice" => null
            ];

            foreach ($estimatedValue as $key => $value) {
                if ($oldCurrencyId == $defaultCurrency->id) {
                    $convertedEstimatedValue->$key = $value * $newCurrency->exch_rate;
                } else {
                    $convertedEstimatedValueDefault = $value / $oldCurrency->exch_rate;
                    $convertedEstimatedValue->$key = $convertedEstimatedValueDefault * $newCurrency->exch_rate;
                }

            }

            return $this->resultService->Success($convertedEstimatedValue);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->npiConvert", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }

    }

    public function newSaleConvert($currencyId, $oldCurrencyId, $estimatedValue){
        try {
            if (empty($currencyId)) {
                return $this->resultService->Error("Currency id cannot be empty");
            }

            if (empty($oldCurrencyId)) {
                return $this->resultService->Error("Old currency id cannot be empty");
            }

            if (is_null($estimatedValue)) {
                return $this->resultService->Error("Estimated value cannot be empty");
            }

            $defaultCurrency = Currency::where("default", self::CURRENCY_DEFAULT)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("No currency is set as default");
            }

            $newCurrency = Currency::where("id", $currencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert to non-existing currency");
            }

            $oldCurrency = Currency::where("id", $oldCurrencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert from non-existing currency");
            }
            $key = null;

            $convertedEstimatedValue = (object)[
                "lmeAl"                 => null,
                "lmeCu"                 => null,
                "buyPriceEngineering"   => null,
                "buyPriceFactory"       => null,
                "buyPrice"              => null,
                "fobCharge" => null,
                "fobChargeValue" => null,
                "freight" => null,
                "freightValue" => null,
                "cifCost" => null,
                "terminalHandling" => null,
                "terminalHandlingValue" => null,
                "deliveryOrder" => null,
                "deliveryOrderValue" => null,
                "customerClearance" => null,
                "customerClearanceValue" => null,
                "advanceFeesValue" => null,
                "lfrValue" => null,
                "haulageWarehouse" => null,
                "haulageWarehouseValue" => null,
                "totalInlandChargesValue" => null,
                "unloadingValue" => null,
                "orderPickingValue" => null,
                "storageValue" => null,
                "adminValue" => null,
                "totalHubCosts" => null,
                "totalOverseaCostsValue" => null,
                "importDutyValue" => null,
                "deliveryToCustomer" => null,
                "deliveryToCustomerValue" => null,
                "costPrice" => null,
                "gandaValue" => null,
                "totalCost" => null,
                "profitValue" => null,
                "sellingPrice" => null
            ];

            foreach ($estimatedValue as $key => $value) {
                if ($oldCurrencyId == $defaultCurrency->id) {
                    $convertedEstimatedValue->$key = $value * $newCurrency->exch_rate;
                } else {
                    $convertedEstimatedValueDefault = $value / $oldCurrency->exch_rate;
                    $convertedEstimatedValue->$key = $convertedEstimatedValueDefault * $newCurrency->exch_rate;
                }
            }
            return $this->resultService->Success($convertedEstimatedValue);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->newSaleConvert", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }

    }

    public function saleConvertToPound($currencyId, $oldCurrencyId, $estimatedValue)
    {
        try {
            if (empty($currencyId)) {
                return $this->resultService->Error("Currency id cannot be empty");
            }

            if (empty($oldCurrencyId)) {
                return $this->resultService->Error("Old currency id cannot be empty");
            }

            if (is_null($estimatedValue)) {
                return $this->resultService->Error("Estimated value cannot be empty");
            }

            $defaultCurrency = Currency::where("default", self::CURRENCY_DEFAULT)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("No currency is set as default");
            }

            $newCurrency = Currency::where("id", $currencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert to non-existing currency");
            }

            $oldCurrency = Currency::where("id", $oldCurrencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert from non-existing currency");
            }
            $key = null;

            $convertedEstimatedValue = (object)[
                "unloading" => null,
                "orderPicking" => null,
                "storage" => null,
                "admin" => null
            ];

            foreach ($estimatedValue as $key => $value) {
                if ($oldCurrencyId == $defaultCurrency->id) {
                    $convertedEstimatedValue->$key = $value * $newCurrency->exch_rate;
                } else {
                    $convertedEstimatedValueDefault = $value / $oldCurrency->exch_rate;
                    $convertedEstimatedValue->$key = $convertedEstimatedValueDefault * $newCurrency->exch_rate;
                }
            }
            return $this->resultService->Success($convertedEstimatedValue);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->newSaleConvert", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }


    public function convertRfqCom($currencyId, $oldCurrencyId, $estimatedPrice, $targetPrice)
    {
        try {
            if (empty($currencyId)) {
                return $this->resultService->Error("Currency id cannot be empty");
            }

            if (empty($oldCurrencyId)) {
                return $this->resultService->Error("Old currency id cannot be empty");
            }


            $defaultCurrency = Currency::where("default", self::CURRENCY_DEFAULT)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("No currency is set as default");
            }

            $newCurrency = Currency::where("id", $currencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert to non-existing currency");
            }

            $oldCurrency = Currency::where("id", $oldCurrencyId)
                ->first();

            if (empty($defaultCurrency)) {
                return $this->resultService->Error("Trying to convert from non-existing currency");
            }

            if(!is_null($estimatedPrice)){
                if ($oldCurrencyId == $defaultCurrency->id) {
                    $convertedEstimatedPrice = $estimatedPrice * $newCurrency->exch_rate;
                } else {
                    $convertedEstimatedPriceDefault = $estimatedPrice / $oldCurrency->exch_rate;
                    $convertedEstimatedPrice = $convertedEstimatedPriceDefault * $newCurrency->exch_rate;
                }
            }else{
                $convertedEstimatedPrice = null;
            }

            if(!is_null($targetPrice)){
                if ($oldCurrencyId == $defaultCurrency->id) {
                    $convertedTargetPrice = $targetPrice * $newCurrency->exch_rate;
                } else {
                    $convertedTargetPriceDefault = $targetPrice / $oldCurrency->exch_rate;
                    $convertedTargetPrice = $convertedTargetPriceDefault * $newCurrency->exch_rate;
                }
            }else{
                $convertedTargetPrice = null;
            }

            return $this->resultService->Success(
                (object)[
                    "estimated_price" => $convertedEstimatedPrice,
                    "target_price" => $convertedTargetPrice
                ]);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->convertRfqCom", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }


    public function change($currencies)
    {
        try {
            foreach($currencies as $c){
                Currency::where("id", $c['id'])
                    ->update(["exch_rate" => $c['exch_rate'], "last_modified_by" => $c['last_modified_by'],"last_modified_date" => $c['last_modified_date']]);
            }

            return $this->resultService->Success(
                (object)[
                    "Ok" => "OK",
                ]);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->convertRfqCom", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function create($longName, $shortName, $symbol, $exchRate, $lastModifiedBy)
    {
        try{
            $currency = new Currency();
            $currency->long_name = $longName;
            $currency->short_name = $shortName;
            $currency->symbol = $symbol;
            $currency->exch_rate = $exchRate;
            $currency->last_modified_by = $lastModifiedBy;
            $currency->last_modified_date = new \DateTime();
            $currency->created_at = new \DateTime();
            $ret = $currency->save();

            return $this->resultService->Success($ret);

        } catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->create", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function delete($currencyId)
    {
        try{
            if(empty($currencyId)){
                return $this->resultService->Error("Currency id cannot be empty.");
            }

            $ret = Currency::where("id", $currencyId)->delete();

            return $this->resultService->Success($ret);

        }catch (Exception $e) {
            $this->logService->log("ERROR CurrencyService->delete", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }
    
}
