<?php
namespace App\Http\Services;

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\TamuraEntity;
use App\NPI;
use DB;

class TamuraEntityService
{

    const TAMURA_ENTITY_TYPE_DESIGN_OFFICE = 1000;
    const TAMURA_ENTITY_TYPE_FACTORY = 2000;
    const TAMURA_ENTITY_TYPE_SALES_OFFICE = 3000;
    const TAMURA_ENTITY_DEACTIVATED_NO = 0;
    const TAMURA_ENTITY_DEACTIVATED_YES = 1;

    private $resultService;
    private $logService;

    public function __construct(ResultService $resultService, LogService $logService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
    }


    public function fetch()
    {
        try{

            $tamuraEntity = TamuraEntity::select('tamura_entities.id as id','tamura_entities.name as name','tamura_entities.short_name as short_name',
                'tamura_entities.location as location','tamura_entities.type as type','countries.name as country_name')
                ->join('countries', 'tamura_entities.location', '=', 'countries.iso2')
                ->orderBy("type",'asc')
                ->orderBy("countries.name",'asc')
                ->get();

            return $this->resultService->Success($tamuraEntity);

        }catch(Exception $e){
            $this->logService->log("ERROR TamuraEntityService->fetch", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getEntityById($id)
    {
        try{

            $tamuraEntity = TamuraEntity::
//                select
//                (DB::raw('persons.id as personId, persons.name as personName, persons.deactivated as personDeactivated,
//                persons.mobile as personMobile, persons.phone as personPhone, persons.sales_manager as personSalesMgr,
//                persons.engineer as personEngineer,persons.email as personEmail,
//                tamura_entities.id as id, tamura_entities.name as name,tamura_entities.short_name as short_name,tamura_entities.location as location,
//                tamura_entities.deactivated as deactivated, tamura_entities.mail as mail,tamura_entities.id as entityId,
//                tamura_entities.type as entityType,
//                tamura_entities.labour_rate,tamura_entities.freight,tamura_entities.admin,tamura_entities.profit,tamura_entities.currency_id,
//                tamura_entities.fob_charge,tamura_entities.terminal_handling,tamura_entities.delivery_order,tamura_entities.customs_clearance,
//                tamura_entities.haulage_to_warehouse,tamura_entities.manufactured_purchased,tamura_entities.fcl_lcl,tamura_entities.teu'))
                with(["tamuraEntityPersons","country"])
                ->where('tamura_entities.id','=',$id)
                ->get();

            return $this->resultService->Success($tamuraEntity);

        }catch(Exception $e){
            $this->logService->log("ERROR TamuraEntityService->getEntityById", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function update($entity)
    {
        try {
            $Entity = TamuraEntity::findOrFail($entity["entityId"]);
            if($Entity) {
                $Entity->location = $entity["location"];
                $Entity->mail = $entity["mail"];
                $Entity->deactivated = $entity["deactivated"];
                $Entity->save();
            }
            return $this->resultService->Success(
                (object)[
                    "Ok" => "OK",
                ]);

        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->update", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function factoryEntityUpdate($currencyId,$factorySalesEntity,$entity){
        try {
            $Entity = TamuraEntity::where("id", $entity["id"])->first();
            if(!empty($Entity)) {
                $Entity->currency_id = isset($currencyId)? $currencyId : null;
                if (isset($entity["labour_rate"])) {
                    if ($entity["labour_rate"] < 0) $Entity->labour_rate = 0;
                    else $Entity->labour_rate = $entity["labour_rate"];
                }
                if (isset($entity["labour_rate"])) {
                    if (($entity["admin"]) < 0) $Entity->admin = 0;
                    else $Entity->admin = $entity["admin"];
                }
                if (isset($entity["freight"])) {
                    if (($entity["freight"]) < 0) $Entity->freight = 0;
                    else $Entity->freight = $entity["freight"] ;
                }
                if (isset($entity["profit"])) {
                    if (($entity["profit"]) < 0) $Entity->profit = 0;
                    else $Entity->profit = $entity["profit"] ;
                }

                $Entity->manufactured_purchased = isset($entity["manufactured_purchased"] ) ? $entity["manufactured_purchased"] : null;

                $Entity->mail = isset($entity["mail"]) ? $entity["mail"] : "@";
                $Entity->location = isset($entity["location"]) ? $entity["location"] : null;
                $Entity->linked_sold = isset($entity["linked_sold"]) ? $entity["linked_sold"] : null;
                $Entity->deactivated = isset($entity["deactivated"])? $entity["deactivated"] : 0;

                $Entity->save();
            }
            $NPI = NPI::where("id", $factorySalesEntity["npiId"])->first();
            if(!empty($NPI)) {
                $NPI->tamura_entity_id_sales_office = isset($factorySalesEntity["salesOfficeId"] ) ? $factorySalesEntity["salesOfficeId"] : null;
                $NPI->save();
            }
            return $this->resultService->Success($Entity);

        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->factoryEntityUpdate", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function salesEntityUpdate($currencyId,$entity){
        try {
            $Entity = TamuraEntity::findOrFail($entity["id"]);
            if($Entity) {
                $Entity->currency_id = isset($currencyId)? $currencyId : null;
                $Entity->fob_charge = isset($entity["fob_charge"] ) ? $entity["fob_charge"] : null;
                $Entity->freight = isset($entity["freight"] ) ? $entity["freight"] : null;
                $Entity->terminal_handling = isset($entity["terminal_handling"] ) ? $entity["terminal_handling"] : null;
                $Entity->delivery_order = isset($entity["delivery_order"] ) ? $entity["delivery_order"] : null;
                if (isset($entity["profit"])){
                    if ($entity["profit"] < 0) $Entity->profit = 0;
                    else $Entity->profit = $entity["profit"];
                }
                else {
                    $Entity->profit = null;
                }

                $Entity->customs_clearance = isset($entity["customs_clearance"] ) ? $entity["customs_clearance"] : null;
                $Entity->haulage_to_warehouse = isset($entity["haulage_to_warehouse"] ) ? $entity["haulage_to_warehouse"] : null;
                $Entity->teu = isset($entity["teu"] ) ? $entity["teu"] : null;
                $Entity->save();
            }
            return $this->resultService->Success(
                (object)[
                    "Ok" => "OK",
                ]);

        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->salesEntityUpdate", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $entities = TamuraEntity::with(['country'])
                        ->orderBy("name")
                        ->get();

            return $this->resultService->Success($entities);

        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->getAll", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getFactories()
    {
        try {

            $factories = TamuraEntity::with(['country'])
                            ->where('type', self::TAMURA_ENTITY_TYPE_FACTORY)
                            ->get();

            return $this->resultService->Success($factories);

        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->getFactories", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getSalesOffices(){
        try {
            $salesOffices = TamuraEntity::with(['country'])
                ->where('type', self::TAMURA_ENTITY_TYPE_SALES_OFFICE)
                ->get();

            return $this->resultService->Success($salesOffices);

        } catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->getSalesOffices", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function create($name, $shortName, $type,$country)
    {
        try {
            if(empty($name)){
                return $this->resultService->Error("Name cannot be empty");
            }
            if(empty($shortName)){
                return $this->resultService->Error("Short name cannot be empty");
            }
            if(empty($type)){
                return $this->resultService->Error("Type cannot be empty");
            }
            if(empty($country)){
                return $this->resultService->Error("Country cannot be empty");
            }

            $te = new TamuraEntity();
            $te->name = $name;
            $te->short_name = $shortName;
            $te->type = (int)$type;
            $te->location = isset($country["iso2"])?$country["iso2"] : null;
            $te->deactivated = 0;
            $res = $te->save();

            return $this->resultService->Success($res);

        }catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->create", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function delete($tamuraEntityId)
    {
        try{
            if(empty($tamuraEntityId)){
                return $this->resultService->Error("Tamura entity id cannot be empty");
            }

            $res = TamuraEntity::where("id", $tamuraEntityId)->delete();
            return $this->resultService->Success($res);

        }catch (Exception $e) {
            $this->logService->log("ERROR TamuraEntityService->delete", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

}