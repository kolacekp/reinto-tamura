<?php

namespace App\Http\Services;

use Response;
use App\Log;

class LogService
{
   
    public function log($type, $message)
    {
        $log = new Log();
        $log->type = $type;
        $log->message = $message;
        $log->save();
    }
    
}
