<?php

namespace App\Http\Services;

/**
 * Description of ProjectService
 *
 * @author Petr Koláček
 */

use App\Http\Services\ResultService;
use App\Http\Services\LogService;
use App\Http\Services\TamuraCodificationService;
use App\Person;
use App\Project;
use App\RFQ;
use App\NPI;
use App\TamuraCodification;
use DB;

class ProjectService {
   
    const PROJECT_STATUS_INIT =   "INIT";
    const PROJECT_STATUS_OPENED = "OPENED";
    const PROJECT_STATUS_CLOSED = "CLOSED";
    
    const RFQ_STATUS_INIT = "INIT";
    const RFQ_STATUS_T10 =  "T10";
    const RFQ_STATUS_T20 =  "T20";
    const RFQ_STATUS_T30 =  "T30";
    const RFQ_STATUS_DEAD = "DEAD";
    const RFQ_STATUS_REJ =  "REJ";
    
    const TAMURA_CODIFICATION_CODE_LIST_MARKET = 'MARKET';
    const TAMURA_CODIFICATION_CODE_LIST_GTC = 'GTC';
    
    const TAMURA_CODIFICATION_TYPE_PARENT = 1;
    const TAMURA_CODIFICATION_TYPE_CHILD2 = 2;
    const TAMURA_CODIFICATION_TYPE_CHILD = 4;


    const TAMURA_CODIFICATION_TYPE_PARENT2 = '2';
    const TAMURA_CODIFICATION_TYPE_PARENT3 = '3';

    const VALID = '1';
    const INVALID = '2';


    private $resultService;
    private $logService;
    private $tamuraCodificationService;
            
    public function __construct(ResultService $resultService, LogService $logService,TamuraCodificationService $tamuraCodificationService)
    {
        $this->resultService = $resultService;
        $this->logService = $logService;
        $this->tamuraCodificationService = $tamuraCodificationService;
    }
    
    public function fetch($projects, $rfqs, $sales)
    {
        try{

            $results = $this->search($projects, $rfqs, $sales);
            return $this->resultService->Success($results);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->fetch", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getCounts($projects, $rfqs, $sales)
    {
        try{

            $results = $this->search($projects, $rfqs, $sales);

            // init values
            $projectsInit = 0;
            $projectsOpened = 0;
            $projectsClosed = 0;
            $rfqsInit = 0;
            $rfqsT10 = 0;
            $rfqsT20 = 0;
            $rfqsT30Opened = 0;
            $rfqsT30Closed = 0;
            $rfqsDeadOpened = 0;
            $rfqsDeadClosed = 0;
            $rfqsRejectedOpened = 0;
            $rfqsRejectedClosed = 0;
            $npisOpened = 0;
            $npisClosed = 0;
            $salesOpened = 0;
            $salesClosed = 0;

            // projects level
            foreach($results as $project) {
                if($project["status"] == "INIT") {
                    $projectsInit++;
                }
                if($project["status"] == "OPENED") {
                    $projectsOpened++;
                }
                if($project["status"] == "CLOSED") {
                    $projectsClosed++;
                }

                // rfqs level
                foreach($project["rfqs"] as $rfq) {
                    if($rfq["status"] == "INIT") {
                        $rfqsInit++;
                    }
                    if($rfq["status"] == "T10") {
                        $rfqsT10++;
                    }
                    if($rfq["status"] == "T20") {
                        $rfqsT20++;
                    }
                    if($rfq["status"] == "T30" && $project["status"] == "OPENED") {
                        $rfqsT30Opened++;
                    }
                    if($rfq["status"] == "T30" && $project["status"] == "CLOSED") {
                        $rfqsT30Closed++;
                    }
                    if($rfq["status"] == "DEAD" && $project["status"] == "OPENED") {
                        $rfqsDeadOpened++;
                    }
                    if($rfq["status"] == "DEAD" && $project["status"] == "CLOSED") {
                        $rfqsDeadClosed++;
                    }
                    if($rfq["status"] == "REJECTED" && $project["status"] == "OPENED") {
                        $rfqsRejectedOpened++;
                    }
                    if($rfq["status"] == "REJECTED" && $project["status"] == "CLOSED") {
                        $rfqsRejectedClosed++;
                    }

                    // npis level
                    foreach($rfq["npis"] as $npi) {
                        if($project["status"] == "OPENED") {
                            $npisOpened++;
                        }
                        if($project["status"] == "CLOSED") {
                            $npisClosed++;
                        }

                        // sales level
                        foreach($npi["sales"] as $sale){
                            if($project["status"] == "OPENED") {
                                $salesOpened++;
                            }
                            if($project["status"] == "CLOSED") {
                                $salesClosed++;
                            }
                        }

                    }
                }

            }

            return $this->resultService->Success([
                'projectsInit' => $projectsInit,
                'projectsOpened' => $projectsOpened,
                'projectsClosed' => $projectsClosed,
                'rfqsInit' => $rfqsInit,
                'rfqsT10' => $rfqsT10,
                'rfqsT20' => $rfqsT20,
                'rfqsT30Opened' => $rfqsT30Opened,
                'rfqsT30Closed' => $rfqsT30Closed,
                'rfqsDeadOpened' => $rfqsDeadOpened,
                'rfqsDeadClosed' => $rfqsDeadClosed,
                'rfqsRejectedOpened' => $rfqsRejectedOpened,
                'rfqsRejectedClosed' => $rfqsRejectedClosed,
                'npisOpened' => $npisOpened,
                'npisClosed' => $npisClosed,
                'salesOpened' => $salesOpened,
                'salesClosed' => $salesClosed

            ]);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->getCounts", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function getMarketParent($market)
    {
        try{
            $ParentEntities = null;
            if($market["type_entity"] == self::TAMURA_CODIFICATION_TYPE_CHILD  && $market["parent"] == null){
                if($market["no_entity_s"] != null) {
                    $ParentEntities = TamuraCodification::where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                        ->where('no_entity', 'like', $market["no_entity_s"])
                        ->first();

                }
                if ($ParentEntities != null && ($ParentEntities["type_entity"] == self::TAMURA_CODIFICATION_TYPE_PARENT2 || $ParentEntities["type_entity"] ==  self::TAMURA_CODIFICATION_TYPE_PARENT3)){
                    if($ParentEntities["no_entity_s"] != null) {
                        $market = $ParentEntities;
                        $ParentEntities = TamuraCodification::where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                            ->where('no_entity', 'like', $market["no_entity_s"])
                            ->first();

                    }
                }
                if ($ParentEntities != null && ($ParentEntities["type_entity"] == self::TAMURA_CODIFICATION_TYPE_PARENT2 || $ParentEntities["type_entity"] ==  self::TAMURA_CODIFICATION_TYPE_PARENT3)){
                    if($ParentEntities["no_entity_s"] != null) {
                        $market = $ParentEntities;
                        $ParentEntities = TamuraCodification::where('code_list', self::TAMURA_CODIFICATION_CODE_LIST_MARKET)
                            ->where('no_entity', 'like', $market["no_entity_s"])
                            ->first();

                    }
                }
            }

            return $ParentEntities;

        }catch(Exception $e){
            $this->logService->log("ERROR TamuraCodificationService->getChildProjectEntities", $e->getMessage());
            return $e->getMessage();
        }
    }

    public function get($projectId)
    {
        try{

            if(empty($projectId)){
                return $this->resultService->Error("Project id cannot be empty");
            }
            
            $project = Project::with([
                'rfqs.npis',
                'customer',
                'manager',
                'purchaseManager',
                'businessLocation',
                'estimatedValueCurrency',
                'market' => function($query){
                    $query->where("type_entity", self::TAMURA_CODIFICATION_TYPE_CHILD)
                          ->where("code_list", self::TAMURA_CODIFICATION_CODE_LIST_MARKET);
                },
                'market.parent' => function($query){
                    $query->where("type_entity", self::TAMURA_CODIFICATION_TYPE_PARENT)
                        ->where("code_list", self::TAMURA_CODIFICATION_CODE_LIST_MARKET);
                },
            ])
            ->where("id", $projectId)
            ->first();
            if ($project["market"] != null && $project["market"]["parent"] == null){
                $project["market"]["parentNew"] = $this->getMarketParent($project["market"] );;
            }
            return $this->resultService->Success($project);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->fetch", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }   
    }

    public function getProjects()
    {
        try{
            $projects = Project::orderBy("id")
                            ->orderBy("name")
                            ->get()
                            ->toArray();

            return $this->resultService->Success($projects);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->getProjects", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function createProject(
        $name,
        $status,
        $marketId,
        $standards,
        $description,
        $remarks,
        $estimatedValue,
        $estimatedValueCurrencyId,
        $itemToDesign,
        $customerId,
        $endCustomer,
        $customerPurchaseManagerId,
        $managerId,
        $businessLocation,
        $lastModifiedBy,
        $life,
        $spotOrder,
        $managerPhone,
        $managerMobile,
        $purchaseManager
    ){
        try{

            $newName = $this->checkAndFixName($name);

            $project = new Project();
            $project->name = !empty($newName) ? $newName : "";
            $project->status = !empty($status) ? $status : "INIT";
            $project->market_id = !empty($marketId) ? $marketId : "";
            $project->standards = !empty($standards) ? $standards : "";
            $project->description = !empty($description) ? $description : "";
            $project->remarks = !empty($remarks) ? $remarks : "";
            $project->estimated_value = !empty($estimatedValue) ? $estimatedValue : null;
            $project->estimated_value_currency_id = !empty($estimatedValueCurrencyId) ? $estimatedValueCurrencyId : null;
            $project->item_to_design = !empty($itemToDesign) ? $itemToDesign : null;
            $project->customer_id = !empty($customerId) ? $customerId : null;
            $project->end_customer = !empty($endCustomer) ? $endCustomer : "";
            $project->customer_purchase_manager_id = !empty($customerPurchaseManagerId) ? $customerPurchaseManagerId : null;
            $project->manager_id = !empty($managerId) ? $managerId : null;
            $project->business_location = !empty($businessLocation) ? $businessLocation : "";
            $project->creation_date = date('Y-m-d H:i:s', strtotime("now"));
            $project->last_modified_on = date('Y-m-d H:i:s', strtotime("now"));
            $project->last_modified_by = !empty($lastModifiedBy) ? $lastModifiedBy : "";
            $project->life = !empty($life) ? $life : null;
            $project->spot_order= !empty($spotOrder) ? (int)$spotOrder : null;
            $project->created_at = date('Y-m-d H:i:s', strtotime("now"));
            $project->save();

            if(!empty($managerId )){
                Person::where('id',$managerId )->update([
                    'phone' => $managerPhone,
                    'mobile' => $managerMobile
                ]);
            }

            if(!empty($purchaseManager)){
                Person::where('id',$purchaseManager["id"] )->update([
                    'phone' => $purchaseManager["phone"],
                    'mobile' => $purchaseManager["mobile"]
                ]);
            }

            return $this->resultService->Success($project);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->createProject", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function updateProject(
        $id,
        $name,
        $status,
        $marketId,
        $standards,
        $description,
        $remarks,
        $estimatedValue,
        $estimatedValueCurrencyId,
        $itemToDesign,
        $customerId,
        $endCustomer,
        $customerPurchaseManagerId,
        $managerId,
        $businessLocation,
        $lastModifiedBy,
        $life,
        $spotOrder,
        $managerPhone,
        $managerMobile,
        $purchaseManager
    ){
        try{
           
            if(empty($id)){
                return $this->resultService->Error("Project Id cannot be empty");
            }
            
            $res = Project::where("id", $id)
                ->update([
                    'name' => !empty($name) ? $name : "",
                    'status' => !empty($status) ? $status : "INIT",
                    'market_id' => !empty($marketId) ? $marketId : "",
                    'standards' =>!empty($standards) ? $standards : "",
                    'description' => !empty($description) ? $description : "",
                    'remarks' => !empty($remarks) ? $remarks : "",
                    'estimated_value' => !empty($estimatedValue) ? $estimatedValue : null,
                    'estimated_value_currency_id' => !empty($estimatedValueCurrencyId) ? $estimatedValueCurrencyId : null,
                    'item_to_design' => !empty($itemToDesign) ? $itemToDesign : null,
                    'customer_id' => !empty($customerId) ? $customerId : null,
                    'end_customer' => !empty($endCustomer) ? $endCustomer : "",
                    'customer_purchase_manager_id' => !empty($customerPurchaseManagerId) ? $customerPurchaseManagerId : null,
                    'manager_id' => !empty($managerId) ? $managerId : null,
                    'business_location' => !empty($businessLocation) ? $businessLocation : "",
                    'last_modified_on' => date('Y-m-d H:i:s', strtotime("now")),
                    'last_modified_by' => !empty($lastModifiedBy) ? $lastModifiedBy : "",
                    'life' => !empty($life) ? $life : null,
                    'spot_order' => !empty($spotOrder) ? (int)$spotOrder : null,
                    'updated_at' => date('Y-m-d H:i:s', strtotime("now"))
                ]);

            if(!empty($managerId)){
                Person::where('id',$managerId )->update([
                    'phone' => $managerPhone,
                    'mobile' => $managerMobile
                ]);
            }

            if(!empty($purchaseManager)){
                Person::where('id',$purchaseManager["id"] )->update([
                    'phone' => $purchaseManager["phone"],
                    'mobile' => $purchaseManager["mobile"]
                ]);
            }
            
            return $this->resultService->Success($res);
            
        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->createProject", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }
    }

    public function checkName($name, $id)
    {
        try{

            if(is_null($name)){
                return $this->resultService->Error("Name cannot be empty");
            }

            $res = Project::where("name", $name)
                ->first();

            if(!empty($id)){
                $result = (!empty($res) && $res->id != $id) ? true : false;
            }else{
                $result = !empty($res) ? true : false;
            }

            return $this->resultService->Success($result);

        }catch(Exception $e){
            $this->logService->log("ERROR ProjectService->checkName", $e->getMessage());
            return $this->resultService->Error($e->getMessage());
        }

    }

    /** PRIVATE FUNCTIONS **/

    private function search($projects, $rfqs, $sales)
    {
        // Project section search

        $results = Project::with([
            'rfqs.npis.sales',
            'rfqs.npis.sales.saleOffice',
            'rfqs.npis.sales.customer',
            'rfqs.npis.sales.currency',
            'rfqs.npis.sales.saleManager',
            'rfqs.npis.sales.purchaseManager',
            'customer',
            'manager',
            'purchaseManager',
            'businessLocation',
            'estimatedValueCurrency',
            'market'
        ]);

        if(!empty($projects->id)){
            $results = $results->where("id", $projects->id);
        }

        if(!empty($projects->name)){
            $results = $results->where("name", $projects->name);
        }

        if( !empty($projects->status->init)
            || !empty($projects->status->opened)
            || !empty($projects->status->closed)){

            $results = $results->where(function($innerQuery) use ($projects){
                if(!empty($projects->status->init)){
                    $results = $innerQuery->orWhere("status", self::PROJECT_STATUS_INIT);
                }
                if(!empty($projects->status->opened)){
                    $results = $innerQuery->orWhere("status", self::PROJECT_STATUS_OPENED);
                }
                if(!empty($projects->status->closed)){
                    $results = $innerQuery->orWhere("status", self::PROJECT_STATUS_CLOSED);
                }
            });

        }

        if(!empty($projects->manager)){
            $results = $results->where("manager_id", $projects->manager->id);
        }

        if(!empty($projects->customer)){
            $results = $results->where("customer_id", $projects->customer->id);
        }

        if(!empty($projects->keyAccountManager)){
            $results = $results->whereHas("customer", function($subQuery) use($projects) {
                $subQuery->where('account_manager_id', $projects->keyAccountManager->id);
            });
        }

        if(!empty($projects->purchaser)){
            $results = $results->where("customer_purchase_manager_id", $projects->purchaser->id);
        }

//        if(!empty($projects->marketParent)){
//            $results = $results->whereHas('market', function($subQuery) use ($projects){
//                $subQuery->where("id", $projects->marketParent->id);
//            });
//        }

        if(!empty($projects->marketChildNumber)){
            $results = $results->where("market_id", $projects->marketChildNumber);
        }

        if(!empty($projects->creationDateFrom)){
            $results = $results->where("creation_date", ">=", date('Y-m-d', strtotime($projects->creationDateFrom)));
        }

        if(!empty($projects->creationDateTo)){
            $results = $results->where("creation_date", "<=", date('Y-m-d 23:59:59', strtotime($projects->creationDateTo)));
        }

        if(!empty($projects->modificationDateFrom)){
            $results = $results->where("last_modified_on", ">=", date('Y-m-d', strtotime($projects->modificationDateFrom)));
        }

        if(!empty($projects->modificationDateTo)){
            $results = $results->where("last_modified_on", "<=", date('Y-m-d 23:59:59', strtotime($projects->modificationDateTo)));
        }

        // RFQ section search

        if(!empty($rfqs->id)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("id", $rfqs->id);
            });
        }


        if(!empty($rfqs->customerPN)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("pn_customer", $rfqs->customerPN);
            });
        }

        if($rfqs->status->init){
            $rfqs->status->init =  self::RFQ_STATUS_INIT;
        }else{
            $rfqs->status->init =  "";
        }
        if($rfqs->status->t10){
            $rfqs->status->t10 =  self::RFQ_STATUS_T10;
        }else{
            $rfqs->status->t10 =  "";
        }
        if($rfqs->status->t20){
            $rfqs->status->t20 = self::RFQ_STATUS_T20;
        }else{
            $rfqs->status->t20 =  "";
        }
        if($rfqs->status->t30){
            $rfqs->status->t30 =  self::RFQ_STATUS_T30;
        }else{
            $rfqs->status->t30 =  "";
        }
        if($rfqs->status->dead){
            $rfqs->status->dead =  self::RFQ_STATUS_DEAD;
        }
        else{
            $rfqs->status->dead = "";
        }
        if($rfqs->status->rejected){
            $rfqs->status->rejected =  self::RFQ_STATUS_REJ;
        }
        else{
            $rfqs->status->rejected =  "";
        }

        if( !empty($rfqs->status->init)
            || !empty($rfqs->status->t10)
            || !empty($rfqs->status->t20)
            || !empty($rfqs->status->t30)
            || !empty($rfqs->status->dead)
            || !empty($rfqs->status->rejected)){

            $results =  $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("status", $rfqs->status->init)
                    ->orWhere("status", $rfqs->status->t10)
                    ->orWhere("status", $rfqs->status->t20)
                    ->orWhere("status", $rfqs->status->t30)
                    ->orWhere("status", $rfqs->status->dead)
                    ->orWhere("status", $rfqs->status->rejected);
                }
            );
        }

        if(!empty($rfqs->deadReason)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("status_dead_reason_id", $rfqs->deadReason->id);
            });
        }

        if($rfqs->engineeringActions->designRequired){
            $rfqs->engineeringActions->designRequired =  1;
        }else{
            $rfqs->engineeringActions->designRequired =  NULL;
        }
        if($rfqs->engineeringActions->costingComplete){
            $rfqs->engineeringActions->costingComplete =  1;
        }else{
            $rfqs->engineeringActions->costingComplete =  NULL;
        }
        if($rfqs->engineeringActions->designComplete){
            $rfqs->engineeringActions->designComplete = 1;
        }else{
            $rfqs->engineeringActions->designComplete =  NULL;
        }
        if($rfqs->engineeringActions->massProdAuth){
            $rfqs->engineeringActions->massProdAuth = 1;
        }else{
            $rfqs->engineeringActions->massProdAuth =  NULL;
        }

        if( !empty($rfqs->engineeringActions->designRequired)
            || !empty($rfqs->engineeringActions->costingComplete)
            || !empty($rfqs->engineeringActions->designComplete)
            || !empty($rfqs->engineeringActions->massProdAuth)){

            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                    $subQuery->where("eng_action_design_required", $rfqs->engineeringActions->designRequired)
                        ->orWhere("eng_action_costing_complete", $rfqs->engineeringActions->costingComplete)
                        ->orWhere("eng_action_design_complete", $rfqs->engineeringActions->designComplete)
                        ->orWhere("eng_action_mass_prod_auth", $rfqs->engineeringActions->massProdAuth);
                }
            );

        }

        if(!empty($rfqs->category)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("pn_category_id", $rfqs->category->id);
            });
        }

        if(!empty($rfqs->PNFormat)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("pn_format", $rfqs->PNFormat->value);
            });
        }

        if(!empty($rfqs->priority)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("priority", $rfqs->priority->value);
            });
        }

        if(!empty($rfqs->newDev)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("pn_new_dev", (int)$rfqs->newDev == 'newDevelopment');
            });
        }

        if(!empty($rfqs->newBiz)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                if( $rfqs->newBiz == 'newBusiness' )
                    $subQuery->where("pn_new_biz", 1);
                if($rfqs->newBiz == 'replacingExistingBusiness')
                    $subQuery->where("pn_new_biz", 2);
            });
        }

        if(!empty($rfqs->creationDateFrom)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("creation_date", ">=", date('Y-m-d', strtotime($rfqs->creationDateFrom)));
            });
        }

        if(!empty($rfqs->creationDateTo)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("creation_date", "<=", date('Y-m-d 23:59:59', strtotime($rfqs->creationDateTo)));
            });
        }

        if(!empty($rfqs->modificationDateFrom)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("last_modified_on", ">=", date('Y-m-d', strtotime($rfqs->modificationDateFrom)));
            });
        }

        if(!empty($rfqs->modificationDateTo)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("last_modified_on", "<=", date('Y-m-d 23:59:59', strtotime($rfqs->modificationDateTo)));
            });
        }

        if(!empty($rfqs->productProduct)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("product_id", $rfqs->productProduct->no_entity);
            });
        }

        if(!empty($rfqs->customerEnginner)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("customer_engineer_id", $rfqs->customerEngineer->id);
            });
        }

        if(!empty($rfqs->electricalEngineer)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("tamura_elec_engineer_id", $rfqs->electricalEngineer->id);
            });
        }

        if(!empty($rfqs->mechanicalEngineer)){
            $results = $results->whereHas('rfqs', function($subQuery) use ($rfqs){
                $subQuery->where("tamura_mec_engineer_id", $rfqs->mechanicalEngineer->id);
            });
        }

        // Sales section search

        if(!empty($sales->customer)){
            $results = $results->whereHas('rfqs.npis.sales.customer', function($subQuery) use ($sales){
                $subQuery->where("id", $sales->customer->id);
            });
        }

        if(!empty($sales->purchaseManager)){
            $results = $results->whereHas('rfqs.npis.sales.purchaseManager', function($subQuery) use ($sales){
                $subQuery->where("id", $sales->purchaseManager->id);
            });
        }

        if(!empty($sales->manager)){
            $results = $results->whereHas('rfqs.npis.sales.saleManager', function($subQuery) use ($sales){
                $subQuery->where("id", $sales->manager->id);
            });
        }
        if (!empty($sales->alsoInvalid) && ($sales->alsoInvalid == self::VALID || $sales->alsoInvalid == self::INVALID)){
            $results = $results->whereHas('rfqs.npis.sales', function($subQuery) use ($sales){
                $subQuery->where("valid", $sales->alsoInvalid);
            });
        }

        return $results->get()->toArray();

    }

    private function checkAndFixName($name)
    {
        $check = true;
        $count = 0;

        // initial check
        $project = Project::where("name", $name)->first();
        if(empty($project))
            return $name;

        while($check){
            $project = Project::where("name", $name . "-" . $count)->first();
            if(!empty($project)){
                $count++;
            }else{
                $check = false;
            }
        }
        return $name . "-" . $count;
    }

}
