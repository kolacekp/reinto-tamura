<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\RfqService;
use Input;
use Monolog\Handler\StubNewRelicHandlerWithoutExtension;

class RfqController extends Controller
{
    private $rfqService;

    public function __construct(RfqService $rfqService)
    {
        $this->rfqService = $rfqService;
    }

    public function get(Request $request)
    {
        $rfqId = $request->input("id");
        return $this->rfqService->get($rfqId);
    }

    public function getDeadReasons()
    {
        return $this->rfqService->getDeadReasons();
    }

    public function getStatusLogs(Request $request)
    {
        $rfqId = $request->input("id");
        return $this->rfqService->getStatusLogs($rfqId);
    }

    public function getSpecifications(Request $request)
    {
        $rfqId = $request->input("id");
        return $this->rfqService->getSpecifications($rfqId);
    }

    public function updateSpecification(Request $request)
    {
        $specification = $request->input("specification");
        return $this->rfqService->updateSpecification($specification);
    }

    public function deleteSpecification(Request $request)
    {
        $specificationId = $request->input("specification_id");
        return $this->rfqService->deleteSpecification($specificationId);
    }

    public function addSpecification(Request $request)
    {
        $specification = $request->input("specification");
        $file = Input::file("file");
        return $this->rfqService->addSpecification($specification, $file);
    }

    public function getMaterialTypes()
    {
        return $this->rfqService->getMaterialTypes();
    }

    public function getMaterialsForType(Request $request)
    {
        $type = $request->input("type");
        return $this->rfqService->getMaterialsForType($type);
    }

    public function getMaterialForTypeAndPartNo(Request $request)
    {
        $type = $request->input("type");
        $partNo = $request->input("partNo");
        return $this->rfqService->getMaterialForTypeAndPartNo($type,$partNo);
    }

    public function createRfqMaterial(Request $request)
    {
        $matId = $request->input("matId");
        $quantity = $request->input("quantity");
        $rfqId = $request->input("rfqId");
        return $this->rfqService->createRfqMaterial($matId, $quantity, $rfqId);
    }

    public function getMaterialsForRfq(Request $request)
    {
        $rfqId = $request->input("rfqId");
        return $this->rfqService->getMaterialsForRfq($rfqId);
    }

    public function deleteRfqMaterial(Request $request)
    {
        $id = $request->input("id");
        return $this->rfqService->deleteRfqMaterial($id);
    }

    public function updateRfqMaterial(Request $request)
    {
        $rfqMatId = $request->input("rfqMatId");
        $matId = $request->input("matId");
        $quantity = $request->input("quantity");
        return $this->rfqService->updateRfqMaterial($rfqMatId, $matId, $quantity);
    }

    public function getCost(Request $request){
        $rfqId = $request->input("rfqId");
        return $this->rfqService->getMaterialCost($rfqId);
    }

    public function getRfqForNpi(Request $request){
        $rfqId = $request->input("rfqId");
        return $this->rfqService->getRfqForNpi($rfqId);
    }

    public function createRfq(Request $request)
    {
        $projectId = $request->input("projectId");
        $lastModifiedBy = $request->input("lastModifiedBy");
        return $this->rfqService->createRfq($projectId, $lastModifiedBy);
    }

    public function updateRfq(Request $request)
    {
        $rfq = $request->input("rfq");
        $lastModifiedBy = $request->input("lastModifiedBy");
        return $this->rfqService->updateRfq($rfq,$lastModifiedBy);
    }

    public function changeRfqProject(Request $request)
    {
        $rfqId = $request->input("rfqId");
        $projectId = $request->input("projectId");
        return $this->rfqService->changeRfqProject($rfqId, $projectId);
    }

    public function copyRfq(Request $request)
    {
        $rfqId = $request->input("rfqId");
        return $this->rfqService->copyRfq($rfqId);
    }

    public function cloneRfq(Request $request)
    {
        $rfqId = $request->input("rfqId");
        $projectId = $request->input("projectId");
        return $this->rfqService->cloneRfq($rfqId, $projectId);
    }

    public function checkCustomerPn(Request $request)
    {
        $customerPN = $request->input("customerPN");
        $id = $request->input("id");
        return $this->rfqService->checkCustomerPn($customerPN, $id);
    }

    public function getProductions(Request $request)
    {
        $id = $request->input("id");
        return $this->rfqService->getProduction($id);
    }

    public function getProductionById(Request $request)
    {
        $id = $request->input("productionId");
        return $this->rfqService->getProductionById($id);
    }

    public function addProduction(Request $request)
    {
        $rfqId = $request->input("rfqId");
        $production = $request->input("production");
        return $this->rfqService->addProduction($rfqId,$production);
    }

    public function editProduction(Request $request)
    {
        $production = $request->input("production");
        return $this->rfqService->editProduction($production);
    }

    public function deleteProduction(Request $request)
    {
        $id = $request->input("id");
        return $this->rfqService->deleteProduction($id);
    }
        
}
