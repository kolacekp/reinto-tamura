<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\SearchParamService;

class SearchParamController extends Controller
{
    private $searchParamService;
            
    public function __construct(SearchParamService $searchParamService)
    {
        $this->searchParamService = $searchParamService;
    }
    
    public function saveSearchParams(Request $request)
    {
        $requestParameters = (object)$request->all();
        return $this->searchParamService->saveSearchParams($requestParameters);
    }
    
    public function getPersonalizations(Request $request)
    {
        $personLogin = $request->input("personLogin");
        return $this->searchParamService->getPersonalizations($personLogin);
    }
    
}
