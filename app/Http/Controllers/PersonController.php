<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\PersonService;

class PersonController extends Controller
{
    private $personService;
            
    public function __construct(PersonService $personService)
    {
        $this->personService = $personService;
    }
    
    public function getSalesManagers()
    {
        return $this->personService->getSalesManagers();
    }
    
    public function getEngineers()
    {
        return $this->personService->getEngineers();
    }

    public  function getDatabaseUsers(){
        return $this->personService->getDatabaseUsers();
    }

    public function createDatabaseUser(Request $request)
    {
        $tamuraEntity = $request->input("tamuraEntity");
        $name = $request->input("name");
        $email = $request->input("email");
        $login = $request->input("login");
        $password = $request->input("password");
        $deactivated = $request->input("deactivated");
        $role = $request->input("role");
        return $this->personService->createDatabaseUser($tamuraEntity, $name, $email, $login, $password, $deactivated,$role);
    }

    public function deleteDatabaseUser(Request $request)
    {
        $id = $request->input("id");
        return $this->personService->deleteDatabaseUser($id);
    }

    public function updateDatabaseUserForm(Request $request)
    {
        $id = $request->input("id");
        $tamuraEntity = $request->input("tamuraEntity");
        $name = $request->input("name");
        $email = $request->input("email");
        $login = $request->input("login");
        $password = $request->input("password");
        $deactivated = $request->input("deactivated");
        $role = $request->input("role");
        return $this->personService->updateDatabaseUserForm($id, $tamuraEntity, $name, $email, $login, $password, $deactivated,$role);
    }

    public function updateDatabaseUser(Request $request)
    {
        $person = $request->input("person");
        return $this->personService->updateDatabaseUser($person);
    }

    public function updateCustomerPerson(Request $request)
    {
        $person = $request->input("person");
        return $this->personService->updateCustomerPerson($person);
    }

    public function changeCustomer(Request $request)
    {
        $customerId = $request->input("customerId");
        $personId = $request->input("personId");
        return $this->personService->changeCustomer($customerId, $personId);
    }

    public function getPerson(Request $request)
    {
        $id = $request->input("id");
        return $this->personService->getPerson($id);
    }

    public function checkEmailExists(Request $request)
    {
        $email = $request->input("email");
        return $this->personService->checkEmailExists($email);
    }

    public function addPerson(Request $request)
    {
        $entity = $request->input('entity');
        $person = $request->input('person');
        $customerId = $request->input('customerId');
        $isEngineer = $request->input('isEngineer');
        return $this->personService->addPerson($person,$entity,$customerId,$isEngineer);
    }
    
}
