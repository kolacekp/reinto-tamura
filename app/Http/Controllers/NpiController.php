<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\NpiService;
use App\TamuraEntity;

class NpiController extends Controller
{
    private $npiService;

    public function __construct(NpiService $npiService)
    {
        $this->npiService = $npiService;
    }

    public function getNpiByTamuraEntity(Request $request)
    {
        $id = $request->input("id");
        return $this->npiService->getNpiByTamuraEntity($id);
    }

    public function get(Request $request)
    {
        $id = $request->input("id");
        return $this->npiService->get($id);
    }

    public function updateComment(Request $request){
        $id = $request->input("id");
        $comment = $request->input("comment");
        return $this->npiService->updateComment($id,$comment);
    }

    public function generateNPI(Request $request){
        $rfq = $request->input("rfq");
        $NPINumber = $request->input("NPINumber");
        $userName = $request->input("userName");
        $customerAddress = $request->input("customerAddress");
        return $this->npiService->generateNPI($rfq,$NPINumber,$userName,$customerAddress);
    }

    public function changeSampleTeuPn(Request $request){
        $npi = $request->input("npi");
        return $this->npiService->changeSampleTeuPn($npi);
    }



}