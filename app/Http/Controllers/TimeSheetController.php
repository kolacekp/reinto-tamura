<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\TimeSheetService;
use App\TimeSheet;


class TimeSheetController extends Controller
{

    private $timeSheetService;

    public function __construct(TimeSheetService $timeSheetService)
    {
        $this->timeSheetService = $timeSheetService;
    }

    public function getSisterCompanies()
    {
        return $this->timeSheetService->getSisterCompanies();
    }

    public function getCustomersByEntityId(Request $request){
        $EntityId = $request->input("id");
        return $this->timeSheetService->getCustomersByEntityId($EntityId);
    }

    public  function getProjectRfqByCustomerId(Request $request){
        $customerId = $request->input("id");
        return $this->timeSheetService->getProjectRfqByCustomerId($customerId);
    }

    public function geTimeSheetByRfqId(Request $request){
        $rfqId = $request->input("id");
        return $this->timeSheetService->geTimeSheetByRfqId($rfqId);
    }

    public function getByRfq(Request $request)
    {
        $rfqId = $request->input("rfqId");
        return $this->timeSheetService->getByRfq($rfqId);
    }

    public function create(Request $request)
    {
        $rfqId = $request->input("rfqId");
        $createdBy = $request->input("createdBy");
        $tamuraEngineerId = $request->input("tamuraEngineerId");
        $date = $request->input("date");
        $action = $request->input("action");
        $hours = $request->input("hours");
        return $this->timeSheetService->create($rfqId, $createdBy, $tamuraEngineerId, $date, $action, $hours);
    }

}
