<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\TamuraEntityService;
use App\Http\Services\PersonService;
use App\TamuraEntity;

class TamuraEntityController extends Controller
{
    private $tamuraEntityService;
    private $personService;

    public function __construct(TamuraEntityService $tamuraEntityService, PersonService $personService)
    {
        $this->tamuraEntityService = $tamuraEntityService;
        $this->personService = $personService;
    }

    public function fetch()
    {
        return $this->tamuraEntityService->fetch();
    }

    public function getEntityById(Request $request)
    {
        $id = $request->input("id");
        return $this->tamuraEntityService->getEntityById($id);
    }

    public function update(Request $request)
    {
        $entity = $request->input("entity");
        return $this->tamuraEntityService->update($entity);
    }

    public function person_update(Request $request)
    {
        $person = $request->input("person");
        return $this->personService->updateDirectoryCustomerPerson($person);
    }

    public function factoryEntityUpdate(Request $request)
    {
        $currencyId = $request->input("currency_id");
        $entity = $request->input("entity");
        $factorySalesEntity = $request->input("factorySalesEntityId");
        return $this->tamuraEntityService->factoryEntityUpdate($currencyId, $factorySalesEntity, $entity);
    }

    public function salesEntityUpdate(Request $request)
    {
        $currencyId = $request->input("currency_id");
        $entity = $request->input("entity");
        return $this->tamuraEntityService->salesEntityUpdate($currencyId, $entity);
    }

    public function getAll()
    {
        return $this->tamuraEntityService->getAll();
    }

    public function getFactories()
    {
        return $this->tamuraEntityService->getFactories();
    }

    public function getSalesOffices()
    {
        return $this->tamuraEntityService->getSalesOffices();
    }

    public function create(Request $request)
    {
        $name = $request->input("name");
        $shortName = $request->input("shortName");
        $type = $request->input("type");
        $country = $request->input("country");
        return $this->tamuraEntityService->create($name, $shortName, $type,$country);
    }

    public function delete(Request $request)
    {
        $tamuraEntityId = $request->input("tamuraEntityId");
        return $this->tamuraEntityService->delete($tamuraEntityId);
    }


}