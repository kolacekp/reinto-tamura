<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\IncotermService;


class IncotermController extends Controller
{
    private $incotermService;

    public function __construct(IncotermService $incotermService)
    {
        $this->incotermService = $incotermService;
    }

    public function getAll()
    {
        return $this->incotermService->getAll();
    }


}