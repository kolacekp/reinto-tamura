<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\UserService;
use Input;

class UserController extends Controller
{

    private $userService;
            
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    
    public function login()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        return $this->userService->login($email, $password);
    }

    public function getUsers(){
        return $this->userService->getUsers();
    }

    public function makeUsersFromPersons()
    {
        return $this->userService->makeUsersFromPersons();
    }

    public function getRoles ()
    {
        return $this->userService->getRoles();
    }

    public function updateUser(){
        $user = Input::get('user');
        return $this->userService->updateUser($user);
    }
    
}
