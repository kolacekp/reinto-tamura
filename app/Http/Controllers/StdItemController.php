<?php

namespace App\Http\Controllers;

use App\Http\Services\StdItemService;
use Illuminate\Http\Request;

class StdItemController extends Controller
{
    private $stdItemService;

    public function __construct(StdItemService $stdItemService)
    {
        $this->stdItemService = $stdItemService;
    }

    public function getStdItems()
    {
        return $this->stdItemService->getStdItems();
    }

    public function create(Request $request)
    {
        $stdItem = $request->input("stdItem");
        return $this->stdItemService->create($stdItem);
    }

    public function update(Request $request)
    {
        $stdItem = $request->input("stdItem");
        return $this->stdItemService->update($stdItem);
    }

    public function delete(Request $request)
    {
        $stdItemId = $request->input("stdItemId");
        return $this->stdItemService->delete($stdItemId);
    }

}
