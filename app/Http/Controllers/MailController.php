<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\MailService;

class MailController extends Controller
{
    private $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function sendMailToPerson(Request $request)
    {
        $to = $request->input("to");
        $subject = $request->input("subject");
        $text = $request->input("text");
        return $this->mailService->sendMailToPerson($to, $subject, $text);
    }

}
