<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\SaleService;
use App\Sale;

class SaleController extends Controller
{
    private $saleService;

    public function __construct(SaleService $saleService)
    {
        $this->saleService = $saleService;
    }

    public function getSalesBtNPI (Request $request){
        $NPIid = $request->input("NPIid");
        return $this->saleService->getSalesBtNPI($NPIid);
    }

    public function findOrCreateSale(Request $request){
        $rfq = $request->input("rfq");
        $npi = $request->input("npi");
        $createdBy = $request->input("createdBy");
        return $this->saleService->findOrCreateSale($rfq,$npi,$createdBy);
    }

    public function getSaleById(Request $request){
        $saleId = $request->input("id");
        return $this->saleService->getSaleById($saleId);
    }

    public function updateSale(Request $request){
        $sale = $request->input("sale");
        $npi = $request->input("npi");
        return $this->saleService->updateSale($sale,$npi);
    }

    public function changeSaleOffice(Request $request){
        $sale_office = $request->input("sale_office");
        $sale = $request->input("sale");
        return $this->saleService->changeSaleOffice($sale_office,$sale);
    }

}