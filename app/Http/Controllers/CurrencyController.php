<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\CurrencyService;

class CurrencyController extends Controller
{
    private $currencyService;
            
    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }
    
    public function all()
    {
        return $this->currencyService->all();
    }

    public function getDefault()
    {
        return $this->currencyService->getDefault();
    }

    public function convert(Request $request)
    {
        $currencyId = $request->input("currencyId");
        $oldCurrencyId = $request->input("oldCurrencyId");
        $estimatedValue = $request->input("estimatedValue");
        return $this->currencyService->convert($currencyId, $oldCurrencyId, $estimatedValue);
    }

    public function npiConvert(Request $request)
    {
        $currencyId = $request->input("currencyId");
        $oldCurrencyId = $request->input("oldCurrencyId");
        $estimatedValue = $request->input("estimatedValue");
        return $this->currencyService->npiConvert($currencyId, $oldCurrencyId, $estimatedValue);
    }

    public function newSaleConvert(Request $request)
    {
        $currencyId = $request->input("currencyId");
        $oldCurrencyId = $request->input("oldCurrencyId");
        $estimatedValue = $request->input("estimatedValue");
        return $this->currencyService->newSaleConvert($currencyId, $oldCurrencyId, $estimatedValue);
    }

    public function saleConvertToPound (Request $request)
    {
        $currencyId = $request->input("currencyId");
        $oldCurrencyId = $request->input("oldCurrencyId");
        $estimatedValue = $request->input("estimatedValue");
        return $this->currencyService->saleConvertToPound($currencyId, $oldCurrencyId, $estimatedValue);
    }

    public function convertRfqComValues(Request $request)
    {
        $currencyId = $request->input("currencyId");
        $oldCurrencyId = $request->input("oldCurrencyId");
        $estimatedPrice = $request->input("estimatedPrice");
        $targetPrice = $request->input("targetPrice");
        return $this->currencyService->convertRfqCom($currencyId, $oldCurrencyId, $estimatedPrice, $targetPrice);
    }

    public function change(Request $request)
    {
        $currencies = $request->input("currencies");
        return $this->currencyService->change($currencies);
    }

    public function changeDefault(Request $request)
    {
        $currencyId = $request->input("currencyId");
        return $this->currencyService->changeDefault($currencyId);
    }

    public function create(Request $request)
    {
        $longName = $request->input("long_name");
        $shortName = $request->input("short_name");
        $symbol = $request->input("symbol");
        $exchRate = $request->input("exch_rate");
        $lastModifiedBy = $request->input("last_modified_by");
        return $this->currencyService->create($longName, $shortName, $symbol, $exchRate, $lastModifiedBy);
    }

    public function delete(Request $request)
    {
        $currencyId = $request->input("currencyId");
        return $this->currencyService->delete($currencyId);
    }
}
