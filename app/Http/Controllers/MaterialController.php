<?php

namespace App\Http\Controllers;

use App\Http\Services\MaterialService;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    private $materialService;

    public function __construct(MaterialService $materialService)
    {
        $this->materialService = $materialService;
    }

    public function getMaterials()
    {
        return $this->materialService->getMaterials();
    }

    public function create(Request $request)
    {
        $type = $request->input("type");
        $partNo = $request->input("partNo");
        $description = $request->input("description");
        $unitaryPrice = $request->input("unitaryPrice");
        return $this->materialService->create($type,$partNo,$description,$unitaryPrice);
    }

    public function update(Request $request)
    {
        $material = $request->input("material");
        return $this->materialService->update($material);
    }

    public function delete(Request $request)
    {
        $materialId = $request->input("materialId");
        return $this->materialService->delete($materialId);
    }

}
