<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\CustomerService;

class CustomerController extends Controller
{
    private $customerService;
            
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }
    
    public function getCompanies()
    {
        return $this->customerService->getCompanies();
    }
    
    public function getPurchasers(Request $request)
    {
        $customerPath = $request->input('customerPath');
        return $this->customerService->getPurchasers($customerPath);
    }
    
    public function getEngineers(Request $request)
    {
        $customerId = $request->input('customerId');
        return $this->customerService->getEngineers($customerId);
    }

    public function getCustomersDirectory(Request $request)
    {
        $deactivated = strtolower($request->input('deactivated')) == "true" ? true : false;
        return $this->customerService->getCustomersDirectory($deactivated);
    }

    public function getCustomerDetail(Request $request)
    {
        $customerId = $request->input("customerId");
        $personsDeactivated = strtolower($request->input('personsDeactivated')) == "true" ? true : false;
        return $this->customerService->getCustomerDetail($customerId, $personsDeactivated);
    }

    public function update(Request $request)
    {
        $customer = $request->input("customer");
        return $this->customerService->update($customer);
    }

    public function getGroupCustomers(Request $request)
    {
        $customerId = $request->input('customerId');
        return $this->customerService->getGroupCustomers($customerId);
    }

    public function getSalesManagers(Request $request)
    {
        $customerPath = $request->input('customerPath');
        return $this->customerService->getSalesManagers($customerPath);
    }

    public function getAll(Request $request)
    {
        $deactivated = strtolower($request->input('deactivated')) == "true" ? true : false;
        return $this->customerService->getAll($deactivated);
    }

    public function addCustomerGroup(Request $request)
    {
        $name = $request->input("name");
        $keyAccountManagerId = $request->input("keyAccountManagerId");
        $deactivated = $request->input("deactivated");
        $sup = $request->input("sup");
        $root = $request->input("root");
        $paymentTerm = $request->input("paymentTermNr");
        $paymentType = $request->input("paymentTermType");
        return $this->customerService->addCustomerGroup($name, $keyAccountManagerId, $deactivated, $sup, $root,$paymentTerm,$paymentType);
    }

    public function addCustomerCompany(Request $request)
    {
        $name = $request->input("name");
        $keyAccountManagerId = $request->input("keyAccountManagerId");
        $deactivated = $request->input("deactivated");
        $sup = $request->input("sup");
        $root = $request->input("root");
        $path = $request->input("path");
        $paymentTermNr = $request->input("paymentTermNr");
        $paymentTermType = $request->input("paymentTermType");
        $keyAccountManager = $request->input("keyAccountManager");
        return $this->customerService->addCustomerCompany($name, $keyAccountManagerId, $deactivated, $sup, $root, $path, $paymentTermNr, $paymentTermType,$keyAccountManager);
    }

    public function deleteCustomer(Request $request)
    {
        $customerId = $request->input("customerId");
        return $this->customerService->deleteCustomer($customerId);
    }
    
}
