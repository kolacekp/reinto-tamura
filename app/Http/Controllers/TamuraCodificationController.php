<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\TamuraCodificationService;

class TamuraCodificationController extends Controller
{
    private $tamuraCodificationService;

    public function __construct(TamuraCodificationService $tamuraCodificationService)
    {
        $this->tamuraCodificationService = $tamuraCodificationService;
    }

    public function getParentMarketEntities()
    {
        return $this->tamuraCodificationService->getParentMarketEntities();
    }

    public function getChildMarketEntities(Request $request)
    {
        $parent = $request->input('parent');
        return $this->tamuraCodificationService->getChildMarketEntities($parent);
    }

    public function getParentProductEntities()
    {
        return $this->tamuraCodificationService->getParentProductEntities();
    }

    public function getChildProductEntities(Request $request)
    {
        $level = $request->input('level');
        $parent = $request->input('parent');
        return $this->tamuraCodificationService->getChildProductEntities($level, $parent);
    }

    public function getChildProductEntity(Request $request)
    {
        $level = $request->input('level');
        $parent = $request->input('parent');
        return $this->tamuraCodificationService->getChildProductEntity($level, $parent);
    }
    
    public function getDesignOffices()
    {
        return $this->tamuraCodificationService->getDesignOffices();
    }

    public function getMarketParent(Request $request)
    {
        $market = $request->input('market');
        return $this->tamuraCodificationService->getMarketParent($market);
    }

}
