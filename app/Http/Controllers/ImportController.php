<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ImportService;

class ImportController extends Controller
{
    private $importService;
            
    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }
    
    public function importAppliParam(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importAppliParam($sheet);
    }
    
    public function importCategory(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importCategory($sheet);
    }
    
    public function importCountry(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importCountry($sheet);
    }
    
    public function importCurrency(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importCurrency($sheet);
    }
    
    public function importCustomer(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importCustomer($sheet);
    }
    
    public function importExcelExtractLog(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importExcelExtractLog($sheet);
    }
    
    public function importIncoterm(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importIncoterm($sheet);
    }
    
    public function importLme(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importLme($sheet);
    }
    
    public function importMat(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importMat($sheet);
    }
    
    public function importNpi(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importNpi($sheet);
    }
    
    public function importPerson(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importPerson($sheet);
    }
    
    public function importProject(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importProject($sheet);
    }
    
    public function importQuotationFormTemplateList(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importQuotationFormTemplateList($sheet);
    }
    
    public function importRfq(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfq($sheet);
    }
    
    public function importRfqEstimatedFactoryCost(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfqEstimatedFactoryCost($sheet);
    }
    
    public function importRfqExportError(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfqExportError($sheet);
    }
    
    public function importRfqMat(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfqMat($sheet);
    }
    
    public function importRfqDeadReason(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfqDeadReason($sheet);
    }
    
    public function importRfqSpecification(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfqSpecification($sheet);
    }
    
    public function importRfqStatusLog(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importRfqStatusLog($sheet);
    }
    
    public function importSale(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importSale($sheet);
    }
    
    public function importSearchParam(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importSearchParam($sheet);
    }
    
    public function importStandardItem(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importStandardItem($sheet);
    }
    
    public function importTamuraCodification(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importTamuraCodification($sheet);
    }
    
    public function importTamuraEntity(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importTamuraEntity($sheet);
    }
    
    public function importTimeSheet(Request $request)
    {
        $sheet = $request->file('sheet');
        return $this->importService->importTimeSheet($sheet);
    }
    
}
