<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\LmeService;

class LmeController extends Controller
{
    private $lmeService;

    public function __construct(LmeService $lmeService)
    {
        $this->lmeService = $lmeService;
    }

    public function getAll()
    {
        return $this->lmeService->all();
    }

    public function update(Request $request)
    {
        $id = $request->input("id");
        $cu = $request->input("cu");
        $al = $request->input("al");
        return($this->lmeService->update($id,$cu,$al));
    }
}