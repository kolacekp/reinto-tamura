<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\AppliParamService;

class AppliParamController extends Controller
{
    private $appliParamService;

    public function __construct(AppliParamService $appliParamService)
    {
        $this->appliParamService = $appliParamService;
    }

    public function updateDate(Request $request)
    {
        $name = $request->input("name");
        return $this->appliParamService->updateDate($name);
    }

    public function getAllAppliParams()
    {
        return $this->appliParamService->getAllAppliParams();
    }
}
