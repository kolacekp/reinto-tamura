<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\CountryService;

class CountryController extends Controller
{
    private $countryService;
            
    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }
    
    public function all()
    {
        return $this->countryService->all();
    }
}
