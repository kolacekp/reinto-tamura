<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ProjectService;

class ProjectController extends Controller
{
    private $projectService;
            
    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }
    
    public function fetch(Request $request)
    {
        $projects = !empty($request->input("projects")) ? json_decode($request->input("projects")) : null;        
        $rfqs =     !empty($request->input("rfqs")) ? json_decode($request->input("rfqs")) : null;
        $sales =    !empty($request->input("sales")) ? json_decode($request->input("sales")) : null;
        return $this->projectService->fetch($projects, $rfqs, $sales);
    }

    public function getCounts(Request $request)
    {
        $projects = !empty($request->input("projects")) ? json_decode($request->input("projects")) : null;
        $rfqs =     !empty($request->input("rfqs")) ? json_decode($request->input("rfqs")) : null;
        $sales =    !empty($request->input("sales")) ? json_decode($request->input("sales")) : null;
        return $this->projectService->getCounts($projects, $rfqs, $sales);
    }
    
    public function get(Request $request)
    {
        $projectId = $request->input("id");
        return $this->projectService->get($projectId);
    }

    public function getProjects(Request $request)
    {
        return $this->projectService->getProjects();
    }

    public function createProject(Request $request)
    {
        $name = $request->input("name");
        $status = $request->input("status");
        $marketId = $request->input("marketId");
        $standards = $request->input("standards");
        $description = $request->input("description");
        $remarks = $request->input("remarks");
        $estimatedValue = $request->input("estimatedValue");
        $estimatedValueCurrencyId = $request->input("estimatedValueCurrencyId");
        $itemToDesign = $request->input("itemToDesign");
        $customerId = $request->input("customerId");
        $endCustomer = $request->input("endCustomer");
        $customerPurchaseManagerId = $request->input("customerPurchaseManagerId");
        $managerId = $request->input("managerId");
        $businessLocation = $request->input("businessLocation");
        $lastModifiedBy = $request->input("lastModifiedBy");
        $life = $request->input("life");
        $spotOrder = $request->input("spotOrder");
        $managerPhone = $request->input("managerPhone");
        $managerMobile  = $request->input("managerMobile");
        $purchaseManager = $request->input("purchaseManager");

        return $this->projectService->createProject(
            $name,
            $status,
            $marketId,
            $standards,
            $description,
            $remarks,
            $estimatedValue,
            $estimatedValueCurrencyId,
            $itemToDesign,
            $customerId,
            $endCustomer,
            $customerPurchaseManagerId,
            $managerId,
            $businessLocation,
            $lastModifiedBy,
            $life,
            $spotOrder,
            $managerPhone,
            $managerMobile,
            $purchaseManager
        );
    }

    public function updateProject(Request $request)
    {
        $id = $request->input("id");
        $name = $request->input("name");
        $status = $request->input("status");
        $marketId = $request->input("marketId");
        $standards = $request->input("standards");
        $description = $request->input("description");
        $remarks = $request->input("remarks");
        $estimatedValue = $request->input("estimatedValue");
        $estimatedValueCurrencyId = $request->input("estimatedValueCurrencyId");
        $itemToDesign = $request->input("itemToDesign");
        $customerId = $request->input("customerId");
        $endCustomer = $request->input("endCustomer");
        $customerPurchaseManagerId = $request->input("customerPurchaseManagerId");
        $managerId = $request->input("managerId");
        $businessLocation = $request->input("businessLocation");
        $lastModifiedBy = $request->input("lastModifiedBy");
        $life = $request->input("life");
        $spotOrder = $request->input("spotOrder");
        $managerPhone = $request->input("managerPhone");
        $managerMobile = $request->input("managerMobile");
        $purchaseManager = $request->input("purchaseManager");

        return $this->projectService->updateProject(
            $id,
            $name,
            $status,
            $marketId,
            $standards,
            $description,
            $remarks,
            $estimatedValue,
            $estimatedValueCurrencyId,
            $itemToDesign,
            $customerId,
            $endCustomer,
            $customerPurchaseManagerId,
            $managerId,
            $businessLocation,
            $lastModifiedBy,
            $life,
            $spotOrder,
            $managerPhone,
            $managerMobile,
            $purchaseManager
        );
    }

    public function checkName(Request $request)
    {
        $name = $request->input("name");
        $id = $request->input("id");
        return $this->projectService->checkName($name, $id);
    }
      
}
