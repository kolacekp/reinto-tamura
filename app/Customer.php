<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'name'
        ,'address'
        ,'type'
        ,'id_sup'
        ,'id_root'
        ,'path'
        ,'deactivated'
        ,'account_manager_id'
        ,'payment_term_nr'
        ,'payment_term_type'       
    ];

    protected $casts = [
        'deactivated' => 'boolean'
    ];

    protected $relations = [
        'persons',
        'keyAccountManager'
    ];

    public function persons()
    {
        return $this->hasMany('App\Person', 'customer_id');
    }

    public function keyAccountManager()
    {
        return $this->hasOne('App\Person', 'id', 'account_manager_id');
    }


}
