<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LME extends Model
{
    protected $table = 'lmes';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'cu'
        ,'al'      
    ];
}
