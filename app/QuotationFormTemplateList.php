<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationFormTemplateList extends Model
{
    protected $table = 'quotation_form_template_lists';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'name'
        ,'comment'
        ,'file_name'
    ];
}
