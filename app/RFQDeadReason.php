<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQDeadReason extends Model
{
    protected $table = 'rfq_dead_reasons';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'reason'
    ];
}
