<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
        ,'name'
        ,'status'
        ,'market_id'
        ,'standards'
        ,'description'
        ,'remarks'
        ,'estimated_value'
        ,'estimated_value_currency_id'
        ,'item_to_design'
        ,'customer_id'
        ,'end_customer'
        ,'customer_purchase_manager_id'
        ,'manager_id'
        ,'business_location'
        ,'creation_date'
        ,'last_modified_on'
        ,'last_modified_by'
        ,'life'
        ,'spot_order'
    ];

    public function getCreationDateAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s A', strtotime($value)) : null;
    }

    public function getLastModifiedOnAttribute($value)
    {
        return !empty($value) ? date('m/d/Y H:i:s A', strtotime($value)) : null;
    }

    protected $relations = [
        'rfqs',
        'customer',
        'manager',
        'purchaseManager',
        'businessLocation',
        'estimatedValueCurrency',
        'market'
    ];
    
    public function getRelations(){
        return $this->relations;
    }
    
    public function rfqs()
    {
        return $this->hasMany('App\RFQ', 'project_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }
    
    public function manager()
    {
        return $this->hasOne('App\Person', 'id', 'manager_id');
    }
    
    public function purchaseManager()
    {
        return $this->hasOne('App\Person', 'id', 'customer_purchase_manager_id');
    }
    
    public function businessLocation()
    {
        return $this->hasOne('App\Country', 'iso2', 'business_location');
    }
    
    public function estimatedValueCurrency()
    {
        return $this->hasOne('App\Currency', 'id', 'estimated_value_currency_id');
    }

    public function market()
    {
        return $this->hasOne('App\TamuraCodification', 'no_entity', 'market_id');
    }

}
