<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RFQSpecification extends Model
{
    protected $table = 'rfq_specifications';
    
    protected $fillable = [
        'id'
        ,'rfq_id'
        ,'label'
        ,'version'
        ,'link'
        ,'status'
        ,'master'
    ];
}
