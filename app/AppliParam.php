<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppliParam extends Model
{
    protected $table = 'appli_params';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value', 'description'
    ];
}
