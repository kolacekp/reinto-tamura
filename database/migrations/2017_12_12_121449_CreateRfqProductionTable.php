<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfqProductionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq_production', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rfq_id')->nullable();
            $table->bigInteger('factory_id')->nullable();
            $table->bigInteger('currency_id')->nullable();
            $table->dateTime('last_production_date')->nullable();
            $table->double('std_cost_roll_up_material')->nullable();
            $table->bigInteger('std_cost_roll_up_labour_time')->nullable();
            $table->double('std_cost_roll_up_labour_euro')->nullable();
            $table->double('std_cost_roll_up_total_euro')->nullable();
            $table->double('so_price_factory_material_cost')->nullable();
            $table->double('so_price_factory_labour_cost')->nullable();
            $table->double('so_price_factory_cost')->nullable();
            $table->double('roll_up_material_estimated_material_cost')->nullable();
            $table->bigInteger('roll_up_labour_estimated_labour_time')->nullable();
            $table->double('roll_up_labour_estimated_labour_cost')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfq_production');
    }
}
