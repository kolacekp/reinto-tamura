<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRFQsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfqs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('father')->nullable();
            $table->boolean('is_std_item')->default(0);
            $table->bigInteger('project_id')->nullable();
            $table->string('status')->nullable();
            $table->string('folder')->nullable();
            $table->string('priority')->nullable();
            $table->bigInteger('design_office_id')->nullable();
            $table->bigInteger('customer_engineer_id')->nullable();
            $table->bigInteger('tamura_elec_engineer_id')->nullable();
            $table->bigInteger('tamura_mec_engineer_id')->nullable();
            $table->string('product_id')->nullable();
            $table->string('pn_customer')->nullable();
            $table->string('pn_desc')->nullable();
            $table->bigInteger('pn_category_id')->nullable();
            $table->string('pn_format')->nullable();
            $table->bigInteger('pn_new_dev')->nullable();
            $table->bigInteger('pn_new_biz')->nullable();
            $table->bigInteger('com_currency_id')->nullable();
            $table->double('com_estimated_price')->nullable();
            $table->double('com_target_price')->nullable();
            $table->bigInteger('com_annual_qty')->nullable();
            $table->double('com_annual_value')->nullable();
            $table->double('com_confidence_level')->nullable();
            $table->double('com_market_share')->nullable();
            $table->string('com_incoterms')->nullable();
            $table->string('com_delivery_location')->nullable();
            $table->boolean('com_teu_group')->default(0);
            $table->dateTime('qualif_quotation_required_date')->nullable();
            $table->dateTime('qualif_sample_date')->nullable();
            $table->dateTime('qualif_mass_prod_date')->nullable();
            $table->longText('comments')->nullable();
            $table->bigInteger('duplicate')->nullable();
            $table->bigInteger('duplicate_from')->nullable();
            $table->dateTime('creation_date')->nullable();
            $table->dateTime('last_modified_on')->nullable();
            $table->string('last_modified_by')->nullable();
            $table->boolean('status_10')->default(0);
            $table->string('status_10_who')->nullable();
            $table->dateTime('status_10_when')->nullable();
            $table->boolean('status_20')->default(0);
            $table->string('status_20_who')->nullable();
            $table->dateTime('status_20_when')->nullable();
            $table->boolean('status_30')->default(0);
            $table->string('status_30_who')->nullable();
            $table->dateTime('status_30_when')->nullable();
            $table->boolean('status_dead')->default(0);
            $table->string('status_dead_who')->nullable();
            $table->dateTime('status_dead_when')->nullable();
            $table->bigInteger('status_dead_reason_id')->nullable();
            $table->boolean('status_rejected')->default(0);
            $table->string('status_rejected_who')->nullable();
            $table->dateTime('status_rejected_when')->nullable();
            $table->dateTime('eng_action_costing_planned_date')->nullable();
            $table->boolean('eng_action_costing_complete')->default(0);
            $table->string('eng_action_costing_complete_who')->nullable();
            $table->dateTime('eng_action_costing_complete_when')->nullable();
            $table->boolean('eng_action_design_required')->default(0);
            $table->string('eng_action_design_required_who')->nullable();
            $table->dateTime('eng_action_design_required_when')->nullable();
            $table->dateTime('eng_action_design_complete_planned_date')->nullable();
            $table->boolean('eng_action_design_complete')->default(0);
            $table->string('eng_action_design_complete_who')->nullable();
            $table->dateTime('eng_action_design_complete_when')->nullable();
            $table->boolean('eng_action_mass_prod_auth')->default(0);
            $table->string('eng_action_mass_prod_auth_who')->nullable();
            $table->dateTime('eng_action_mass_prod_auth_when')->nullable();
            $table->string('product_type')->nullable();
            $table->double('product_length')->nullable();
            $table->double('product_width')->nullable();
            $table->double('product_height')->nullable();
            $table->double('product_weight')->nullable();
            $table->double('product_field1')->nullable();
            $table->double('product_field2')->nullable();
            $table->double('product_field3')->nullable();
            $table->double('product_field4')->nullable();
            $table->double('product_field5')->nullable();
            $table->double('product_field6')->nullable();
            $table->double('product_other_materials')->nullable();
            $table->double('product_other_materials_value')->nullable();
            $table->double('product_handling_scrap')->nullable();
            $table->double('product_handling_scrap_value')->nullable();
            $table->bigInteger('mat_currency_id')->nullable();
            $table->double('mat_total_value')->nullable();
            $table->double('pallet_length')->nullable();
            $table->double('pallet_width')->nullable();
            $table->double('pallet_space')->nullable();
            $table->double('pallet_layers')->nullable();
            $table->bigInteger('pallet_quantity')->nullable();
            $table->bigInteger('labour1')->nullable();
            $table->bigInteger('labour2')->nullable();
            $table->bigInteger('labour3')->nullable();
            $table->double('labour_time1')->nullable();
            $table->double('labour_time2')->nullable();
            $table->double('labour_time3')->nullable();
            $table->double('labour_time4')->nullable();          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfqs');
    }
}
