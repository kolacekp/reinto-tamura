<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->text('address')->nullable();
            $table->string('type')->nullable();
            $table->bigInteger('id_sup')->nullable();
            $table->bigInteger('id_root')->nullable();
            $table->string('path')->nullable();
            $table->boolean('deactivated')->default(0);
            $table->bigInteger('account_manager_id')->nullable();
            $table->string('payment_term_nr')->nullable();
            $table->string('payment_term_type')->nullable();           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
