<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRFQSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq_specifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rfq_id')->nullable();
            $table->string('label')->nullable();
            $table->string('version')->nullable();
            $table->longText('link')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('master')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfq_specifications');
    }
}
