<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTableAndUpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->after('remember_token')->default(30)->nullable();
        });

        DB::table('roles')->insert(array('id'=>20,'name'=>'basic_user', 'display_name'=>'user','created_at'=>new \DateTime(),'updated_at'=>new \DateTime()));
        DB::table('roles')->insert(array('id'=>30,'name'=>'super_user', 'display_name'=>'super_user','created_at'=>new \DateTime(),'updated_at'=>new \DateTime()));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('roles');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role_id');
        });
    }
}
