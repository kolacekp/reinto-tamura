<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_sheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rfq_id')->nullable();
            $table->string('created_by')->nullable();
            $table->datetime('creation_date')->nullable();
            $table->bigInteger('tamura_engineer_id')->nullable();
            $table->datetime('date')->nullable();
            $table->bigInteger('hours')->nullable();
            $table->string('action')->nullable();
            $table->string('invoice_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('time_sheets');
    }
}
