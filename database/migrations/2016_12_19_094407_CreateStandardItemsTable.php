<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandardItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standard_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('category')->nullable();
            $table->string('design_office')->nullable();
            $table->string('factory')->nullable();
            $table->string('sales_office')->nullable();
            $table->double('tam_id')->nullable();
            $table->string('part_number')->nullable();
            $table->string('description')->nullable();
            $table->double('box_quantity')->nullable();
            $table->double('carton_quantity')->nullable();
            $table->double('pallet_quantity')->nullable();
            $table->double('unit_weight')->nullable();
            $table->string('currency')->nullable();
            $table->double('buy_price_fca_hk')->nullable();
            $table->longText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('standard_items');
    }
}
