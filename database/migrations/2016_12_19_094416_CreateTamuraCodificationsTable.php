<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTamuraCodificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tamura_codifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('type_entity')->nullable();
            $table->string('no_entity')->nullable();
            $table->string('entity')->nullable();
            $table->string('no_entity_s')->nullable();
            $table->string('code_list')->nullable();
            $table->string('entity_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tamura_codifications');
    }
}
