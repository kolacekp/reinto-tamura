<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNPIsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('npis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rfq_id')->nullable();
            $table->bigInteger('tamura_entity_id_factory')->nullable();
            $table->bigInteger('tamura_entity_id_sales_office')->nullable();
            $table->bigInteger('currency_id')->nullable();
            $table->double('rfq_material_cost_value')->nullable();
            $table->double('rfq_profit_factory')->nullable();
            $table->double('rfq_profit_value_factory')->nullable();
            $table->double('rfq_labour')->nullable();
            $table->double('rfq_labour_time')->nullable();
            $table->double('rfq_labour_value')->nullable();
            $table->double('rfq_freight')->nullable();
            $table->double('rfq_freight_value')->nullable();
            $table->double('rfq_admin')->nullable();
            $table->double('rfq_admin_value')->nullable();
            $table->double('rfq_selling_price_factory')->nullable();
            $table->double('rfq_profit_sales_office')->nullable();
            $table->double('rfq_profit_value_sales_office')->nullable();
            $table->double('rfq_selling_price_sales_office')->nullable();
            $table->double('material_cost_value')->nullable();
            $table->double('profit_factory')->nullable();
            $table->double('profit_value_factory')->nullable();
            $table->double('labour')->nullable();
            $table->double('labour_time')->nullable();
            $table->double('labour_value')->nullable();
            $table->double('freight')->nullable();
            $table->double('freight_value')->nullable();
            $table->double('admin')->nullable();
            $table->double('admin_value')->nullable();
            $table->double('selling_price_factory')->nullable();
            $table->double('profit_sales_office')->nullable();
            $table->double('profit_value_sales_office')->nullable();
            $table->double('selling_price_sales_office')->nullable();
            $table->bigInteger('sample_quantity_required')->nullable();
            $table->dateTime('sample_delivery_date')->nullable();
            $table->boolean('sample_chargeable')->default(0);
            $table->double('sample_price')->nullable();
            $table->string('sample_incoterms_id')->nullable();
            $table->bigInteger('sample_manufactured_purchased')->nullable();
            $table->bigInteger('sample_lead_time')->nullable();
            $table->string('sample_teu_pn')->nullable();
            $table->string('sample_customer_address')->nullable();
            $table->bigInteger('minimum_order_quantity')->nullable();
            $table->bigInteger('minimum_package_quantity')->nullable();
            $table->longText('comment')->nullable();
            $table->dateTime('creation_date')->nullable();
            $table->string('created_by')->nullable();
            $table->dateTime('last_modified_on')->nullable();
            $table->string('last_modified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('npis');
    }
}
