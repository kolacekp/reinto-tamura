<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('npi_id')->nullable();
            $table->boolean('valid')->nullable();
            $table->dateTime('valid_last_update_date')->nullable();
            $table->bigInteger('currency_id')->nullable();
            $table->bigInteger('sales_manager_id')->nullable();
            $table->bigInteger('tamura_entity_id_sales_office')->nullable();
            $table->double('buy_price_engineering')->nullable();
            $table->double('buy_price_factory')->nullable();
            $table->double('buy_price')->nullable();
            $table->double('tamura_entity_fob_charge')->nullable();
            $table->double('tamura_entity_fob_charge_value')->nullable();
            $table->double('tamura_entity_freight')->nullable();
            $table->double('tamura_entity_freight_value')->nullable();
            $table->double('cif_cost')->nullable();
            $table->double('tamura_entity_terminal_handling')->nullable();
            $table->double('tamura_entity_terminal_handling_value')->nullable();
            $table->double('tamura_entity_delivery_order')->nullable();
            $table->double('tamura_entity_delivery_order_value')->nullable();
            $table->double('tamura_entity_customs_clearance')->nullable();
            $table->double('tamura_entity_customs_clearance_value')->nullable();
            $table->double('advance_fees')->nullable();
            $table->double('advance_fees_value')->nullable();
            $table->double('lfr')->nullable();
            $table->double('lfr_value')->nullable();
            $table->double('tamura_entity_haulage_to_warehouse')->nullable();
            $table->double('tamura_entity_haulage_to_warehouse_value')->nullable();
            $table->double('total_inland_charges_value')->nullable();
            $table->double('unloading')->nullable();
            $table->double('unloading_value')->nullable();
            $table->double('order_picking')->nullable();
            $table->double('order_picking_value')->nullable();
            $table->double('storage')->nullable();
            $table->double('storage_value')->nullable();
            $table->double('admin')->nullable();
            $table->double('admin_value')->nullable();
            $table->double('total_hub_costs')->nullable();
            $table->double('total_oversea_costs')->nullable();
            $table->double('total_oversea_costs_value')->nullable();
            $table->boolean('total_oversea_cost_flag')->default(0);
            $table->double('import_duty')->nullable();
            $table->double('import_duty_value')->nullable();
            $table->double('delivery_to_customer')->nullable();
            $table->double('delivery_to_customer_value')->nullable();
            $table->double('cost_price')->nullable();
            $table->double('cost_price_percentage')->nullable();
            $table->double('gand_a')->nullable();
            $table->double('gand_a_value')->nullable();
            $table->double('total_cost')->nullable();
            $table->double('total_cost_percentage')->nullable();
            $table->double('profit')->nullable();
            $table->double('profit_value')->nullable();
            $table->double('selling_price')->nullable();
            $table->longText('comment')->nullable();
            $table->longText('special_req')->nullable();
            $table->string('incoterms_id')->nullable();
            $table->dateTime('creation_date')->nullable();
            $table->dateTime('last_update_date')->nullable();
            $table->string('created_by')->nullable();
            $table->bigInteger('pallet_qty')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->string('customer_payment_term_nr')->nullable();
            $table->string('customer_payment_term_type')->nullable();
            $table->bigInteger('purchaser_id')->nullable();
            $table->string('fcl_lcl')->nullable();
            $table->double('lme_cu')->nullable();
            $table->double('lme_cu_at_creation')->nullable();
            $table->double('lme_al')->nullable();
            $table->double('lme_al_at_creation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
