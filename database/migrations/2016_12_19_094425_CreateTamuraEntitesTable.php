<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTamuraEntitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tamura_entities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('short_name')->nullable();
            $table->string('location')->nullable();
            $table->bigInteger('type')->nullable();
            $table->boolean('deactivated')->nullable();
            $table->double('labour_rate')->nullable();
            $table->double('freight')->nullable();
            $table->double('admin')->nullable();
            $table->double('profit')->nullable();
            $table->bigInteger('linked_sold')->nullable();
            $table->bigInteger('currency_id')->nullable();
            $table->string('mail')->nullable();
            $table->double('fob_charge')->nullable();
            $table->double('terminal_handling')->nullable();
            $table->double('delivery_order')->nullable();
            $table->double('customs_clearance')->nullable();
            $table->double('haulage_to_warehouse')->nullable();
            $table->bigInteger('manufactured_purchased')->nullable();
            $table->string('fcl_lcl')->nullable();
            $table->boolean('teu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tamura_entities');
    }
}
