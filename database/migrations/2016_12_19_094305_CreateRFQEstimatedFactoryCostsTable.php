<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRFQEstimatedFactoryCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfq_estimated_factory_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rfq_id')->nullable();
            $table->bigInteger('tamura_entity_id_factory1')->nullable();
            $table->bigInteger('tamura_entity_currency_factory1')->nullable();
            $table->double('rfq_material_factory1')->nullable();
            $table->double('tamura_entity_profit_factory1')->nullable();
            $table->double('tamura_entity_profit_value_factory1')->nullable();
            $table->double('tamura_entity_labour_rate_choice_factory1')->nullable();
            $table->double('tamura_entity_labour_time_value_factory1')->nullable();
            $table->double('tamura_entity_labour_rate_factory1')->nullable();
            $table->double('tamura_entity_labour_rate_value_factory1')->nullable();
            $table->double('tamura_entity_freight_factory1')->nullable();
            $table->double('tamura_entity_freight_value_factory1')->nullable();
            $table->double('tamura_entity_admin_factory1')->nullable();
            $table->double('tamura_entity_admin_value_factory1')->nullable();
            $table->double('tamura_entity_selling_price_factory1')->nullable();
            $table->bigInteger('tamura_entity_id_sales_office1')->nullable();
            $table->double('tamura_entity_profit_sales_office1')->nullable();
            $table->double('tamura_entity_profit_value_sales_office1')->nullable();
            $table->double('tamura_entity_selling_price_sales_office1')->nullable();
            $table->bigInteger('tamura_entity_id_factory2')->nullable();
            $table->bigInteger('tamura_entity_currency_factory2')->nullable();
            $table->double('rfq_material_factory2')->nullable();
            $table->double('tamura_entity_profit_factory2')->nullable();
            $table->double('tamura_entity_profit_value_factory2')->nullable();
            $table->double('tamura_entity_labour_rate_choice_factory2')->nullable();
            $table->double('tamura_entity_labour_time_value_factory2')->nullable();
            $table->double('tamura_entity_labour_rate_factory2')->nullable();
            $table->double('tamura_entity_labour_rate_value_factory2')->nullable();
            $table->double('tamura_entity_freight_factory2')->nullable();
            $table->double('tamura_entity_freight_value_factory2')->nullable();
            $table->double('tamura_entity_admin_factory2')->nullable();
            $table->double('tamura_entity_admin_value_factory2')->nullable();
            $table->double('tamura_entity_selling_price_factory2')->nullable();
            $table->bigInteger('tamura_entity_id_sales_office2')->nullable();
            $table->double('tamura_entity_profit_sales_office2')->nullable();
            $table->double('tamura_entity_profit_value_sales_office2')->nullable();
            $table->double('tamura_entity_selling_price_sales_office2')->nullable();
            $table->bigInteger('tamura_entity_id_factory3')->nullable();
            $table->bigInteger('tamura_entity_currency_factory3')->nullable();
            $table->double('rfq_material_factory3')->nullable();
            $table->double('tamura_entity_profit_factory3')->nullable();
            $table->double('tamura_entity_profit_value_factory3')->nullable();
            $table->double('tamura_entity_labour_rate_choice_factory3')->nullable();
            $table->double('tamura_entity_labour_time_value_factory3')->nullable();
            $table->double('tamura_entity_labour_rate_factory3')->nullable();
            $table->double('tamura_entity_labour_rate_value_factory3')->nullable();
            $table->double('tamura_entity_freight_factory3')->nullable();
            $table->double('tamura_entity_freight_value_factory3')->nullable();
            $table->double('tamura_entity_admin_factory3')->nullable();
            $table->double('tamura_entity_admin_value_factory3')->nullable();
            $table->double('tamura_entity_selling_price_factory3')->nullable();
            $table->bigInteger('tamura_entity_id_sales_office3')->nullable();
            $table->double('tamura_entity_profit_sales_office3')->nullable();
            $table->double('tamura_entity_profit_value_sales_office3')->nullable();
            $table->double('tamura_entity_selling_price_sales_office3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rfq_estimated_factory_costs');
    }
}
