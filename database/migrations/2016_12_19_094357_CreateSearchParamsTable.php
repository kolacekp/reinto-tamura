<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_params', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('person_login')->nullable();
            $table->string('person_name')->nullable();
            $table->bigInteger('project_id')->nullable();
            $table->boolean('project_status_init')->nullable();
            $table->boolean('project_status_opened')->nullable();
            $table->boolean('project_status_closed')->nullable();
            $table->string('project_manager_id')->nullable();
            $table->bigInteger('key_account_manager_id')->nullable();
            $table->bigInteger('project_customer_id')->nullable();
            $table->bigInteger('project_customer_root_id')->nullable();
            $table->boolean('project_include_all_group')->nullable();
            $table->bigInteger('project_purchaser_id')->nullable();
            $table->string('project_market_id')->nullable();
            $table->string('project_market_parent_id')->nullable();
            $table->dateTime('project_creation_date_from')->nullable();
            $table->dateTime('project_creation_date_to')->nullable();
            $table->dateTime('project_modification_date_from')->nullable();
            $table->dateTime('project_modification_date_to')->nullable();
            $table->bigInteger('rfq_id')->nullable();
            $table->boolean('rfq_status_init')->nullable()->default(0);
            $table->boolean('rfq_status_t10')->nullable()->default(0);
            $table->boolean('rfq_status_t20')->nullable()->default(0);
            $table->boolean('rfq_status_t30')->nullable()->default(0);
            $table->boolean('rfq_status_dead')->nullable()->default(0);
            $table->boolean('rfq_status_rej')->nullable()->default(0);
            $table->string('rfq_customer_pn')->nullable();
            $table->boolean('rfq_eng_action_design_required')->nullable()->default(0);
            $table->boolean('rfq_eng_action_costing_complete')->nullable()->default(0);
            $table->boolean('rfq_eng_action_design_complete')->nullable()->default(0);
            $table->boolean('rfq_eng_action_mass_prod_auth')->nullable()->default(0);
            $table->bigInteger('rfq_dead_reason_id')->nullable();
            $table->string('rfq_pn_category')->nullable();
            $table->string('rfq_pn_format')->nullable();
            $table->bigInteger('rfq_pn_new_dev')->nullable();
            $table->bigInteger('rfq_pn_new_biz')->nullable();
            $table->string('rfq_priority')->nullable();
            $table->bigInteger('rfq_customer_engineer_id')->nullable();
            $table->bigInteger('rfq_tamura_elec_engineer_id')->nullable();
            $table->bigInteger('rfq_tamura_mec_engineer_id')->nullable();
            $table->string('rfq_product_id')->nullable();
            $table->string('rfq_product_child_one_id')->nullable();
            $table->string('rfq_product_child_two_id')->nullable();
            $table->string('rfq_product_parent_id')->nullable();
            $table->dateTime('rfq_creation_date_from')->nullable();
            $table->dateTime('rfq_creation_date_to')->nullable();
            $table->dateTime('rfq_modification_date_from')->nullable();
            $table->dateTime('rfq_modification_date_to')->nullable();
            $table->bigInteger('sales_customer_id')->nullable();
            $table->bigInteger('sales_purchase_manager_id')->nullable();
            $table->bigInteger('sales_manager_id')->nullable();
            $table->boolean('sales_valid')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('search_params');
    }
}
