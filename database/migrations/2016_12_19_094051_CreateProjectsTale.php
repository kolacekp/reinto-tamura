<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->string('market_id')->nullable();
            $table->string('standards')->nullable();
            $table->string('description')->nullable();
            $table->string('remarks')->nullable();
            $table->bigInteger('estimated_value')->nullable();
            $table->bigInteger('estimated_value_currency_id')->nullable();
            $table->bigInteger('item_to_design')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->string('end_customer')->nullable();
            $table->bigInteger('customer_purchase_manager_id')->nullable();
            $table->bigInteger('manager_id')->nullable();
            $table->string('business_location')->nullable();
            $table->dateTime('creation_date')->nullable();
            $table->dateTime('last_modified_on')->nullable();
            $table->string('last_modified_by')->nullable();
            $table->bigInteger('life')->nullable();
            $table->bigInteger('spot_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
